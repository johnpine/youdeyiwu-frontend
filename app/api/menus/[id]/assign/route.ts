import { NextRequest, NextResponse } from 'next/server';
import {
  apiAuthMiddleware,
  apiQueryBody,
  apiQueryParams,
  apiResponse,
} from '@/lib/api';
import { assignMenu, unAssignMenu } from '@/services/api';

export async function PUT(request: NextRequest, { params }: any) {
  try {
    const response = (await assignMenu({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      data: await apiQueryBody({
        request,
      }),
      id: apiQueryParams({
        request,
        params,
      }).id,
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export async function DELETE(request: NextRequest, { params }: any) {
  try {
    const queryParams = apiQueryParams({
      request,
      params,
    });

    const response = (await unAssignMenu({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      data: queryParams as any,
      id: queryParams.id,
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
