import { NextRequest, NextResponse } from 'next/server';
import { apiResponse } from '@/lib/api';
import { X_POWERED_BY_HEADER } from '@/lib/constant';

const now = new Date().toISOString();

export async function GET(request: NextRequest) {
  try {
    const body = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
<url><loc>${process.env.APP_URL}</loc><lastmod>${now}</lastmod><changefreq>daily</changefreq><priority>1</priority></url>
<url><loc>${process.env.APP_URL}/avatar</loc><lastmod>${now}</lastmod><changefreq>daily</changefreq><priority>1</priority></url>
<url><loc>${process.env.APP_URL}/login</loc><lastmod>${now}</lastmod><changefreq>daily</changefreq><priority>1</priority></url>
<url><loc>${process.env.APP_URL}/register</loc><lastmod>${now}</lastmod><changefreq>daily</changefreq><priority>1</priority></url>
<url><loc>${process.env.APP_URL}/privacy-policy</loc><lastmod>${now}</lastmod><changefreq>daily</changefreq><priority>1</priority></url>
<url><loc>${process.env.APP_URL}/terms</loc><lastmod>${now}</lastmod><changefreq>daily</changefreq><priority>1</priority></url>
<url><loc>${process.env.APP_URL}/sections</loc><lastmod>${now}</lastmod><changefreq>daily</changefreq><priority>1</priority></url>
<url><loc>${process.env.APP_URL}/docs</loc><lastmod>${now}</lastmod><changefreq>daily</changefreq><priority>1</priority></url>
</urlset>`;

    return new Response(body, {
      headers: {
        'Content-Type': 'text/xml',
        ...X_POWERED_BY_HEADER,
      },
    });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export const dynamic = 'force-dynamic';
