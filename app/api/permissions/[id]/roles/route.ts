import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiQueryParams, apiResponse } from '@/lib/api';
import { queryPermissionRolesById } from '@/services/api';

export async function GET(request: NextRequest, { params }: any) {
  try {
    const queryParams = apiQueryParams({
      request,
      params,
    });

    const response = (await queryPermissionRolesById({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      query: queryParams,
      id: queryParams.id,
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
