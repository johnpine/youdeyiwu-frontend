import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiQueryParams, apiResponse } from '@/lib/api';
import { clientQueryUserDetails } from '@/services/api';

export async function GET(request: NextRequest, { params }: any) {
  try {
    const queryParams = apiQueryParams({
      request,
      params,
    });

    const response = (await clientQueryUserDetails({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: queryParams.id,
      query: queryParams,
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
