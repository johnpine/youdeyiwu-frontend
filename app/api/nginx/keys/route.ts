import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiResponse } from '@/lib/api';
import { createNginxKey, queryNginxKeys } from '@/services/api';

export async function GET(request: NextRequest) {
  try {
    const response = (await queryNginxKeys({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export async function POST(request: NextRequest) {
  try {
    const response = (await createNginxKey({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
