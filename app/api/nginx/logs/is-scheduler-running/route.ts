import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiQueryParams, apiResponse } from '@/lib/api';
import { queryNginxLogIsSchedulerRunning } from '@/services/api';

export async function GET(request: NextRequest) {
  try {
    const response = (await queryNginxLogIsSchedulerRunning({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      query: await apiQueryParams({
        request,
      }),
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
