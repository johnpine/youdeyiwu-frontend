import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiQueryBody, apiResponse } from '@/lib/api';
import { queryEmailConfig, updateEmailConfig } from '@/services/api';

export async function GET(request: NextRequest) {
  try {
    const response = (await queryEmailConfig({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export async function PUT(request: NextRequest) {
  try {
    const response = (await updateEmailConfig({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      data: await apiQueryBody({
        request,
      }),
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
