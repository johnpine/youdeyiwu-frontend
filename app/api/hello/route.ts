import { NextRequest, NextResponse } from 'next/server';
import { apiResponse } from '@/lib/api';

export async function GET(request: NextRequest) {
  try {
    const data = { hello: 'world' };
    return apiResponse({ request, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
