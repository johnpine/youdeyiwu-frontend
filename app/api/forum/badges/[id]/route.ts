import { NextRequest, NextResponse } from 'next/server';
import {
  apiAuthMiddleware,
  apiQueryBody,
  apiQueryParams,
  apiResponse,
} from '@/lib/api';
import {
  queryPostBadge,
  removePostBadge,
  updatePostBadge,
} from '@/services/api';

export async function GET(request: NextRequest, { params }: any) {
  try {
    const response = (await queryPostBadge({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: apiQueryParams({
        request,
        params,
      }).id,
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export async function PUT(request: NextRequest, { params }: any) {
  try {
    const response = (await updatePostBadge({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: apiQueryParams({
        request,
        params,
      }).id,
      data: await apiQueryBody({
        request,
      }),
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export async function DELETE(request: NextRequest, { params }: any) {
  try {
    const response = (await removePostBadge({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: apiQueryParams({
        request,
        params,
      }).id,
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
