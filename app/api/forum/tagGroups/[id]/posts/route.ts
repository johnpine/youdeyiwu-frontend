import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiQueryParams, apiResponse } from '@/lib/api';
import { queryPostsByTagGroup } from '@/services/api';

export async function GET(request: NextRequest, { params }: any) {
  try {
    const response = (await queryPostsByTagGroup({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: apiQueryParams({
        request,
        params,
      }).id,
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
