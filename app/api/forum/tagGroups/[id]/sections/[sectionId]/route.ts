import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiQueryParams, apiResponse } from '@/lib/api';
import { createSectionTagGroup, removeSectionTagGroup } from '@/services/api';

export async function POST(request: NextRequest, { params }: any) {
  try {
    const queryParams = apiQueryParams({
      request,
      params,
    });

    const response = (await createSectionTagGroup({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: queryParams.id,
      data: queryParams as any,
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export async function DELETE(request: NextRequest, { params }: any) {
  try {
    const queryParams = apiQueryParams({
      request,
      params,
    });

    const response = (await removeSectionTagGroup({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: queryParams.id,
      data: queryParams as any,
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
