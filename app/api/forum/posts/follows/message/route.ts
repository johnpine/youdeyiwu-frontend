import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiResponse } from '@/lib/api';
import { queryPostFollowMessage } from '@/services/api';

export async function GET(request: NextRequest, { params }: any) {
  try {
    const response = (await queryPostFollowMessage({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
