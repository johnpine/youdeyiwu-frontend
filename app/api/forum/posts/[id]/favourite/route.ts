import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiQueryParams, apiResponse } from '@/lib/api';
import { postCancelFavourite, postFavourite } from '@/services/api';

export async function GET(request: NextRequest, { params }: any) {
  try {
    const response = (await postFavourite({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: apiQueryParams({
        request,
        params,
      }).id,
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export async function DELETE(request: NextRequest, { params }: any) {
  try {
    const response = (await postCancelFavourite({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: apiQueryParams({
        request,
        params,
      }).id,
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
