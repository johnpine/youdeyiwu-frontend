import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiQueryParams, apiResponse } from '@/lib/api';
import { queryDraft, removeDraft } from '@/services/api';

export async function GET(request: NextRequest, { params }: any) {
  try {
    const response = (await queryDraft({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: apiQueryParams({
        request,
        params,
      }).id,
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export async function DELETE(request: NextRequest, { params }: any) {
  try {
    const response = (await removeDraft({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: apiQueryParams({
        request,
        params,
      }).id,
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
