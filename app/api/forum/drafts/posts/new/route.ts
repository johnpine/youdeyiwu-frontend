import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiQueryBody, apiResponse } from '@/lib/api';
import { createNewPostDraft, queryPostDrafts } from '@/services/api';

export async function GET(request: NextRequest, { params }: any) {
  try {
    const response = (await queryPostDrafts({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export async function POST(request: NextRequest, { params }: any) {
  try {
    const response = (await createNewPostDraft({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      data: await apiQueryBody({
        request,
      }),
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
