import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiResponse } from '@/lib/api';
import { getServerSideSitemap } from 'next-sitemap';
import { queryUserSitemap } from '@/services/api';
import { X_POWERED_BY_HEADER } from '@/lib/constant';

export async function GET(request: NextRequest) {
  try {
    return getServerSideSitemap(
      (
        await (
          (await queryUserSitemap({
            baseURL: process.env.APP_API_SERVER,
            token: apiAuthMiddleware(request),
          })) as Response
        ).json()
      ).data.map((id: number) => ({
        loc: `${process.env.APP_URL}/users/${id}`,
        lastmod: new Date().toISOString(),
        changefreq: 'daily',
        priority: 1,
      })),
      X_POWERED_BY_HEADER,
    );
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export const dynamic = 'force-dynamic';
