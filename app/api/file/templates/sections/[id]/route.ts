import { NextRequest, NextResponse } from 'next/server';
import {
  apiAuthMiddleware,
  apiQueryBody,
  apiQueryParams,
  apiResponse,
  createConfig,
} from '@/lib/api';

export async function POST(request: NextRequest, { params }: any) {
  try {
    const config = createConfig({
      method: 'POST',
      baseURL: process.env.APP_API_SERVER,
      body: await apiQueryBody({
        request,
        type: 'formData',
      }),
      token: apiAuthMiddleware(request),
      id: apiQueryParams({
        request,
        params,
        filter: [{ name: 'id' }],
      }).id,
    });
    const response = await fetch(
      config.baseURL + `/templates/sections/${config.id}`,
      config
    );
    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
