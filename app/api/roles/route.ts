import { NextRequest, NextResponse } from 'next/server';
import {
  apiAuthMiddleware,
  apiQueryBody,
  apiQueryParams,
  apiResponse,
} from '@/lib/api';
import { createRole, queryAllRole } from '@/services/api';

export async function GET(request: NextRequest) {
  try {
    const response = (await queryAllRole({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      query: apiQueryParams({
        request,
      }),
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export async function POST(request: NextRequest) {
  try {
    const response = (await createRole({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      data: await apiQueryBody({
        request,
      }),
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
