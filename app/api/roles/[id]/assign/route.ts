import { NextRequest, NextResponse } from 'next/server';
import {
  apiAuthMiddleware,
  apiQueryBody,
  apiQueryParams,
  apiResponse,
} from '@/lib/api';
import { assignRole, unAssignRole } from '@/services/api';

export async function PUT(request: NextRequest, { params }: any) {
  try {
    const response = (await assignRole({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      data: await apiQueryBody({
        request,
      }),
      id: apiQueryParams({
        request,
        params,
      }).id,
    })) as Response;

    const data = await response.json();
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export async function DELETE(request: NextRequest, { params }: any) {
  try {
    const queryParams = apiQueryParams({
      request,
      params,
    });

    const response = (await unAssignRole({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      data: queryParams as any,
      id: queryParams.id,
    })) as Response;

    return apiResponse({ request, response, NextResponse });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
