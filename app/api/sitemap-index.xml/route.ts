import { NextRequest, NextResponse } from 'next/server';
import { apiResponse } from '@/lib/api';
import { getServerSideSitemapIndex } from 'next-sitemap';
import { X_POWERED_BY_HEADER } from '@/lib/constant';

export async function GET(request: NextRequest) {
  try {
    return getServerSideSitemapIndex(
      [
        `${process.env.APP_URL}/api/sitemap-section.xml`,
        `${process.env.APP_URL}/api/sitemap-post.xml`,
        `${process.env.APP_URL}/api/sitemap-user.xml`,
      ],
      X_POWERED_BY_HEADER
    );
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export const dynamic = 'force-dynamic';
