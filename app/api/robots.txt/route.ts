import { NextRequest, NextResponse } from 'next/server';
import { apiResponse } from '@/lib/api';
import { X_POWERED_BY_HEADER } from '@/lib/constant';

export async function GET(request: NextRequest) {
  try {
    const body = `# *
User-agent: *
Allow: /
Disallow: /admin
Disallow: /admin/*
Disallow: /api
Disallow: /api/*
Disallow: /lib
Disallow: /lib/*
Disallow: /_next
Disallow: /_next/*
Disallow: /posts/new
Disallow: /posts/*/edit

# sitemaps
Sitemap: ${process.env.APP_URL}/api/sitemap.xml
Sitemap: ${process.env.APP_URL}/api/sitemap-index.xml`;

    return new Response(body, {
      headers: {
        'Content-Type': 'text/plain',
        ...X_POWERED_BY_HEADER,
      },
    });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export const dynamic = 'force-dynamic';
