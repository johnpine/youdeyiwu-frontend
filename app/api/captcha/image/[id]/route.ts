import { NextRequest, NextResponse } from 'next/server';
import { apiAuthMiddleware, apiQueryParams, apiResponse } from '@/lib/api';
import { generateImageById } from '@/services/api';

export async function GET(request: NextRequest, { params }: any) {
  try {
    const response = (await generateImageById({
      baseURL: process.env.APP_API_SERVER,
      token: apiAuthMiddleware(request),
      id: apiQueryParams({
        request,
        params,
        filter: [
          {
            name: 'id',
          },
        ],
      }).id,
    })) as Response;

    const data = response.body;
    return apiResponse({ request, response, NextResponse, data });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}
