import { NextRequest, NextResponse } from 'next/server';
import {
  apiClearToken,
  apiParseToken,
  apiResponse,
  apiResult,
} from '@/lib/api';

export async function GET(request: NextRequest) {
  try {
    let headers = {};
    if (apiParseToken(request)) {
      headers = apiClearToken();
    }

    const data = apiResult({});
    return apiResponse({ request, NextResponse, data, headers });
  } catch (e) {
    return apiResponse({ request, NextResponse, data: e, e });
  }
}

export const dynamic = 'force-dynamic';
