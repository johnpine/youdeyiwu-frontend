import Link from 'next/link';
import Image from 'next/image';

export default function Logo() {
  return (
    <Link href="/" className="navbar-brand me-0 me-lg-3">
      <Image
        src="/images/logo.svg"
        alt={`${process.env.APP_NAME}`}
        width={48}
        height={48}
        placeholder="blur"
        blurDataURL={process.env.APP_BLUR_DATA_URL}
      />
    </Link>
  );
}
