'use client';

import Link from 'next/link';
import { useQuery } from '@tanstack/react-query';
import { queryPostFollowMessage } from '@/services/api';
import type { IPostFollowsMessage } from '@/interfaces';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function FollowMessageNavItem({
  isLogin,
  translatedFields,
}: {
  isLogin: boolean;
  translatedFields: PrefixedTTranslatedFields<'navbar'>;
}) {
  const queryPostFollowMessageQuery = useQuery(
    ['/forum', '/posts', '/follows', '/message'],
    async () => {
      return (await queryPostFollowMessage()) as IPostFollowsMessage[];
    },
    {
      enabled: isLogin,
    },
  );

  return (
    <>
      {isLogin && (
        <li className="nav-item">
          <Link
            href="/follow"
            className="nav-link cursor-pointer d-flex align-items-center"
          >
            <span className="position-relative">
              {translatedFields.follow}
              {queryPostFollowMessageQuery.data &&
                queryPostFollowMessageQuery.data.length > 0 && (
                  <span className="position-absolute top-0 start-100 translate-middle p-1 bg-danger border border-light rounded-circle">
                    <span className="visually-hidden">
                      {queryPostFollowMessageQuery.data.length}
                    </span>
                  </span>
                )}
            </span>
          </Link>
        </li>
      )}
    </>
  );
}
