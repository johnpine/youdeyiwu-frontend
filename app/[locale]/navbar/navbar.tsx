import Logo from '@/app/[locale]/navbar/logo';
import type { IPath } from '@/interfaces';
import LinkNavItem from '@/app/[locale]/navbar/link';
import MessageNavItem from '@/app/[locale]/navbar/message';
import MoreNavItem from '@/app/[locale]/navbar/more';
import AliasNavItem from '@/app/[locale]/navbar/alias';
import FollowMessageNavItem from '@/app/[locale]/navbar/follow';
import TranslateItem from '@/app/[locale]/navbar/translate';
import ColorModeItem from '@/app/[locale]/navbar/colorMode';
import Link from 'next/link';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import { getTranslatedFields } from '@/lib/dictionaries';

export default async function Navbar({
  source,
  locale,
}: {
  source: {
    path: IPath;
  };
  locale: string;
}) {
  const path = source.path;
  const siteConfig = path.siteConfig;
  const user = path.user;
  const isLogin = !!user;
  const translatedFields = (await getTranslatedFields(
    locale,
    'navbar',
  )) as PrefixedTTranslatedFields<'navbar'>;

  return (
    <nav className="navbar navbar-expand-lg bg-body-tertiary sticky-top yw-navbar-layout user-select-none">
      <div className="container-fluid">
        <span
          className="navbar-toggler-icon d-lg-none text-black"
          data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasNavbar"
          aria-controls="offcanvasNavbar"
          aria-label="Toggle navigation"
        ></span>

        <Logo />

        <Link href="/search" className="d-lg-none">
          <i className="bi bi-search fs-5 text-secondary"></i>
        </Link>

        <div
          className="offcanvas offcanvas-start"
          id="offcanvasNavbar"
          aria-labelledby="offcanvasNavbarLabel"
        >
          <div className="offcanvas-header">
            <h5 className="offcanvas-title">{process.env.APP_NAME}</h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></button>
          </div>
          <div className="offcanvas-body justify-content-between">
            <ul className="navbar-nav">
              <LinkNavItem href="/" name={translatedFields.homePage} />
              <LinkNavItem href="/sections" name={translatedFields.content} />
              <MessageNavItem
                isLogin={isLogin}
                translatedFields={translatedFields}
              />
              <FollowMessageNavItem
                isLogin={isLogin}
                translatedFields={translatedFields}
              />
              <MoreNavItem
                isLogin={isLogin}
                user={user}
                siteConfig={siteConfig}
                translatedFields={translatedFields}
              />
              <AliasNavItem alias={user?.alias} />
            </ul>

            <div className="d-flex gap-2 gap-lg-0">
              <TranslateItem />
              <ColorModeItem translatedFields={translatedFields} />
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}
