import Link from 'next/link';
import type { IConfigSite, IUserOv } from '@/interfaces';
import LogoutDropdownItem from '@/app/[locale]/navbar/logout';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default async function MoreNavItem({
  isLogin,
  user,
  siteConfig,
  translatedFields,
}: {
  isLogin: boolean;
  user?: IUserOv;
  siteConfig?: IConfigSite;
  translatedFields: PrefixedTTranslatedFields<'navbar'>;
}) {
  return (
    <li className="nav-item dropdown">
      <a
        className="nav-link dropdown-toggle"
        id="navbarDropdown"
        role="button"
        data-bs-toggle="dropdown"
        aria-expanded="false"
      >
        {translatedFields.more}
      </a>
      <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
        {isLogin ? (
          <>
            <LinkDropdownItem
              href="/posts/new"
              name={translatedFields.createPost}
            />
            <DropdownDivider />

            <LinkDropdownItem
              href={`/users/${user!.id}`}
              name={translatedFields.profile}
            />
            <LinkDropdownItem href="/admin" name={translatedFields.dashboard} />
            <SearchDropdownItem name={translatedFields.search} />
            <DropdownDivider />

            <LogoutDropdownItem translatedFields={translatedFields} />
          </>
        ) : (
          <>
            <LinkDropdownItem href="/login" name={translatedFields.login} />
            <LinkDropdownItem
              href="/register"
              name={translatedFields.register}
            />
            <SearchDropdownItem name={translatedFields.search} />
          </>
        )}

        {siteConfig && (
          <>
            {Object.values(siteConfig).filter((item) => item).length > 1 && (
              <DropdownDivider />
            )}
            {siteConfig.helpLink && (
              <LinkDropdownItem
                href={siteConfig.helpLink}
                name={translatedFields.help}
              />
            )}
            {siteConfig.feedbackLink && (
              <LinkDropdownItem
                href={siteConfig.feedbackLink}
                name={translatedFields.feedback}
              />
            )}
            {siteConfig.reportLink && (
              <LinkDropdownItem
                href={siteConfig.reportLink}
                name={translatedFields.report}
              />
            )}
          </>
        )}
      </ul>
    </li>
  );
}

const DropdownDivider = () => {
  return (
    <li>
      <hr className="dropdown-divider" />
    </li>
  );
};

const LinkDropdownItem = ({ href, name }: { href: string; name: string }) => {
  return (
    <li>
      <Link href={href} className="dropdown-item">
        {name}
      </Link>
    </li>
  );
};

const SearchDropdownItem = async ({ name }: { name: string }) => {
  return (
    <li>
      <Link href="/search" className="dropdown-item cursor-pointer">
        {name}
      </Link>
    </li>
  );
};
