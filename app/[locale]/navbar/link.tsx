import Link from 'next/link';

export default function LinkNavItem({
  href,
  name,
}: {
  href: string;
  name: string;
}) {
  return (
    <li className="nav-item">
      <Link href={href} className="nav-link">
        {name}
      </Link>
    </li>
  );
}
