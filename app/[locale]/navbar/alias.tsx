export default function AliasNavItem({
  alias,
}: {
  alias?: string | undefined;
}) {
  return (
    <>
      {alias && (
        <li className="nav-item cursor-not-allowed">
          <a className="nav-link disabled text-truncate">{alias}</a>
        </li>
      )}
    </>
  );
}
