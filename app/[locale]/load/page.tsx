import LoadPage from '@/app/[locale]/load/load';
import type { Metadata as MetadataNext } from 'next';
import { getMetadata } from '@/lib/tool';
import { getTranslator } from 'next-intl/server';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('clientPage.loading') });
}

export default function Page({
  params = {},
  searchParams = {},
}: {
  params: {};
  searchParams: {};
}) {
  return <LoadPage />;
}
