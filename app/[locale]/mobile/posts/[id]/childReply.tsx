import { useContext, useEffect, useState } from 'react';
import type {
  IPostCommentChildReply,
  IPostCommentParentReply,
  IUserOv,
} from '@/interfaces';
import Link from 'next/link';
import Image from 'next/image';
import classNames from 'classnames';
import { getUserAvatar, toRelativeTime } from '@/lib/tool';
import useToast from 'hooks/useToast';
import LoadMoreBtn from '@/app/[locale]/mobile/posts/[id]/loadMoreBtn';
import { useInfiniteQuery } from '@tanstack/react-query';
import { queryAllChildReplyByParentReplyId } from '@/services/api';
import ChildReplyBtnPostIdH5Page from '@/app/[locale]/mobile/posts/[id]/childReplyBtn';
import ContentHtml from '@/app/[locale]/common/content/html';
import { AppContext } from '@/contexts/app';
import { PostIdPageContext } from '@/contexts/post-id';

export default function ChildReplyPostIdH5Page({
  itemData,
}: {
  itemData: IPostCommentParentReply;
}) {
  const context = useContext(PostIdPageContext)!;
  const { show } = useToast();
  const [pages, setPages] = useState<IPostCommentChildReply[]>(
    itemData.content,
  );
  const queryKey = [
    '/forum',
    '/replies',
    itemData.reply.id,
    '/child',
    'infinite',
  ];

  const replyQuery = useInfiniteQuery(
    queryKey,
    async (context) => {
      return (await queryAllChildReplyByParentReplyId({
        id: context.queryKey[2],
        query: context.pageParam,
      })) as IPostCommentParentReply;
    },
    {
      keepPreviousData: true,
      getPreviousPageParam: (firstPage) => {
        if (!firstPage.pageable.previous) {
          return;
        }
        return {
          page: Math.max(firstPage.pageable.page - 1, 0),
        };
      },
      getNextPageParam: (lastPage) => {
        if (!lastPage.pageable.next) {
          return;
        }
        return {
          page: Math.min(lastPage.pageable.page + 1, lastPage.pageable.pages),
        };
      },
      initialData: () => {
        return {
          pages: [itemData],
          pageParams: [{ page: 0 }],
        };
      },
    },
  );

  useEffect(() => {
    if (replyQuery.data) {
      setPages(
        replyQuery.data.pages
          .flatMap((item) => item.content)
          .map((item) => {
            item.reply._createdOnText = toRelativeTime(item.reply.createdOn);
            return item;
          }),
      );
    }
  }, [replyQuery.data]);

  async function onClickLoadMore() {
    try {
      await replyQuery.fetchNextPage();
    } catch (e) {
      show({
        type: 'DANGER',
        message: e,
      });
    }
  }

  return (
    <PostIdPageContext.Provider value={{ ...context, queryKey }}>
      <div className="vstack gap-4">
        {pages.map((item, index) => {
          return (
            <Item
              key={item.reply.id}
              index={index}
              id={item.reply.id}
              content={item.reply.content}
              user={item.user}
              createdOn={item.reply.createdOn}
              _createdOnText={item.reply._createdOnText}
            />
          );
        })}

        <LoadMoreBtn
          isLoading={replyQuery.isFetchingNextPage}
          onClickLoadMore={onClickLoadMore}
        />

        <div></div>
      </div>
    </PostIdPageContext.Provider>
  );
}

const Item = ({
  id,
  content,
  index,
  user,
  createdOn,
  _createdOnText,
}: {
  id: number;
  content: string;
  index: number;
  user: IUserOv;
  createdOn: string;
  _createdOnText: string | undefined;
}) => {
  return (
    <div className="vstack gap-2">
      <Name
        id={id}
        createdOn={createdOn}
        _createdOnText={_createdOnText}
        user={user}
        left={index % 2 === 0}
      />
      <Content content={content} />
    </div>
  );
};

const Name = ({
  id,
  createdOn,
  _createdOnText,
  user,
  left = true,
  width = 56,
  height = 56,
  bgColor = 'bg-white',
  stickyTop = false,
}: {
  id: number;
  createdOn: string;
  _createdOnText: string | undefined;
  user: IUserOv;
  left?: boolean;
  width?: number;
  height?: number;
  bgColor?: string;
  stickyTop?: boolean;
}) => {
  const context = useContext(AppContext);
  const metadata = context.metadata!;
  const env = metadata.env;

  return (
    <div
      className={classNames(
        'hstack gap-4 py-2 pb-3',
        {
          'flex-row-reverse': !left,
          'sticky-top': stickyTop,
        },
        bgColor ? bgColor : false,
      )}
    >
      <div className="hstack gap-4 justify-content-between align-items-center flex-grow-1">
        <div className="hstack gap-4 align-items-center">
          <div className="flex-shrink-0">
            <Link
              href={`/users/${user.id}`}
              className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
            >
              <Image
                className="rounded-4"
                src={getUserAvatar(user, metadata).mediumAvatarUrl}
                alt="avatar"
                width={width}
                height={height}
                placeholder="blur"
                blurDataURL={env.APP_BLUR_DATA_URL}
              />
            </Link>
          </div>
          <div
            className={classNames('flex-grow-1 vstack gap-2', {
              'text-end': !left,
            })}
          >
            <Link
              href={`/users/${user.id}`}
              className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
            >
              {user.alias}
            </Link>
            <time dateTime={createdOn} className="text-secondary small">
              {_createdOnText}
            </time>
          </div>
        </div>
        <ChildReplyBtnPostIdH5Page id={id} alias={user.alias} />
      </div>
    </div>
  );
};

const Content = ({ content }: { content: string }) => {
  return (
    <div className="border rounded-4 bg-light-subtle vstack gap-3 p-3 overflow-hidden">
      <div className="vstack gap-3 justify-content-center">
        <ContentHtml content={content} />
      </div>
    </div>
  );
};
