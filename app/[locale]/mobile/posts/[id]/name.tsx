import { useContext } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { getUserAvatar } from '@/lib/tool';
import PostName from '@/app/[locale]/common/post/name';
import { PostIdPageContext } from '@/contexts/post-id';
import { AppContext } from '@/contexts/app';

export default function NamePostIdH5Page() {
  const {
    source: { path, postDetails },
  } = useContext(PostIdPageContext)!;
  const appContext = useContext(AppContext);
  const metadata = appContext.metadata!;
  const env = metadata.env;
  const basic = postDetails.basic;
  const user = postDetails.user;

  return (
    <div className="d-flex gap-4 justify-content-between py-2 pb-3 bg-body">
      <PostName
        item={basic}
        isEdit={path?.user?.id === basic.createdBy}
        boxClassName="flex-grow-1"
      />
      <Link
        href={`/users/${user.id}`}
        className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
      >
        <Image
          className="rounded-4"
          src={getUserAvatar(user, metadata).mediumAvatarUrl}
          alt="avatar"
          width={56}
          height={56}
          placeholder="blur"
          blurDataURL={env.APP_BLUR_DATA_URL}
        />
      </Link>
    </div>
  );
}
