import { useContext } from 'react';
import { PostIdPageContext } from '@/contexts/post-id';

export default function StatementPostIdH5Page() {
  const {
    source: { postDetails },
  } = useContext(PostIdPageContext)!;
  const statement = postDetails.basic.statement;

  return (
    <p className="text-secondary text-center user-select-none small">
      {statement}
    </p>
  );
}
