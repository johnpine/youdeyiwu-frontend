import { type MouseEvent, useContext, useEffect, useState } from 'react';
import type {
  IPageable,
  IPostComment,
  IPostCommentReply,
  IUserOv,
} from '@/interfaces';
import Link from 'next/link';
import Image from 'next/image';
import classNames from 'classnames';
import { getUserAvatar } from '@/lib/tool';
import useOffcanvas from 'hooks/useOffcanvas';
import ReplyPostIdH5Page from '@/app/[locale]/mobile/posts/[id]/replies';
import ReplyBtnPostIdH5Page from '@/app/[locale]/mobile/posts/[id]/replyBtn';
import ContentHtml from '@/app/[locale]/common/content/html';
import { PostIdPageContext } from '@/contexts/post-id';
import { AppContext } from '@/contexts/app';
import Spinner from '@/app/[locale]/component/spinner/spinner';

export default function CommentPostIdH5Page({
  pages,
}: {
  pages: IPostComment[];
}) {
  return (
    <div className="vstack gap-4">
      {pages.map((item, index) => {
        return (
          <Item
            key={item.comment.id}
            index={index}
            id={item.comment.id}
            content={item.comment.content}
            user={item.user}
            createdOn={item.comment.createdOn}
            _createdOnText={item.comment._createdOnText}
            items={item.content}
            pageable={item.pageable}
            itemData={item}
          />
        );
      })}
    </div>
  );
}

const Item = ({
  id,
  content,
  index,
  user,
  createdOn,
  _createdOnText,
  items,
  pageable,
  itemData,
}: {
  id: number;
  content: string;
  index: number;
  user: IUserOv;
  createdOn: string;
  _createdOnText: string | undefined;
  items: IPostCommentReply[];
  pageable: IPageable;
  itemData: IPostComment;
}) => {
  return (
    <div className="vstack gap-2">
      <Name
        id={id}
        createdOn={createdOn}
        _createdOnText={_createdOnText}
        user={user}
        left={index % 2 === 0}
      />
      <Content
        id={id}
        items={items}
        pageable={pageable}
        content={content}
        itemData={itemData}
      />
    </div>
  );
};

const Name = ({
  id,
  createdOn,
  _createdOnText,
  user,
  left = true,
  width = 56,
  height = 56,
  stickyTop = false,
}: {
  id: number;
  createdOn: string;
  _createdOnText: string | undefined;
  user: IUserOv;
  left?: boolean;
  width?: number;
  height?: number;
  bgColor?: string;
  stickyTop?: boolean;
}) => {
  const appContext = useContext(AppContext);
  const metadata = appContext.metadata!;
  const env = metadata.env;

  return (
    <div
      className={classNames('hstack gap-4 py-2 pb-3', {
        'sticky-top': stickyTop,
      })}
    >
      <div
        className={classNames(
          'hstack gap-4 justify-content-between align-items-center flex-grow-1 bg-body',
          {
            'flex-row-reverse': !left,
          },
        )}
      >
        <div className="hstack gap-4 align-items-center">
          <div className="flex-shrink-0">
            <Link
              href={`/users/${user.id}`}
              className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
            >
              <Image
                className="rounded-4"
                src={getUserAvatar(user, metadata).mediumAvatarUrl}
                alt="avatar"
                width={width}
                height={height}
                placeholder="blur"
                blurDataURL={env.APP_BLUR_DATA_URL}
              />
            </Link>
          </div>
          <div
            className={classNames('flex-grow-1 vstack gap-2', {
              'text-end': !left,
            })}
          >
            <Link
              href={`/users/${user.id}`}
              className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
            >
              {user.alias}
            </Link>
            <time dateTime={createdOn} className="text-secondary small">
              {_createdOnText}
            </time>
          </div>
        </div>
        <ReplyBtnPostIdH5Page id={id} alias={user.alias} left={left} />
      </div>
    </div>
  );
};

const Content = ({
  id,
  content,
  items,
  itemData,
}: {
  id: number;
  content: string;
  items: IPostCommentReply[];
  pageable: IPageable;
  itemData: IPostComment;
}) => {
  return (
    <div className="border rounded-4 bg-light-subtle vstack gap-3 p-3 overflow-hidden">
      <div className="vstack gap-3 justify-content-center">
        <ContentHtml content={content} />
        {items.length > 0 && (
          <div className="vstack gap-4">
            <PartialReplyContent id={id} items={items} />
            <ViewMoreReplies itemData={itemData} />
          </div>
        )}
      </div>
    </div>
  );
};

const PartialReplyContent = ({
  id,
  items,
}: {
  id: number;
  items: IPostCommentReply[];
}) => {
  return (
    <div className="vstack gap-2 border rounded-4 bg-light-subtle p-3">
      {items.map((item, index) => {
        return (
          <div key={item.reply.id}>
            <Name
              id={id}
              width={48}
              height={48}
              createdOn={item.reply.createdOn}
              _createdOnText={item.reply._createdOnText}
              user={item.user}
              left={index % 2 === 0}
              stickyTop={false}
            />
            <div
              className="content-layer"
              dangerouslySetInnerHTML={{ __html: item.reply.content }}
            />
          </div>
        );
      })}
    </div>
  );
};

const ViewMoreReplies = ({ itemData }: { itemData: IPostComment }) => {
  const context = useContext(PostIdPageContext)!;
  const { translatedFields } = context;
  const { isLoadingOffcanvasShowing, showOffcanvas } = useOffcanvas();
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(isLoadingOffcanvasShowing);
  }, [isLoadingOffcanvasShowing]);

  function onClickSeeMore(e: MouseEvent<HTMLAnchorElement>) {
    e.stopPropagation();
    e.preventDefault();

    if (isLoading) {
      return;
    }

    showOffcanvas({
      title: `@${itemData.user.alias}`,
      bottom: true,
      content: (
        <PostIdPageContext.Provider value={context}>
          <ReplyPostIdH5Page itemData={itemData} />
        </PostIdPageContext.Provider>
      ),
      offcanvasBodyClass: 'pt-0',
      offcanvasStyle: {
        height: '85vh',
      },
    });
  }

  return (
    <div className="small">
      <a
        onClick={onClickSeeMore}
        className="text-primary link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
      >
        <span>{translatedFields.viewMoreReplies}</span>
        {isLoading ? (
          <Spinner classs="ms-1" />
        ) : (
          <i className="bi bi-chevron-right ms-1"></i>
        )}
      </a>
    </div>
  );
};
