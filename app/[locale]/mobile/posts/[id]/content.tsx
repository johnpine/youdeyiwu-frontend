import { useContext } from 'react';
import ContentHtml from '@/app/[locale]/common/content/html';
import { PostIdPageContext } from '@/contexts/post-id';

export default function ContentPostIdH5Page() {
  const {
    source: { postDetails },
  } = useContext(PostIdPageContext)!;
  const content = postDetails.content;

  return (
    <div className="my-4">
      <ContentHtml content={content} />
    </div>
  );
}
