import Spinner from '@/app/[locale]/component/spinner/spinner';

export default function LoadMoreBtn({
  onClickLoadMore,
  isLoading,
}: {
  onClickLoadMore: () => void;
  isLoading?: boolean;
}) {
  return (
    <div className="row mx-0">
      <div className="col px-0">
        <div className="card border-0">
          <div className="card-body">
            <button
              onClick={onClickLoadMore}
              disabled={isLoading}
              type="button"
              className="btn rounded-pill text-secondary-emphasis w-100"
            >
              {isLoading ? <Spinner /> : <i className="bi bi-three-dots"></i>}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
