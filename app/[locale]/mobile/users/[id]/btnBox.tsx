import { type ReactNode } from 'react';

export default function BtnBoxUserIdH5Page({
  children,
}: {
  children: ReactNode;
}) {
  return <div className="rounded-2 border">{children}</div>;
}
