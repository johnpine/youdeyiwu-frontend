import Image from 'next/image';
import { getUserAvatar } from '@/lib/tool';
import { useContext } from 'react';
import { AppContext } from '@/contexts/app';
import { UserIdPageContext } from '@/contexts/user-id';

export default function NameUserIdH5Page() {
  const {
    source: { userDetails },
  } = useContext(UserIdPageContext)!;
  const user = userDetails.user;
  const context = useContext(AppContext);
  const metadata = context.metadata!;
  const env = metadata.env;

  return (
    <div className="d-flex gap-4 justify-content-between">
      <div className="vstack flex-grow-1 gap-2 justify-content-center">
        {user.details.personalizedSignature && (
          <div className="user-select-none">
            {user.details.personalizedSignature}
          </div>
        )}
        <div className="h4 mb-0">{user.alias}</div>
      </div>
      <Image
        className="rounded-4"
        src={getUserAvatar(user, metadata).mediumAvatarUrl}
        alt="avatar"
        width={56}
        height={56}
        placeholder="blur"
        blurDataURL={env.APP_BLUR_DATA_URL}
      />
    </div>
  );
}
