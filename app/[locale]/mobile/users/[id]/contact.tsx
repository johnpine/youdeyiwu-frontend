import { isHttpOrHttps } from '@/lib/tool';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import { useContext } from 'react';
import { UserIdPageContext } from '@/contexts/user-id';

export default function ContactUserIdH5Page() {
  const {
    source: { userDetails },
  } = useContext(UserIdPageContext)!;
  const contacts = userDetails.user.details.contacts ?? [];

  return (
    <>
      {contacts.length > 0 ? (
        <div className="vstack gap-2">
          {contacts.map((item) => {
            return (
              <div key={item.id}>
                <span className="me-2">{item.key}</span>
                {item.val.includes('@') && item.val.includes('.') ? (
                  <a
                    className="link-dark text-decoration-none text-decoration-underline-hover"
                    href={`mailto:${item.val}`}
                  >
                    {item.val}
                  </a>
                ) : (
                  <>
                    {isHttpOrHttps(item.val) ? (
                      <a
                        className="link-dark text-decoration-none text-decoration-underline-hover"
                        href={item.val}
                      >
                        {item.val}
                      </a>
                    ) : (
                      <span>{item.val}</span>
                    )}
                  </>
                )}
              </div>
            );
          })}
        </div>
      ) : (
        <Nodata />
      )}
    </>
  );
}
