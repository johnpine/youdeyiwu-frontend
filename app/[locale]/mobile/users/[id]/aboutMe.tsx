import Nodata from '@/app/[locale]/common/nodata/nodata';
import { useContext } from 'react';
import { UserIdPageContext } from '@/contexts/user-id';

export default function AboutMmeUserIdH5Page() {
  const {
    source: { userDetails },
  } = useContext(UserIdPageContext)!;
  const about = userDetails.user.details.about;

  return (
    <>
      {about ? (
        <div
          dangerouslySetInnerHTML={{
            __html: about,
          }}
        />
      ) : (
        <Nodata />
      )}
    </>
  );
}
