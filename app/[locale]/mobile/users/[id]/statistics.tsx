import type { IUserOv } from '@/interfaces';
import { formatCount } from '@/lib/tool';
import { useContext } from 'react';
import { UserIdPageContext } from '@/contexts/user-id';

export default function StatisticsUserIdH5Page({
  user,
}: {
  user: IUserOv | undefined;
}) {
  const { translatedFields } = useContext(UserIdPageContext)!;

  return (
    <div className="row row-cols-1 g-4">
      <div className="col">
        <div className="card rounded-5">
          <div className="card-body">
            <div className="mb-1">
              <i className="bi bi-bar-chart fs-5"></i>
            </div>
            <p className="card-text">
              {translatedFields.numberOfContents}&nbsp;
              {formatCount(user ? user.statistic.sections : 0)}
            </p>
          </div>
        </div>
      </div>

      <div className="col">
        <div className="card rounded-5">
          <div className="card-body">
            <div className="mb-1">
              <i className="bi bi-bar-chart fs-5"></i>
            </div>
            <p className="card-text">
              {translatedFields.numberOfTags}&nbsp;
              {formatCount(user ? user.statistic.tags : 0)}
            </p>
          </div>
        </div>
      </div>

      <div className="col">
        <div className="card rounded-5">
          <div className="card-body">
            <div className="mb-1">
              <i className="bi bi-bar-chart fs-5"></i>
            </div>
            <p className="card-text">
              {translatedFields.numberOfPosts}&nbsp;
              {formatCount(user ? user.statistic.posts : 0)}
            </p>
          </div>
        </div>
      </div>

      <div className="col">
        <div className="card rounded-5">
          <div className="card-body">
            <div className="mb-1">
              <i className="bi bi-bar-chart fs-5"></i>
            </div>
            <p className="card-text">
              {translatedFields.numberOfComments}&nbsp;
              {formatCount(user ? user.statistic.comments : 0)}
            </p>
          </div>
        </div>
      </div>

      <div className="col">
        <div className="card rounded-5">
          <div className="card-body">
            <div className="mb-1">
              <i className="bi bi-bar-chart fs-5"></i>
            </div>
            <p className="card-text">
              {translatedFields.numberOfReplies}&nbsp;
              {formatCount(user ? user.statistic.replies : 0)}
            </p>
          </div>
        </div>
      </div>

      <div className="col">
        <div className="card rounded-5">
          <div className="card-body">
            <div className="mb-1">
              <i className="bi bi-bar-chart fs-5"></i>
            </div>
            <p className="card-text">
              {translatedFields.numberOfViews}&nbsp;
              {formatCount(user ? user.statistic.views : 0)}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
