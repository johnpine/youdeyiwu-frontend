import classNames from 'classnames';
import { useRouter } from 'next/navigation';
import { type MouseEvent, type ReactNode, useContext } from 'react';
import useOffcanvas from 'hooks/useOffcanvas';
import { UserIdPageContext } from '@/contexts/user-id';

export default function BtnUserIdH5Page({
  path,
  iName = 'signpost',
  name,
  content,
  fn,
  isLoading,
}: {
  path?: string;
  iName?: string;
  name: string;
  content?: ReactNode;
  fn?: () => void;
  isLoading?: boolean;
}) {
  const router = useRouter();
  const { showOffcanvas } = useOffcanvas();
  const context = useContext(UserIdPageContext)!;

  function onClickLink(e: MouseEvent<HTMLDivElement>) {
    e.stopPropagation();
    e.preventDefault();

    if (path) {
      router.push(path);
    } else if (fn) {
      if (isLoading) {
        return;
      }

      fn();
    } else if (content) {
      showOffcanvas({
        title: name,
        content: (
          <UserIdPageContext.Provider value={context}>
            {content}
          </UserIdPageContext.Provider>
        ),
      });
    }
  }

  return (
    <div
      onClick={onClickLink}
      className={classNames(
        'd-flex align-items-center gap-4 justify-content-between p-2 btn',
      )}
    >
      <div className="hstack gap-3 align-items-center">
        <div>
          <i className={`bi bi-${iName}`}></i>
        </div>
        <div>{name}</div>
      </div>
      <div>
        {fn ? (
          isLoading ? (
            <span
              className="spinner-border spinner-border-sm text-secondary"
              role="status"
              aria-hidden="true"
            ></span>
          ) : (
            <i className="bi bi-chevron-right"></i>
          )
        ) : (
          <i className="bi bi-chevron-right"></i>
        )}
      </div>
    </div>
  );
}
