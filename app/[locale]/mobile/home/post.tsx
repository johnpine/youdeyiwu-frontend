'use client';

import type { IPost } from '@/interfaces';
import classNames from 'classnames';
import Link from 'next/link';
import { useContext } from 'react';
import PostItem from '@/app/[locale]/common/post/item';
import { HomePageContext } from '@/contexts/home';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import { getGivenTranslatedFields } from '@/lib/tool';

export default function PostHomeH5Page(
  this: any,
  {
    items = [],
  }: {
    items?: IPost[];
  },
) {
  const {
    source: { sectionDetails },
    translatedFields,
  } = useContext(HomePageContext)!;

  return (
    <div className="vstack gap-3">
      <div className="hstack gap-4 align-items-center justify-content-between">
        <div className="h4 mb-0">{translatedFields.allPost}</div>
        {sectionDetails && (
          <Link
            className={classNames(
              'text-decoration-none',
              items.length === 0 ? 'text-secondary' : 'text-reset',
            )}
            href={`/sections/${sectionDetails.basic.id}`}
          >
            {translatedFields.more}
          </Link>
        )}
      </div>

      {items.map((item, index) => {
        return (
          <div className="row" key={item.id}>
            <div className="col-12">
              {index > 0 && (
                <div
                  className="yw-bg"
                  style={{ height: '0.35rem', transform: 'scale(1.1)' }}
                ></div>
              )}
            </div>
            <div className="col-12 rounded-3">
              <PostItem
                translatedFields={getGivenTranslatedFields(
                  ['comment', 'reply'],
                  translatedFields,
                )}
                item={item}
              />
            </div>
          </div>
        );
      })}

      {items.length === 0 && <Nodata />}
    </div>
  );
}
