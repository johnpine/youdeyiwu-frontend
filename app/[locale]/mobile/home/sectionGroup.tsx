'use client';

import type { ISectionGroup } from '@/interfaces';
import classNames from 'classnames';
import { useContext, useState } from 'react';
import Link from 'next/link';
import { generateRandomLinearGradientColor, getQueryStrings } from '@/lib/tool';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import { HomePageContext } from '@/contexts/home';
import { useRouter, useSearchParams } from 'next/navigation';

export default function SectionGroupHomeH5Page(this: any) {
  const searchParams = useSearchParams();
  const router = useRouter();
  const {
    source: {
      sections,
      queryParams: { sectionGroupId },
    },
    translatedFields,
  } = useContext(HomePageContext)!;
  const sectionGroups: ISectionGroup[] = sections
    .filter((item) => !!item.sectionGroup)
    .filter(
      (item, index, array) =>
        array.findIndex(
          (value) => value.sectionGroup!.id === item.sectionGroup!.id,
        ) === index,
    )
    .map((item) => item.sectionGroup)
    .sort((a, b) => a!.sort - b!.sort) as [];
  const currentSectionGroupItem = sectionGroups.find(
    (item) => item.id + '' === sectionGroupId,
  );
  const [cardBackgroundColor, setCardBackgroundColor] = useState<string[]>(
    sectionGroups.map((_) => generateRandomLinearGradientColor()),
  );

  function onClickItem(item: ISectionGroup) {
    let url;
    if (currentSectionGroupItem) {
      url = `?${getQueryStrings(
        {},
        searchParams,
        ['sectionGroupId', 'sgid'],
        true,
      )}`;
    } else {
      url = `?${getQueryStrings(
        {
          sgid: item.id,
        },
        searchParams,
      )}`;
    }
    router.push(url);
  }

  return (
    <div className="vstack gap-3">
      <div className="hstack gap-4 align-items-center justify-content-between">
        <div className="h4 mb-0">{translatedFields.relatedContent}</div>
        <Link
          className={classNames(
            'text-decoration-none',
            sectionGroups.length === 0 ? 'text-secondary' : 'text-reset',
          )}
          href="/sections"
        >
          {translatedFields.more}
          <i className="bi bi-chevron-right"></i>
        </Link>
      </div>
      <div className="pb-2 hstack gap-4 flex-nowrap text-nowrap overflow-x-auto">
        {sectionGroups.map((item, index) => {
          if (
            cardBackgroundColor.length > 0 &&
            currentSectionGroupItem?.id === item.id
          ) {
            return (
              <div
                key={item.id}
                onClick={onClickItem.bind(this, item)}
                className="rounded-4 flex-shrink-0 d-flex justify-content-center align-items-center cursor-pointer"
                style={{
                  width: 260,
                  height: 146,
                  background: cardBackgroundColor[index],
                  mixBlendMode: 'difference',
                }}
              >
                {item.name}
              </div>
            );
          } else {
            return (
              <div
                key={item.id}
                onClick={onClickItem.bind(this, item)}
                className="rounded-4 bg-light flex-shrink-0 d-flex justify-content-center align-items-center cursor-pointer"
                style={{
                  width: 260,
                  height: 146,
                }}
              >
                {item.name}
              </div>
            );
          }
        })}
      </div>
      {sectionGroups.length === 0 && <Nodata />}
    </div>
  );
}
