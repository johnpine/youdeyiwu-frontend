import DeniedPage from '@/app/[locale]/403/denied';
import type { Metadata as MetadataNext } from 'next';
import { getMetadata } from '@/lib/tool';
import { getTranslator } from 'next-intl/server';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('clientPage.permissionRequired') });
}

export default function Page() {
  return <DeniedPage />;
}
