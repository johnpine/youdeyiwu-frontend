type TAlertLoadTranslatedField = 'loading';

export default function AlertLoad({
  translatedFields = {
    loading: 'Loading',
  },
}: {
  translatedFields?: Record<TAlertLoadTranslatedField, any>;
}) {
  return (
    <div className="container-fluid my-4">
      <div className="d-flex align-items-center justify-content-between gap-2">
        <div>{translatedFields.loading}</div>
        <div className="spinner-border" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
    </div>
  );
}
