import classNames from 'classnames';

export default function Alert({
  type = 'info',
  message,
}: {
  type?: 'info' | 'error' | 'warning' | 'success';
  message: any;
}) {
  function getAlert(message: any) {
    let _message;
    if (typeof message === 'object' && 'message' in message) {
      _message = message.message;
    } else if (typeof message === 'string') {
      _message = message;
    } else {
      _message = message + '';
    }
    return _message ?? 'Sorry, an error occurred while accessing this resource';
  }

  return (
    <div className="container-fluid">
      <div
        className={classNames('alert d-flex align-items-center', {
          'alert-primary': type === 'info',
          'alert-success': type === 'success',
          'alert-danger': type === 'error',
          'alert-warning': type === 'warning',
        })}
        role="alert"
      >
        {type === 'info' && <i className="bi bi-info-circle-fill me-2"></i>}
        {type === 'success' && <i className="bi bi-check-circle-fill me-2"></i>}
        {type === 'error' && (
          <i className="bi bi-exclamation-triangle-fill me-2"></i>
        )}
        <div>{getAlert(message)}</div>
      </div>
    </div>
  );
}
