'use client';

import { oauthClientAuthCallback } from '@/services/api';
import { useEffect } from 'react';
import { useMutation } from '@tanstack/react-query';
import useToast from '@/hooks/useToast';
import type { IYwPageContext } from '@/contexts/yw';
import Spinner from '@/app/[locale]/component/spinner/spinner';

export default function YwPage({ source, translatedFields }: IYwPageContext) {
  const { show } = useToast();

  const oauthClientAuthCallbackMutation = useMutation(oauthClientAuthCallback);

  useEffect(() => {
    oauthClientAuthCallbackMutation
      .mutateAsync({
        data: source.queryParams,
      })
      .then((value) => {
        show({
          message: translatedFields.openAuthenticationAuthorizationCompleted,
          type: 'SUCCESS',
        });

        setTimeout(() => {
          show({
            message: translatedFields.redirectToBeDone,
            type: 'PRIMARY',
          });
        }, 1000);

        setTimeout(() => {
          location.href = '/';
        }, 1500);
      })
      .catch((e) => {
        show({
          type: 'DANGER',
          message: e,
        });
      });
  }, []);

  return (
    <div className="col px-0 vh-100">
      <div className="card border-0 h-100">
        <div className="card-body align-items-center card-body d-flex justify-content-center text-center">
          <div>
            <div className="mb-4 d-flex align-items-center justify-content-center">
              <Spinner classs="me-2" />
              <span>{translatedFields.processingInProgress}</span>
            </div>
            <div className="text-secondary">
              {
                translatedFields.reminderProcessWillAutomaticallyRedirectUponCompletion
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
