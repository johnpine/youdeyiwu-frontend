import ErrorPage from '@/app/[locale]/error/error';
import type { Metadata as MetadataNext } from 'next';
import { getMetadata } from '@/lib/tool';
import { getTranslator } from 'next-intl/server';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('clientPage.errorEncountered') });
}

export default function Page() {
  return <ErrorPage />;
}
