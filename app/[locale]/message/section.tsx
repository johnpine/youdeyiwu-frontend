import type { IClientMessage } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import ContentHtml from '@/app/[locale]/common/content/html';

export default function SectionMessage({ item }: { item: IClientMessage }) {
  const content = JSON.parse(item.content) || {};

  if (content.key === 'create_section') {
    return <CreateSection item={item} />;
  } else if (content.key === 'update_section') {
    return <UpdateSection item={item} />;
  } else if (content.key === 'update_section_content') {
    return <UpdateSectionContent item={item} />;
  } else if (content.key === 'update_section_state') {
    return <UpdateSectionState item={item} />;
  } else if (content.key === 'update_section_other_state') {
    return <UpdateSectionOtherState item={item} />;
  } else if (content.key === 'update_section_tag') {
    return <UpdateSectionTag item={item} />;
  } else if (content.key === 'remove_section_tag') {
    return <RemoveSectionTag item={item} />;
  } else if (content.key === 'update_section_admin') {
    return <UpdateSectionAdmin item={item} />;
  } else if (content.key === 'remove_section_admin') {
    return <RemoveSectionAdmin item={item} />;
  }

  return (
    <div className="card-body">
      <Nodata />
    </div>
  );
}

const CreateSection = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        {content.state === 'SHOW' && (
          <i className="bi bi-eye-fill text-success" title="显示"></i>
        )}

        {content.state === 'HIDE' && (
          <i className="bi bi-eye-slash-fill text-danger" title="隐藏"></i>
        )}

        {content.state === 'LOCK' && (
          <i className="bi bi-lock-fill text-primary" title="锁定"></i>
        )}

        {content.state === 'CLOSE' && (
          <i className="bi bi-x-circle-fill text-warning" title="关闭"></i>
        )}

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/sections/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdateSection = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i className="bi bi-info-circle-fill text-success" title="基本信息"></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/sections/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdateSectionContent = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i className="bi bi-info-circle-fill text-success" title="版块内容"></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/sections/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdateSectionState = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        {content.state === 'SHOW' && (
          <i className="bi bi-eye-fill text-success" title="显示"></i>
        )}

        {content.state === 'HIDE' && (
          <i className="bi bi-eye-slash-fill text-danger" title="隐藏"></i>
        )}

        {content.state === 'LOCK' && (
          <i className="bi bi-lock-fill text-primary" title="锁定"></i>
        )}

        {content.state === 'CLOSE' && (
          <i className="bi bi-x-circle-fill text-warning" title="关闭"></i>
        )}

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/sections/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdateSectionOtherState = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i
          className="bi bi-exclamation-circle-fill text-success"
          title="其他"
        ></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/sections/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdateSectionTag = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i className="bi bi-tag-fill text-primary" title="标签"></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/sections/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const RemoveSectionTag = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i className="bi bi-tag-fill text-danger" title="标签"></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/sections/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdateSectionAdmin = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i className="bi bi-people-fill text-primary" title="版块管理员"></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/sections/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const RemoveSectionAdmin = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i className="bi bi-people-fill text-danger" title="版块管理员"></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/sections/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};
