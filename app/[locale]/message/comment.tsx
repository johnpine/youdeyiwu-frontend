import type { IClientMessage } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import ContentHtml from '@/app/[locale]/common/content/html';

export default function CommentMessage({ item }: { item: IClientMessage }) {
  const content = JSON.parse(item.content) || {};

  if (content.key === 'create_post_comment') {
    return <CreateComment item={item} />;
  } else if (content.key === 'update_comment_state') {
    return <UpdateCommentState item={item} />;
  }

  return (
    <div className="card-body">
      <Nodata />
    </div>
  );
}

const CreateComment = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  let overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  if (overview) {
    overview = overview.replace(
      content.commentContent,
      `<div class="my-2 text-wrap bg-body-secondary border">${content.commentContent}</div>`,
    );
  }

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        {content.reviewState === 'APPROVED' && (
          <i
            className="bi bi-patch-check-fill text-success"
            title="审核通过"
          ></i>
        )}

        {content.reviewState === 'DENIED' && (
          <i
            className="bi bi-patch-exclamation-fill text-danger"
            title="审核不通过"
          ></i>
        )}

        {content.reviewState === 'PENDING' && (
          <i
            className="bi bi-patch-exclamation-fill text-primary"
            title="等待审核"
          ></i>
        )}

        {content.reviewState === 'CLOSE' && (
          <i
            className="bi bi-patch-exclamation-fill text-warning"
            title="关闭审核"
          ></i>
        )}

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/posts/${content.postId}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdateCommentState = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        {content.reviewState === 'APPROVED' && (
          <i
            className="bi bi-patch-check-fill text-success"
            title="审核通过"
          ></i>
        )}

        {content.reviewState === 'DENIED' && (
          <i
            className="bi bi-patch-exclamation-fill text-danger"
            title="审核不通过"
          ></i>
        )}

        {content.reviewState === 'PENDING' && (
          <i
            className="bi bi-patch-exclamation-fill text-primary"
            title="等待审核"
          ></i>
        )}

        {content.reviewState === 'CLOSE' && (
          <i
            className="bi bi-patch-exclamation-fill text-warning"
            title="关闭审核"
          ></i>
        )}

        <ContentHtml content={overview} />
      </div>

      <div
        dangerouslySetInnerHTML={{
          __html: content.commentContent || '',
        }}
      ></div>

      <a
        href={`/posts/${content.postId}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};
