import type { IClientMessage } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import ContentHtml from '@/app/[locale]/common/content/html';

export default function OauthClientMessage({ item }: { item: IClientMessage }) {
  const content = JSON.parse(item.content) || {};

  if (content.key === 'create_oauth_client') {
    return <CreateOauthClient item={item} />;
  } else if (content.key === 'update_oauth_client_state') {
    return <UpdateOauthClientState item={item} />;
  } else if (content.key === 'update_oauth_client_review_state') {
    return <UpdateOauthClientReviewState item={item} />;
  }

  return (
    <div className="card-body">
      <Nodata />
    </div>
  );
}

const CreateOauthClient = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        {content.reviewState === 'SUCCESSFUL' && (
          <i className="bi bi-patch-check-fill text-success" title="通过"></i>
        )}

        {content.reviewState === 'FAILED' && (
          <i
            className="bi bi-patch-exclamation-fill text-danger"
            title="拒绝"
          ></i>
        )}

        {content.reviewState === 'PENDING' && (
          <i
            className="bi bi-patch-exclamation-fill text-secondary"
            title="待审"
          ></i>
        )}

        <ContentHtml content={overview} />
      </div>

      <a
        href="/oauth/client"
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdateOauthClientState = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        {content.state === 'ENABLE' && (
          <i className="bi bi-patch-check-fill text-success" title="启用"></i>
        )}

        {content.state === 'DISABLED' && (
          <i
            className="bi bi-patch-exclamation-fill text-danger"
            title="禁用"
          ></i>
        )}

        <ContentHtml content={overview} />
      </div>

      <a
        href="/oauth/client"
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdateOauthClientReviewState = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        {content.reviewState === 'SUCCESSFUL' && (
          <i className="bi bi-patch-check-fill text-success" title="通过"></i>
        )}

        {content.reviewState === 'FAILED' && (
          <i
            className="bi bi-patch-exclamation-fill text-danger"
            title="拒绝"
          ></i>
        )}

        {content.reviewState === 'PENDING' && (
          <i
            className="bi bi-patch-exclamation-fill text-secondary"
            title="待审"
          ></i>
        )}

        <ContentHtml content={overview} />
      </div>

      <a
        href="/oauth/client"
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};
