import type { IClientMessage } from '@/interfaces';
import PostMessage from '@/app/[locale]/message/post';
import CommentMessage from '@/app/[locale]/message/comment';
import ReplyMessage from '@/app/[locale]/message/reply';
import SectionMessage from '@/app/[locale]/message/section';
import OauthClientMessage from '@/app/[locale]/message/oauthClient';
import OauthClientApiMessage from '@/app/[locale]/message/oauthClientApi';
import Nodata from '@/app/[locale]/common/nodata/nodata';

export default function GeneralMessage({ item }: { item: IClientMessage }) {
  if (item.messageType === 'POST') {
    return <PostMessage item={item} />;
  } else if (item.messageType === 'COMMENT') {
    return <CommentMessage item={item} />;
  } else if (item.messageType === 'REPLY') {
    return <ReplyMessage item={item} />;
  } else if (item.messageType === 'SECTION') {
    return <SectionMessage item={item} />;
  } else if (item.messageType === 'OAUTH_CLIENT') {
    return <OauthClientMessage item={item} />;
  } else if (item.messageType === 'OAUTH_CLIENT_API') {
    return <OauthClientApiMessage item={item} />;
  }

  return (
    <div className="card-body">
      <Nodata />
    </div>
  );
}
