import type { IClientMessage } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import ContentHtml from '@/app/[locale]/common/content/html';

export default function PostMessage({ item }: { item: IClientMessage }) {
  const content = JSON.parse(item.content) ?? {};

  if (content.key === 'create_post') {
    return <CreatePost item={item} />;
  } else if (content.key === 'update_post_content') {
    return <UpdatePostContent item={item} />;
  } else if (content.key === 'update_post_state') {
    return <UpdatePostState item={item} />;
  } else if (content.key === 'update_post_sort_state') {
    return <UpdatePostSortState item={item} />;
  } else if (content.key === 'update_post_other_state') {
    return <UpdatePostOtherState item={item} />;
  } else if (content.key === 'update_post_tag') {
    return <UpdatePostTag item={item} />;
  } else if (content.key === 'remove_post_tag') {
    return <RemovePostTag item={item} />;
  } else if (content.key === 'update_post_badge') {
    return <UpdatePostBadge item={item} />;
  } else if (content.key === 'remove_post_badge') {
    return <RemovePostBadge item={item} />;
  }

  return (
    <div className="card-body">
      <Nodata />
    </div>
  );
}

const CreatePost = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        {content.reviewState === 'APPROVED' && (
          <i
            className="bi bi-patch-check-fill text-success"
            title="审核通过"
          ></i>
        )}

        {content.reviewState === 'DENIED' && (
          <i
            className="bi bi-patch-exclamation-fill text-danger"
            title="审核不通过"
          ></i>
        )}

        {content.reviewState === 'PENDING' && (
          <i
            className="bi bi-patch-exclamation-fill text-primary"
            title="等待审核"
          ></i>
        )}

        {content.reviewState === 'CLOSE' && (
          <i
            className="bi bi-patch-exclamation-fill text-warning"
            title="关闭审核"
          ></i>
        )}

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/posts/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdatePostContent = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="bd-callout bd-callout-info my-0">当前帖子需要审核</div>

      <div className="card-text hstack gap-2">
        <i
          className="bi bi-patch-check-fill text-danger"
          title="审核不通过"
        ></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/posts/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdatePostState = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i
          className="bi bi-patch-exclamation-fill text-danger"
          title="审核不通过"
        ></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/posts/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdatePostSortState = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i className="bi bi-funnel-fill text-success" title="排序"></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/posts/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdatePostOtherState = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i
          className="bi bi-exclamation-circle-fill text-success"
          title="其他"
        ></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/posts/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdatePostTag = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i className="bi bi-tag-fill text-primary" title="标签"></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/posts/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const RemovePostTag = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i className="bi bi-tag-fill text-danger" title="标签"></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/posts/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdatePostBadge = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i className="bi bi-award-fill text-primary" title="徽章"></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/posts/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const RemovePostBadge = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        <i className="bi bi-award-fill text-danger" title="徽章"></i>

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/posts/${content.id}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};
