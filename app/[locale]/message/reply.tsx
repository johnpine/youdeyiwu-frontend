import type { IClientMessage } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import ContentHtml from '@/app/[locale]/common/content/html';

export default function ReplyMessage({ item }: { item: IClientMessage }) {
  const content = JSON.parse(item.content) || {};

  if (
    content.key === 'create_post_reply' ||
    content.key === 'create_post_parent_reply' ||
    content.key === 'create_post_child_reply'
  ) {
    return <CreateReply item={item} />;
  } else if (
    content.key === 'update_post_reply_state' ||
    content.key === 'update_post_parent_reply_state' ||
    content.key === 'update_post_child_reply_state'
  ) {
    return <UpdateReplyState item={item} />;
  }

  return (
    <div className="card-body">
      <Nodata />
    </div>
  );
}

const CreateReply = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  let overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  if (overview) {
    overview = overview.replace(
      content.replyContent,
      `<div class="my-2 text-wrap bg-body-secondary border">${content.replyContent}</div>`,
    );
  }

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        {content.reviewState === 'APPROVED' && (
          <i
            className="bi bi-patch-check-fill text-success"
            title="审核通过"
          ></i>
        )}

        {content.reviewState === 'DENIED' && (
          <i
            className="bi bi-patch-exclamation-fill text-danger"
            title="审核不通过"
          ></i>
        )}

        {content.reviewState === 'PENDING' && (
          <i
            className="bi bi-patch-exclamation-fill text-primary"
            title="等待审核"
          ></i>
        )}

        {content.reviewState === 'CLOSE' && (
          <i
            className="bi bi-patch-exclamation-fill text-warning"
            title="关闭审核"
          ></i>
        )}

        <ContentHtml content={overview} />
      </div>

      <a
        href={`/posts/${content.postId}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};

const UpdateReplyState = ({ item }: { item: IClientMessage }) => {
  const name = item.name;
  const overview = item.overview || '';
  const content = JSON.parse(item.content) || {};

  return (
    <div className="card-body vstack gap-4">
      <div className="card-title">{name}</div>

      <div className="card-text hstack gap-2">
        {content.reviewState === 'APPROVED' && (
          <i
            className="bi bi-patch-check-fill text-success"
            title="审核通过"
          ></i>
        )}

        {content.reviewState === 'DENIED' && (
          <i
            className="bi bi-patch-exclamation-fill text-danger"
            title="审核不通过"
          ></i>
        )}

        {content.reviewState === 'PENDING' && (
          <i
            className="bi bi-patch-exclamation-fill text-primary"
            title="等待审核"
          ></i>
        )}

        {content.reviewState === 'CLOSE' && (
          <i
            className="bi bi-patch-exclamation-fill text-warning"
            title="关闭审核"
          ></i>
        )}

        <ContentHtml content={overview} />
      </div>

      <div
        dangerouslySetInnerHTML={{
          __html: content.replyContent || '',
        }}
      ></div>

      <a
        href={`/posts/${content.postId}`}
        className="card-link text-decoration-none text-decoration-underline-hover"
      >
        {content.name}
      </a>
    </div>
  );
};
