import H5Box from '@/app/[locale]/common/other/h5';
import Link from 'next/link';
import type { IPath } from '@/interfaces';
import { getTranslator } from 'next-intl/server';

const year = new Date().getFullYear();

export default async function Footer(
  this: any,
  { locale, source }: { source: { path: IPath }; locale: string },
) {
  const isLogin = !!source.path?.user;
  const t = await getTranslator(locale);

  return (
    <>
      {process.env.APP_ICP_NUM && process.env.APP_ICP_LINK && (
        <footer className="py-4 small">
          <div className="d-flex flex-column align-items-center justify-content-center">
            <p className="mb-2">
              &copy; {year}Y &nbsp;{process.env.APP_NAME}
            </p>
            <a
              href={process.env.APP_ICP_LINK}
              className="text-decoration-none"
              rel="noopener noreferrer"
              target="_blank"
            >
              {process.env.APP_ICP_NUM}
            </a>
          </div>
        </footer>
      )}

      <H5Box>
        <div className="py-4 mb-5"></div>
        <footer className="fixed-bottom py-1 border-top border-1 border-light-subtle card">
          <div className="hstack gap-4 justify-content-between align-items-center user-select-none card-body p-0">
            <div className="vstack align-items-center">
              <i className="bi bi-house-door fs-4"></i>
              <Link href="/" className="small text-decoration-none text-reset">
                {t('navbar.homePage')}
              </Link>
            </div>
            <div className="vstack align-items-center">
              <i className="bi bi-card-text fs-4"></i>
              <Link
                href="/sections"
                className="small text-decoration-none text-reset"
              >
                {t('navbar.content')}
              </Link>
            </div>
            <div className="vstack align-items-center">
              <i className="bi bi-bell fs-4"></i>
              <Link
                href={isLogin ? '/message' : '/login'}
                className="small text-decoration-none text-reset"
              >
                {t('navbar.message')}
              </Link>
            </div>
            <div className="vstack align-items-center">
              <i className="bi bi-person fs-4"></i>
              <Link
                href={isLogin ? `/users/${source.path.user!.id}` : '/login'}
                className="small text-decoration-none text-reset"
              >
                {t('navbar.mine')}
              </Link>
            </div>
          </div>
        </footer>
      </H5Box>
    </>
  );
}
