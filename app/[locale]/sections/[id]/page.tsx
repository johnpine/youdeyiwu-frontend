import SectionIdPage from '@/app/[locale]/sections/[id]/sectionid';
import { authMiddleware } from '@/lib/api';
import { clientQuerySectionDetailsById, queryPath } from '@/services/api';
import { cookies } from 'next/headers';
import type { Metadata as MetadataNext } from 'next';
import type { IPath, ISectionDetails } from '@/interfaces';
import {
  createError,
  filterNumericParams,
  getMetadata,
  isNum,
} from '@/lib/tool';
import { notFound } from 'next/navigation';
import { getTranslator } from 'next-intl/server';
import ResetPage from '@/app/[locale]/reset/reset';
import { getTranslatedFields } from '@/lib/dictionaries';

async function getData(
  id: string,
  searchParams: {
    tagId?: string;
    tagGroupId?: string;
  },
) {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/',
      },
    });
    const req2 = clientQuerySectionDetailsById({
      baseURL: process.env.APP_API_SERVER,
      token,
      id,
      query: searchParams,
    });

    const responses = await Promise.all([req1, req2]);
    const resp1 = await ((await responses[0]) as Response).json();
    const resp2 = await ((await responses[1]) as Response).json();

    return {
      isSuccess: true,
      isError: false,
      data: {
        path: resp1.data as IPath,
        sectionDetails: resp2.data as ISectionDetails,
        queryParams: searchParams,
      },
    };
  } catch (e: any) {
    return {
      isSuccess: false,
      isError: true,
      error: e,
    };
  }
}

export async function generateMetadata({
  params: { id, locale },
  searchParams: { tagId, tid, tagGroupId, tgid, v },
}: {
  params: { id?: string; locale: string };
  searchParams: {
    tagId?: string;
    tid?: string;
    tagGroupId?: string;
    tgid?: string;
    v?: 'h5';
  };
}): Promise<MetadataNext> {
  if (!id || !isNum(id)) {
    notFound();
  }

  const t = await getTranslator(locale);
  const data = await getData(
    id,
    filterNumericParams({
      tagId: tagId ?? tid,
      tagGroupId: tagGroupId ?? tgid,
    }),
  );
  if (data.isError) {
    return getMetadata({ title: t('sectionIdPage.contentDetail') });
  }

  const source = data.data!;
  const basic = source.sectionDetails.basic;
  const tags = source.sectionDetails.tags;

  return getMetadata({
    title: basic.name,
    authors: {
      url: `/users/${basic.createdBy}`,
      name: basic.creatorAlias,
    },
    creator: `${basic.creatorAlias}(ID. ${basic.createdBy})`,
    description: basic.overview
      ? `${basic.overview} - ${
          tags.map((tag) => `#${tag.name}`).join(', ') || ''
        }`
      : '',
  });
}

export default async function Page({
  params: { id, locale },
  searchParams: { tagId, tid, tagGroupId, tgid, v },
}: {
  params: { id?: string; locale: string };
  searchParams: {
    tagId?: string;
    tid?: string;
    tagGroupId?: string;
    tgid?: string;
    v?: 'h5';
  };
}) {
  if (!id || !isNum(id)) {
    notFound();
  }

  const data = await getData(
    id,
    filterNumericParams({
      tagId: tagId ?? tid,
      tagGroupId: tagGroupId ?? tgid,
    }),
  );
  if (data.isError) {
    return <ResetPage error={createError(data.error)} />;
  }

  const source = data.data!;
  const translatedFields = await getTranslatedFields(locale, 'sectionIdPage');

  return <SectionIdPage source={source} translatedFields={translatedFields} />;
}
