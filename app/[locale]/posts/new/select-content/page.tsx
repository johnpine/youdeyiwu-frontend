import { authMiddleware } from '@/lib/api';
import { queryPath, queryPostNewInfo } from '@/services/api';
import { cookies } from 'next/headers';
import ResetPage from '@/app/[locale]/reset/reset';
import type { Metadata as MetadataNext } from 'next';
import { createError, customException, getMetadata } from '@/lib/tool';
import type { IPath, IPostNewInfo } from '@/interfaces';
import SelectContent from '@/app/[locale]/posts/[id]/edit/select-content/select-content';
import { getTranslator } from 'next-intl/server';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('postEditPage.createPost') });
}

async function getData() {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/',
      },
    });
    const req2 = queryPostNewInfo({
      baseURL: process.env.APP_API_SERVER,
      token,
    });

    const responses = await Promise.all([req1, req2]);
    const resp1 = await ((await responses[0]) as Response).json();
    const resp2 = await ((await responses[1]) as Response).json();

    return {
      isSuccess: true,
      isError: false,
      path: resp1.data as IPath,
      info: resp2.data as IPostNewInfo,
    };
  } catch (e: any) {
    return {
      isSuccess: false,
      isError: true,
      error: e,
    };
  }
}

export default async function Page({
  params: { locale },
  searchParams = {},
}: {
  params: { locale: string };
  searchParams: { v?: 'h5' };
}) {
  const data = await getData();
  if (data.isError) {
    return <ResetPage error={createError(data.error)} />;
  }

  const t = await getTranslator(locale);
  const path = data.path!;
  const info = data.info!;

  if (!path.user) {
    return (
      <ResetPage
        error={createError(
          customException(401, t('clientPage.unauthorizedValue')),
        )}
      />
    );
  }

  return (
    <SelectContent
      source={info}
      translatedFields={{
        sectionIdPlaceholder: t(
          'postEditPage.selectContentPage.sectionIdPlaceholder',
        ),
        update: t('postEditPage.selectContentPage.update'),
        updateCompleted: t('postEditPage.selectContentPage.updateCompleted'),
      }}
    />
  );
}
