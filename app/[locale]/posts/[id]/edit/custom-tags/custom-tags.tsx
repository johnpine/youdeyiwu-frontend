'use client';

import type { IPostEditInfo, IPostNewInfo } from '@/interfaces';
import { type ChangeEvent, useContext } from 'react';
import { PostEditContext } from '@/contexts/post-edit';
import useToast from '@/hooks/useToast';
import { useMutation } from '@tanstack/react-query';
import { updatePostEditInfo } from '@/services/api';

export type TCustomTagsTranslatedField =
  | 'update'
  | 'customTagsPlaceholder'
  | 'updateCompleted';

export default function CustomTags({
  source,
  translatedFields = {
    update: '更新',
    customTagsPlaceholder: '请输入自定义标签，多个标签使用逗号分隔开',
    updateCompleted: '更新完成',
  },
}: {
  source: IPostEditInfo | IPostNewInfo;
  translatedFields?: Record<TCustomTagsTranslatedField, any>;
}) {
  const context = useContext(PostEditContext);
  const isEdit = 'basic' in source;
  const { form, setForm } = context;
  const { show } = useToast();

  const updatePostEditInfoMutation = useMutation(updatePostEditInfo);

  async function onClickUpdate() {
    if (!isEdit) {
      show({
        type: 'SUCCESS',
        message: translatedFields.updateCompleted,
      });
      return;
    }

    try {
      const _form: any = {
        customTags: form.customTags,
      };

      await updatePostEditInfoMutation.mutateAsync({
        id: source.basic.id,
        data: _form,
      });
    } catch (e) {
      updatePostEditInfoMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    }
  }

  function onChange(
    e: ChangeEvent<HTMLSelectElement | HTMLInputElement | HTMLTextAreaElement>,
  ) {
    const name = e.target.name;
    const value = e.target.value;

    if (name === 'customTags') {
      setForm({
        ...form,
        customTags: value.replaceAll('，', ',').split(','),
      });
    } else {
      setForm({
        ...form,
        [name]: value,
      });
    }
  }

  return (
    <div className="card border-0">
      <div className="card-body d-flex flex-column gap-4 my-4">
        <input
          name="customTags"
          value={form.customTags.join(',')}
          onChange={onChange}
          className="form-control"
          placeholder={translatedFields.customTagsPlaceholder}
        />

        <div className="d-grid col-2">
          <button
            onClick={onClickUpdate}
            type="button"
            className="btn btn-success"
          >
            {translatedFields.update}
          </button>
        </div>
      </div>
    </div>
  );
}
