'use client';

import Link from 'next/link';
import { type MouseEvent, useContext } from 'react';
import { PostEditContext } from '@/contexts/post-edit';
import { useSelectedLayoutSegment } from 'next/navigation';

export type TEditDropdownItemTranslatedField = 'name';

export default function EditDropdownItem({
  translatedFields = {
    name: 'name',
  },
  name,
  href = '',
  segment,
}: {
  translatedFields?: Record<TEditDropdownItemTranslatedField, any>;
  name: string;
  href?: string;
  segment?: any;
}) {
  const { onNavItemName } = useContext(PostEditContext);
  const selectedLayoutSegment = useSelectedLayoutSegment();

  function onClick(e: MouseEvent<HTMLAnchorElement>) {
    if (!href) {
      e.stopPropagation();
      e.preventDefault();
    }

    onNavItemName(name);
  }

  return (
    <li>
      <Link href={href} onClick={onClick} className="dropdown-item">
        {translatedFields.name}
      </Link>
    </li>
  );
}
