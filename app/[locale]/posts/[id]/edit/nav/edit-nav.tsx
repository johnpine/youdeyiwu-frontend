import EditNavItem from '@/app/[locale]/posts/[id]/edit/nav/edit-nav-item';
import EditDropdownItem from '@/app/[locale]/posts/[id]/edit/nav/edit-dropdown-item';
import { getTranslator } from 'next-intl/server';

export default async function EditNav({
  id,
  locale,
}: {
  id?: string;
  locale: string;
}) {
  const t = await getTranslator(locale);

  return (
    <ul className="nav nav-tabs flex-column flex-sm-row">
      <EditNavItem
        segment={null}
        name="编辑"
        iconName="bi-pencil-square"
        translatedFields={{ name: t('postEditPage.editNavPage.edit') }}
        href={id ? `/posts/${id}/edit` : '/posts/new'}
      />
      <EditNavItem
        segment="draft"
        name="草稿"
        translatedFields={{ name: t('postEditPage.editNavPage.draft') }}
        href={id ? `/posts/${id}/edit/draft` : '/posts/new/draft'}
      />

      {id && (
        <EditNavItem
          segment="history"
          name="历史"
          translatedFields={{ name: t('postEditPage.editNavPage.history') }}
          href={`/posts/${id}/edit/history`}
        />
      )}

      <EditNavItem
        segment="template"
        name="模板"
        translatedFields={{ name: t('postEditPage.editNavPage.template') }}
        href={id ? `/posts/${id}/edit/template` : '/posts/new/template'}
      />
      <EditNavItem
        segment="more"
        name="更多"
        translatedFields={{ name: t('postEditPage.editNavPage.more') }}
      >
        <EditDropdownItem
          segment="select-content"
          name="选择内容"
          translatedFields={{
            name: t('postEditPage.editNavPage.selectContent'),
          }}
          href={
            id
              ? `/posts/${id}/edit/select-content`
              : '/posts/new/select-content'
          }
        />
        <EditDropdownItem
          segment="content-type"
          name="内容类型"
          translatedFields={{ name: t('postEditPage.editNavPage.contentType') }}
          href={
            id ? `/posts/${id}/edit/content-type` : '/posts/new/content-type'
          }
        />
        <EditDropdownItem
          segment="access-setting"
          name="访问设置"
          translatedFields={{
            name: t('postEditPage.editNavPage.accessSetting'),
          }}
          href={
            id
              ? `/posts/${id}/edit/access-setting`
              : '/posts/new/access-setting'
          }
        />
        <EditDropdownItem
          segment="overview"
          name="文章简介"
          translatedFields={{ name: t('postEditPage.editNavPage.overview') }}
          href={id ? `/posts/${id}/edit/overview` : '/posts/new/overview'}
        />
        <EditDropdownItem
          segment="custom-tags"
          name="文章标签"
          translatedFields={{ name: t('postEditPage.editNavPage.customTags') }}
          href={id ? `/posts/${id}/edit/custom-tags` : '/posts/new/custom-tags'}
        />
        <EditDropdownItem
          segment="statement"
          name="文章声明"
          translatedFields={{ name: t('postEditPage.editNavPage.statement') }}
          href={id ? `/posts/${id}/edit/statement` : '/posts/new/statement'}
        />
        <EditDropdownItem
          segment="cover"
          name="上传封面"
          translatedFields={{ name: t('postEditPage.editNavPage.cover') }}
          href={id ? `/posts/${id}/edit/cover` : '/posts/new/cover'}
        />
        <EditDropdownItem
          segment="chapter"
          name="上下章节"
          translatedFields={{ name: t('postEditPage.editNavPage.chapter') }}
          href={id ? `/posts/${id}/edit/chapter` : '/posts/new/chapter'}
        />
      </EditNavItem>
      <EditNavItem
        segment="help"
        name="帮助"
        isShowName={false}
        iconName="bi-question-circle"
        isDisabled={true}
      />
      <EditNavItem
        segment="center"
        name="居中"
        isShowName={false}
        iconName="bi-text-center"
        isCustom
      />

      {id && (
        <EditNavItem
          segment="return"
          id={id}
          name="返回"
          isShowName={false}
          iconName="bi-arrow-return-left"
          isCustom
        />
      )}
    </ul>
  );
}
