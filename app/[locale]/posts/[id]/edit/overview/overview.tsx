'use client';

import type { IPostEditInfo, IPostNewInfo } from '@/interfaces';
import { type ChangeEvent, useContext } from 'react';
import { PostEditContext } from '@/contexts/post-edit';
import useToast from '@/hooks/useToast';
import { useMutation } from '@tanstack/react-query';
import { updatePostEditInfo } from '@/services/api';
import sanitizeHtml from 'sanitize-html';

export type TOverviewTranslatedField =
  | 'update'
  | 'overviewPlaceholder'
  | 'currentWordCount'
  | 'updateCompleted';

export default function Overview({
  source,
  translatedFields = {
    update: '更新',
    overviewPlaceholder: '请输入简介（建议字数控制在 150 个字左右）',
    currentWordCount: '当前字数',
    updateCompleted: '更新完成',
  },
}: {
  source: IPostEditInfo | IPostNewInfo;
  translatedFields?: Record<TOverviewTranslatedField, any>;
}) {
  const context = useContext(PostEditContext);
  const isEdit = 'basic' in source;
  const { form, setForm } = context;
  const { show } = useToast();

  const updatePostEditInfoMutation = useMutation(updatePostEditInfo);

  async function onClickUpdate() {
    if (!isEdit) {
      show({
        type: 'SUCCESS',
        message: translatedFields.updateCompleted,
      });
      return;
    }

    try {
      const _form: any = {
        overview: form.overview,
      };

      if (_form.overview) {
        _form.overview = sanitizeHtml(_form.overview);
      }

      await updatePostEditInfoMutation.mutateAsync({
        id: source.basic.id,
        data: _form,
      });
    } catch (e) {
      updatePostEditInfoMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    }
  }

  function onChange(
    e: ChangeEvent<HTMLSelectElement | HTMLInputElement | HTMLTextAreaElement>,
  ) {
    const name = e.target.name;
    const value = e.target.value;

    setForm({
      ...form,
      [name]: value,
    });
  }

  return (
    <div className="card border-0">
      <div className="card-body d-flex flex-column gap-4 my-4">
        <div>
          <textarea
            name="overview"
            value={form.overview}
            onChange={onChange}
            className="form-control"
            rows={3}
            placeholder={translatedFields.overviewPlaceholder}
          />

          {form.overview.length > 0 && (
            <div className="form-text">{`${translatedFields.currentWordCount} ${form.overview.length}`}</div>
          )}
        </div>

        <div className="d-grid col-2">
          <button
            onClick={onClickUpdate}
            type="button"
            className="btn btn-success"
          >
            {translatedFields.update}
          </button>
        </div>
      </div>
    </div>
  );
}
