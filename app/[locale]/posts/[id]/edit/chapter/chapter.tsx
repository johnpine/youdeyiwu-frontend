'use client';

import type { IPostEditInfo, IPostNewInfo } from '@/interfaces';
import { type ChangeEvent, useContext } from 'react';
import { PostEditContext } from '@/contexts/post-edit';
import useToast from '@/hooks/useToast';
import { useMutation } from '@tanstack/react-query';
import { updatePostEditInfo } from '@/services/api';

export type TChapterTranslatedField =
  | 'update'
  | 'prevNamePlaceholder'
  | 'prevLinkPlaceholder'
  | 'nextNamePlaceholder'
  | 'nextLinkPlaceholder'
  | 'updateCompleted';

export default function Chapter({
  source,
  translatedFields = {
    update: '更新',
    prevNamePlaceholder: '请输入上一个导航名称',
    prevLinkPlaceholder: '请输入上一个导航链接',
    nextNamePlaceholder: '请输入下一个导航名称',
    nextLinkPlaceholder: '请输入下一个导航链接',
    updateCompleted: '更新完成',
  },
}: {
  source: IPostEditInfo | IPostNewInfo;
  translatedFields?: Record<TChapterTranslatedField, any>;
}) {
  const context = useContext(PostEditContext);
  const { form, setForm } = context;
  const isEdit = 'basic' in source;
  const { show } = useToast();

  const updatePostEditInfoMutation = useMutation(updatePostEditInfo);

  async function onClickUpdate() {
    if (!isEdit) {
      show({
        type: 'SUCCESS',
        message: translatedFields.updateCompleted,
      });
      return;
    }

    try {
      const _form: any = {
        paginationNavLink: form.paginationNavLink,
      };

      await updatePostEditInfoMutation.mutateAsync({
        id: source.basic.id,
        data: _form,
      });
    } catch (e) {
      updatePostEditInfoMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    }
  }

  function onChange(
    e: ChangeEvent<HTMLSelectElement | HTMLInputElement | HTMLTextAreaElement>,
  ) {
    const name = e.target.name;
    const value = e.target.value;

    setForm({
      ...form,
      paginationNavLink: {
        ...form.paginationNavLink,
        [name]: value,
      },
    });
  }

  return (
    <div className="card border-0">
      <div className="card-body d-flex flex-column gap-4 my-4">
        <div className="input-group">
          <input
            name="prevName"
            value={form.paginationNavLink.prevName}
            onChange={onChange}
            className="form-control"
            placeholder={translatedFields['prevNamePlaceholder']}
          />
          <textarea
            name="prevLink"
            value={form.paginationNavLink.prevLink}
            onChange={onChange}
            className="form-control"
            rows={1}
            placeholder={translatedFields['prevLinkPlaceholder']}
          />
        </div>
        <div className="input-group">
          <input
            name="nextName"
            value={form.paginationNavLink.nextName}
            onChange={onChange}
            className="form-control"
            placeholder={translatedFields['nextNamePlaceholder']}
          />
          <textarea
            name="nextLink"
            value={form.paginationNavLink.nextLink}
            onChange={onChange}
            className="form-control"
            rows={1}
            placeholder={translatedFields['nextLinkPlaceholder']}
          />
        </div>

        <div className="d-grid col-2">
          <button
            onClick={onClickUpdate}
            type="button"
            className="btn btn-success"
          >
            {translatedFields.update}
          </button>
        </div>
      </div>
    </div>
  );
}
