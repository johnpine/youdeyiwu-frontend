export default function EditorSuspense({
  message = 'Loading...',
}: {
  message?: string;
}) {
  return (
    <div className="d-flex align-items-center text-secondary">
      <div className="text-secondary">{message}</div>
      <div
        className="spinner-border ms-auto"
        role="status"
        aria-hidden="true"
      ></div>
    </div>
  );
}
