import { type ReactNode } from 'react';
import { authMiddleware } from '@/lib/api';
import { cookies } from 'next/headers';
import { queryPath, queryPostEditInfo } from '@/services/api';
import type { IPath, IPostEditInfo } from '@/interfaces';
import { notFound } from 'next/navigation';
import { isNum } from '@/lib/tool';
import EditNav from '@/app/[locale]/posts/[id]/edit/nav/edit-nav';
import Wrapper from '@/app/[locale]/posts/[id]/edit/wrapper';

async function getData(id: string) {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/',
      },
    });
    const req2 = queryPostEditInfo({
      baseURL: process.env.APP_API_SERVER,
      token,
      id,
    });

    const responses = await Promise.all([req1, req2]);
    const resp1 = await ((await responses[0]) as Response).json();
    const resp2 = await ((await responses[1]) as Response).json();

    return {
      isSuccess: true,
      path: resp1.data as IPath,
      info: resp2.data as IPostEditInfo,
    };
  } catch (e) {
    return {
      isSuccess: false,
    };
  }
}

export default async function DashboardLayout({
  children,
  params: { locale, id },
}: {
  children: ReactNode;
  params: { locale: string; id?: string };
}) {
  if (!id || !isNum(id)) {
    notFound();
  }

  const data = await getData(id);

  if (data.isSuccess) {
    return (
      <Wrapper data={data.info!}>
        <EditNav id={id} locale={locale} />
        {children}
      </Wrapper>
    );
  }

  return <>{children}</>;
}
