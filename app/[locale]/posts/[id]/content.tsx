import Image from 'next/image';
import Link from 'next/link';
import { getUserAvatar } from '@/lib/tool';
import ButtonAreaPostIdPage from '@/app/[locale]/posts/[id]/buttonArea';
import Outline from '@/app/[locale]/common/post/outline';
import ContentHtml from '@/app/[locale]/common/content/html';
import PaginationNavLink from '@/app/[locale]/posts/[id]/paginationNavLink';
import { useContext } from 'react';
import { PostIdPageContext } from '@/contexts/post-id';
import { AppContext } from '@/contexts/app';

export default function Content() {
  const {
    source: { postDetails },
    translatedFields,
  } = useContext(PostIdPageContext)!;
  const appContext = useContext(AppContext);
  const metadata = appContext.metadata!;
  const env = metadata.env;

  return (
    <div className="row mx-0">
      <div className="col-12 col-lg-2 px-0">
        <div
          className="card h-100 border-0 rounded-0 vstack gap-2"
          style={{
            borderBottomLeftRadius: '0.376rem !important',
          }}
        >
          <div
            className="align-self-center my-4"
            style={{ width: 128, height: 128 }}
          >
            <Image
              className="rounded-circle card-img-top object-fit-contain"
              src={getUserAvatar(postDetails.user, metadata).mediumAvatarUrl}
              alt={postDetails.user.alias}
              width={128}
              height={128}
              placeholder="blur"
              blurDataURL={env.APP_BLUR_DATA_URL}
            />
          </div>
          <div className="card-body align-self-center text-center">
            <h5 className="card-title">
              <Link
                href={`/users/${postDetails.user.id}`}
                className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
              >
                {postDetails.user.alias}
              </Link>
            </h5>

            <p className="card-text my-3">
              {postDetails.user.roles
                .filter((item) => !item.hide)
                .map((item) => item.name)
                .join(' / ')}
            </p>
          </div>
          <div className="card-footer border-top-0 border-bottom border-secondary border-opacity-10">
            <small className="text-secondary">
              {translatedFields.lastUpdated}&nbsp;
              <time dateTime={postDetails.basic.contentUpdatedOn}>
                {postDetails.basic._contentUpdatedOnText}
              </time>
            </small>
          </div>
        </div>
      </div>
      <div className="col col-lg-10 px-0">
        <div
          className="card border-0 border-secondary border-opacity-10 rounded-0 h-100 border-bottom"
          style={{
            borderBottomRightRadius: '0.376rem !important',
          }}
        >
          <div className="card-body pb-0">
            {postDetails.basic.contentType === 'DEFAULT' && (
              <>
                <Outline content={postDetails.content} />
                <ContentHtml
                  content={postDetails.content}
                  metadata={metadata}
                />
              </>
            )}

            <ButtonAreaPostIdPage
              id={postDetails.basic.id}
              name={postDetails.basic.name}
              like={postDetails.isLike}
              likeNum={postDetails.details.likeCount}
              follow={postDetails.isFollow}
              followNum={postDetails.details.followCount}
              favourite={postDetails.isFavourite}
              favouriteNum={postDetails.details.favoriteCount}
            />

            {postDetails.basic.statement && (
              <div className="my-5">
                <p className="text-secondary">{postDetails.basic.statement}</p>
              </div>
            )}

            <PaginationNavLink
              source={postDetails.basic.paginationNavLink}
              translatedFields={{
                prevName: translatedFields.previousArticle,
                nextName: translatedFields.nextArticle,
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
