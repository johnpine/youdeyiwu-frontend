import type { IPostPaginationNavLink } from '@/interfaces';
import Link from 'next/link';

export type TPaginationNavLinkTranslatedField = 'prevName' | 'nextName';

export default function PaginationNavLink({
  source,
  translatedFields = {
    prevName: '上一篇',
    nextName: '下一篇',
  },
}: {
  source?: IPostPaginationNavLink;
  translatedFields?: Record<TPaginationNavLinkTranslatedField, any>;
}) {
  if (!source) {
    return <></>;
  }

  return (
    <div className="row">
      {source.prevName && source.prevLink && (
        <div className="col">
          <div className="small">{translatedFields.prevName}</div>
          <Link
            href={source.prevLink}
            className="link-body-emphasis link-offset-3 link-offset-3-hover link-underline link-underline-opacity-0 link-underline-opacity-100-hover"
          >
            <div className="d-flex gap-2">
              <i className="bi bi-chevron-double-left"></i>
              <span>{source.prevName}</span>
            </div>
          </Link>
        </div>
      )}
      {source.nextName && source.nextLink && (
        <div className="col d-flex flex-column align-items-end">
          <div className="small">{translatedFields.nextName}</div>
          <Link
            href={source.nextLink}
            className="link-body-emphasis link-offset-3 link-offset-3-hover link-underline link-underline-opacity-0 link-underline-opacity-100-hover"
          >
            <div className="d-flex gap-2">
              <span>{source.nextName}</span>
              <i className="bi bi-chevron-double-right"></i>
            </div>
          </Link>
        </div>
      )}
    </div>
  );
}
