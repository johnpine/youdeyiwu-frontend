import classNames from 'classnames';

export default function Spinner({
  classs,
  sm = true,
}: {
  classs?: string;
  sm?: boolean;
}) {
  return (
    <span
      className={classNames(
        'spinner-border',
        {
          'spinner-border-sm': sm,
        },
        classs,
      )}
      aria-hidden="true"
    ></span>
  );
}
