import BtnBoxUserIdH5Page from '@/app/[locale]/mobile/users/[id]/btnBox';
import BtnUserIdH5Page from '@/app/[locale]/mobile/users/[id]/btn';
import PostUserIdH5Page from '@/app/[locale]/mobile/users/[id]/post';
import StatisticsUserIdH5Page from '@/app/[locale]/mobile/users/[id]/statistics';
import AboutMeUserIdH5Page from '@/app/[locale]/mobile/users/[id]/aboutMe';
import ContactUserIdH5Page from '@/app/[locale]/mobile/users/[id]/contact';
import NameUserIdH5Page from '@/app/[locale]/mobile/users/[id]/name';
import { useContext } from 'react';
import { UserIdPageContext } from '@/contexts/user-id';
import H5Box from '@/app/[locale]/common/other/h5';
import useToast from '@/hooks/useToast';
import FavouritesUserIdH5Page from '@/app/[locale]/mobile/users/[id]/favourites';

export default function UserIdH5Page(this: any) {
  const { source, translatedFields } = useContext(UserIdPageContext)!;
  const user = source.userDetails.user;
  const { show } = useToast();

  function emptyPrompt(prompt: string = '没有内容') {
    show({
      message: prompt,
    });
  }

  return (
    <H5Box classs="col px-2 py-4">
      <div className="card rounded-5 border-0">
        <div className="card-body">
          <div className="vstack gap-3">
            <NameUserIdH5Page />

            <div></div>

            {!source.path.user && (
              <BtnBoxUserIdH5Page>
                <BtnUserIdH5Page
                  path="/login"
                  iName="person"
                  name={translatedFields.loginPrompt}
                />
                <BtnUserIdH5Page
                  path="/register"
                  iName="person-add"
                  name={translatedFields.quickRegistration}
                />
              </BtnBoxUserIdH5Page>
            )}

            {source.path.user && (
              <BtnBoxUserIdH5Page>
                <BtnUserIdH5Page
                  content={<FavouritesUserIdH5Page />}
                  iName="star"
                  name={translatedFields.myFavorites}
                />
              </BtnBoxUserIdH5Page>
            )}

            <BtnBoxUserIdH5Page>
              {user ? (
                <BtnUserIdH5Page
                  content={<PostUserIdH5Page type="post" />}
                  name={translatedFields.relPosts}
                />
              ) : (
                <BtnUserIdH5Page
                  fn={emptyPrompt.bind(this, translatedFields.noInformation)}
                  name={translatedFields.relPosts}
                />
              )}

              {user ? (
                <BtnUserIdH5Page
                  content={<PostUserIdH5Page type="section" />}
                  name={translatedFields.relContent}
                />
              ) : (
                <BtnUserIdH5Page
                  fn={emptyPrompt.bind(this, translatedFields.noInformation)}
                  name={translatedFields.relContent}
                />
              )}

              {user ? (
                <BtnUserIdH5Page
                  content={<PostUserIdH5Page type="tag" />}
                  name={translatedFields.relTags}
                />
              ) : (
                <BtnUserIdH5Page
                  fn={emptyPrompt.bind(this, translatedFields.noInformation)}
                  name={translatedFields.relTags}
                />
              )}

              {user ? (
                <BtnUserIdH5Page
                  name={translatedFields.relStatistics}
                  content={<StatisticsUserIdH5Page user={user} />}
                />
              ) : (
                <BtnUserIdH5Page
                  fn={emptyPrompt.bind(this, translatedFields.noInformation)}
                  name={translatedFields.relStatistics}
                />
              )}
            </BtnBoxUserIdH5Page>

            <BtnBoxUserIdH5Page>
              <BtnUserIdH5Page
                name={translatedFields.aboutMe}
                content={<AboutMeUserIdH5Page />}
              />
              <BtnUserIdH5Page
                iName="person-lines-fill"
                name={translatedFields.contactInformation}
                content={<ContactUserIdH5Page />}
              />
              <BtnUserIdH5Page path="/terms" name={translatedFields.terms} />
              <BtnUserIdH5Page
                path="/privacy-policy"
                name={translatedFields.privacyPolicy}
              />
            </BtnBoxUserIdH5Page>

            <div></div>
          </div>
        </div>
      </div>
    </H5Box>
  );
}
