'use client';

import type { IPath } from '@/interfaces';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import Nodata from '@/app/[locale]/common/nodata/nodata';

export default function PrivacyPolicyPage({
  source,
  translatedFields,
}: {
  source: { path: IPath };
  translatedFields: PrefixedTTranslatedFields<'privacyPolicyPage'>;
}) {
  return (
    <div className="col px-2 py-4">
      <div className="container-fluid">
        <Nodata />
      </div>
    </div>
  );
}
