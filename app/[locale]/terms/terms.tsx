'use client';

import type { IPath } from '@/interfaces';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import Nodata from '@/app/[locale]/common/nodata/nodata';

export default function TermsPage({
  source,
  translatedFields,
}: {
  source: { path: IPath };
  translatedFields: PrefixedTTranslatedFields<'termsPage'>;
}) {
  return (
    <div className="col px-2 py-4">
      <div className="container-fluid">
        <Nodata />
      </div>
    </div>
  );
}
