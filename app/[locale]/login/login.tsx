'use client';

import Image from 'next/image';
import { useContext, useState } from 'react';
import Link from 'next/link';
import UsernameLogin from '@/app/[locale]/login/username';
import PhoneLogin from '@/app/[locale]/login/phone';
import EmailLogin from '@/app/[locale]/login/email';
import QqLogin from '@/app/[locale]/login/qq';
import type { ILoginPageContext } from '@/contexts/login';
import { LoginPageContext } from '@/contexts/login';
import { AppContext } from '@/contexts/app';
import PcBox from '@/app/[locale]/common/other/pc';
import LoginH5Page from '@/app/[locale]/login/h5';

export default function LoginPage(
  this: any,
  { source, translatedFields }: ILoginPageContext,
) {
  const appContext = useContext(AppContext);
  const metadata = appContext.metadata!;
  const env = metadata.env;
  const loginType = source.queryParams.type;
  const [switchingLoginMethod, setSwitchingLoginMethod] = useState({
    username:
      loginType === 'username' &&
      !source.path.phoneConfig?.enable &&
      !source.path.emailConfig?.enable &&
      !source.path.qqConfig?.enable,
    phone: loginType === 'phone' && source.path.phoneConfig?.enable,
    email: loginType === 'email' && source.path.emailConfig?.enable,
    qq: loginType === 'qq' && source.path.qqConfig?.enable,
  });

  function onClickLogin(name: string) {
    const obj = { ...switchingLoginMethod } as any;
    for (const objKey in obj) {
      obj[objKey] = false;
    }
    setSwitchingLoginMethod({ ...obj, [name]: true });
  }

  return (
    <LoginPageContext.Provider value={{ source, translatedFields }}>
      <PcBox classs="col py-5 my-5">
        <div className="container">
          <div className="row">
            <div className="col pe-0">
              <div className="card rounded-start-4 border-0 h-100">
                <div className="card-body ps-4 py-5 my-5 animate__animated animate__fadeInLeft">
                  <div className="mb-5 hstack gap-2 justify-content-end">
                    <div className="cursor-default">
                      {translatedFields.noAccountYet}
                    </div>
                    <div>
                      <Link className="btn btn-light" href="/register">
                        {translatedFields.startRegistration}
                      </Link>
                    </div>
                  </div>

                  <div className="fs-4 text-center fw-bold">
                    {translatedFields.loginAccount}
                  </div>

                  <div className="my-5 vstack gap-4 px-5">
                    {(source.path.phoneConfig?.enable ||
                      source.path.emailConfig?.enable ||
                      source.path.qqConfig?.enable ||
                      source.path.clientConfig?.ywClientUrls.find(
                        (item) => item.enable && item.url,
                      )) && (
                      <>
                        {!switchingLoginMethod.username && (
                          <button
                            onClick={onClickLogin.bind(this, 'username')}
                            type="button"
                            className="btn btn-light"
                          >
                            <div className="row">
                              <div className="col text-center align-self-center position-relative">
                                <div className="position-absolute top-50 start-0 translate-middle-y ms-5">
                                  <i className="bi bi-person fs-4"></i>
                                </div>
                                <div>{translatedFields.accountLogin}</div>
                              </div>
                            </div>
                          </button>
                        )}

                        {!switchingLoginMethod.phone &&
                          source.path.phoneConfig?.enable && (
                            <button
                              onClick={onClickLogin.bind(this, 'phone')}
                              type="button"
                              className="btn btn-light"
                            >
                              <div className="row">
                                <div className="col text-center align-self-center position-relative">
                                  <div className="position-absolute top-50 start-0 translate-middle-y ms-5">
                                    <i className="bi bi-phone fs-4"></i>
                                  </div>
                                  <div>{translatedFields.smsLogin}</div>
                                </div>
                              </div>
                            </button>
                          )}

                        {!switchingLoginMethod.email &&
                          source.path.emailConfig?.enable && (
                            <button
                              onClick={onClickLogin.bind(this, 'email')}
                              type="button"
                              className="btn btn-light"
                            >
                              <div className="row">
                                <div className="col text-center align-self-center position-relative">
                                  <div className="position-absolute top-50 start-0 translate-middle-y ms-5">
                                    <i className="bi bi-envelope fs-4"></i>
                                  </div>
                                  <div>{translatedFields.emailLogin}</div>
                                </div>
                              </div>
                            </button>
                          )}

                        {(source.path.clientConfig?.ywClientUrls || [])
                          .filter((item) => item.enable && item.url)
                          .map((item) => {
                            return (
                              <Link
                                key={item.clientId}
                                href={item.url}
                                className="btn btn-light"
                              >
                                <div className="row">
                                  <div className="col text-center align-self-center position-relative">
                                    <div className="position-absolute top-50 start-0 translate-middle-y ms-5">
                                      <Image
                                        className="object-fit-contain"
                                        src={item.clientLogo}
                                        alt={item.clientName}
                                        width={24}
                                        height={24}
                                        placeholder="blur"
                                        blurDataURL={env.APP_BLUR_DATA_URL}
                                      />
                                    </div>
                                    <div>
                                      {item.clientName + translatedFields.login}
                                    </div>
                                  </div>
                                </div>
                              </Link>
                            );
                          })}

                        {!switchingLoginMethod.qq &&
                          source.path.qqConfig?.enable && (
                            <button
                              onClick={onClickLogin.bind(this, 'qq')}
                              type="button"
                              className="btn btn-light"
                            >
                              <div className="row">
                                <div className="col text-center align-self-center position-relative">
                                  <div className="position-absolute top-50 start-0 translate-middle-y ms-5">
                                    <i className="bi bi-tencent-qq fs-4"></i>
                                  </div>
                                  <div>{translatedFields.thirdPartyLogin}</div>
                                </div>
                              </div>
                            </button>
                          )}
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-auto px-0">
              <div className="card border-0 h-100">
                <div className="card-body d-flex align-items-center position-relative py-0">
                  <div className="d-flex position-absolute start-0 end-0 top-0 h-100 py-5 justify-content-center">
                    <div className="vr text-secondary text-opacity-75"></div>
                  </div>
                  <div className="bg-body z-1 border border-secondary border-opacity-25 rounded-3 text-secondary px-2 py-3">
                    OR
                  </div>
                </div>
              </div>
            </div>
            <div className="col ps-0">
              <div className="card rounded-end-4 border-0 h-100">
                <div className="card-body py-5 pe-4 vstack gap-4 my-5 animate__animated animate__fadeInRight">
                  {switchingLoginMethod.username && <UsernameLogin />}

                  {switchingLoginMethod.phone &&
                    source.path.phoneConfig?.enable && <PhoneLogin />}

                  {switchingLoginMethod.email &&
                    source.path.emailConfig?.enable && <EmailLogin />}

                  {switchingLoginMethod.qq && source.path.qqConfig?.enable && (
                    <QqLogin />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </PcBox>

      <LoginH5Page />
    </LoginPageContext.Provider>
  );
}
