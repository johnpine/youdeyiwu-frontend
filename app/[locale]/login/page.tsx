import { authMiddleware } from '@/lib/api';
import { cookies } from 'next/headers';
import { queryPath } from '@/services/api';
import LoginPage from '@/app/[locale]/login/login';
import ResetPage from '@/app/[locale]/reset/reset';
import type { Metadata as MetadataNext } from 'next';
import { createError, getMetadata } from '@/lib/tool';
import { getTranslator } from 'next-intl/server';
import type { IPath } from '@/interfaces';
import { getTranslatedFields } from '@/lib/dictionaries';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('loginPage.userLogin') });
}

async function getData(searchParams: {
  type: 'username' | 'email' | 'phone' | 'qq';
}) {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/login',
      },
    });

    const responses = await Promise.all([req1]);
    const resp1 = await ((await responses[0]) as Response).json();

    return {
      isSuccess: true,
      isError: false,
      data: {
        path: resp1.data as IPath,
        queryParams: searchParams,
      },
    };
  } catch (e: any) {
    return {
      isSuccess: false,
      isError: true,
      error: e,
    };
  }
}

export default async function Page({
  params: { locale },
  searchParams: { v, type },
}: {
  params: { locale: string };
  searchParams: {
    v?: 'h5';
    type?: 'username' | 'email' | 'phone' | 'qq';
  };
}) {
  const data = await getData({
    type: type ?? 'username',
  });
  if (data.isError) {
    return <ResetPage error={createError(data.error)} />;
  }

  const source = data.data!;
  const translatedFields = await getTranslatedFields(locale, 'loginPage');

  return <LoginPage source={source} translatedFields={translatedFields} />;
}
