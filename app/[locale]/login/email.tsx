import type { KeyboardEvent } from 'react';
import {
  type ChangeEvent,
  type FormEvent,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import type JSEncrypt from 'jsencrypt';
import useToast from '@/hooks/useToast';
import { useMutation } from '@tanstack/react-query';
import {
  generateCaptchaByEmail,
  loginByEmail,
  queryAccountPublicKey,
} from '@/services/api';
import dayjs from 'dayjs';
import { useSearchParams } from 'next/navigation';
import { LoginPageContext } from '@/contexts/login';

export default function EmailLogin() {
  const {
    source: { path: data },
    translatedFields,
  } = useContext(LoginPageContext)!;
  const [form, setForm] = useState({
    email: '',
    captcha: '',
  });
  const [emailCaptchaConfig, setEmailCaptchaConfig] = useState({
    ...data.phoneConfig,
    isClick: false,
    countdown: 0,
    countdownId: undefined,
  });
  const [disableLogin, setDisableLogin] = useState(false);
  const captchaInputRef = useRef<HTMLInputElement>(null);
  const jsEncryptRef = useRef<JSEncrypt>();
  const urlSearchParams = useSearchParams();
  const { show } = useToast();

  useEffect(() => {
    const countdownId = emailCaptchaConfig.countdownId;
    return () => {
      if (countdownId) {
        clearInterval(countdownId);
      }
    };
  }, [emailCaptchaConfig.countdownId]);

  const loginByEmailMutation = useMutation(loginByEmail);
  const queryAccountPublicKeyMutation = useMutation(queryAccountPublicKey);
  const generateCaptchaByEmailMutation = useMutation(generateCaptchaByEmail);

  function onChangeForm(e: ChangeEvent<HTMLInputElement>) {
    const name = e.target.name;
    const value = e.target.value;
    setForm({ ...form, [name]: value.trim() });
  }

  async function onSubmit(
    e: FormEvent<HTMLFormElement> | KeyboardEvent<HTMLInputElement>,
  ) {
    try {
      e.preventDefault();
      e.stopPropagation();

      const { email, captcha } = form;
      const emailConfig = data.emailConfig;
      if (!email) {
        show({
          type: 'DANGER',
          message: translatedFields.enterValidEmail,
        });
        return;
      } else if (emailConfig && emailConfig.enable && !captcha) {
        show({
          type: 'DANGER',
          message: translatedFields.enterValidCaptcha,
        });
        return;
      }

      if (!email.includes('@') || !email.includes('.')) {
        show({
          type: 'DANGER',
          message: translatedFields.invalidEmailFormat,
        });
        return;
      }

      const body = await assemblyData();
      if (!Object.keys(body).length) {
        show({
          type: 'DANGER',
          message: translatedFields.loginFailed,
        });
        return;
      }
      await loginByEmailMutation.mutateAsync({ data: body });
      setDisableLogin(true);

      show({
        message: translatedFields.loginCompleted,
        type: 'SUCCESS',
      });

      setTimeout(() => {
        show({
          message: translatedFields.redirecting,
          type: 'PRIMARY',
        });
      }, 1000);

      setTimeout(() => {
        const responseType =
          urlSearchParams?.get('response_type') ??
          urlSearchParams?.get('responseType') ??
          urlSearchParams?.get('response-type');
        const clientId =
          urlSearchParams?.get('client_id') ??
          urlSearchParams?.get('clientId') ??
          urlSearchParams?.get('client-id');
        const redirectUri =
          urlSearchParams?.get('redirect_uri') ??
          urlSearchParams?.get('redirectUri') ??
          urlSearchParams?.get('redirect-uri');
        const scope =
          urlSearchParams?.get('response_type') ??
          urlSearchParams?.get('responseType') ??
          urlSearchParams?.get('response-type');
        const state = urlSearchParams?.get('state');
        const params = {
          responseType,
          clientId,
          redirectUri,
          scope,
        } as any;

        if (state) {
          params.state = state;
        }

        if (responseType && clientId && redirectUri && scope) {
          location.href = `/oauth/auth?${new URLSearchParams(params)}`;
        } else {
          location.href = '/';
        }
      }, 1500);
    } catch (e) {
      loginByEmailMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    }
  }

  async function onKeyDownCaptcha(e: KeyboardEvent<HTMLInputElement>) {
    e.preventDefault();
    e.stopPropagation();

    if (e.key === 'Enter' && e.code === 'Enter') {
      const emailConfig = data.emailConfig;
      if (emailConfig && emailConfig.enable && !form.captcha) {
        await onClickRefreshCaptcha();
      } else {
        await onSubmit(e);
      }
    }
  }

  async function assemblyData() {
    const { email, captcha } = form;
    const body = {} as any;
    if (data.emailConfig && data.emailConfig.enable) {
      body.code = captcha;
    }
    const eEmail = await getEncryptedEmail(email);
    if (!eEmail) {
      show({
        type: 'DANGER',
        message: translatedFields.loginFailed,
      });
      return;
    }
    body.email = eEmail;
    return body;
  }

  async function onClickRefreshCaptcha() {
    try {
      const { email } = form;
      if (!email) {
        show({
          type: 'DANGER',
          message: translatedFields.enterEmailToSend,
        });
        return;
      }

      if (!email.includes('@') || !email.includes('.')) {
        show({
          type: 'DANGER',
          message: translatedFields.enterValidEmail,
        });
        return;
      }

      if (emailCaptchaConfig.isClick) {
        if (emailCaptchaConfig.countdown === 0) {
          return;
        }

        show({
          message: `${emailCaptchaConfig.countdown} ${translatedFields.secondsToRetry}`,
          type: 'PRIMARY',
        });
        return;
      }

      if (emailCaptchaConfig.total && emailCaptchaConfig.total < 1) {
        show({
          type: 'DANGER',
          message: translatedFields.captchaLimitExceeded,
        });
        return;
      }

      const eEmail = await getEncryptedEmail(email);
      if (!eEmail) {
        show({
          type: 'DANGER',
          message: translatedFields.encryptEmailFailed,
        });
        return;
      }

      await generateCaptchaByEmailMutation.mutateAsync({
        data: {
          email: eEmail,
          contentType: 'LOGIN',
        },
      });

      if (emailCaptchaConfig.interval) {
        let seconds = dayjs.duration(emailCaptchaConfig.interval).asSeconds();
        const countdownId = setInterval(() => {
          if (
            seconds === 0 &&
            typeof emailCaptchaConfig.total !== 'undefined'
          ) {
            clearInterval(countdownId);
            setEmailCaptchaConfig({
              ...emailCaptchaConfig,
              isClick: false,
              total: emailCaptchaConfig.total - 1,
              countdownId: undefined,
            });
          } else {
            seconds -= 1;
            setEmailCaptchaConfig({
              ...emailCaptchaConfig,
              countdown: seconds,
              isClick: true,
            });
          }
        }, 1000);
      }

      captchaInputRef.current?.focus();
      show({
        type: 'PRIMARY',
        message: translatedFields.sendCaptchaCompleted,
      });
    } catch (e) {
      generateCaptchaByEmailMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    }
  }

  async function getEncryptedEmail(email: string) {
    try {
      const publicKey = (await queryAccountPublicKeyMutation.mutateAsync(
        {},
      )) as string;
      const jsEncrypt = await getJsEncrypt();
      jsEncrypt.setPublicKey(publicKey);
      return jsEncrypt.encrypt(email);
    } catch (e) {
      queryAccountPublicKeyMutation.reset();
      show({
        type: 'DANGER',
        message: translatedFields.encryptEmailFailed,
      });
    }
  }

  async function getJsEncrypt(): Promise<JSEncrypt> {
    let jsEncrypt;
    if (jsEncryptRef.current) {
      jsEncrypt = jsEncryptRef.current;
    } else {
      const JSEncrypt = (await import('jsencrypt')).JSEncrypt;
      jsEncrypt = new JSEncrypt();
      jsEncryptRef.current = jsEncrypt;
    }
    return jsEncrypt;
  }

  return (
    <form onSubmit={onSubmit}>
      <div className="my-3">
        <label className="form-label">
          <span className="text-danger fw-bold">*</span>
          {translatedFields.email}
        </label>
        <input
          required
          pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
          value={form.email}
          onChange={onChangeForm}
          name="email"
          type="email"
          className="form-control"
          placeholder={translatedFields.enterEmail}
          aria-describedby={translatedFields.enterEmail}
        />
      </div>

      {data.emailConfig && data.emailConfig.enable && (
        <div className="my-3">
          <label htmlFor="phoneCaptcha" className="form-label">
            <span className="text-danger fw-bold">*</span>
            {translatedFields.captcha}
          </label>
          <div className="d-flex justify-content-between align-items-center">
            <div className="flex-grow-1 me-3">
              <input
                required
                onKeyDown={onKeyDownCaptcha}
                value={form.captcha}
                onChange={onChangeForm}
                name="captcha"
                type="text"
                placeholder={translatedFields.enterCaptcha}
                className="form-control"
              />
            </div>
            <div className="flex-shrink-0">
              <button
                disabled={
                  emailCaptchaConfig.countdown !== 0 ||
                  generateCaptchaByEmailMutation.isLoading
                }
                onClick={onClickRefreshCaptcha}
                type="button"
                className="btn btn-secondary"
              >
                {generateCaptchaByEmailMutation.isLoading && (
                  <span
                    className="spinner-border spinner-border-sm me-2"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                {translatedFields.sendCaptcha}
                {emailCaptchaConfig.countdown !== 0 &&
                  `(${emailCaptchaConfig.countdown})`}
              </button>
            </div>
          </div>
        </div>
      )}

      <button
        disabled={loginByEmailMutation.isLoading || disableLogin}
        type="submit"
        className="btn btn-outline-primary mt-4 mb-3 w-100"
      >
        {loginByEmailMutation.isLoading && (
          <span
            className="spinner-border spinner-border-sm me-2"
            role="status"
            aria-hidden="true"
          ></span>
        )}
        {disableLogin ? (
          <span>{translatedFields.loginCompleted}</span>
        ) : (
          <span>{translatedFields.loginPrompt}</span>
        )}
      </button>
    </form>
  );
}
