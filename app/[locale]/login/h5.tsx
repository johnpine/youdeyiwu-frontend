import Image from 'next/image';
import { useContext, useState } from 'react';
import Link from 'next/link';
import UsernameLogin from '@/app/[locale]/login/username';
import PhoneLogin from '@/app/[locale]/login/phone';
import EmailLogin from '@/app/[locale]/login/email';
import QqLogin from '@/app/[locale]/login/qq';
import { AppContext } from '@/contexts/app';
import { LoginPageContext } from '@/contexts/login';
import H5Box from '@/app/[locale]/common/other/h5';

export default function LoginH5Page(this: any) {
  const {
    source: { path: data, queryParams },
    translatedFields,
  } = useContext(LoginPageContext)!;
  const appContext = useContext(AppContext);
  const metadata = appContext.metadata!;
  const env = metadata.env;
  const loginType = queryParams.type;
  const [switchingLoginMethod, setSwitchingLoginMethod] = useState({
    username:
      loginType === 'username' &&
      !data.phoneConfig?.enable &&
      !data.emailConfig?.enable &&
      !data.qqConfig?.enable,
    phone: loginType === 'phone' && data.phoneConfig?.enable,
    email: loginType === 'email' && data.emailConfig?.enable,
    qq: loginType === 'qq' && data.qqConfig?.enable,
  });

  function onClickLogin(name: string) {
    const obj = { ...switchingLoginMethod } as any;
    for (const objKey in obj) {
      obj[objKey] = false;
    }
    setSwitchingLoginMethod({ ...obj, [name]: true });
  }

  return (
    <H5Box classs="col px-2 py-4">
      <div className="card rounded-5 border-0">
        <div className="card-body">
          <div className="hstack gap-2 my-5 justify-content-center">
            <div className="cursor-default">
              {translatedFields.noAccountYet}
            </div>
            <div>
              <Link className="btn btn-light" href="/register">
                {translatedFields.startRegistration}
              </Link>
            </div>
          </div>
          <div className="align-self-center vstack gap-4">
            <div className="fs-4 text-center fw-bold">
              {translatedFields.loginAccount}
            </div>

            {switchingLoginMethod.username && <UsernameLogin />}

            {switchingLoginMethod.phone && data.phoneConfig?.enable && (
              <PhoneLogin />
            )}

            {switchingLoginMethod.email && data.emailConfig?.enable && (
              <EmailLogin />
            )}

            {switchingLoginMethod.qq && data.qqConfig?.enable && <QqLogin />}

            {(data.phoneConfig?.enable ||
              data.emailConfig?.enable ||
              data.qqConfig?.enable ||
              data.clientConfig?.ywClientUrls.find(
                (item) => item.enable && item.url,
              )) && (
              <>
                <div className="d-flex align-items-center user-select-none">
                  <div
                    className="border-bottom"
                    style={{ height: '1px', width: '45%' }}
                  />
                  <div
                    className="text-secondary text-center"
                    style={{ width: '10%' }}
                  >
                    OR
                  </div>
                  <div
                    className="border-bottom"
                    style={{ height: '1px', width: '45%' }}
                  />
                </div>

                <div className="vstack gap-4">
                  {!switchingLoginMethod.username && (
                    <button
                      onClick={onClickLogin.bind(this, 'username')}
                      type="button"
                      className="d-flex btn btn-light w-100 justify-content-center"
                    >
                      <i className="bi bi-person me-2"></i>
                      <span>{translatedFields.accountLogin}</span>
                    </button>
                  )}

                  {!switchingLoginMethod.phone && data.phoneConfig?.enable && (
                    <button
                      onClick={onClickLogin.bind(this, 'phone')}
                      type="button"
                      className="d-flex btn btn-light w-100 justify-content-center"
                    >
                      <i className="bi bi-phone me-2"></i>
                      <span>{translatedFields.smsLogin}</span>
                    </button>
                  )}

                  {!switchingLoginMethod.email && data.emailConfig?.enable && (
                    <button
                      onClick={onClickLogin.bind(this, 'email')}
                      type="button"
                      className="d-flex btn btn-light w-100 justify-content-center"
                    >
                      <i className="bi bi-envelope me-2"></i>
                      <span>{translatedFields.emailLogin}</span>
                    </button>
                  )}

                  <div className="hstack gap-4 justify-content-center">
                    {(data.clientConfig?.ywClientUrls || [])
                      .filter((item) => item.enable && item.url)
                      .map((item) => {
                        return (
                          <a
                            key={item.clientId}
                            style={{ width: 48, height: 48 }}
                            href={item.url}
                            className="btn btn-light rounded-circle position-relative overflow-hidden"
                          >
                            <Image
                              className="object-fit-contain position-absolute top-50 start-50 translate-middle"
                              src={item.clientLogo}
                              alt={item.clientName}
                              title={item.clientName + translatedFields.login}
                              width={40}
                              height={40}
                              placeholder="blur"
                              blurDataURL={env.APP_BLUR_DATA_URL}
                            />
                          </a>
                        );
                      })}

                    {!switchingLoginMethod.qq && data.qqConfig?.enable && (
                      <div className="p-2 hstack gap-4">
                        <svg
                          onClick={onClickLogin.bind(this, 'qq')}
                          xmlns="http://www.w3.org/2000/svg"
                          width="40"
                          height="40"
                          fill="currentColor"
                          className="bi bi-tencent-qq cursor-pointer"
                          viewBox="0 0 16 16"
                        >
                          <path d="M6.048 3.323c.022.277-.13.523-.338.55-.21.026-.397-.176-.419-.453-.022-.277.13-.523.338-.55.21-.026.397.176.42.453Zm2.265-.24c-.603-.146-.894.256-.936.333-.027.048-.008.117.037.15.045.035.092.025.119-.003.361-.39.751-.172.829-.129l.011.007c.053.024.147.028.193-.098.023-.063.017-.11-.006-.142-.016-.023-.089-.08-.247-.118Z" />
                          <path d="M11.727 6.719c0-.022.01-.375.01-.557 0-3.07-1.45-6.156-5.015-6.156-3.564 0-5.014 3.086-5.014 6.156 0 .182.01.535.01.557l-.72 1.795a25.85 25.85 0 0 0-.534 1.508c-.68 2.187-.46 3.093-.292 3.113.36.044 1.401-1.647 1.401-1.647 0 .979.504 2.256 1.594 3.179-.408.126-.907.319-1.228.556-.29.213-.253.43-.201.518.228.386 3.92.246 4.985.126 1.065.12 4.756.26 4.984-.126.052-.088.088-.305-.2-.518-.322-.237-.822-.43-1.23-.557 1.09-.922 1.594-2.2 1.594-3.178 0 0 1.041 1.69 1.401 1.647.168-.02.388-.926-.292-3.113a25.78 25.78 0 0 0-.534-1.508l-.72-1.795ZM9.773 5.53a.095.095 0 0 1-.009.096c-.109.159-1.554.943-3.033.943h-.017c-1.48 0-2.925-.784-3.034-.943a.098.098 0 0 1-.018-.055c0-.015.004-.028.01-.04.13-.287 1.43-.606 3.042-.606h.017c1.611 0 2.912.319 3.042.605Zm-4.32-.989c-.483.022-.896-.529-.922-1.229-.026-.7.344-1.286.828-1.308.483-.022.896.529.922 1.23.027.7-.344 1.286-.827 1.307Zm2.538 0c-.484-.022-.854-.607-.828-1.308.027-.7.44-1.25.923-1.23.483.023.853.608.827 1.309-.026.7-.439 1.251-.922 1.23ZM2.928 8.99c.213.042.426.081.639.117v2.336s1.104.222 2.21.068V9.363c.326.018.64.026.937.023h.017c1.117.013 2.474-.136 3.786-.396.097.622.151 1.386.097 2.284-.146 2.45-1.6 3.99-3.846 4.012h-.091c-2.245-.023-3.7-1.562-3.846-4.011-.054-.9 0-1.663.097-2.285Z" />
                        </svg>
                      </div>
                    )}
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </H5Box>
  );
}
