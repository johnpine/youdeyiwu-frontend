import { authMiddleware } from '@/lib/api';
import { cookies } from 'next/headers';
import { queryPath } from '@/services/api';
import RegisterPage from '@/app/[locale]/register/register';
import ResetPage from '@/app/[locale]/reset/reset';
import type { Metadata as MetadataNext } from 'next';
import { createError, getMetadata } from '@/lib/tool';
import type { IConfigRegister, IPath } from '@/interfaces';
import { getTranslator } from 'next-intl/server';
import { getTranslatedFields } from '@/lib/dictionaries';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('registerPage.userRegistration') });
}

async function getData(searchParams: {
  type: 'username' | 'email' | 'phone' | 'qq';
}) {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/register',
      },
    });

    const responses = await Promise.all([req1]);
    const resp1 = await ((await responses[0]) as Response).json();
    const path = resp1.data as IPath;

    return {
      isSuccess: true,
      isError: false,
      data: {
        path,
        registerConfig: path.registerConfig! as IConfigRegister,
        queryParams: searchParams,
      },
    };
  } catch (e: any) {
    return {
      isSuccess: false,
      isError: true,
      error: e,
    };
  }
}

export default async function Page({
  params: { locale },
  searchParams: { v, type },
}: {
  params: { locale: string };
  searchParams: {
    v?: 'h5';
    type?: 'username' | 'email' | 'phone' | 'qq';
  };
}) {
  const data = await getData({
    type: type ?? 'username',
  });
  if (data.isError) {
    return <ResetPage error={createError(data.error)} />;
  }

  const source = data.data!;
  const translatedFields = await getTranslatedFields(locale, 'registerPage');

  return <RegisterPage source={source} translatedFields={translatedFields} />;
}
