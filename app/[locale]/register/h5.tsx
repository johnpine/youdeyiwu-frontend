import { useContext, useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import UsernameRegister from '@/app/[locale]/register/username';
import PhoneRegister from '@/app/[locale]/register/phone';
import EmailRegister from '@/app/[locale]/register/email';
import QqRegister from '@/app/[locale]/register/qq';
import { AppContext } from '@/contexts/app';
import { RegisterPageContext } from '@/contexts/register';
import H5Box from '@/app/[locale]/common/other/h5';

export default function RegisterH5Page(this: any) {
  const { source, translatedFields } = useContext(RegisterPageContext)!;
  const appContext = useContext(AppContext);
  const metadata = appContext.metadata!;
  const env = metadata.env;
  const registerType = source.queryParams.type;
  const [switchingRegisterMethod, setSwitchingRegisterMethod] = useState({
    username:
      registerType === 'username' &&
      !source.path.phoneConfig?.enable &&
      !source.path.emailConfig?.enable &&
      !source.path.qqConfig?.enable,
    phone: registerType === 'phone' && source.path.phoneConfig?.enable,
    email: registerType === 'email' && source.path.emailConfig?.enable,
    qq: registerType === 'qq' && source.path.qqConfig?.enable,
  });
  const data = source.path;

  function onClickRegister(name: string) {
    const obj = { ...switchingRegisterMethod } as any;
    for (const objKey in obj) {
      obj[objKey] = false;
    }
    setSwitchingRegisterMethod({ ...obj, [name]: true });
  }

  return (
    <H5Box classs="col px-2 py-4">
      <div className="card rounded-5 border-0">
        <div className="card-body">
          <div className="my-5 hstack gap-2 justify-content-center">
            <div className="cursor-default">
              {translatedFields.accountExist}
            </div>
            <div>
              <Link className="btn btn-light" href="/login">
                {translatedFields.startLogin}
              </Link>
            </div>
          </div>
          <div className="vstack gap-4">
            <div className="fs-4 text-start fw-bold">
              {env.APP_NAME ? (
                <div>
                  {translatedFields.welcomeTo}
                  {env.APP_NAME}
                </div>
              ) : (
                <div>{translatedFields.welcome}</div>
              )}
              <div className="mt-3 fs-5 fw-normal">
                {translatedFields.inputInfo}
              </div>
            </div>

            {switchingRegisterMethod.username && <UsernameRegister />}

            {switchingRegisterMethod.phone && data.phoneConfig?.enable && (
              <PhoneRegister />
            )}

            {switchingRegisterMethod.email && data.emailConfig?.enable && (
              <EmailRegister />
            )}

            {switchingRegisterMethod.qq && data.qqConfig?.enable && (
              <QqRegister />
            )}

            {(data.phoneConfig?.enable ||
              data.emailConfig?.enable ||
              data.qqConfig?.enable ||
              data.clientConfig?.ywClientUrls.find(
                (item) => item.enable && item.url,
              )) && (
              <>
                <div className="d-flex align-items-center user-select-none">
                  <div
                    className="border-bottom"
                    style={{ height: '1px', width: '45%' }}
                  />
                  <div
                    className="text-secondary text-center"
                    style={{ width: '10%' }}
                  >
                    OR
                  </div>
                  <div
                    className="border-bottom"
                    style={{ height: '1px', width: '45%' }}
                  />
                </div>
                <div className="vstack gap-4">
                  <div className="hstack gap-4">
                    {!switchingRegisterMethod.username && (
                      <button
                        onClick={onClickRegister.bind(this, 'username')}
                        type="button"
                        className="d-flex btn btn-light w-100 justify-content-center"
                      >
                        <i className="bi bi-person me-2"></i>
                        <span>{translatedFields.accountRegistration}</span>
                      </button>
                    )}

                    {!switchingRegisterMethod.phone &&
                      data.phoneConfig?.enable && (
                        <button
                          onClick={onClickRegister.bind(this, 'phone')}
                          type="button"
                          className="d-flex btn btn-light w-100 justify-content-center"
                        >
                          <i className="bi bi-phone me-2"></i>
                          <span>{translatedFields.smsRegistration}</span>
                        </button>
                      )}

                    {!switchingRegisterMethod.email &&
                      data.emailConfig?.enable && (
                        <button
                          onClick={onClickRegister.bind(this, 'email')}
                          type="button"
                          className="d-flex btn btn-light w-100 justify-content-center"
                        >
                          <i className="bi bi-envelope me-2"></i>
                          <span>{translatedFields.emailRegistration}</span>
                        </button>
                      )}
                  </div>

                  <div className="hstack gap-4 justify-content-center">
                    {(data.clientConfig?.ywClientUrls || [])
                      .filter((item) => item.enable && item.url)
                      .map((item) => {
                        return (
                          <a
                            key={item.clientId}
                            style={{ width: 48, height: 48 }}
                            href={item.url}
                            className="btn btn-light rounded-circle position-relative overflow-hidden"
                          >
                            <Image
                              className="object-fit-contain position-absolute top-50 start-50 translate-middle"
                              src={item.clientLogo}
                              alt={item.clientName}
                              title={
                                item.clientName + translatedFields.registration
                              }
                              width={40}
                              height={40}
                              placeholder="blur"
                              blurDataURL={env.APP_BLUR_DATA_URL}
                            />
                          </a>
                        );
                      })}

                    {!switchingRegisterMethod.qq && data.qqConfig?.enable && (
                      <div className="w-100 p-2 hstack justify-content-center gap-4">
                        <i
                          onClick={onClickRegister.bind(this, 'qq')}
                          className="bi bi-tencent-qq cursor-pointer fs-1"
                        ></i>
                      </div>
                    )}
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </H5Box>
  );
}
