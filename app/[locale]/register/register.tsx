'use client';

import { useContext, useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import UsernameRegister from '@/app/[locale]/register/username';
import PhoneRegister from '@/app/[locale]/register/phone';
import EmailRegister from '@/app/[locale]/register/email';
import QqRegister from '@/app/[locale]/register/qq';
import type { IRegisterPageContext } from '@/contexts/register';
import { RegisterPageContext } from '@/contexts/register';
import { AppContext } from '@/contexts/app';
import PcBox from '@/app/[locale]/common/other/pc';
import RegisterH5Page from '@/app/[locale]/register/h5';

export default function RegisterPage(
  this: any,
  { source, translatedFields }: IRegisterPageContext,
) {
  const appContext = useContext(AppContext);
  const metadata = appContext.metadata!;
  const env = metadata.env;
  const registerType = source.queryParams.type;
  const [switchingRegisterMethod, setSwitchingRegisterMethod] = useState({
    username:
      registerType === 'username' &&
      !source.path.phoneConfig?.enable &&
      !source.path.emailConfig?.enable &&
      !source.path.qqConfig?.enable,
    phone: registerType === 'phone' && source.path.phoneConfig?.enable,
    email: registerType === 'email' && source.path.emailConfig?.enable,
    qq: registerType === 'qq' && source.path.qqConfig?.enable,
  });
  const data = source.path;

  function onClickRegister(name: string) {
    const obj = { ...switchingRegisterMethod } as any;
    for (const objKey in obj) {
      obj[objKey] = false;
    }
    setSwitchingRegisterMethod({ ...obj, [name]: true });
  }

  return (
    <RegisterPageContext.Provider value={{ source, translatedFields }}>
      <PcBox classs="col py-5 my-5">
        <div className="container">
          <div className="row">
            <div className="col pe-0">
              <div className="card rounded-start-4 border-0 h-100">
                <div className="card-body ps-4 py-5 my-5 animate__animated animate__fadeInLeft">
                  <div className="mb-5 hstack gap-2 justify-content-end">
                    <div className="cursor-default">
                      {translatedFields.accountExist}
                    </div>
                    <div>
                      <Link className="btn btn-light" href="/login">
                        {translatedFields.startLogin}
                      </Link>
                    </div>
                  </div>

                  <div className="fs-4 text-start fw-bold ms-5">
                    {env.APP_NAME ? (
                      <div>
                        {translatedFields.welcomeTo}
                        &nbsp;
                        {env.APP_NAME}
                      </div>
                    ) : (
                      <div>{translatedFields.welcome}</div>
                    )}
                    <div className="mt-3 fs-5 fw-normal">
                      {translatedFields.inputInfo}
                    </div>
                  </div>

                  <div className="my-5 vstack gap-4 px-5">
                    {(data.phoneConfig?.enable ||
                      data.emailConfig?.enable ||
                      data.qqConfig?.enable ||
                      data.clientConfig?.ywClientUrls.find(
                        (item) => item.enable && item.url,
                      )) && (
                      <>
                        {!switchingRegisterMethod.username && (
                          <button
                            onClick={onClickRegister.bind(this, 'username')}
                            type="button"
                            className="btn btn-light"
                          >
                            <div className="row">
                              <div className="col text-center align-self-center position-relative">
                                <div className="position-absolute top-50 start-0 translate-middle-y ms-5">
                                  <i className="bi bi-person fs-4"></i>
                                </div>
                                <div>
                                  {translatedFields.accountRegistration}
                                </div>
                              </div>
                            </div>
                          </button>
                        )}

                        {!switchingRegisterMethod.phone &&
                          data.phoneConfig?.enable && (
                            <button
                              onClick={onClickRegister.bind(this, 'phone')}
                              type="button"
                              className="btn btn-light"
                            >
                              <div className="row">
                                <div className="col text-center align-self-center position-relative">
                                  <div className="position-absolute top-50 start-0 translate-middle-y ms-5">
                                    <i className="bi bi-phone fs-4"></i>
                                  </div>
                                  <div>{translatedFields.smsRegistration}</div>
                                </div>
                              </div>
                            </button>
                          )}

                        {!switchingRegisterMethod.email &&
                          data.emailConfig?.enable && (
                            <button
                              onClick={onClickRegister.bind(this, 'email')}
                              type="button"
                              className="btn btn-light"
                            >
                              <div className="row">
                                <div className="col text-center align-self-center position-relative">
                                  <div className="position-absolute top-50 start-0 translate-middle-y ms-5">
                                    <i className="bi bi-envelope fs-4"></i>
                                  </div>
                                  <div>
                                    {translatedFields.emailRegistration}
                                  </div>
                                </div>
                              </div>
                            </button>
                          )}

                        {(data.clientConfig?.ywClientUrls || [])
                          .filter((item) => item.enable && item.url)
                          .map((item) => {
                            return (
                              <Link
                                key={item.clientId}
                                href={item.url}
                                className="btn btn-light"
                              >
                                <div className="row">
                                  <div className="col text-center align-self-center position-relative">
                                    <div className="position-absolute top-50 start-0 translate-middle-y ms-5">
                                      <Image
                                        className="object-fit-contain"
                                        src={item.clientLogo}
                                        alt={item.clientName}
                                        width={24}
                                        height={24}
                                        placeholder="blur"
                                        blurDataURL={env.APP_BLUR_DATA_URL}
                                      />
                                    </div>
                                    <div>
                                      {item.clientName +
                                        translatedFields.registration}
                                    </div>
                                  </div>
                                </div>
                              </Link>
                            );
                          })}

                        {!switchingRegisterMethod.qq &&
                          data.qqConfig?.enable && (
                            <button
                              onClick={onClickRegister.bind(this, 'qq')}
                              type="button"
                              className="btn btn-light"
                            >
                              <div className="row">
                                <div className="col text-center align-self-center position-relative">
                                  <div className="position-absolute top-50 start-0 translate-middle-y ms-5">
                                    <i className="bi bi-tencent-qq fs-4"></i>
                                  </div>
                                  <div>{translatedFields.thirdPartyLogin}</div>
                                </div>
                              </div>
                            </button>
                          )}
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-auto px-0">
              <div className="card border-0 h-100">
                <div className="card-body d-flex align-items-center position-relative py-0">
                  <div className="d-flex position-absolute start-0 end-0 top-0 h-100 py-5 justify-content-center">
                    <div className="vr text-secondary text-opacity-75"></div>
                  </div>
                  <div className="bg-body z-1 border border-secondary border-opacity-25 rounded-3 text-secondary px-2 py-3">
                    OR
                  </div>
                </div>
              </div>
            </div>
            <div className="col ps-0">
              <div className="card rounded-end-4 border-0 h-100">
                <div className="card-body py-5 pe-4 vstack gap-4 my-5 animate__animated animate__fadeInRight">
                  {switchingRegisterMethod.username && <UsernameRegister />}

                  {switchingRegisterMethod.phone &&
                    data.phoneConfig?.enable && <PhoneRegister />}

                  {switchingRegisterMethod.email &&
                    data.emailConfig?.enable && <EmailRegister />}

                  {switchingRegisterMethod.qq && data.qqConfig?.enable && (
                    <QqRegister />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </PcBox>

      <RegisterH5Page />
    </RegisterPageContext.Provider>
  );
}
