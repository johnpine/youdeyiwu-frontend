import {
  type ChangeEvent,
  type FormEvent,
  type KeyboardEvent,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import type JSEncrypt from 'jsencrypt';
import useToast from '@/hooks/useToast';
import { useMutation } from '@tanstack/react-query';
import {
  generateCaptchaByPhone,
  queryAccountPublicKey,
  registerByPhone,
} from '@/services/api';
import dayjs from 'dayjs';
import { isPhone } from '@/lib/tool';
import Link from 'next/link';
import { useSearchParams } from 'next/navigation';
import { RegisterPageContext } from '@/contexts/register';

export default function PhoneRegister() {
  const {
    source: { path: data, registerConfig },
    translatedFields,
  } = useContext(RegisterPageContext)!;
  const [form, setForm] = useState({
    phone: '',
    captcha: '',
    alias: '',
    isAgreeAgreement: false,
  });
  const [phoneCaptchaConfig, setPhoneCaptchaConfig] = useState({
    ...data.phoneConfig,
    isClick: false,
    countdown: 0,
    countdownId: undefined,
  });
  const [disableRegister, setDisableRegister] = useState(false);
  const captchaInputRef = useRef<HTMLInputElement>(null);
  const jsEncryptRef = useRef<JSEncrypt>();
  const agreementInputRef = useRef<HTMLInputElement>(null);
  const urlSearchParams = useSearchParams();
  const { show } = useToast();

  useEffect(() => {
    const countdownId = phoneCaptchaConfig.countdownId;
    return () => {
      if (countdownId) {
        clearInterval(countdownId);
      }
    };
  }, [phoneCaptchaConfig.countdownId]);

  const registerByPhoneMutation = useMutation(registerByPhone);
  const queryAccountPublicKeyMutation = useMutation(queryAccountPublicKey);
  const generateCaptchaByPhoneMutation = useMutation(generateCaptchaByPhone);

  function onChangeForm(e: ChangeEvent<HTMLInputElement>) {
    const name = e.target.name;
    const value = e.target.value;

    if (name === 'agreement') {
      setForm({ ...form, isAgreeAgreement: !form.isAgreeAgreement });
    } else {
      setForm({ ...form, [name]: value.trim() });
    }
  }

  async function onSubmit(
    e: FormEvent<HTMLFormElement> | KeyboardEvent<HTMLInputElement>,
  ) {
    e.preventDefault();
    e.stopPropagation();

    try {
      const { phone, captcha, isAgreeAgreement } = form;
      const phoneConfig = data.phoneConfig;
      if (registerConfig.enableRegistrationConsent && !isAgreeAgreement) {
        agreementInputRef.current?.focus();
        show({
          type: 'DANGER',
          message: translatedFields.agreeBeforeRegister,
        });
        return;
      } else if (!phone) {
        show({
          type: 'DANGER',
          message: translatedFields.invalidPhoneNumber,
        });
        return;
      } else if (phoneConfig && phoneConfig.enable && !captcha) {
        show({
          type: 'DANGER',
          message: translatedFields.invalidVerificationCode,
        });
        return;
      }

      if (phone.length !== 11 || !isPhone(phone)) {
        show({
          type: 'DANGER',
          message: translatedFields.invalidFormat,
        });
        return;
      }

      const body = await assemblyData();
      if (!Object.keys(body).length) {
        show({
          type: 'DANGER',
          message: translatedFields.registerFailed,
        });
        return;
      }
      await registerByPhoneMutation.mutateAsync({ data: body });
      setDisableRegister(true);

      show({
        message: translatedFields.refreshComplete,
        type: 'SUCCESS',
      });

      show({
        message: translatedFields.autoLogin,
        type: 'SUCCESS',
      });

      setTimeout(() => {
        show({
          message: translatedFields.redirect,
          type: 'PRIMARY',
        });
      }, 1000);

      setTimeout(() => {
        const responseType =
          urlSearchParams?.get('response_type') ??
          urlSearchParams?.get('responseType') ??
          urlSearchParams?.get('response-type');
        const clientId =
          urlSearchParams?.get('client_id') ??
          urlSearchParams?.get('clientId') ??
          urlSearchParams?.get('client-id');
        const redirectUri =
          urlSearchParams?.get('redirect_uri') ??
          urlSearchParams?.get('redirectUri') ??
          urlSearchParams?.get('redirect-uri');
        const scope =
          urlSearchParams?.get('response_type') ??
          urlSearchParams?.get('responseType') ??
          urlSearchParams?.get('response-type');
        const state = urlSearchParams?.get('state');
        const params = {
          responseType,
          clientId,
          redirectUri,
          scope,
        } as any;

        if (state) {
          params.state = state;
        }

        if (responseType && clientId && redirectUri && scope) {
          location.href = `/oauth/auth?${new URLSearchParams(params)}`;
        } else {
          location.href = '/';
        }
      }, 1500);
    } catch (e) {
      registerByPhoneMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    }
  }

  async function onKeyDownCaptcha(e: KeyboardEvent<HTMLInputElement>) {
    e.preventDefault();
    e.stopPropagation();

    if (e.key === 'Enter' && e.code === 'Enter') {
      const phoneConfig = data.phoneConfig;
      if (phoneConfig && phoneConfig.enable && !form.captcha) {
        await onClickRefreshCaptcha();
      } else {
        await onSubmit(e);
      }
    }
  }

  async function assemblyData() {
    const { phone, alias, captcha } = form;
    const body = {} as any;
    if (alias) {
      body.alias = alias;
    }
    if (data.phoneConfig && data.phoneConfig.enable) {
      body.code = captcha;
    }
    const ePhone = await getEncryptedPhone(phone);
    if (!ePhone) {
      show({
        type: 'DANGER',
        message: translatedFields.registerFailed,
      });
      return;
    }
    body.phone = ePhone;
    return body;
  }

  async function onClickRefreshCaptcha() {
    try {
      const { phone } = form;
      if (!phone) {
        show({
          type: 'DANGER',
          message: translatedFields.inputToSend,
        });
        return;
      }

      if (phone.length !== 11 || !isPhone(phone)) {
        show({
          type: 'DANGER',
          message: translatedFields.invalidFormat,
        });
        return;
      }

      if (phoneCaptchaConfig.isClick) {
        if (phoneCaptchaConfig.countdown === 0) {
          return;
        }

        show({
          message: `${phoneCaptchaConfig.countdown} ${translatedFields.refreshAfter}`,
          type: 'PRIMARY',
        });
        return;
      }

      if (phoneCaptchaConfig.total && phoneCaptchaConfig.total < 1) {
        show({
          type: 'DANGER',
          message: translatedFields.captchaLimitExceeded,
        });
        return;
      }

      const ePhone = await getEncryptedPhone(phone);
      if (!ePhone) {
        show({
          type: 'DANGER',
          message: translatedFields.failedToSendVerificationCode,
        });
        return;
      }

      await generateCaptchaByPhoneMutation.mutateAsync({
        data: {
          phone: ePhone,
          contentType: 'REGISTER',
        },
      });

      if (phoneCaptchaConfig.interval) {
        let seconds = dayjs.duration(phoneCaptchaConfig.interval).asSeconds();
        const countdownId = setInterval(() => {
          if (
            seconds === 0 &&
            typeof phoneCaptchaConfig.total !== 'undefined'
          ) {
            clearInterval(countdownId);
            setPhoneCaptchaConfig({
              ...phoneCaptchaConfig,
              isClick: false,
              total: phoneCaptchaConfig.total - 1,
              countdownId: undefined,
            });
          } else {
            seconds -= 1;
            setPhoneCaptchaConfig({
              ...phoneCaptchaConfig,
              countdown: seconds,
              isClick: true,
            });
          }
        }, 1000);
      }

      captchaInputRef.current?.focus();
      show({
        type: 'PRIMARY',
        message: translatedFields.verificationCodeSent,
      });
    } catch (e) {
      generateCaptchaByPhoneMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    }
  }

  async function getEncryptedPhone(phone: string) {
    try {
      const publicKey = (await queryAccountPublicKeyMutation.mutateAsync(
        {},
      )) as string;
      const jsEncrypt = await getJsEncrypt();
      jsEncrypt.setPublicKey(publicKey);
      return jsEncrypt.encrypt(phone);
    } catch (e) {
      queryAccountPublicKeyMutation.reset();
      show({
        type: 'DANGER',
        message: translatedFields.encryptionFailed,
      });
    }
  }

  async function getJsEncrypt(): Promise<JSEncrypt> {
    let jsEncrypt;
    if (jsEncryptRef.current) {
      jsEncrypt = jsEncryptRef.current;
    } else {
      const JSEncrypt = (await import('jsencrypt')).JSEncrypt;
      jsEncrypt = new JSEncrypt();
      jsEncryptRef.current = jsEncrypt;
    }
    return jsEncrypt;
  }

  return (
    <form onSubmit={onSubmit}>
      <div className="my-3">
        <label className="form-label">{translatedFields.aliasOptional}</label>
        <input
          value={form.alias}
          onChange={onChangeForm}
          name="alias"
          type="text"
          className="form-control"
          placeholder={translatedFields.enterAlias}
          aria-describedby={translatedFields.enterAlias}
        />
      </div>

      <div className="my-3">
        <label className="form-label">
          <span className="text-danger fw-bold">*</span>
          {translatedFields.phoneNumber}
        </label>
        <input
          required
          minLength={11}
          maxLength={11}
          pattern="\d{11}"
          value={form.phone}
          onChange={onChangeForm}
          name="phone"
          type="tel"
          className="form-control"
          placeholder={translatedFields.enterPhoneNumber}
          aria-describedby={translatedFields.enterPhoneNumber}
        />
      </div>

      {data.phoneConfig && data.phoneConfig.enable && (
        <div className="my-3">
          <label htmlFor="phoneCaptcha" className="form-label">
            <span className="text-danger fw-bold">*</span>
            {translatedFields.captcha}
          </label>
          <div className="d-flex justify-content-between align-items-center">
            <div className="flex-grow-1 me-3">
              <input
                onKeyDown={onKeyDownCaptcha}
                required
                value={form.captcha}
                onChange={onChangeForm}
                name="captcha"
                type="text"
                placeholder={translatedFields.enterCaptcha}
                className="form-control"
              />
            </div>
            <div className="flex-shrink-0">
              <button
                disabled={
                  phoneCaptchaConfig.countdown !== 0 ||
                  generateCaptchaByPhoneMutation.isLoading
                }
                onClick={onClickRefreshCaptcha}
                type="button"
                className="btn btn-secondary"
              >
                {generateCaptchaByPhoneMutation.isLoading && (
                  <span
                    className="spinner-border spinner-border-sm me-2"
                    role="status"
                    aria-hidden="true"
                  ></span>
                )}
                {translatedFields.sendCaptcha}
                {phoneCaptchaConfig.countdown !== 0 &&
                  `(${phoneCaptchaConfig.countdown})`}
              </button>
            </div>
          </div>
        </div>
      )}

      <button
        disabled={registerByPhoneMutation.isLoading || disableRegister}
        type="submit"
        className="btn btn-outline-primary mt-4 mb-3 w-100"
      >
        {registerByPhoneMutation.isLoading && (
          <span
            className="spinner-border spinner-border-sm me-2"
            role="status"
            aria-hidden="true"
          ></span>
        )}
        {disableRegister ? (
          <span>{translatedFields.refreshComplete}</span>
        ) : (
          <span>{translatedFields.quickRegistration}</span>
        )}
      </button>

      {registerConfig.enableRegistrationConsent && (
        <div className="form-check my-3">
          <input
            ref={agreementInputRef}
            name="agreement"
            value="agreement"
            onChange={onChangeForm}
            type="checkbox"
            className="form-check-input cursor-pointer"
          />
          <label className="form-check-label user-select-none">
            <span>{translatedFields.readAndAgree}</span>
            <Link href={registerConfig.terms ?? '/terms'}>
              {translatedFields.serviceAgreement}
            </Link>
            <span>{translatedFields.and}</span>
            <Link href={registerConfig.privacyPolicy ?? '/privacy-policy'}>
              {translatedFields.privacyPolicy}
            </Link>
          </label>
        </div>
      )}
    </form>
  );
}
