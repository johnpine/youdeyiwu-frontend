import { type ReactNode } from 'react';

export default function Box({
  children,
  v,
}: {
  children: ReactNode;
  v?: 'h5' | 'pc';
}) {
  if (v === 'h5') {
    return (
      <div className="col p-2">
        <div className="card rounded-5 border-0">
          <div className="card-body px-0">{children}</div>
        </div>
      </div>
    );
  }

  return children;
}
