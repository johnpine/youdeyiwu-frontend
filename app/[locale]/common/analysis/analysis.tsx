'use client';

import dynamic from 'next/dynamic';

const AnalysisDynamic: any = dynamic(() => import('../../analysis/wrapper'), {
  ssr: false,
});

export default function Analysis() {
  return <AnalysisDynamic />;
}
