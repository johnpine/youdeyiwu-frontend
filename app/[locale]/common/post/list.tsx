import type { IPost } from '@/interfaces';
import PostItem from '@/app/[locale]/common/post/item';

export default function PostList({
  items,
  translatedFields,
}: {
  items: IPost[];
  translatedFields?: {
    comment: string;
    reply: string;
  };
}) {
  return (
    <>
      {items.map((item) => {
        return (
          <PostItem
            key={item.id}
            item={item}
            translatedFields={translatedFields}
          />
        );
      })}
    </>
  );
}
