import type { IPost, IUserOv } from '@/interfaces';
import PostName from '@/app/[locale]/common/post/name';
import PostOverview from '@/app/[locale]/common/post/overview';
import PostCustomTags from '@/app/[locale]/common/post/customTags';
import PostImages from '@/app/[locale]/common/post/images';
import PostDivider from '@/app/[locale]/common/post/divider';
import PostUser from '@/app/[locale]/common/post/user';

export default function PostItem({
  item,
  user,
  translatedFields,
}: {
  item: IPost;
  user?: IUserOv;
  translatedFields?: {
    comment: string;
    reply: string;
  };
}) {
  return (
    <div className="row">
      <div className="col px-0">
        <div className="card border-0">
          <div className="card-body vstack gap-4 p-4">
            <PostName item={item} />

            <PostOverview item={item} />

            <PostCustomTags item={item} />

            <PostImages item={item} />

            <PostDivider />

            <PostUser
              item={item}
              user={user}
              translatedFields={translatedFields}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
