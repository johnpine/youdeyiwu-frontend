import type { IPost } from '@/interfaces';
import classNames from 'classnames';

export default function PostOverview({
  item,
  bodClassName,
}: {
  item: IPost;
  bodClassName?: string;
}) {
  const { overview, contentType } = item;

  return (
    <>
      {contentType === 'DEFAULT' && overview && (
        <div
          className={classNames('text-body-secondary', bodClassName)}
          dangerouslySetInnerHTML={{
            __html: overview,
          }}
        ></div>
      )}
    </>
  );
}
