import type { IPost, IUserOv } from '@/interfaces';
import Link from 'next/link';
import Image from 'next/image';
import { formatCount, getUserAvatar } from '@/lib/tool';
import { useContext } from 'react';
import { AppContext } from '@/contexts/app';

export default function PostUser({
  item,
  user,
  translatedFields = {
    comment: '评论',
    reply: '回复',
  },
}: {
  item: IPost;
  user?: IUserOv;
  translatedFields?: {
    comment: string;
    reply: string;
  };
}) {
  const { contentUpdatedOn, _contentUpdatedOnText, details } = item;
  const currentUser = item.user ?? user!;
  const context = useContext(AppContext);
  const metadata = context.metadata!;
  const env = metadata.env;

  return (
    <div className="d-flex gap-2">
      <div className="d-flex">
        <div
          className="rounded-circle me-4 flex-shrink-0"
          style={{ width: 56, height: 56 }}
        >
          <Link href={`/users/${currentUser.id}`}>
            <Image
              className="rounded-circle object-fit-contain"
              src={getUserAvatar(currentUser, metadata).mediumAvatarUrl}
              alt={currentUser.alias}
              title={`${currentUser.alias}(${currentUser.id})`}
              width={56}
              height={56}
              placeholder="blur"
              blurDataURL={env.APP_BLUR_DATA_URL}
            />
          </Link>
        </div>
        <div className="d-flex flex-column gap-2 justify-content-center flex-shrink-0">
          <Link
            className="text-secondary link-underline-secondary link-underline-opacity-0 link-underline-opacity-100-hover link-offset-3"
            href={`/users/${currentUser.id}`}
          >
            {currentUser.alias}
          </Link>
          {_contentUpdatedOnText && (
            <time className="text-secondary" dateTime={contentUpdatedOn}>
              {_contentUpdatedOnText}
            </time>
          )}
        </div>
      </div>

      {details && (
        <div className="flex-shrink-0 flex-grow-1 gap-4 justify-content-end hstack ms-4 text-secondary">
          {details.commentCount > 0 && (
            <div>
              {translatedFields.comment}&nbsp;
              {formatCount(details.commentCount)}
            </div>
          )}
          {details.replyCount > 0 && (
            <div>
              {translatedFields.reply}&nbsp;{formatCount(details.replyCount)}
            </div>
          )}
        </div>
      )}
    </div>
  );
}
