import type { IPost } from '@/interfaces';

export default function PostCustomTags({ item }: { item: IPost }) {
  const { customTags } = item;

  return (
    <>
      {customTags.length > 0 && (
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            {customTags.map((value) => {
              return (
                <li key={value} className="breadcrumb-item active">
                  {value}
                </li>
              );
            })}
          </ol>
        </nav>
      )}
    </>
  );
}
