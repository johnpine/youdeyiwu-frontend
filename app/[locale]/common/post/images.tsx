import type { IPost } from '@/interfaces';
import Image from 'next/image';
import PostCover from '@/app/[locale]/common/post/cover';
import { useContext } from 'react';
import { AppContext } from '@/contexts/app';

export default function PostImages({ item }: { item: IPost }) {
  const { images } = item;
  const appContext = useContext(AppContext);
  const env = appContext.metadata!.env;

  return (
    <>
      {images.length > 0 && (
        <div className="d-flex gap-2 overflow-x-auto flex-nowrap text-nowrap pb-2">
          <PostCover item={item} />

          {images.map((value, index) => {
            return (
              <div
                key={value}
                className="ratio ratio-16x9 flex-shrink-0"
                style={{ width: 260, height: 195 }}
              >
                <Image
                  className="rounded object-fit-contain"
                  width={260}
                  height={195}
                  src={value}
                  placeholder="blur"
                  blurDataURL={env.APP_BLUR_DATA_URL}
                  alt={`grid${index + 1}`}
                />
              </div>
            );
          })}
        </div>
      )}
    </>
  );
}
