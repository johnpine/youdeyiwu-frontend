export default function PostDivider() {
  return <hr className="text-secondary text-opacity-75" />;
}
