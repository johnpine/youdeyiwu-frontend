import type { IPost, IPostReviewHistory } from '@/interfaces';
import { useQuery } from '@tanstack/react-query';
import { queryAllPostReviewHistoryById } from '@/services/api';
import useModal from '@/hooks/useModal';
import classNames from 'classnames';
import { toRelativeTime } from '@/lib/tool';
import type { TPostCompositeState } from '@/types';
import Alert from '@/app/[locale]/alert/alert';

export type TPostReviewHistoryTranslatedFields = {
  system: string;
  auditRecords: string;
  auditStatus: string;
  auditReason: string;
  auditPersonnel: string;
  auditTime: string;
  enums: {
    state: {
      APPROVED: string;
      DENIED: string;
      PENDING: string;
      CLOSE: string;
      SHOW: string;
      HIDE: string;
      LOCK: string;
    };
    sortState: {
      GLOBAL: string;
      CURRENT: string;
      RECOMMEND: string;
      POPULAR: string;
      DEFAULT: string;
    };
  };
  sortingStatus: string;
  sortingReason: string;
};

export default function PostReviewHistory(
  this: any,
  {
    item,
    translatedFields,
  }: { item: IPost; translatedFields: TPostReviewHistoryTranslatedFields },
) {
  const { showModal } = useModal();

  const queryAllPostReviewHistoryByIdQuery = useQuery(
    ['/forum', '/posts', item.id, '/reviewHistory'],
    async (context) => {
      return (await queryAllPostReviewHistoryById({
        id: context.queryKey[2] + '',
      })) as IPostReviewHistory[];
    },
  );

  function onClickHistory(data: IPostReviewHistory[]) {
    showModal({
      title: translatedFields.auditRecords,
      centered: false,
      content: (
        <ReviewHistory
          data={data}
          createdOn={item.createdOn}
          translatedFields={translatedFields}
        />
      ),
      isShowFooter: false,
      large: true,
    });
  }

  if (queryAllPostReviewHistoryByIdQuery.data) {
    const data = queryAllPostReviewHistoryByIdQuery.data;
    return (
      <i
        className="bi bi-clock-history cursor-pointer"
        onClick={onClickHistory.bind(this, data)}
      ></i>
    );
  }

  return (
    <span
      className="spinner-grow spinner-grow-sm"
      role="status"
      aria-hidden="true"
    ></span>
  );
}

const ReviewHistory = ({
  data,
  createdOn,
  translatedFields,
}: {
  data: IPostReviewHistory[];
  createdOn: string;
  translatedFields: TPostReviewHistoryTranslatedFields;
}) => {
  const reviewStates: IPostReviewHistory[] = [];
  const sortStates: IPostReviewHistory[] = [];
  let _isDefault = false;

  data.forEach((item) => {
    if (item.previousReviewState && item.finalReviewState) {
      reviewStates.push(item);
    } else if (item.previousSortState && item.finalSortState) {
      sortStates.push(item);
    }
  });

  if (data.length === 0) {
    _isDefault = true;
    const defaultItem: any = {
      id: 1,
      previousReviewState: 'PENDING',
      finalReviewState: 'APPROVED',
      createdOn,
      _isDefault,
    };
    reviewStates.push(defaultItem);
  }

  return (
    <div className="vstack gap-3">
      {reviewStates.length > 0 && (
        <CompositeStateTable
          items={reviewStates}
          _isDefault={_isDefault}
          translatedFields={translatedFields}
        />
      )}
      {sortStates.length > 0 && (
        <SortStateTable
          items={sortStates}
          translatedFields={translatedFields}
        />
      )}
      {reviewStates.length === 0 && sortStates.length === 0 && (
        <Alert message="nodata" />
      )}
    </div>
  );
};

const CompositeStateTable = ({
  items,
  _isDefault,
  translatedFields,
}: {
  items: IPostReviewHistory[];
  _isDefault?: boolean;
  translatedFields: TPostReviewHistoryTranslatedFields;
}) => {
  return (
    <div className="table-responsive">
      <table className="table table-hover align-middle">
        <thead>
          <tr className="text-nowrap">
            <th scope="col" className="fw-normal text-nowrap">
              {translatedFields.auditStatus}
            </th>

            {!_isDefault && (
              <th scope="col" className="fw-normal text-nowrap">
                {translatedFields.auditReason}
              </th>
            )}

            <th scope="col" className="fw-normal text-nowrap">
              {translatedFields.auditPersonnel}
            </th>
            <th scope="col" className="fw-normal text-nowrap">
              {translatedFields.auditTime}
            </th>
          </tr>
        </thead>
        <tbody>
          {items.map((item, index) => {
            return (
              <tr key={item.id} className="text-nowrap">
                <td>
                  {item.previousReviewState && item.finalReviewState && (
                    <div className="row flex-nowrap">
                      <div className="col-auto">
                        <CompositeStateIcon
                          compositeState={item.finalReviewState}
                        />
                      </div>
                      <div className="col-auto">
                        {translatedFields.enums.state[item.finalReviewState]}
                      </div>
                      <div className="col-auto">
                        <i
                          className={classNames(
                            'bi bi-arrow-left',
                            index === 0 ? 'text-primary' : 'text-secondary',
                          )}
                        ></i>
                      </div>
                      <div className="col-auto text-secondary">
                        {translatedFields.enums.state[item.previousReviewState]}
                      </div>
                    </div>
                  )}
                </td>

                {!item._isDefault && (
                  <td title={item.reason || ''} className="text-wrap">
                    <div
                      dangerouslySetInnerHTML={{
                        __html: item.reason || '',
                      }}
                    ></div>
                  </td>
                )}

                <td>
                  {item._isDefault
                    ? translatedFields.system
                    : `${item.reviewer.alias} (${item.reviewer.id})`}
                </td>
                <td>
                  <time dateTime={item.createdOn} title={item.createdOn}>
                    {toRelativeTime(item.createdOn)}
                  </time>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

const SortStateTable = ({
  items,
  translatedFields,
}: {
  items: IPostReviewHistory[];
  translatedFields: TPostReviewHistoryTranslatedFields;
}) => {
  return (
    <div className="table-responsive">
      <table className="table table-hover align-middle">
        <thead>
          <tr className="text-nowrap">
            <th scope="col" className="fw-normal text-nowrap">
              {translatedFields.sortingStatus}
            </th>
            <th scope="col" className="fw-normal text-nowrap">
              {translatedFields.sortingReason}
            </th>
            <th scope="col" className="fw-normal text-nowrap">
              {translatedFields.auditPersonnel}
            </th>
            <th scope="col" className="fw-normal text-nowrap">
              {translatedFields.auditTime}
            </th>
          </tr>
        </thead>
        <tbody>
          {items.map((item, index) => {
            return (
              <tr key={item.id} className="text-nowrap">
                <td>
                  {item.previousSortState && item.finalSortState && (
                    <div className="row flex-nowrap">
                      <div className="col-auto">
                        {translatedFields.enums.sortState[item.finalSortState]}
                      </div>
                      <div className="col-auto">
                        <i
                          className={classNames(
                            'bi bi-arrow-left',
                            index === 0 ? 'text-primary' : 'text-secondary',
                          )}
                        ></i>
                      </div>
                      <div className="col-auto text-secondary">
                        {
                          translatedFields.enums.sortState[
                            item.previousSortState
                          ]
                        }
                      </div>
                    </div>
                  )}
                </td>
                <td title={item.sortReason || ''} className="text-wrap">
                  <div
                    dangerouslySetInnerHTML={{
                      __html: item.sortReason || '',
                    }}
                  ></div>
                </td>
                <td>{`${item.reviewer.alias} (${item.reviewer.id})`}</td>
                <td>
                  <time dateTime={item.createdOn} title={item.createdOn}>
                    {toRelativeTime(item.createdOn)}
                  </time>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

const CompositeStateIcon = ({
  compositeState,
}: {
  compositeState: TPostCompositeState;
}) => {
  return (
    <>
      {compositeState === 'APPROVED' && (
        <i className="bi bi-patch-check-fill text-success"></i>
      )}

      {compositeState === 'PENDING' && (
        <i className="bi bi-patch-check-fill text-secondary"></i>
      )}

      {compositeState === 'DENIED' && (
        <i className="bi bi-patch-check-fill text-danger"></i>
      )}

      {compositeState === 'CLOSE' && (
        <i className="bi bi-patch-check-fill text-black"></i>
      )}

      {compositeState === 'SHOW' && (
        <i className="bi bi-patch-check text-success"></i>
      )}

      {compositeState === 'HIDE' && (
        <i className="bi bi-patch-check text-danger"></i>
      )}

      {compositeState === 'LOCK' && (
        <i className="bi bi-patch-check text-secondary"></i>
      )}
    </>
  );
};
