import type { IPost } from '@/interfaces';
import Image from 'next/image';
import Link from 'next/link';
import { getCover } from '@/lib/tool';
import { useContext } from 'react';
import { AppContext } from '@/contexts/app';

export default function PostCover({ item }: { item: IPost }) {
  const { id, cover, images } = item;
  const context = useContext(AppContext);
  const metadata = context.metadata!;
  const env = metadata.env;

  return (
    <>
      {cover && (
        <div
          className="flex-shrink-0 ratio ratio-16x9"
          style={{ width: 260, height: 146 }}
        >
          <Link href={`/posts/${id}`}>
            <Image
              className="rounded object-fit-contain"
              src={getCover(cover, metadata)}
              alt="cover"
              width={260}
              height={146}
              placeholder="blur"
              blurDataURL={env.APP_BLUR_DATA_URL}
            />
          </Link>
        </div>
      )}
    </>
  );
}
