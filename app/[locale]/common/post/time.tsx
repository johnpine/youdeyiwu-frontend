import type { IPost } from '@/interfaces';

export default function PostTime({ item }: { item: IPost }) {
  const { contentUpdatedOn, _contentUpdatedOnText } = item;

  return (
    <>
      {_contentUpdatedOnText && (
        <div className="cursor-default text-secondary">
          <time dateTime={contentUpdatedOn}>{_contentUpdatedOnText}</time>
        </div>
      )}
    </>
  );
}
