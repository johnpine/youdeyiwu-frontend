import { useTranslations } from 'use-intl';

export default function DefaultErrorPage({
  isLoading,
  errorDetails,
  reset,
}: {
  isLoading?: boolean;
  errorDetails: {
    message: string;
    status: number;
    code: number;
    digest?: string;
  };
  reset?: () => void;
}) {
  const t = useTranslations('clientPage');

  function onClickReset() {
    if (typeof reset === 'function') {
      reset();
    } else {
      location.reload();
    }
  }

  return (
    <div className="col px-0 vh-100">
      <div className="card border-0 h-100">
        <div className="card-body align-items-center card-body d-flex justify-content-center text-center">
          <div>
            <h1 className="text-primary display-5">
              ERROR - ENCOUNTERED ERROR
            </h1>
            <p className="lead">{t('sorryEncounteredError')}</p>

            {isLoading ? (
              <div className="hstack gap-3 justify-content-center align-items-center">
                <div>{t('loadingErrorDetails')}...</div>
                <div
                  className="spinner-border text-danger"
                  role="status"
                  aria-hidden="true"
                ></div>
              </div>
            ) : (
              <p className="text-danger">
                {t('details')}：
                {errorDetails.digest && `[${errorDetails.digest}]`}
                &nbsp;
                {errorDetails.message}
              </p>
            )}

            <div className="hstack flex-wrap justify-content-center my-5 gap-4">
              <a
                href="/"
                className="fw-bold text-primary text-decoration-none cursor-pointer"
              >
                <div className="border border-2 border-primary rounded-5 px-4 py-2 d-inline-block">
                  {t('backToHome')}
                </div>
              </a>

              <a
                onClick={onClickReset}
                className="fw-bold text-primary text-decoration-none cursor-pointer"
              >
                <div className="border border-2 border-primary rounded-5 px-4 py-2 d-inline-block">
                  {t('refreshPage')}
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
