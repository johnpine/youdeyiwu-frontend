import styles from '@/app/[locale]/common/nodata/nodata.module.scss';
import classNames from 'classnames';

export default function Nodata({
  title = 'No data',
  name = 'nodata',
  summary,
  isVh100,
}: {
  title?: string;
  name?: string;
  summary?: string;
  isVh100?: boolean;
}) {
  return (
    <div className="row">
      <div
        className={classNames('col px-0', {
          'd-flex align-items-center justify-content-center': isVh100,
        })}
      >
        <div
          title={title}
          className={classNames(
            'card animate__animated animate__zoomIn',
            styles.card,
            {
              'vh-100 vw-100': isVh100,
            },
          )}
        >
          <div
            className={classNames(
              'card-body d-flex flex-column gap-2 align-items-center justify-content-center text-center user-select-none text-body-tertiary',
              styles.box,
            )}
          >
            <div className="flex-shrink-0">
              <i className="bi bi-box fs-2"></i>
            </div>
            <div>{name}</div>
            {summary && <div className="small">{summary}</div>}
          </div>
        </div>
      </div>
    </div>
  );
}
