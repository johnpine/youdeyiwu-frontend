import type { ReactNode } from 'react';
import classNames from 'classnames';

export default function H5Box({
  classs,
  children,
}: {
  classs?: string;
  children: ReactNode;
}) {
  return (
    <div className={classNames('d-block d-lg-none', classs)}>{children}</div>
  );
}
