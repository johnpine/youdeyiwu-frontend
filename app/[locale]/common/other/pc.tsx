import type { ReactNode } from 'react';
import classNames from 'classnames';

export default function PcBox({
  classs,
  children,
}: {
  classs?: string;
  children: ReactNode;
}) {
  return (
    <div className={classNames('d-none d-lg-block', classs)}>{children}</div>
  );
}
