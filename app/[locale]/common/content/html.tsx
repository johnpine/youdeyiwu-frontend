import Nodata from '@/app/[locale]/common/nodata/nodata';
import classNames from 'classnames';
import useContentLayer from '@/hooks/useContentLayer';
import type { TMetadata } from '@/types';
import { useContext, useRef } from 'react';
import { AppContext } from '@/contexts/app';

export default function ContentHtml({
  content,
  nodata,
  classs,
  metadata,
}: {
  content: string | undefined | null;
  nodata?: boolean;
  classs?: string;
  metadata?: TMetadata;
}) {
  const context = useContext(AppContext);
  const contentRef = useRef<HTMLDivElement>(null);

  useContentLayer({
    contentRef,
    appOssServer:
      metadata?.env.APP_OSS_SERVER ?? context?.metadata?.env.APP_OSS_SERVER,
  });

  if (content) {
    return (
      <div
        ref={contentRef}
        className={classNames('content-layer overflow-auto', classs)}
        dangerouslySetInnerHTML={{ __html: content }}
      />
    );
  }

  if (nodata) {
    return <Nodata />;
  }

  return <></>;
}
