import classNames from 'classnames';
import styles from '@/app/[locale]/home/home.module.scss';
import SectionLeftHome from '@/app/[locale]/home/section';
import TagLeftHome from '@/app/[locale]/home/tag';
import SectionGroupLeftHome from '@/app/[locale]/home/sectionGroup';

export default function LeftHome() {
  return (
    <div
      className={classNames(
        'd-none d-lg-block col-2 ms-2 position-sticky overflow-y-auto mt-4',
        styles.pc_left,
      )}
    >
      <div className="vstack gap-4">
        <SectionGroupLeftHome />
        <SectionLeftHome />
        <TagLeftHome />
      </div>
    </div>
  );
}
