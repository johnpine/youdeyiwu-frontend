import classNames from 'classnames';
import styles from '@/app/[locale]/home/home.module.scss';
import { useContext } from 'react';
import { HomePageContext } from '@/contexts/home';
import type { ISectionClient } from '@/interfaces';
import { getCover, getQueryStrings } from '@/lib/tool';
import Image from 'next/image';
import { AppContext } from '@/contexts/app';
import { useRouter, useSearchParams } from 'next/navigation';

export default function SectionLeftHome(this: any) {
  const {
    source: {
      sections,
      sectionDetails,
      queryParams: { sectionGroupId },
    },
    translatedFields,
  } = useContext(HomePageContext)!;
  const appContext = useContext(AppContext);
  const metadata = appContext.metadata!;
  const env = metadata.env;
  const router = useRouter();
  const searchParams = useSearchParams();
  const currentSectionItem = sectionDetails?.basic;
  const items = sections.filter((item) =>
    sectionGroupId && item.sectionGroup
      ? sectionGroupId === item.sectionGroup.id + ''
      : true,
  );

  function onClickItem(item: ISectionClient | undefined) {
    let url;
    if (item) {
      url = `?${getQueryStrings(
        {
          sid: item.id,
        },
        searchParams,
      )}`;
    } else {
      url = `?${getQueryStrings({}, searchParams, ['sectionId', 'sid'], true)}`;
    }
    router.push(url);
  }

  return (
    <div className="row">
      <div className="col px-0">
        <div className="card border-0">
          <div className="card-body vstack gap-3">
            <div
              onClick={onClickItem.bind(this, undefined)}
              className={classNames(
                currentSectionItem ? 'cursor-pointer' : 'yw-bg cursor-default',
              )}
            >
              <div
                className={classNames(
                  'd-flex align-items-center gap-2 p-2 rounded',
                  styles.pc_left_section,
                )}
              >
                <div
                  className="d-flex rounded justify-content-center align-items-center flex-shrink-0 yw-bg"
                  style={{ width: 75, height: 56 }}
                >
                  <i className="bi bi-journals fs-2"></i>
                </div>
                <div className="text-truncate">
                  {translatedFields.allContent}
                </div>
              </div>
            </div>

            {items.map((item) => {
              return (
                <div
                  title={item.overview ? item.overview : item.name}
                  onClick={onClickItem.bind(this, item)}
                  key={item.id}
                  className={classNames(
                    item.id === currentSectionItem?.id
                      ? 'yw-bg cursor-default'
                      : 'cursor-pointer',
                  )}
                >
                  <div
                    className={classNames(
                      'd-flex align-items-center gap-2 p-2 rounded',
                      styles.pc_left_section,
                    )}
                  >
                    <div
                      className="d-flex rounded justify-content-center align-items-center flex-shrink-0 yw-bg ratio ratio-4x3 overflow-hidden"
                      style={{ width: 75, height: 56 }}
                    >
                      {item.cover ? (
                        <Image
                          className="rounded object-fit-contain"
                          src={getCover(item.cover, metadata)}
                          alt={item.name}
                          width={75}
                          height={56}
                          placeholder="blur"
                          blurDataURL={env.APP_BLUR_DATA_URL}
                        />
                      ) : (
                        <i className="bi bi-journals fs-2 justify-content-center d-inline-flex align-items-center"></i>
                      )}
                    </div>
                    <div
                      className={classNames(
                        'flex-grow-1 vstack gap-2 overflow-hidden',
                        item.overview
                          ? 'justify-content-between'
                          : 'justify-content-center',
                      )}
                    >
                      <div className="text-truncate">{item.name}</div>
                      {item.overview && (
                        <div
                          className="text-truncate small text-secondary"
                          dangerouslySetInnerHTML={{
                            __html: item.overview,
                          }}
                        ></div>
                      )}
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
