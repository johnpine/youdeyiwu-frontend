import classNames from 'classnames';
import styles from '@/app/[locale]/home/home.module.scss';
import { useContext } from 'react';
import { HomePageContext } from '@/contexts/home';
import type { ISectionGroup } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import { getQueryStrings } from '@/lib/tool';
import { useRouter, useSearchParams } from 'next/navigation';

export default function SectionGroupLeftHome(this: any) {
  const searchParams = useSearchParams();
  const router = useRouter();
  const {
    source: {
      sections,
      queryParams: { sectionGroupId },
    },
    translatedFields,
  } = useContext(HomePageContext)!;
  const sectionGroups: ISectionGroup[] = sections
    .filter((item) => !!item.sectionGroup)
    .filter(
      (item, index, array) =>
        array.findIndex(
          (value) => value.sectionGroup!.id === item.sectionGroup!.id,
        ) === index,
    )
    .map((item) => item.sectionGroup)
    .sort((a, b) => a!.sort - b!.sort) as [];
  const currentSectionGroupItem = sectionGroups.find(
    (item) => item.id + '' === sectionGroupId,
  );

  function onClickItem(item: ISectionGroup | undefined) {
    let url;
    if (item) {
      url = `?${getQueryStrings(
        {
          sgid: item.id,
        },
        searchParams,
      )}`;
    } else {
      url = `?${getQueryStrings(
        {},
        searchParams,
        ['sectionGroupId', 'sgid'],
        true,
      )}`;
    }
    router.push(url);
  }

  return (
    <>
      {sectionGroups.length > 0 && (
        <div className="row">
          <div className="col px-0">
            <div className="card border-0">
              <div className="card-body vstack gap-3">
                <div
                  onClick={onClickItem.bind(this, undefined)}
                  className={classNames(
                    currentSectionGroupItem
                      ? 'cursor-pointer'
                      : 'yw-bg cursor-default',
                  )}
                >
                  <div
                    className={classNames(
                      'd-flex align-items-center gap-2 p-2 rounded',
                      styles.pc_left_section,
                    )}
                  >
                    <div
                      className="d-flex rounded justify-content-center align-items-center flex-shrink-0 yw-bg"
                      style={{ width: 75, height: 56 }}
                    >
                      <i className="bi bi-journals fs-2"></i>
                    </div>
                    <div className="text-truncate">
                      {translatedFields.relatedContent}
                    </div>
                  </div>
                </div>

                {sectionGroups.map((item) => {
                  return (
                    <div
                      title={item.name}
                      onClick={onClickItem.bind(this, item)}
                      key={item.id}
                      className={classNames(
                        item.id === currentSectionGroupItem?.id
                          ? 'yw-bg cursor-default'
                          : 'cursor-pointer',
                      )}
                    >
                      <div
                        className={classNames(
                          'd-flex align-items-center gap-2 p-2 rounded',
                          styles.pc_left_section,
                        )}
                      >
                        <div
                          className="d-flex rounded justify-content-center align-items-center flex-shrink-0 yw-bg ratio ratio-4x3 overflow-hidden"
                          style={{ width: 75, height: 56 }}
                        >
                          <i className="bi bi-journals fs-2 justify-content-center d-inline-flex align-items-center"></i>
                        </div>
                        <div className="text-truncate">{item.name}</div>
                      </div>
                    </div>
                  );
                })}

                {sectionGroups.length === 0 && <Nodata />}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
