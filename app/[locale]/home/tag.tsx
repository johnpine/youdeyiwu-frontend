import classNames from 'classnames';
import styles from '@/app/[locale]/home/home.module.scss';
import Link from 'next/link';
import { useContext } from 'react';
import { HomePageContext } from '@/contexts/home';
import type { ITag } from '@/interfaces';
import { useRouter, useSearchParams } from 'next/navigation';
import { getQueryStrings } from '@/lib/tool';

export default function TagLeftHome(this: any) {
  const {
    source: {
      sectionDetails,
      queryParams: { tagId },
    },
    translatedFields,
  } = useContext(HomePageContext)!;
  const router = useRouter();
  const searchParams = useSearchParams();
  const currentSectionItem = sectionDetails?.basic;
  const tags = sectionDetails?.tags ?? [];
  const currentTagItem = tags.find((item) => item.id + '' === tagId);

  function onClickItem(item: ITag | undefined) {
    let url;
    if (item) {
      url = `?${getQueryStrings(
        {
          tid: item.id,
        },
        searchParams,
      )}`;
    } else {
      url = `?${getQueryStrings({}, searchParams, ['tagId', 'tid'], true)}`;
    }
    router.push(url);
  }

  return (
    <div className="row">
      <div className="col px-0">
        <div className="card border-0">
          <div className="card-body vstack gap-3">
            <div className="d-flex align-items-center fs-5 fw-semibold gap-2">
              <span className="text-truncate">
                {currentSectionItem?.name ?? translatedFields.relatedTags}
              </span>
              {currentSectionItem && (
                <Link
                  href={`/sections/${currentSectionItem.id}`}
                  className="text-decoration-none link-dark flex-grow-1 text-end"
                >
                  <i
                    className={classNames(
                      'bi bi-link fs-4',
                      styles.pc_left_section_arrow,
                    )}
                  ></i>
                </Link>
              )}
            </div>

            <div
              onClick={onClickItem.bind(this, undefined)}
              className={classNames(
                currentTagItem ? 'cursor-pointer' : 'yw-bg cursor-default',
              )}
            >
              <div
                className={classNames(
                  'd-flex align-items-center gap-2 p-2 rounded',
                  styles.pc_left_section,
                )}
              >
                <div
                  className="d-flex rounded justify-content-center align-items-center flex-shrink-0 yw-bg"
                  style={{ width: 75, height: 56 }}
                >
                  <i className="bi bi-tag fs-2"></i>
                </div>
                <div className="text-truncate">{translatedFields.all}</div>
              </div>
            </div>

            {tags.map((item) => {
              return (
                <div
                  title={item.name}
                  onClick={onClickItem.bind(this, item)}
                  key={item.id}
                  className={classNames(
                    'd-flex align-items-center gap-2 p-2 rounded',
                    styles.pc_left_section,
                    item.id === currentTagItem?.id
                      ? 'yw-bg cursor-default'
                      : 'cursor-pointer',
                  )}
                >
                  <div
                    className="d-flex rounded justify-content-center align-items-center flex-shrink-0 yw-bg"
                    style={{ width: 75, height: 56 }}
                  >
                    <i className="bi bi-tag fs-2"></i>
                  </div>
                  <div className="flex-grow-1 overflow-hidden">
                    <div className="text-truncate">{item.name}</div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
