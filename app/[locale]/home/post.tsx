import type { IPost } from '@/interfaces';
import PostList from '@/app/[locale]/common/post/list';

export default function PostHome({
  items = [],
  translatedFields,
}: {
  items?: IPost[];
  translatedFields?: {
    comment: string;
    reply: string;
  };
}) {
  return <PostList items={items} translatedFields={translatedFields} />;
}
