'use client';

import ResetPage from '@/app/[locale]/reset/reset';

export default function Error({
  error,
  reset,
}: {
  error: Error & { digest?: string };
  reset: () => void;
}) {
  return <ResetPage error={error} reset={reset} />;
}
