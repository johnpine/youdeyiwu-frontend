import type { ITag } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryTagTagGroup({
  source,
  translatedFields,
}: {
  source: ITag;
  translatedFields: PrefixedTTranslatedFields<'tagAdminPage'>;
}) {
  const tagGroups = source.tagGroups || [];

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-tags me-2"></i>
        {translatedFields.tagGroup}
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle">
            <thead>
              <tr className="text-nowrap">
                <th scope="col" className="fw-normal">
                  ID
                </th>
                <th scope="col" className="fw-normal">
                  {translatedFields.name}
                </th>
                <th scope="col" className="fw-normal">
                  {translatedFields.sort}
                </th>
              </tr>
            </thead>
            <tbody>
              {tagGroups.map((item) => {
                return (
                  <tr key={item.id} className="text-nowrap">
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.sort}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>

        {tagGroups.length === 0 && <Nodata />}
      </div>
    </div>
  );
}
