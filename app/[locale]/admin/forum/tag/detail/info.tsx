import type { ITag } from '@/interfaces';
import GeneralizedProperties from '@/app/[locale]/admin/common/generalized-properties';
import PrimaryProperties from '@/app/[locale]/admin/common/primary-properties';
import { useState } from 'react';
import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QuerySectionInfo({
  source,
  translatedFields,
}: {
  source: ITag;
  translatedFields: PrefixedTTranslatedFields<'tagAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-info-square me-2"></i>
        {source.name}
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle caption-top">
            <caption>
              <ToggleProperties
                isTable={false}
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
                items={[
                  {
                    index: 0,
                    name: translatedFields.mainAttribute,
                  },
                  {
                    index: 1,
                    name: translatedFields.otherAttribute,
                  },
                ]}
              />
            </caption>
            <tbody>
              {tabIndex === 0 && (
                <PrimaryProperties
                  source={[
                    {
                      field: 'name',
                      translatedField: translatedFields.name,
                      value: `${source.name} (ID. ${source.id})`,
                    },
                    {
                      field: 'tagGroup',
                      translatedField: translatedFields.tagGroup,
                      value: (source.tagGroups || [])
                        .map((item) => `${item.name} (ID. ${item.id})`)
                        .join(', '),
                    },
                  ]}
                />
              )}

              {tabIndex === 1 && (
                <GeneralizedProperties
                  source={source}
                  translatedFields={translatedFields.generalizedProperties}
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
