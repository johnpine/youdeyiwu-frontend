import { queryTagSections } from '@/services/api';
import { useQuery } from '@tanstack/react-query';
import type { IPagination, IQueryParams, ISection, ITag } from '@/interfaces';
import { useState } from 'react';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Alert from '@/app/[locale]/alert/alert';
import AlertLoad from '@/app/[locale]/alert/load';
import Pagination from '@/app/[locale]/admin/common/pagination';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryTagSections({
  source,
  translatedFields,
}: {
  source: ITag;
  translatedFields: PrefixedTTranslatedFields<'tagAdminPage'>;
}) {
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });

  const queryTagSectionsQuery = useQuery(
    ['/forum', '/tags', source.id, '/sections', params],
    async (context) => {
      return (await queryTagSections({
        id: context.queryKey[2] + '',
        query: context.queryKey[4] as any,
      })) as IPagination<ISection>;
    },
    {
      keepPreviousData: true,
    },
  );

  function onClickPrevious() {
    const data = queryTagSectionsQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryTagSectionsQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-card-list me-2"></i>
        {translatedFields.section}
      </div>
      <div className="card-body vstack gap-4">
        {queryTagSectionsQuery.data &&
          queryTagSectionsQuery.data.content.length > 0 && (
            <div className="table-responsive">
              <table className="table align-middle">
                <thead>
                  <tr className="text-nowrap">
                    <th scope="col" className="fw-normal">
                      ID
                    </th>
                    <th scope="col" className="fw-normal">
                      {translatedFields.name}
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {queryTagSectionsQuery.data.content.map((item) => {
                    return (
                      <tr key={item.id} className="text-nowrap">
                        <td>{item.id}</td>
                        <td>{item.name}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          )}

        {queryTagSectionsQuery.data &&
          queryTagSectionsQuery.data.content.length === 0 && <Nodata />}

        {queryTagSectionsQuery.data && (
          <Pagination
            isShow={queryTagSectionsQuery.data.pageable.pages > 0}
            onPrevious={onClickPrevious}
            onNext={onClickNext}
            isPrevious={queryTagSectionsQuery.data.pageable.previous}
            isNext={queryTagSectionsQuery.data.pageable.next}
            translatedFields={translatedFields.pagination}
          />
        )}

        {queryTagSectionsQuery.isError && (
          <Alert message={queryTagSectionsQuery.error} type="error" />
        )}

        {queryTagSectionsQuery.isLoading && <AlertLoad />}
      </div>
    </div>
  );
}
