'use client';

import type { ITag } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QuerySectionInfo from '@/app/[locale]/admin/forum/tag/detail/info';
import QueryTagTagGroup from '@/app/[locale]/admin/forum/tag/detail/tag-group';
import QueryTagSections from '@/app/[locale]/admin/forum/tag/detail/sections';
import QueryTagPosts from '@/app/[locale]/admin/forum/tag/detail/posts';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailTagAdminPage({
  source,
  translatedFields,
}: {
  source: ITag;
  translatedFields: PrefixedTTranslatedFields<'tagAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QuerySectionInfo
            source={source}
            translatedFields={translatedFields}
          />

          <QueryTagTagGroup
            source={source}
            translatedFields={translatedFields}
          />

          <div className="row">
            <div className="col">
              <QueryTagSections
                source={source}
                translatedFields={translatedFields}
              />
            </div>
            <div className="col">
              <QueryTagPosts
                source={source}
                translatedFields={translatedFields}
              />
            </div>
          </div>
        </div>
      </div>
    </Box>
  );
}
