'use client';

import useToast from '@/hooks/useToast';
import { type ChangeEvent, type FormEvent, useEffect, useState } from 'react';
import type { IDifference, ITag, ITagGroup } from '@/interfaces';
import { useMutation } from '@tanstack/react-query';
import { createTagTagGroup, removeTagTagGroup } from '@/services/api';
import diff from 'microdiff';
import { clearFormData, getDiffData, isNum } from '@/lib/tool';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateTagTagGroup(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: ITag;
    translatedFields: PrefixedTTranslatedFields<'tagAdminPage'>;
  },
) {
  const { show } = useToast();
  const [form, setForm] = useState<{
    id: string;
    sort: number;
  }>({
    id: '',
    sort: 0,
  });
  const [differenceData, setDifferenceData] = useState<IDifference[]>([]);
  const [currentRemoveItem, setCurrentRemoveItem] = useState<ITagGroup>();
  const [isDisabledSave, setIsDisabledSave] = useState(true);

  const createTagTagGroupMutation = useMutation(createTagTagGroup);
  const removeTagTagGroupMutation = useMutation(removeTagTagGroup);

  useEffect(() => {
    const diffData = diff(
      {
        id: '',
        sort: 0,
      },
      {
        id: isNum(form.id) ? form.id : '',
        sort: form.sort,
      },
    );
    setDifferenceData(diffData);
    setIsDisabledSave(diffData.length === 0);
  }, [form]);

  function checkForm() {
    const id = form.id;

    if (!id) {
      throw translatedFields.idRequired;
    }

    if (!isNum(id)) {
      throw translatedFields.invalidIdFormat;
    }
  }

  async function onClickSave(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      checkForm();

      const tagId = source.id + '';
      const data = getDiffData(differenceData);
      const id = data.id;
      const sort = data.sort;
      await createTagTagGroupMutation.mutateAsync({
        id,
        data: {
          tagId,
          sort,
        },
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.updateCompleted,
      });
    } catch (e) {
      createTagTagGroupMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setForm(clearFormData(form));
      setDifferenceData([]);
      setCurrentRemoveItem(undefined);
    }
  }

  async function onClickRemove(item: ITagGroup) {
    try {
      setCurrentRemoveItem(item);

      const id = item.id;
      const tagId = source.id + '';
      await removeTagTagGroupMutation.mutateAsync({
        id,
        data: {
          tagId,
        },
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.deleteCompleted,
      });
    } catch (e) {
      removeTagTagGroupMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setCurrentRemoveItem(undefined);
    }
  }

  function onChangeForm(e: ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
    const name = e.target.name;
    const value = e.target.value;
    setForm({ ...form, [name]: value });
  }

  return (
    <Box>
      <div className="vstack gap-4">
        {(source.tagGroups || []).length > 0 && (
          <div className="table-responsive">
            <table className="table align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="fw-normal">
                    ID
                  </th>
                  <th scope="col" className="fw-normal">
                    {translatedFields.name}
                  </th>
                  <th scope="col" className="fw-normal">
                    {translatedFields.sort}
                  </th>
                  <th scope="col" className=""></th>
                </tr>
              </thead>
              <tbody>
                {(source.tagGroups || []).map((item) => {
                  return (
                    <tr key={item.id} className="text-nowrap">
                      <td>{item.id}</td>
                      <td>{item.name}</td>
                      <td>{item.sort}</td>
                      <td>
                        <button
                          disabled={
                            currentRemoveItem &&
                            currentRemoveItem.id === item.id &&
                            removeTagTagGroupMutation.isLoading
                          }
                          onClick={onClickRemove.bind(this, item)}
                          className="btn btn-sm btn-danger"
                          type="button"
                        >
                          {currentRemoveItem &&
                          currentRemoveItem.id === item.id &&
                          removeTagTagGroupMutation.isLoading ? (
                            <Spinner classs="me-2" />
                          ) : (
                            <i className="bi bi-trash me-2"></i>
                          )}
                          {translatedFields.delete}
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        )}

        <form onSubmit={onClickSave} className="vstack gap-4">
          <div>
            <label className="form-label">ID</label>
            <input
              type="text"
              className="form-control"
              name="id"
              value={form.id}
              onChange={onChangeForm}
              aria-describedby="id"
              placeholder={translatedFields.idPlaceholder}
            />
          </div>

          <div>
            <label className="form-label">{translatedFields.sort}</label>
            <input
              min={0}
              name="sort"
              type="number"
              className="form-control"
              value={form.sort}
              onChange={onChangeForm}
              aria-describedby="sort"
              placeholder={translatedFields.sortPlaceholder}
            />
          </div>

          <button
            type="submit"
            disabled={createTagTagGroupMutation.isLoading || isDisabledSave}
            className="btn btn-success col col-lg-2 my-4"
          >
            {createTagTagGroupMutation.isLoading && <Spinner classs="me-2" />}
            {translatedFields.add}
          </button>
        </form>
      </div>
    </Box>
  );
}
