'use client';

import useToast from '@/hooks/useToast';
import { type ChangeEvent, type FormEvent, useEffect, useState } from 'react';
import type { IDifference, IPagination, ITag } from '@/interfaces';
import { useMutation } from '@tanstack/react-query';
import { updateTag } from '@/services/api';
import diff from 'microdiff';
import { getDiffData } from '@/lib/tool';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateTagInfo({
  source,
  tags,
  translatedFields,
}: {
  source: ITag;
  tags: IPagination<ITag>;
  translatedFields: PrefixedTTranslatedFields<'tagAdminPage'>;
}) {
  const { show } = useToast();
  const [form, setForm] = useState<{
    name: string;
  }>({
    name: source.name,
  });
  const [differenceData, setDifferenceData] = useState<IDifference[]>([]);
  const [isDisabledSave, setIsDisabledSave] = useState(true);

  const updateTagMutation = useMutation(updateTag);

  useEffect(() => {
    const diffData = diff(
      {
        name: source.name,
      },
      form,
    );
    setDifferenceData(diffData);
    setIsDisabledSave(diffData.length === 0);
  }, [form, source]);

  function checkName(name?: string) {
    if (
      (name && name === source.name) ||
      tags.content.find((item) => item.name === name)
    ) {
      throw translatedFields.nameDuplicated;
    }
  }

  async function onClickSave(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      const id = source.id;
      const data = getDiffData(differenceData) as any;

      checkName(data.name);

      await updateTagMutation.mutateAsync({
        id,
        data,
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.updateCompleted,
      });
    } catch (e) {
      updateTagMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setDifferenceData([]);
    }
  }

  function onChangeForm(
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) {
    const name = e.target.name;
    const value = e.target.value;
    setForm({ ...form, [name]: value });
  }

  return (
    <Box>
      <form onSubmit={onClickSave} className="vstack gap-4">
        <div>
          <label className="form-label">{translatedFields.name}</label>
          <input
            type="text"
            className="form-control"
            name="name"
            value={form.name}
            onChange={onChangeForm}
            aria-describedby="name"
            placeholder={translatedFields.namePlaceholder}
          />
        </div>

        <button
          type="submit"
          disabled={updateTagMutation.isLoading || isDisabledSave}
          className="btn btn-success col col-lg-2 my-4"
        >
          {updateTagMutation.isLoading && <Spinner classs="me-2" />}
          {translatedFields.update}
        </button>
      </form>
    </Box>
  );
}
