import type { Metadata as MetadataNext } from 'next';
import { createError, getMetadata, isNum } from '@/lib/tool';
import { getTranslatedFields } from '@/lib/dictionaries';
import { authMiddleware } from '@/lib/api';
import { cookies } from 'next/headers';
import { queryAllTag, queryPath, queryTagDetails } from '@/services/api';
import type { IPagination, IPath, ITag } from '@/interfaces';
import ResetPage from '@/app/[locale]/reset/reset';
import { notFound } from 'next/navigation';
import UpdateTagInfo from '@/app/[locale]/admin/forum/tag/update/info';
import { getTranslator } from 'next-intl/server';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('tagAdminPage.updateTag') });
}

async function getData(id: string) {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/',
      },
    });
    const req2 = queryTagDetails({
      baseURL: process.env.APP_API_SERVER,
      token,
      id,
    });
    const req3 = queryAllTag({
      baseURL: process.env.APP_API_SERVER,
      token,
    });

    const responses = await Promise.all([req1, req2, req3]);
    const resp1 = await ((await responses[0]) as Response).json();
    const resp2 = await ((await responses[1]) as Response).json();
    const resp3 = await ((await responses[2]) as Response).json();

    return {
      isSuccess: true,
      isError: false,
      path: resp1.data as IPath,
      info: resp2.data as ITag,
      list: resp3.data as IPagination<ITag>,
    };
  } catch (e: any) {
    return {
      isSuccess: false,
      isError: true,
      error: e,
    };
  }
}

export default async function Page({
  params: { locale },
  searchParams: { id },
}: {
  params: { locale: string };
  searchParams: { id?: string };
}) {
  if (!id || !isNum(id)) {
    notFound();
  }

  const data = await getData(id);
  if (data.isError) {
    return <ResetPage error={createError(data.error)} />;
  }

  const source = data.info!;
  const tags = data.list!;
  const translatedFields = await getTranslatedFields(locale, 'tagAdminPage');

  return (
    <UpdateTagInfo
      source={source}
      tags={tags}
      translatedFields={translatedFields}
    />
  );
}
