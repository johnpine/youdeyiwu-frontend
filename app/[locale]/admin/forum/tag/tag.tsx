'use client';

import { useQuery } from '@tanstack/react-query';
import { queryAllTag, queryTagStatistics, searchTag } from '@/services/api';
import type {
  IPagination,
  IQueryParams,
  ITag,
  ITagStatistics,
} from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import Alert from '@/app/[locale]/alert/alert';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Pagination from '@/app/[locale]/admin/common/pagination';
import { type ChangeEvent, useState } from 'react';
import { useRouter } from 'next/navigation';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function TagAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IPagination<ITag>;
    translatedFields: PrefixedTTranslatedFields<'tagAdminPage'>;
  },
) {
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });
  const [searchName, setSearchName] = useState('');
  const router = useRouter();

  const queryTagStatisticsQuery = useQuery(
    ['/forum', '/tags', '/statistics'],
    async () => {
      return (await queryTagStatistics()) as ITagStatistics;
    },
  );
  const queryAllTagQuery = useQuery(
    ['/forum', '/tags', params, searchName],
    async (context) => {
      if (searchName) {
        return (await searchTag({
          query: {
            ...(context.queryKey[2] as any),
            name: context.queryKey[3],
          },
        })) as IPagination<ITag>;
      } else {
        return (await queryAllTag({
          query: context.queryKey[2] as any,
        })) as IPagination<ITag>;
      }
    },
    {
      keepPreviousData: true,
      initialData: source,
    },
  );

  function onClickDetail(item: ITag) {
    router.push(`tag/detail?id=${item.id}`);
  }

  function onClickUpdate(item: ITag) {
    router.push(`tag/update?id=${item.id}`);
  }

  function onClickDelete(item: ITag) {
    router.push(`tag/delete?id=${item.id}`);
  }

  function onClickPrevious() {
    const data = queryAllTagQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryAllTagQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  function onChangeSearch(e: ChangeEvent<HTMLInputElement>) {
    const value = e.target.value.trim();
    setSearchName(value.toLowerCase());
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.count}
              </h6>
              <h5 className="card-title">
                {queryTagStatisticsQuery.data &&
                  queryTagStatisticsQuery.data.count}

                {queryTagStatisticsQuery.isError && (
                  <Alert message={queryTagStatisticsQuery.error} type="error" />
                )}

                {queryTagStatisticsQuery.isLoading && <Spinner />}
              </h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col">
        <Box>
          <div className="card">
            <div className="card-header bg-transparent">
              <div className="row row-cols-auto justify-content-between g-2">
                <div className="col">
                  <div className="input-group">
                    <span className="input-group-text">
                      <i className="bi bi-search"></i>
                    </span>
                    <input
                      onChange={onChangeSearch}
                      type="search"
                      className="form-control"
                      placeholder={translatedFields.searchPlaceholder}
                    />
                  </div>
                </div>
                <div className="col"></div>
              </div>
            </div>
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        ID
                      </th>
                      <th scope="col" className="">
                        名称
                      </th>
                      <th scope="col" className=""></th>
                    </tr>
                  </thead>
                  <tbody>
                    {queryAllTagQuery.data.content.map((item) => {
                      return (
                        <tr key={item.id} className="text-nowrap">
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.id}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.name}
                          </td>
                          <td>
                            <div className="hstack gap-2">
                              <button
                                onClick={onClickUpdate.bind(this, item)}
                                className="btn btn-sm btn-light"
                                type="button"
                              >
                                <i className="bi bi-pencil me-2"></i>
                                {translatedFields.update}
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>

              {queryAllTagQuery.data.content.length === 0 && <Nodata />}

              <Pagination
                isShow={queryAllTagQuery.data.pageable.pages > 0}
                onPrevious={onClickPrevious}
                onNext={onClickNext}
                isPrevious={queryAllTagQuery.data.pageable.previous}
                isNext={queryAllTagQuery.data.pageable.next}
                translatedFields={translatedFields.pagination}
              />
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
