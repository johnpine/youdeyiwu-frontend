'use client';

import { useQuery } from '@tanstack/react-query';
import {
  queryAllSectionGroup,
  querySectionGroupStatistics,
} from '@/services/api';
import type { ISectionGroup, ISectionGroupStatistics } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import Alert from '@/app/[locale]/alert/alert';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import { useRouter } from 'next/navigation';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function SectionGroupAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: ISectionGroup[];
    translatedFields: PrefixedTTranslatedFields<'sectionGroupAdminPage'>;
  },
) {
  const router = useRouter();

  const querySectionGroupStatisticsQuery = useQuery(
    ['/forum', '/sectionGroups', '/statistics'],
    async () => {
      return (await querySectionGroupStatistics()) as ISectionGroupStatistics;
    },
  );

  const queryAllSectionGroupQuery = useQuery(
    ['/forum', '/sectionGroups'],
    async () => {
      return (await queryAllSectionGroup()) as ISectionGroup[];
    },
    {
      keepPreviousData: true,
      initialData: source,
    },
  );

  function onClickDetail(item: ISectionGroup) {
    router.push(`section-group/detail?id=${item.id}`);
  }

  function onClickUpdate(item: ISectionGroup) {
    router.push(`section-group/update?id=${item.id}`);
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.count.sectionGroup}
              </h6>
              <h5 className="card-title">
                {querySectionGroupStatisticsQuery.data &&
                  querySectionGroupStatisticsQuery.data.count}

                {querySectionGroupStatisticsQuery.isError && (
                  <Alert
                    message={querySectionGroupStatisticsQuery.error}
                    type="error"
                  />
                )}

                {querySectionGroupStatisticsQuery.isLoading && <Spinner />}
              </h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col">
        <Box>
          <div className="card">
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        ID
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.name}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.sort}
                      </th>
                      <th scope="col" className=""></th>
                    </tr>
                  </thead>
                  <tbody>
                    {queryAllSectionGroupQuery.data.map((item) => {
                      return (
                        <tr key={item.id} className="text-nowrap">
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.id}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.name}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.sort}
                          </td>
                          <td>
                            <div className="hstack gap-2">
                              <button
                                onClick={onClickUpdate.bind(this, item)}
                                className="btn btn-sm btn-light"
                                type="button"
                              >
                                <i className="bi bi-pencil me-2"></i>
                                {translatedFields.operate.update}
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>

              {queryAllSectionGroupQuery.data.length === 0 && <Nodata />}
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
