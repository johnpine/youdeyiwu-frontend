import type { ISectionGroup } from '@/interfaces';
import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import PrimaryProperties from '@/app/[locale]/admin/common/primary-properties';
import GeneralizedProperties from '@/app/[locale]/admin/common/generalized-properties';
import { useState } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QuerySectionGroupInfo({
  source,
  translatedFields,
}: {
  source: ISectionGroup;
  translatedFields: PrefixedTTranslatedFields<'sectionGroupAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-info-square me-2"></i>
        {source.name}
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle caption-top">
            <caption>
              <ToggleProperties
                isTable={false}
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
                items={[
                  {
                    index: 0,
                    name: translatedFields.operate.mainAttribute,
                  },
                  {
                    index: 1,
                    name: translatedFields.operate.otherAttribute,
                  },
                ]}
              />
            </caption>
            <tbody>
              {tabIndex === 0 && (
                <PrimaryProperties
                  source={[
                    {
                      field: 'name',
                      translatedField: translatedFields.properties.name,
                      value: `${source.name} (ID. ${source.id})`,
                    },
                    {
                      field: 'sort',
                      translatedField: translatedFields.properties.sort,
                      value: source.sort,
                    },
                  ]}
                />
              )}

              {tabIndex === 1 && (
                <GeneralizedProperties
                  source={source}
                  translatedFields={translatedFields.generalizedProperties}
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
