import type { ISectionGroup } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QuerySectionGroupSections({
  source,
  translatedFields,
}: {
  source: ISectionGroup;
  translatedFields: PrefixedTTranslatedFields<'sectionGroupAdminPage'>;
}) {
  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-card-list me-2"></i>
        {translatedFields.properties.section}
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle">
            <thead>
              <tr className="text-nowrap">
                <th scope="col" className="fw-normal">
                  ID
                </th>
                <th scope="col" className="fw-normal">
                  {translatedFields.properties.name}
                </th>
                <th scope="col" className="fw-normal">
                  {translatedFields.properties.sort}
                </th>
              </tr>
            </thead>
            <tbody>
              {(source.sections || []).map((item) => {
                return (
                  <tr key={item.id} className="text-nowrap">
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.sort}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>

        {(source.sections || []).length === 0 && <Nodata />}
      </div>
    </div>
  );
}
