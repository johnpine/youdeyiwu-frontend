'use client';

import type { ISectionGroup } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QuerySectionGroupInfo from '@/app/[locale]/admin/forum/section-group/detail/info';
import QuerySectionGroupSections from '@/app/[locale]/admin/forum/section-group/detail/sections';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailSectionGroupAdminPage({
  source,
  translatedFields,
}: {
  source: ISectionGroup;
  translatedFields: PrefixedTTranslatedFields<'sectionGroupAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QuerySectionGroupInfo
            source={source}
            translatedFields={translatedFields}
          />

          <QuerySectionGroupSections
            source={source}
            translatedFields={translatedFields}
          />
        </div>
      </div>
    </Box>
  );
}
