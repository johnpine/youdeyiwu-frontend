'use client';

import useToast from '@/hooks/useToast';
import { type ChangeEvent, type FormEvent, useEffect, useState } from 'react';
import type { IDifference, ISectionDetails, IUser } from '@/interfaces';
import { useMutation } from '@tanstack/react-query';
import {
  removeSectionAdminByUserId,
  updateSectionAdminByUserId,
} from '@/services/api';
import diff from 'microdiff';
import { clearFormData, getDiffData, isNum } from '@/lib/tool';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateSectionAdmin(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: ISectionDetails;
    translatedFields: PrefixedTTranslatedFields<'sectionAdminPage'>;
  },
) {
  const { show } = useToast();
  const [form, setForm] = useState<{
    id: string;
  }>({
    id: '',
  });
  const [differenceData, setDifferenceData] = useState<IDifference[]>([]);
  const [currentRemoveItem, setCurrentRemoveItem] = useState<IUser>();
  const [isDisabledSave, setIsDisabledSave] = useState(true);

  const updateSectionAdminByUserIdMutation = useMutation(
    updateSectionAdminByUserId,
  );
  const removeSectionAdminByUserIdMutation = useMutation(
    removeSectionAdminByUserId,
  );

  useEffect(() => {
    const diffData = diff(
      {
        id: '',
      },
      {
        id: isNum(form.id) ? form.id : '',
      },
    );
    setDifferenceData(diffData);
    setIsDisabledSave(diffData.length === 0);
  }, [form]);

  function checkForm() {
    const id = form.id;

    if (!id) {
      throw translatedFields.updateAdminPage.idEmpty;
    }

    if (!isNum(id)) {
      throw translatedFields.updateAdminPage.idCheck;
    }

    if (source.admins.find((item) => item.id === parseInt(id))) {
      throw translatedFields.updateAdminPage.adminIdExisted;
    }
  }

  async function onClickSave(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      checkForm();

      const data = getDiffData(differenceData);
      const id = source.basic.id;
      const userId = data.userId;
      await updateSectionAdminByUserIdMutation.mutateAsync({
        id,
        data: {
          userId,
        },
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.updateAdminPage.updateCompleted,
      });
    } catch (e) {
      updateSectionAdminByUserIdMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setForm(clearFormData(form));
      setDifferenceData([]);
      setCurrentRemoveItem(undefined);
    }
  }

  async function onClickRemove(item: IUser) {
    try {
      setCurrentRemoveItem(item);

      const id = source.basic.id;
      const userId = item.id + '';
      await removeSectionAdminByUserIdMutation.mutateAsync({
        id,
        data: {
          userId,
        },
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.updateAdminPage.deleteCompleted,
      });
    } catch (e) {
      removeSectionAdminByUserIdMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setForm(clearFormData(form));
      setDifferenceData([]);
      setCurrentRemoveItem(undefined);
    }
  }

  function onChangeForm(e: ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
    const name = e.target.name;
    const value = e.target.value;
    setForm({ ...form, [name]: value });
  }

  return (
    <Box>
      <div className="vstack gap-4">
        <div className="table-responsive">
          <table className="table align-middle">
            <thead>
              <tr className="text-nowrap">
                <th scope="col" className="fw-normal">
                  {translatedFields.updateAdminPage.admin}
                </th>
                <th scope="col" className=""></th>
              </tr>
            </thead>
            <tbody>
              {source.admins.map((item) => {
                return (
                  <tr key={item.id} className="text-nowrap">
                    <td>{`${item.alias} (ID. ${item.id})`}</td>
                    <td>
                      <button
                        disabled={
                          currentRemoveItem &&
                          currentRemoveItem.id === item.id &&
                          removeSectionAdminByUserIdMutation.isLoading
                        }
                        onClick={onClickRemove.bind(this, item)}
                        className="btn btn-sm btn-danger"
                        type="button"
                      >
                        {currentRemoveItem &&
                        currentRemoveItem.id === item.id &&
                        removeSectionAdminByUserIdMutation.isLoading ? (
                          <Spinner classs="me-2" />
                        ) : (
                          <i className="bi bi-trash me-2"></i>
                        )}
                        {translatedFields.updateAdminPage.delete}
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>

        <form onSubmit={onClickSave} className="vstack gap-4">
          <div>
            <label className="form-label">Id</label>
            <input
              type="text"
              className="form-control"
              name="id"
              value={form.id}
              onChange={onChangeForm}
              aria-describedby="id"
              placeholder={translatedFields.updateAdminPage.idPlaceholder}
            />
          </div>

          <button
            type="submit"
            disabled={
              updateSectionAdminByUserIdMutation.isLoading || isDisabledSave
            }
            className="btn btn-success col col-lg-2 my-4"
          >
            {updateSectionAdminByUserIdMutation.isLoading && (
              <Spinner classs="me-2" />
            )}
            {translatedFields.updateAdminPage.add}
          </button>
        </form>
      </div>
    </Box>
  );
}
