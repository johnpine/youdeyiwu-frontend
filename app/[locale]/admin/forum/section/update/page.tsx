import type { Metadata as MetadataNext } from 'next';
import { createError, getMetadata, isNum } from '@/lib/tool';
import { authMiddleware } from '@/lib/api';
import { cookies } from 'next/headers';
import {
  queryAllSection,
  queryPath,
  querySectionDetailsById,
} from '@/services/api';
import type {
  IPagination,
  IPath,
  ISection,
  ISectionDetails,
} from '@/interfaces';
import ResetPage from '@/app/[locale]/reset/reset';
import { notFound } from 'next/navigation';
import UpdateSectionInfo from '@/app/[locale]/admin/forum/section/update/info';
import { getTranslator } from 'next-intl/server';
import { getTranslatedFields } from '@/lib/dictionaries';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('sectionAdminPage.title.updateInfo') });
}

async function getData(id: string) {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/',
      },
    });
    const req2 = querySectionDetailsById({
      baseURL: process.env.APP_API_SERVER,
      token,
      id,
    });
    const req3 = queryAllSection({
      baseURL: process.env.APP_API_SERVER,
      token,
    });

    const responses = await Promise.all([req1, req2, req3]);
    const resp1 = await ((await responses[0]) as Response).json();
    const resp2 = await ((await responses[1]) as Response).json();
    const resp3 = await ((await responses[2]) as Response).json();

    return {
      isSuccess: true,
      isError: false,
      path: resp1.data as IPath,
      info: resp2.data as ISectionDetails,
      list: resp3.data as IPagination<ISection>,
    };
  } catch (e: any) {
    return {
      isSuccess: false,
      isError: true,
      error: e,
    };
  }
}

export default async function Page({
  params: { locale },
  searchParams: { id },
}: {
  params: { locale: string };
  searchParams: { id?: string };
}) {
  if (!id || !isNum(id)) {
    notFound();
  }

  const data = await getData(id);
  if (data.isError) {
    return <ResetPage error={createError(data.error)} />;
  }

  const source = data.info!;
  const sections = data.list!;
  const translatedFields = await getTranslatedFields(
    locale,
    'sectionAdminPage',
  );

  return (
    <UpdateSectionInfo
      source={source}
      sections={sections}
      translatedFields={translatedFields}
    />
  );
}
