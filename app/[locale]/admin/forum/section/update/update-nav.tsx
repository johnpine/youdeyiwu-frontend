'use client';

import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateNav({
  translatedFields,
}: {
  translatedFields: PrefixedTTranslatedFields<'sectionAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);
  const searchParams = useSearchParams();
  const id = searchParams.get('id');

  return (
    <ToggleProperties
      tabIndex={tabIndex}
      setTabIndex={setTabIndex}
      mb4={false}
      isTable={false}
      items={[
        {
          index: 0,
          name: translatedFields.nav.info,
          href: `/admin/forum/section/update?id=${id}`,
          segment: null,
        },
        {
          index: 1,
          name: translatedFields.nav.state,
          href: `/admin/forum/section/update/state?id=${id}`,
          segment: 'state',
        },
        {
          index: 2,
          name: translatedFields.nav.tag,
          href: `/admin/forum/section/update/tag?id=${id}`,
          segment: 'tag',
        },
        {
          index: 3,
          name: translatedFields.nav.tagGroup,
          href: `/admin/forum/section/update/tag-group?id=${id}`,
          segment: 'tag-group',
        },
        {
          index: 4,
          name: translatedFields.nav.sectionGroup,
          href: `/admin/forum/section/update/section-group?id=${id}`,
          segment: 'section-group',
        },
        {
          index: 5,
          name: translatedFields.nav.admin,
          href: `/admin/forum/section/update/admin?id=${id}`,
          segment: 'admin',
        },
      ]}
    />
  );
}
