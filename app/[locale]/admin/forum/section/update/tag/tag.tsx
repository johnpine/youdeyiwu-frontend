'use client';

import useToast from '@/hooks/useToast';
import { type ChangeEvent, type FormEvent, useEffect, useState } from 'react';
import type { IDifference, ISectionDetails, ITag } from '@/interfaces';
import { useMutation } from '@tanstack/react-query';
import { removeSectionTagByName, updateSectionTagByName } from '@/services/api';
import diff from 'microdiff';
import { clearFormData, getDiffData } from '@/lib/tool';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateSectionTag(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: ISectionDetails;
    translatedFields: PrefixedTTranslatedFields<'sectionAdminPage'>;
  },
) {
  const { show } = useToast();
  const [form, setForm] = useState<{
    name: string;
  }>({
    name: '',
  });
  const [differenceData, setDifferenceData] = useState<IDifference[]>([]);
  const [currentRemoveItem, setCurrentRemoveItem] = useState<ITag>();
  const [isDisabledSave, setIsDisabledSave] = useState(true);

  const updateSectionTagByNameMutation = useMutation(updateSectionTagByName);
  const removeSectionTagByNameMutation = useMutation(removeSectionTagByName);

  useEffect(() => {
    const diffData = diff(
      {
        name: '',
      },
      {
        name: form.name,
      },
    );
    setDifferenceData(diffData);
    setIsDisabledSave(diffData.length === 0);
  }, [form]);

  async function onClickSave(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      checkForm();

      const id = source.basic.id;
      const data = getDiffData(differenceData) as any;
      await updateSectionTagByNameMutation.mutateAsync({
        id,
        data,
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.updateTagPage.updateCompleted,
      });
    } catch (e) {
      updateSectionTagByNameMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setForm(clearFormData(form));
      setDifferenceData([]);
    }
  }

  async function onClickRemove(item: ITag) {
    try {
      setCurrentRemoveItem(item);

      const id = source.basic.id;
      const data = getDiffData(differenceData) as any;
      await removeSectionTagByNameMutation.mutateAsync({
        id,
        data,
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.updateTagPage.deleteCompleted,
      });
    } catch (e) {
      removeSectionTagByNameMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setCurrentRemoveItem(undefined);
    }
  }

  function checkForm() {
    const name = form.name;

    if (!name) {
      throw translatedFields.updateTagPage.nameEmpty;
    }

    if (source.tags.find((item) => item.name === name)) {
      throw translatedFields.updateTagPage.nameExisted;
    }
  }

  function onChangeForm(e: ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
    const name = e.target.name;
    const value = e.target.value;
    setForm({ ...form, [name]: value });
  }

  return (
    <Box>
      <div className="vstack gap-4">
        {source.tags.length > 0 && (
          <div className="table-responsive">
            <table className="table align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="fw-normal" colSpan={2}>
                    {translatedFields.updateTagPage.name}
                  </th>
                </tr>
              </thead>
              <tbody>
                {source.tags.map((item) => {
                  return (
                    <tr key={item.id} className="text-nowrap">
                      <td>{item.name}</td>
                      <td>
                        <button
                          disabled={
                            currentRemoveItem &&
                            currentRemoveItem.id === item.id &&
                            removeSectionTagByNameMutation.isLoading
                          }
                          onClick={onClickRemove.bind(this, item)}
                          className="btn btn-sm btn-danger"
                          type="button"
                        >
                          {currentRemoveItem &&
                          currentRemoveItem.id === item.id &&
                          removeSectionTagByNameMutation.isLoading ? (
                            <Spinner classs="me-2" />
                          ) : (
                            <i className="bi bi-trash me-2"></i>
                          )}
                          {translatedFields.updateTagPage.delete}
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        )}

        <form onSubmit={onClickSave} className="vstack gap-4">
          <div>
            <label className="form-label">
              {translatedFields.updateTagPage.name}
            </label>
            <input
              type="text"
              className="form-control"
              name="name"
              value={form.name}
              onChange={onChangeForm}
              aria-describedby="name"
              placeholder={translatedFields.updateTagPage.namePlaceholder}
            />
          </div>

          <button
            type="submit"
            disabled={
              updateSectionTagByNameMutation.isLoading || isDisabledSave
            }
            className="btn btn-success col col-lg-2 my-4"
          >
            {updateSectionTagByNameMutation.isLoading && (
              <Spinner classs="me-2" />
            )}
            {translatedFields.updateTagPage.add}
          </button>
        </form>
      </div>
    </Box>
  );
}
