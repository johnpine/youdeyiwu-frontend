'use client';

import useToast from '@/hooks/useToast';
import { type ChangeEvent, type FormEvent, useEffect, useState } from 'react';
import type { IDifference, ISectionDetails, ISectionGroup } from '@/interfaces';
import { useMutation } from '@tanstack/react-query';
import {
  createSectionGroupSection,
  removeSectionGroupSection,
} from '@/services/api';
import diff from 'microdiff';
import { clearFormData, getDiffData, isNum } from '@/lib/tool';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateSectionSectionGroup(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: ISectionDetails;
    translatedFields: PrefixedTTranslatedFields<'sectionAdminPage'>;
  },
) {
  const { show } = useToast();
  const [form, setForm] = useState<{
    id: string;
  }>({
    id: '',
  });
  const [differenceData, setDifferenceData] = useState<IDifference[]>([]);
  const [currentRemoveItem, setCurrentRemoveItem] = useState<ISectionGroup>();
  const [isDisabledSave, setIsDisabledSave] = useState(true);

  const createSectionGroupSectionMutation = useMutation(
    createSectionGroupSection,
  );
  const removeSectionGroupSectionMutation = useMutation(
    removeSectionGroupSection,
  );

  useEffect(() => {
    const diffData = diff(
      {
        id: '',
      },
      {
        id: isNum(form.id) ? form.id : '',
      },
    );
    setDifferenceData(diffData);
    setIsDisabledSave(diffData.length === 0);
  }, [form]);

  function checkForm() {
    const id = form.id;

    if (!id) {
      throw translatedFields.updateSectionGroupPage.idEmpty;
    }

    if (!isNum(id)) {
      throw translatedFields.updateSectionGroupPage.idCheck;
    }
  }

  async function onClickSave(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      checkForm();

      const data = getDiffData(differenceData);
      const id = data.id;
      const sectionId = source.basic.id + '';
      await createSectionGroupSectionMutation.mutateAsync({
        id,
        data: {
          sectionId,
        },
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.updateSectionGroupPage.updateCompleted,
      });
    } catch (e) {
      createSectionGroupSectionMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setForm(clearFormData(form));
      setDifferenceData([]);
      setCurrentRemoveItem(undefined);
    }
  }

  async function onClickRemove(item: ISectionGroup) {
    try {
      setCurrentRemoveItem(item);

      const id = item.id;
      const sectionId = source.basic.id + '';
      await removeSectionGroupSectionMutation.mutateAsync({
        id,
        data: {
          sectionId,
        },
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.updateSectionGroupPage.deleteCompleted,
      });
    } catch (e) {
      removeSectionGroupSectionMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setCurrentRemoveItem(undefined);
    }
  }

  function onChangeForm(e: ChangeEvent<HTMLSelectElement | HTMLInputElement>) {
    const name = e.target.name;
    const value = e.target.value;
    setForm({ ...form, [name]: value });
  }

  return (
    <Box>
      <div className="vstack gap-4">
        {(source.sectionGroup ? [source.sectionGroup] : []).length > 0 && (
          <div className="table-responsive">
            <table className="table align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="fw-normal">
                    ID
                  </th>
                  <th scope="col" className="fw-normal">
                    {translatedFields.updateSectionGroupPage.name}
                  </th>
                  <th scope="col" className=""></th>
                </tr>
              </thead>
              <tbody>
                {(source.sectionGroup ? [source.sectionGroup] : []).map(
                  (item) => {
                    return (
                      <tr key={item.id} className="text-nowrap">
                        <td>{item.id}</td>
                        <td>{item.name}</td>
                        <td>
                          <button
                            disabled={
                              currentRemoveItem &&
                              currentRemoveItem.id === item.id &&
                              removeSectionGroupSectionMutation.isLoading
                            }
                            onClick={onClickRemove.bind(this, item)}
                            className="btn btn-sm btn-danger"
                            type="button"
                          >
                            {currentRemoveItem &&
                            currentRemoveItem.id === item.id &&
                            removeSectionGroupSectionMutation.isLoading ? (
                              <Spinner classs="me-2" />
                            ) : (
                              <i className="bi bi-trash me-2"></i>
                            )}
                            {translatedFields.updateSectionGroupPage.delete}
                          </button>
                        </td>
                      </tr>
                    );
                  },
                )}
              </tbody>
            </table>
          </div>
        )}

        <form onSubmit={onClickSave} className="vstack gap-4">
          <div>
            <label className="form-label">ID</label>
            <input
              type="text"
              className="form-control"
              name="id"
              value={form.id}
              onChange={onChangeForm}
              aria-describedby="id"
              placeholder={
                translatedFields.updateSectionGroupPage.idPlaceholder
              }
            />
          </div>

          <button
            type="submit"
            disabled={
              createSectionGroupSectionMutation.isLoading || isDisabledSave
            }
            className="btn btn-success col col-lg-2 my-4"
          >
            {createSectionGroupSectionMutation.isLoading && (
              <Spinner classs="me-2" />
            )}
            {translatedFields.updateSectionGroupPage.add}
          </button>
        </form>
      </div>
    </Box>
  );
}
