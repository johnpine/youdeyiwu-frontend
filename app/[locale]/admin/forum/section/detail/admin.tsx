import type { ISectionDetails } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import { type PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QuerySectionAdmin({
  details,
  translatedFields,
}: {
  details: ISectionDetails;
  translatedFields: PrefixedTTranslatedFields<'sectionAdminPage'>;
}) {
  const { admins } = details;

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-people me-2"></i>
        {translatedFields.detailPage.QuerySectionAdmin.admin}
      </div>

      <div className="card-body">
        {admins.length > 0 && (
          <ul className="list-group list-group-flush">
            {admins.map((item) => {
              return (
                <li key={item.id} className="list-group-item">
                  {`${item.alias} (ID. ${item.id})`}
                </li>
              );
            })}
          </ul>
        )}

        {admins.length === 0 && <Nodata />}
      </div>
    </div>
  );
}
