import type { ISectionDetails } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import { type PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QuerySectionTag({
  details,
  translatedFields,
}: {
  details: ISectionDetails;
  translatedFields: PrefixedTTranslatedFields<'sectionAdminPage'>;
}) {
  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-tags me-2"></i>
        {translatedFields.detailPage.QuerySectionTag.tag}
      </div>

      <div className="card-body">
        {details.tags.length > 0 && (
          <ul className="list-group list-group-flush">
            {details.tags.map((item) => {
              return (
                <li key={item.id} className="list-group-item">
                  {item.name}
                </li>
              );
            })}
          </ul>
        )}

        {details.tags.length === 0 && <Nodata />}
      </div>
    </div>
  );
}
