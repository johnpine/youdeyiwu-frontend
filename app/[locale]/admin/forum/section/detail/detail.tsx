'use client';

import QuerySectionInfo from '@/app/[locale]/admin/forum/section/detail/info';
import QuerySectionTag from '@/app/[locale]/admin/forum/section/detail/tag';
import QuerySectionTagGroup from '@/app/[locale]/admin/forum/section/detail/tagGroup';
import QuerySectionSectionGroup from '@/app/[locale]/admin/forum/section/detail/sectionGroup';
import QuerySectionAdmin from '@/app/[locale]/admin/forum/section/detail/admin';
import QuerySectionContent from '@/app/[locale]/admin/forum/section/detail/content';
import type { ISectionDetails } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailSectionAdminPage({
  source,
  translatedFields,
}: {
  source: ISectionDetails;
  translatedFields: PrefixedTTranslatedFields<'sectionAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QuerySectionInfo
            details={source}
            translatedFields={translatedFields}
          />

          <div className="row">
            <div className="col">
              <QuerySectionTag
                details={source}
                translatedFields={translatedFields}
              />
            </div>
            <div className="col">
              <QuerySectionTagGroup
                details={source}
                translatedFields={translatedFields}
              />
            </div>
            <div className="col">
              <QuerySectionSectionGroup
                details={source}
                translatedFields={translatedFields}
              />
            </div>
            <div className="col">
              <QuerySectionAdmin
                details={source}
                translatedFields={translatedFields}
              />
            </div>
          </div>

          <QuerySectionContent
            details={source}
            translatedFields={translatedFields}
          />
        </div>
      </div>
    </Box>
  );
}
