import type { ISectionDetails } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import { type PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QuerySectionSectionGroup({
  details,
  translatedFields,
}: {
  details: ISectionDetails;
  translatedFields: PrefixedTTranslatedFields<'sectionAdminPage'>;
}) {
  const sectionGroup = details.sectionGroup;

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-card-list me-2"></i>
        {translatedFields.detailPage.QuerySectionSectionGroup.sectionGroup}
      </div>

      <div className="card-body">
        {sectionGroup && (
          <ul className="list-group list-group-flush">
            <li key={sectionGroup.id} className="list-group-item">
              {sectionGroup.name}
            </li>
          </ul>
        )}

        {!sectionGroup && <Nodata />}
      </div>
    </div>
  );
}
