import type { ISectionDetails } from '@/interfaces';
import Link from 'next/link';
import GeneralizedProperties from '@/app/[locale]/admin/common/generalized-properties';
import PrimaryProperties from '@/app/[locale]/admin/common/primary-properties';
import { useState } from 'react';
import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import { type PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QuerySectionInfo({
  details,
  translatedFields,
}: {
  details: ISectionDetails;
  translatedFields: PrefixedTTranslatedFields<'sectionAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);

  let stateValue;
  if (details.basic.state === 'SHOW') {
    stateValue = translatedFields.detailPage.QuerySectionInfo.show;
  } else if (details.basic.state === 'HIDE') {
    stateValue = translatedFields.detailPage.QuerySectionInfo.hide;
  } else if (details.basic.state === 'LOCK') {
    stateValue = translatedFields.detailPage.QuerySectionInfo.lock;
  } else if (details.basic.state === 'CLOSE') {
    stateValue = translatedFields.detailPage.QuerySectionInfo.close;
  }

  let otherStatesValue;
  if (details.basic.otherStates) {
    otherStatesValue = details.basic.otherStates.map((value) => {
      if (value === 'DEFAULT') {
        return translatedFields.detailPage.QuerySectionInfo.default;
      } else if (value === 'ALLOW') {
        return translatedFields.detailPage.QuerySectionInfo.allow;
      } else if (value === 'BLOCK') {
        return translatedFields.detailPage.QuerySectionInfo.block;
      } else if (value === 'LOGIN_SEE') {
        return translatedFields.detailPage.QuerySectionInfo.loginSee;
      } else {
        return value;
      }
    });
  }

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-info-square me-2"></i>
        <Link
          href={`/sections/${details.basic.id}`}
          className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
        >
          {details.basic.name}
        </Link>
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle caption-top">
            <caption>
              <ToggleProperties
                isTable={false}
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
                items={[
                  {
                    index: 0,
                    name: translatedFields.detailPage.QuerySectionInfo
                      .mainAttribute,
                  },
                  {
                    index: 1,
                    name: translatedFields.detailPage.QuerySectionInfo
                      .otherAttribute,
                  },
                ]}
              />
            </caption>
            <tbody>
              {tabIndex === 0 && (
                <PrimaryProperties
                  source={[
                    {
                      field: 'name',
                      translatedField:
                        translatedFields.detailPage.QuerySectionInfo.name,
                      value: `${details.basic.name} (ID. ${details.basic.id})`,
                      href: `/sections/${details.basic.id}`,
                    },
                    {
                      field: 'overview',
                      translatedField:
                        translatedFields.detailPage.QuerySectionInfo.overview,
                      value: details.basic.overview,
                    },
                    {
                      field: 'cover',
                      translatedField:
                        translatedFields.detailPage.QuerySectionInfo.cover,
                      value: details.basic.cover,
                    },
                    {
                      field: 'sort',
                      translatedField:
                        translatedFields.detailPage.QuerySectionInfo.sort,
                      value: details.basic.sort,
                    },
                    {
                      field: 'state',
                      translatedField:
                        translatedFields.detailPage.QuerySectionInfo.state,
                      value: stateValue,
                    },
                    {
                      field: 'otherStates',
                      translatedField:
                        translatedFields.detailPage.QuerySectionInfo.otherState,
                      value: otherStatesValue,
                    },
                    {
                      field: 'allow',
                      translatedField:
                        translatedFields.detailPage.QuerySectionInfo.allow,
                      value: details.basic.allow,
                    },
                    {
                      field: 'block',
                      translatedField:
                        translatedFields.detailPage.QuerySectionInfo.block,
                      value: details.basic.block,
                    },
                  ]}
                />
              )}

              {tabIndex === 1 && (
                <GeneralizedProperties
                  source={details.basic}
                  translatedFields={translatedFields.detailPage.Generalized}
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
