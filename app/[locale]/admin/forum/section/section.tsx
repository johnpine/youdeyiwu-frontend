'use client';

import { useQuery } from '@tanstack/react-query';
import {
  queryAllSection,
  querySectionStatistics,
  searchSection,
} from '@/services/api';
import Box from '@/app/[locale]/admin/common/box';
import Alert from '@/app/[locale]/alert/alert';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type {
  IPagination,
  IQueryParams,
  ISection,
  ISectionStatistics,
} from '@/interfaces';
import { useRouter } from 'next/navigation';
import { type ChangeEvent, useState } from 'react';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Pagination from '@/app/[locale]/admin/common/pagination';
import { type PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function SectionAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IPagination<ISection>;
    translatedFields: PrefixedTTranslatedFields<'sectionAdminPage'>;
  },
) {
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });
  const [searchName, setSearchName] = useState('');
  const router = useRouter();

  const querySectionStatisticsQuery = useQuery(
    ['/forum', '/sections', '/statistics'],
    async () => {
      return (await querySectionStatistics()) as ISectionStatistics;
    },
  );
  const queryAllSectionQuery = useQuery(
    ['/forum', '/sections', params, searchName],
    async (context) => {
      if (searchName) {
        return (await searchSection({
          query: {
            ...(context.queryKey[2] as any),
            name: context.queryKey[3],
          },
        })) as IPagination<ISection>;
      } else {
        return (await queryAllSection({
          query: context.queryKey[2] as any,
        })) as IPagination<ISection>;
      }
    },
    {
      keepPreviousData: true,
      initialData: source,
    },
  );

  function onClickDetail(item: ISection) {
    router.push(`section/detail?id=${item.id}`);
  }

  function onClickUpdate(item: ISection) {
    router.push(`section/update?id=${item.id}`);
  }

  function onClickDelete(item: ISection) {
    router.push(`section/delete?id=${item.id}`);
  }

  async function onClickPost(item: ISection) {
    router.push(`/admin/forum/post?sid=${item.id}`);
  }

  function onClickPrevious() {
    const data = queryAllSectionQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryAllSectionQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  function onChangeSearch(e: ChangeEvent<HTMLInputElement>) {
    const value = e.target.value.trim();
    setSearchName(value.toLowerCase());
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.page.total}
              </h6>
              <h5 className="card-title">
                {querySectionStatisticsQuery.data &&
                  querySectionStatisticsQuery.data.count}

                {querySectionStatisticsQuery.isError && (
                  <Alert
                    message={querySectionStatisticsQuery.error}
                    type="error"
                  />
                )}

                {querySectionStatisticsQuery.isLoading && <Spinner />}
              </h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col">
        <Box>
          <div className="card">
            <div className="card-header bg-transparent">
              <div className="row row-cols-auto justify-content-between g-2">
                <div className="col">
                  <div className="input-group">
                    <span className="input-group-text">
                      <i className="bi bi-search"></i>
                    </span>
                    <input
                      onChange={onChangeSearch}
                      type="search"
                      className="form-control"
                      placeholder={translatedFields.page.searchPlaceholder}
                    />
                  </div>
                </div>
                <div className="col"></div>
              </div>
            </div>
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        ID
                      </th>
                      <th scope="col" className="">
                        {translatedFields.page.name}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.page.sort}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.page.state}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.page.otherState}
                      </th>
                      <th scope="col" className=""></th>
                    </tr>
                  </thead>
                  <tbody>
                    {queryAllSectionQuery.data.content.map((item) => {
                      return (
                        <tr key={item.id} className="text-nowrap">
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.id}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.name}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.sort}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.state === 'SHOW' &&
                              translatedFields.page.show}
                            {item.state === 'HIDE' &&
                              translatedFields.page.hide}
                            {item.state === 'LOCK' &&
                              translatedFields.page.lock}
                            {item.state === 'CLOSE' &&
                              translatedFields.page.close}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.otherStates &&
                              (item.otherStates.length === 0
                                ? ['DEFAULT']
                                : item.otherStates
                              )
                                .map((value) => {
                                  if (value === 'DEFAULT') {
                                    return translatedFields.page.default;
                                  } else if (value === 'ALLOW') {
                                    return translatedFields.page.allow;
                                  } else if (value === 'BLOCK') {
                                    return translatedFields.page.block;
                                  } else if (value === 'LOGIN_SEE') {
                                    return translatedFields.page.loginSee;
                                  } else {
                                    return value;
                                  }
                                })
                                .join(',')}
                          </td>
                          <td>
                            <div className="hstack gap-2">
                              <button
                                onClick={onClickPost.bind(this, item)}
                                className="btn btn-sm btn-light"
                                type="button"
                              >
                                <i className="bi bi-view-list me-2"></i>
                                {translatedFields.page.post}
                              </button>

                              <button
                                onClick={onClickUpdate.bind(this, item)}
                                className="btn btn-sm btn-light"
                                type="button"
                              >
                                <i className="bi bi-pencil me-2"></i>
                                {translatedFields.page.update}
                              </button>

                              <button
                                onClick={onClickDelete.bind(this, item)}
                                className="btn btn-sm btn-light text-danger"
                                type="button"
                              >
                                <i className="bi bi-trash me-2"></i>
                                {translatedFields.page.delete}
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>

              {queryAllSectionQuery.data.content.length === 0 && <Nodata />}

              <Pagination
                isShow={queryAllSectionQuery.data.pageable.pages > 0}
                onPrevious={onClickPrevious}
                onNext={onClickNext}
                isPrevious={queryAllSectionQuery.data.pageable.previous}
                isNext={queryAllSectionQuery.data.pageable.next}
                translatedFields={{
                  previous: translatedFields.page.previous,
                  next: translatedFields.page.next,
                }}
              />
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
