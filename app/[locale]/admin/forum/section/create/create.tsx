'use client';

import type { IDifference, IPagination, ISection } from '@/interfaces';
import { createSection } from '@/services/api';
import { useMutation } from '@tanstack/react-query';
import { type ChangeEvent, type FormEvent, useEffect, useState } from 'react';
import useToast from '@/hooks/useToast';
import diff from 'microdiff';
import { clearFormData, getDiffData } from '@/lib/tool';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import { useRouter } from 'next/navigation';
import { type PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function CreateSectionAdminPage({
  source,
  translatedFields,
}: {
  source: IPagination<ISection>;
  translatedFields: PrefixedTTranslatedFields<'sectionAdminPage'>;
}) {
  const [form, setForm] = useState({
    name: '',
  });
  const [differenceData, setDifferenceData] = useState<IDifference[]>([]);
  const { show } = useToast();
  const router = useRouter();
  const [isDisabledSave, setIsDisabledSave] = useState(true);

  const createSectionMutation = useMutation(createSection);

  useEffect(() => {
    const diffData = diff(
      {
        name: '',
      },
      form,
    );
    setDifferenceData(diffData);
    setIsDisabledSave(diffData.length === 0);
  }, [form]);

  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      checkForm();

      const data: any = getDiffData(differenceData);
      await createSectionMutation.mutateAsync({
        data,
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.createPage.saveCompleted,
      });

      setTimeout(() => {
        show({
          type: 'PRIMARY',
          message: translatedFields.createPage.returning,
        });
      }, 1000);

      setTimeout(() => {
        router.back();
      }, 1500);
    } catch (e) {
      createSectionMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setForm(clearFormData(form));
      setDifferenceData([]);
    }
  }

  function checkForm() {
    const { name } = form;
    if (!name.trim()) {
      throw translatedFields.createPage.nameRequired;
    }

    if (source.content.find((item) => item.name === name)) {
      throw translatedFields.createPage.nameDuplicated;
    }
  }

  function onChange(e: ChangeEvent<HTMLInputElement>) {
    const name = e.target.name;
    const value = e.target.value;
    setForm({ ...form, [name]: value });
  }

  return (
    <Box>
      <div className="card">
        <div className="card-body">
          <form onSubmit={onSubmit} className="vstack gap-4">
            <div>
              <label className="form-label">
                {translatedFields.createPage.name}
              </label>
              <input
                type="text"
                name="name"
                value={form.name}
                onChange={onChange}
                className="form-control"
                aria-describedby="name"
                placeholder={translatedFields.createPage.namePlaceholder}
              />
            </div>

            <button
              type="submit"
              disabled={createSectionMutation.isLoading || isDisabledSave}
              className="btn btn-primary col col-lg-2 mt-4"
            >
              {createSectionMutation.isLoading && <Spinner classs="me-2" />}
              {translatedFields.createPage.save}
            </button>
          </form>
        </div>
      </div>
    </Box>
  );
}
