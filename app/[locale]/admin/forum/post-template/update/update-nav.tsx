'use client';

import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateNav({
  translatedFields,
}: {
  translatedFields: PrefixedTTranslatedFields<'postTemplateAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);
  const searchParams = useSearchParams();
  const id = searchParams.get('id');

  return (
    <ToggleProperties
      tabIndex={tabIndex}
      setTabIndex={setTabIndex}
      mb4={false}
      isTable={false}
      items={[
        {
          index: 0,
          name: translatedFields.properties.info,
          href: `/admin/forum/post-template/update?id=${id}`,
          segment: null,
        },
        {
          index: 1,
          name: translatedFields.properties.content,
          href: `/admin/forum/post-template/update/content?id=${id}`,
          segment: 'content',
        },
      ]}
    />
  );
}
