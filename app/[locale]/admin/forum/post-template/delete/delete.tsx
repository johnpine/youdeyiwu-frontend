'use client';

import type { IPostTemplate } from '@/interfaces';
import { removePostTemplate } from '@/services/api';
import { useMutation } from '@tanstack/react-query';
import { type FormEvent } from 'react';
import useToast from '@/hooks/useToast';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import { useRouter } from 'next/navigation';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DeletePostTemplateAdminPage({
  source,
  translatedFields,
}: {
  source: IPostTemplate;
  translatedFields: PrefixedTTranslatedFields<'postTemplateAdminPage'>;
}) {
  const { show } = useToast();
  const router = useRouter();

  const removePostTemplateMutation = useMutation(removePostTemplate);

  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      const id = source.id;
      await removePostTemplateMutation.mutateAsync({
        id,
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.operate.deleteCompleted,
      });

      setTimeout(() => {
        show({
          type: 'SUCCESS',
          message: translatedFields.operate.refreshPending,
        });
      }, 1200);

      setTimeout(() => {
        router.back();
      }, 1500);
    } catch (e) {
      removePostTemplateMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    }
  }

  return (
    <Box>
      <div className="card">
        <div className="card-body">
          <form
            onSubmit={onSubmit}
            className="vstack gap-4 align-items-center py-4"
          >
            <div>
              <span>{translatedFields.tip.isDeletionConfirmed}&nbsp;</span>
              <span className="fw-semibold text-danger lead">{`⌈ ${source.name} (ID. ${source.id}) ⌋`}</span>
            </div>
            <button
              type="submit"
              disabled={removePostTemplateMutation.isLoading}
              className="btn btn-danger col col-lg-2 mt-4"
            >
              {removePostTemplateMutation.isLoading && (
                <Spinner classs="me-2" />
              )}
              {translatedFields.operate.delete}
            </button>
          </form>
        </div>
      </div>
    </Box>
  );
}
