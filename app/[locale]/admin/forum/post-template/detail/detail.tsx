'use client';

import type { IPostTemplate } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QueryPostTemplateInfo from '@/app/[locale]/admin/forum/post-template/detail/info';
import QueryPostTemplateContent from '@/app/[locale]/admin/forum/post-template/detail/content';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailPostTemplateAdminPage({
  source,
  translatedFields,
}: {
  source: IPostTemplate;
  translatedFields: PrefixedTTranslatedFields<'postTemplateAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QueryPostTemplateInfo
            source={source}
            translatedFields={translatedFields}
          />

          <QueryPostTemplateContent
            source={source}
            translatedFields={translatedFields}
          />
        </div>
      </div>
    </Box>
  );
}
