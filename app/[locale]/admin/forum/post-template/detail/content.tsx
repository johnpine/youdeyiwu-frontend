import type { IPostTemplate } from '@/interfaces';
import ContentHtml from '@/app/[locale]/common/content/html';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPostTemplateContent({
  source,
  translatedFields,
}: {
  source: IPostTemplate;
  translatedFields: PrefixedTTranslatedFields<'postTemplateAdminPage'>;
}) {
  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-card-text me-2"></i>
        {translatedFields.properties.content}
      </div>
      <div className="card-body">
        <ContentHtml content={source.content} nodata />
      </div>
    </div>
  );
}
