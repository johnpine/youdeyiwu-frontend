'use client';

import type { IDifference } from '@/interfaces';
import { createTagGroup } from '@/services/api';
import { useMutation } from '@tanstack/react-query';
import { type ChangeEvent, type FormEvent, useEffect, useState } from 'react';
import useToast from '@/hooks/useToast';
import diff from 'microdiff';
import { clearFormData, getDiffData } from '@/lib/tool';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import { useRouter } from 'next/navigation';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function CreateTagGroupAdminPage({
  source,
  translatedFields,
}: {
  source?: any;
  translatedFields: PrefixedTTranslatedFields<'tagGroupAdminPage'>;
}) {
  const [form, setForm] = useState({
    name: '',
  });
  const [differenceData, setDifferenceData] = useState<IDifference[]>([]);
  const { show } = useToast();
  const router = useRouter();
  const [isDisabledSave, setIsDisabledSave] = useState(true);

  const createTagGroupMutation = useMutation(createTagGroup);

  useEffect(() => {
    const diffData = diff(
      {
        name: '',
      },
      form,
    );
    setDifferenceData(diffData);
    setIsDisabledSave(diffData.length === 0);
  }, [form]);

  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      checkForm();

      const data: any = getDiffData(differenceData);
      await createTagGroupMutation.mutateAsync({
        data,
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.operate.saveCompleted,
      });

      setTimeout(() => {
        show({
          type: 'PRIMARY',
          message: translatedFields.operate.refreshPending,
        });
      }, 1000);

      setTimeout(() => {
        router.back();
      }, 1500);
    } catch (e) {
      createTagGroupMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setForm(clearFormData(form));
      setDifferenceData([]);
    }
  }

  function checkForm() {
    const { name } = form;
    if (!name.trim()) {
      throw translatedFields.tip.required;
    }
  }

  function onChange(e: ChangeEvent<HTMLInputElement>) {
    const name = e.target.name;
    const value = e.target.value;
    setForm({ ...form, [name]: value });
  }

  return (
    <Box>
      <div className="card">
        <div className="card-body">
          <form onSubmit={onSubmit} className="vstack gap-4">
            <div>
              <label className="form-label">
                {translatedFields.properties.name}
              </label>
              <input
                type="text"
                name="name"
                value={form.name}
                onChange={onChange}
                className="form-control"
                aria-describedby="name"
                placeholder={translatedFields.tip.required}
              />
            </div>

            <button
              type="submit"
              disabled={createTagGroupMutation.isLoading || isDisabledSave}
              className="btn btn-primary col col-lg-2 mt-4"
            >
              {createTagGroupMutation.isLoading && <Spinner classs="me-2" />}
              {translatedFields.operate.save}
            </button>
          </form>
        </div>
      </div>
    </Box>
  );
}
