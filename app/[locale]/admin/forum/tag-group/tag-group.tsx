'use client';

import { useQuery } from '@tanstack/react-query';
import {
  queryAllTagGroup,
  queryTagGroupStatistics,
  searchTagGroup,
} from '@/services/api';
import type {
  IPagination,
  IQueryParams,
  ITagGroup,
  ITagGroupStatistics,
} from '@/interfaces';
import { getSectionQueryParams } from '@/lib/tool';
import Box from '@/app/[locale]/admin/common/box';
import Alert from '@/app/[locale]/alert/alert';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Pagination from '@/app/[locale]/admin/common/pagination';
import { type ChangeEvent, useState } from 'react';
import { useRouter, useSearchParams } from 'next/navigation';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function TagGroupAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IPagination<ITagGroup>;
    translatedFields: PrefixedTTranslatedFields<'tagGroupAdminPage'>;
  },
) {
  const urlSearchParams = useSearchParams();
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
    sectionId: getSectionQueryParams(urlSearchParams)?.id,
  });
  const [searchName, setSearchName] = useState('');
  const router = useRouter();

  const queryTagGroupStatisticsQuery = useQuery(
    ['/forum', '/tagGroups', '/statistics'],
    async () => {
      return (await queryTagGroupStatistics()) as ITagGroupStatistics;
    },
  );

  const queryAllTagGroupQuery = useQuery(
    ['/forum', '/tagGroups', params, searchName],
    async (context) => {
      if (searchName) {
        return (await searchTagGroup({
          query: {
            ...(context.queryKey[2] as any),
            name: context.queryKey[3],
          },
        })) as IPagination<ITagGroup>;
      } else {
        return (await queryAllTagGroup({
          query: context.queryKey[2] as any,
        })) as IPagination<ITagGroup>;
      }
    },
    {
      keepPreviousData: true,
      initialData: source,
    },
  );

  function onClickDetail(item: ITagGroup) {
    router.push(`tag-group/detail?id=${item.id}`);
  }

  function onClickUpdate(item: ITagGroup) {
    router.push(`tag-group/update?id=${item.id}`);
  }

  function onClickPrevious() {
    const data = queryAllTagGroupQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryAllTagGroupQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  function onChangeSearch(e: ChangeEvent<HTMLInputElement>) {
    const value = e.target.value;
    setSearchName(value.toLowerCase());
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.count.tagGroup}
              </h6>
              <h5 className="card-title">
                {queryTagGroupStatisticsQuery.data &&
                  queryTagGroupStatisticsQuery.data.count}

                {queryTagGroupStatisticsQuery.isError && (
                  <Alert
                    message={queryTagGroupStatisticsQuery.error}
                    type="error"
                  />
                )}

                {queryTagGroupStatisticsQuery.isLoading && <Spinner />}
              </h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col">
        <Box>
          <div className="card">
            <div className="card-header bg-transparent">
              <div className="row row-cols-auto justify-content-between g-2">
                <div className="col">
                  <div className="input-group">
                    <span className="input-group-text">
                      <i className="bi bi-search"></i>
                    </span>
                    <input
                      onChange={onChangeSearch}
                      type="search"
                      className="form-control"
                      placeholder={translatedFields.tip.searchPlaceholder}
                    />
                  </div>
                </div>
                <div className="col"></div>
              </div>
            </div>
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        ID
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.name}
                      </th>
                      <th scope="col" className=""></th>
                    </tr>
                  </thead>
                  <tbody>
                    {queryAllTagGroupQuery.data.content.map((item) => {
                      return (
                        <tr key={item.id} className="text-nowrap">
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.id}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.name}
                          </td>
                          <td>
                            <div className="hstack gap-2">
                              <button
                                onClick={onClickUpdate.bind(this, item)}
                                className="btn btn-sm btn-light"
                                type="button"
                              >
                                <i className="bi bi-pencil me-2"></i>
                                {translatedFields.operate.update}
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>

              {queryAllTagGroupQuery.data.content.length === 0 && <Nodata />}

              <Pagination
                isShow={queryAllTagGroupQuery.data.pageable.pages > 0}
                onPrevious={onClickPrevious}
                onNext={onClickNext}
                isPrevious={queryAllTagGroupQuery.data.pageable.previous}
                isNext={queryAllTagGroupQuery.data.pageable.next}
                translatedFields={translatedFields.pagination}
              />
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
