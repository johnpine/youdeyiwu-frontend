'use client';

import type { ITagGroup } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QueryTagGroupInfo from '@/app/[locale]/admin/forum/tag-group/detail/info';
import QueryTagGroupTags from '@/app/[locale]/admin/forum/tag-group/detail/tags';
import QueryTagGroupSections from '@/app/[locale]/admin/forum/tag-group/detail/sections';
import QueryTagGroupPosts from '@/app/[locale]/admin/forum/tag-group/detail/posts';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailTagGroupAdminPage({
  source,
  translatedFields,
}: {
  source: ITagGroup;
  translatedFields: PrefixedTTranslatedFields<'tagGroupAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QueryTagGroupInfo
            source={source}
            translatedFields={translatedFields}
          />

          <QueryTagGroupTags
            source={source}
            translatedFields={translatedFields}
          />

          <div className="row">
            <div className="col">
              <QueryTagGroupSections
                source={source}
                translatedFields={translatedFields}
              />
            </div>
            <div className="col">
              <QueryTagGroupPosts
                source={source}
                translatedFields={translatedFields}
              />
            </div>
          </div>
        </div>
      </div>
    </Box>
  );
}
