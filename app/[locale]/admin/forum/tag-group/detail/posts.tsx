import { queryPostsByTagGroup } from '@/services/api';
import { useQuery } from '@tanstack/react-query';
import type { IPagination, IPost, IQueryParams, ITagGroup } from '@/interfaces';
import { useState } from 'react';
import Alert from '@/app/[locale]/alert/alert';
import AlertLoad from '@/app/[locale]/alert/load';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Pagination from '@/app/[locale]/admin/common/pagination';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryTagGroupPosts({
  source,
  translatedFields,
}: {
  source: ITagGroup;
  translatedFields: PrefixedTTranslatedFields<'tagGroupAdminPage'>;
}) {
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });

  const queryPostsByTagGroupQuery = useQuery(
    ['/forum', '/tagGroups', source.id, '/posts', params],
    async (context) => {
      return (await queryPostsByTagGroup({
        id: context.queryKey[2] + '',
        query: context.queryKey[4] as any,
      })) as IPagination<IPost>;
    },
    {
      keepPreviousData: true,
    },
  );

  function onClickPrevious() {
    const data = queryPostsByTagGroupQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryPostsByTagGroupQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  if (queryPostsByTagGroupQuery.data) {
    const data = queryPostsByTagGroupQuery.data;

    return (
      <div className="card">
        <div className="card-header bg-transparent text-secondary">
          <i className="bi bi-list me-2"></i>
          帖子
        </div>
        <div className="card-body">
          <div className="table-responsive">
            <table className="table align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="fw-normal">
                    ID
                  </th>
                  <th scope="col" className="fw-normal">
                    {translatedFields.properties.name}
                  </th>
                </tr>
              </thead>
              <tbody>
                {data.content.map((item) => {
                  return (
                    <tr key={item.id} className="text-nowrap">
                      <td>{item.id}</td>
                      <td>{item.name}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>

          {data.content.length === 0 && <Nodata />}

          <Pagination
            isShow={data.pageable.pages > 0}
            onPrevious={onClickPrevious}
            onNext={onClickNext}
            isPrevious={data.pageable.previous}
            isNext={data.pageable.next}
            translatedFields={translatedFields.pagination}
          />
        </div>
      </div>
    );
  }

  if (queryPostsByTagGroupQuery.error) {
    return <Alert message={queryPostsByTagGroupQuery.error} type="error" />;
  }

  return <AlertLoad />;
}
