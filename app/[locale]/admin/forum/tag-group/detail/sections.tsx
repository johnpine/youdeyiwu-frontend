import { querySectionsByTagGroup } from '@/services/api';
import { useQuery } from '@tanstack/react-query';
import type { ISection, ITagGroup } from '@/interfaces';
import Alert from '@/app/[locale]/alert/alert';
import AlertLoad from '@/app/[locale]/alert/load';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryTagGroupSections({
  source,
  translatedFields,
}: {
  source: ITagGroup;
  translatedFields: PrefixedTTranslatedFields<'tagGroupAdminPage'>;
}) {
  const querySectionsByTagGroupQuery = useQuery(
    ['/forum', '/tagGroups', source.id, '/sections'],
    async (context) => {
      return (await querySectionsByTagGroup({
        id: context.queryKey[2] + '',
      })) as ISection[];
    },
  );

  if (querySectionsByTagGroupQuery.data) {
    const data = querySectionsByTagGroupQuery.data;

    return (
      <div className="card">
        <div className="card-header bg-transparent text-secondary">
          <i className="bi bi-card-list me-2"></i>
          {translatedFields.properties.section}
        </div>
        <div className="card-body">
          <div className="table-responsive">
            <table className="table align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="fw-normal">
                    ID
                  </th>
                  <th scope="col" className="fw-normal">
                    {translatedFields.properties.name}
                  </th>
                </tr>
              </thead>
              <tbody>
                {data.map((item) => {
                  return (
                    <tr key={item.id} className="text-nowrap">
                      <td>{item.id}</td>
                      <td>{item.name}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>

          {data.length === 0 && <Nodata />}
        </div>
      </div>
    );
  }

  if (querySectionsByTagGroupQuery.error) {
    return <Alert message={querySectionsByTagGroupQuery.error} type="error" />;
  }

  return <AlertLoad />;
}
