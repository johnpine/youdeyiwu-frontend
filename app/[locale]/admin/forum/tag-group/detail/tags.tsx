import type { ITagGroup } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryTagGroupTags({
  source,
  translatedFields,
}: {
  source: ITagGroup;
  translatedFields: PrefixedTTranslatedFields<'tagGroupAdminPage'>;
}) {
  const tags = source.tags || [];

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-tags me-2"></i>
        {translatedFields.properties.tag}
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle">
            <thead>
              <tr className="text-nowrap">
                <th scope="col" className="fw-normal">
                  ID
                </th>
                <th scope="col" className="fw-normal">
                  {translatedFields.properties.name}
                </th>
              </tr>
            </thead>
            <tbody>
              {tags.map((item) => {
                return (
                  <tr key={item.id} className="text-nowrap">
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>

        {tags.length === 0 && <Nodata />}
      </div>
    </div>
  );
}
