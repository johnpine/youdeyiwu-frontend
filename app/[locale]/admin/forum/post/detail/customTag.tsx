import type { IQueryPostDetails } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPostCustomTag({
  source,
  translatedFields,
}: {
  source: IQueryPostDetails;
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  const basic = source.basic;

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-tags me-2"></i>
        {translatedFields.customTags}
      </div>
      <div className="card-body">
        {basic.customTags.length > 0 && (
          <ul className="list-group list-group-flush">
            {basic.customTags.map((item) => {
              return (
                <li key={item} className="list-group-item">
                  {item}
                </li>
              );
            })}
          </ul>
        )}

        {basic.customTags.length === 0 && <Nodata />}
      </div>
    </div>
  );
}
