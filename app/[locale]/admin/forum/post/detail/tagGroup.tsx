import type { IQueryPostDetails } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPostTagGroup({
  source,
  translatedFields,
}: {
  source: IQueryPostDetails;
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  const tagGroups = source.tagGroups || [];

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-tags me-2"></i>
        {translatedFields.tagGroup}
      </div>
      <div className="card-body">
        {tagGroups.length > 0 && (
          <ul className="list-group list-group-flush">
            {tagGroups.map((item) => {
              return (
                <li key={item.id} className="list-group-item">
                  {item.name}
                </li>
              );
            })}
          </ul>
        )}

        {tagGroups.length === 0 && <Nodata />}
      </div>
    </div>
  );
}
