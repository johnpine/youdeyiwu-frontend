import type { IQueryPostDetails } from '@/interfaces';
import classNames from 'classnames';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPostBadge({
  source,
  translatedFields,
}: {
  source: IQueryPostDetails;
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  const basic = source.basic;

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-stars me-2"></i>
        {translatedFields.badges}
      </div>
      <div className="card-body">
        {basic.badges.length > 0 && (
          <ul className="list-group list-group-flush">
            {basic.badges
              .sort((a, b) => a.sort - b.sort)
              .map((item) => {
                const style: any = {};
                if (item.colorMode !== 'DEFAULT' && item.color) {
                  style.color = item.color;
                }
                if (item.colorMode !== 'DEFAULT' && item.backgroundColor) {
                  style.backgroundColor = item.backgroundColor;
                }

                return (
                  <li key={item.id} className="list-group-item">
                    <span
                      className={classNames('badge lh-base', {
                        'bg-info':
                          item.colorMode === 'DEFAULT' &&
                          item.backgroundColorMode === 'DEFAULT',
                        'rounded-pill': item.roundedPill,
                      })}
                      style={style}
                    >
                      {item.name}
                    </span>
                  </li>
                );
              })}
          </ul>
        )}

        {basic.badges.length === 0 && <Nodata />}
      </div>
    </div>
  );
}
