import type { IPostStyle } from '@/interfaces';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function Styles({
  styles,
  translatedFields,
}: {
  styles: IPostStyle[];
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  return (
    <>
      {styles.length > 0 && (
        <ul className="list-unstyled">
          {styles.map((value) => {
            return (
              <li key={value.id}>
                <ul className="list-unstyled">
                  <li>
                    {translatedFields.name}：{value.name}
                  </li>
                  <li>
                    {translatedFields.icon}：{value.icons.join(', ')}
                  </li>
                  <li>
                    {translatedFields.className}：{value.classes.join(', ')}
                  </li>
                  <li>
                    <p>{translatedFields.style}：</p>
                    <ul className="list-unstyled">
                      {Object.keys(value.styles).map((key) => {
                        return <li key={key}>{value.styles[key]}</li>;
                      })}
                    </ul>
                  </li>
                </ul>
              </li>
            );
          })}
        </ul>
      )}
    </>
  );
}
