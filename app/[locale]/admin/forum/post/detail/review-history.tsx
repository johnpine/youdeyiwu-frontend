import type { IPostReviewHistory, IQueryPostDetails } from '@/interfaces';
import { useQuery } from '@tanstack/react-query';
import { queryAllPostReviewHistoryById } from '@/services/api';
import Alert from '@/app/[locale]/alert/alert';
import AlertLoad from '@/app/[locale]/alert/load';
import { toRelativeTime } from '@/lib/tool';
import classNames from 'classnames';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPostReviewHistory({
  source,
  translatedFields,
}: {
  source: IQueryPostDetails;
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  const queryAllPostReviewHistoryByIdQuery = useQuery(
    ['/forum', '/posts', source.basic.id, '/reviewHistory'],
    async (context) => {
      return (await queryAllPostReviewHistoryById({
        id: context.queryKey[2] + '',
      })) as IPostReviewHistory[];
    },
  );

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-clock-history me-2"></i>
        {translatedFields.reviewHistory}
      </div>
      <div className="card-body">
        {queryAllPostReviewHistoryByIdQuery.data &&
          queryAllPostReviewHistoryByIdQuery.data.length > 0 && (
            <div className="table-responsive">
              <table className="table align-middle">
                <thead>
                  <tr className="text-nowrap">
                    <th scope="col" className="fw-normal text-nowrap">
                      {translatedFields.reviewHistoryPage.state}
                    </th>
                    <th scope="col" className="fw-normal text-nowrap">
                      {translatedFields.reviewHistoryPage.reason}
                    </th>
                    <th scope="col" className="fw-normal text-nowrap">
                      {translatedFields.reviewHistoryPage.remark}
                    </th>
                    <th scope="col" className="fw-normal text-nowrap">
                      {translatedFields.reviewHistoryPage.sortState}
                    </th>
                    <th scope="col" className="fw-normal text-nowrap">
                      {translatedFields.reviewHistoryPage.sortReason}
                    </th>
                    <th scope="col" className="fw-normal text-nowrap">
                      {translatedFields.reviewHistoryPage.sortRemark}
                    </th>
                    <th scope="col" className="fw-normal text-nowrap">
                      {translatedFields.reviewHistoryPage.auditors}
                    </th>
                    <th scope="col" className="fw-normal text-nowrap">
                      {translatedFields.reviewHistoryPage.reviewTime}
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {queryAllPostReviewHistoryByIdQuery.data.map(
                    (item, index) => {
                      return (
                        <tr key={item.id} className="text-nowrap">
                          <td>
                            {item.previousReviewState &&
                              item.finalReviewState && (
                                <div className="row flex-nowrap">
                                  <div className="col-auto">
                                    {
                                      translatedFields.COMPOSITE_STATE[
                                        item.finalReviewState
                                      ]
                                    }
                                  </div>
                                  <div className="col-auto">
                                    <i
                                      className={classNames(
                                        'bi bi-arrow-left',
                                        index === 0
                                          ? 'text-primary'
                                          : 'text-secondary',
                                      )}
                                    ></i>
                                  </div>
                                  <div className="col-auto text-secondary">
                                    {
                                      translatedFields.COMPOSITE_STATE[
                                        item.previousReviewState
                                      ]
                                    }
                                  </div>
                                </div>
                              )}
                          </td>
                          <td title={item.reason || ''} className="text-wrap">
                            <div
                              dangerouslySetInnerHTML={{
                                __html: item.reason || '',
                              }}
                            ></div>
                          </td>
                          <td title={item.remark || ''} className="text-wrap">
                            <div
                              dangerouslySetInnerHTML={{
                                __html: item.remark || '',
                              }}
                            ></div>
                          </td>
                          <td>
                            {item.previousSortState && item.finalSortState && (
                              <div className="row flex-nowrap">
                                <div className="col-auto">
                                  {
                                    (translatedFields as any).SORT_STATE[
                                      item.finalSortState
                                    ]
                                  }
                                </div>
                                <div className="col-auto">
                                  <i
                                    className={classNames(
                                      'bi bi-arrow-left',
                                      index === 0
                                        ? 'text-primary'
                                        : 'text-secondary',
                                    )}
                                  ></i>
                                </div>
                                <div className="col-auto text-secondary">
                                  {
                                    (translatedFields as any).SORT_STATE[
                                      item.previousSortState
                                    ]
                                  }
                                </div>
                              </div>
                            )}
                          </td>
                          <td
                            title={item.sortReason || ''}
                            className="text-wrap"
                          >
                            <div
                              dangerouslySetInnerHTML={{
                                __html: item.sortReason || '',
                              }}
                            ></div>
                          </td>
                          <td
                            title={item.sortRemark || ''}
                            className="text-wrap"
                          >
                            <div
                              dangerouslySetInnerHTML={{
                                __html: item.sortRemark || '',
                              }}
                            ></div>
                          </td>
                          <td>{`${item.reviewer.alias} (ID. ${item.reviewer.id})`}</td>
                          <td>
                            <time
                              dateTime={item.createdOn}
                              title={item.createdOn}
                            >
                              {toRelativeTime(item.createdOn)}
                            </time>
                          </td>
                        </tr>
                      );
                    },
                  )}
                </tbody>
              </table>
            </div>
          )}

        {queryAllPostReviewHistoryByIdQuery.data &&
          queryAllPostReviewHistoryByIdQuery.data.length === 0 && <Nodata />}

        {queryAllPostReviewHistoryByIdQuery.isError && (
          <Alert
            message={queryAllPostReviewHistoryByIdQuery.error}
            type="error"
          />
        )}

        {queryAllPostReviewHistoryByIdQuery.isLoading && <AlertLoad />}
      </div>
    </div>
  );
}
