'use client';

import type { IQueryPostDetails } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QueryPostInfo from '@/app/[locale]/admin/forum/post/detail/info';
import QueryPostReviewHistory from '@/app/[locale]/admin/forum/post/detail/review-history';
import QueryPostCount from '@/app/[locale]/admin/forum/post/detail/count';
import QueryPostTags from '@/app/[locale]/admin/forum/post/detail/tags';
import QueryPostTagGroup from '@/app/[locale]/admin/forum/post/detail/tagGroup';
import QueryPostCustomTag from '@/app/[locale]/admin/forum/post/detail/customTag';
import QueryPostBadge from '@/app/[locale]/admin/forum/post/detail/badge';
import QueryPostContent from '@/app/[locale]/admin/forum/post/detail/content';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailPostAdminPage({
  source,
  translatedFields,
}: {
  source: IQueryPostDetails;
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QueryPostInfo source={source} translatedFields={translatedFields} />

          <QueryPostReviewHistory
            source={source}
            translatedFields={translatedFields}
          />

          <QueryPostCount source={source} translatedFields={translatedFields} />

          <div className="row">
            <div className="col">
              <QueryPostTags
                source={source}
                translatedFields={translatedFields}
              />
            </div>
            <div className="col">
              <QueryPostTagGroup
                source={source}
                translatedFields={translatedFields}
              />
            </div>
            <div className="col">
              <QueryPostCustomTag
                source={source}
                translatedFields={translatedFields}
              />
            </div>
            <div className="col">
              <QueryPostBadge
                source={source}
                translatedFields={translatedFields}
              />
            </div>
          </div>

          <QueryPostContent
            source={source}
            translatedFields={translatedFields}
          />
        </div>
      </div>
    </Box>
  );
}
