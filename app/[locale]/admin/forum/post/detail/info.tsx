import type { IQueryPostDetails } from '@/interfaces';
import Link from 'next/link';
import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import PrimaryProperties from '@/app/[locale]/admin/common/primary-properties';
import GeneralizedProperties from '@/app/[locale]/admin/common/generalized-properties';
import { useState } from 'react';
import { formatDateTime } from '@/lib/tool';
import { Badges } from '@/app/[locale]/common/post/name';
import Styles from '@/app/[locale]/admin/forum/post/detail/styles';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPostInfo({
  source,
  translatedFields,
}: {
  source: IQueryPostDetails;
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-info-square me-2"></i>
        <Link
          href={`/posts/${source.basic.id}`}
          className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
        >
          {source.basic.name}
        </Link>
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle caption-top">
            <caption>
              <ToggleProperties
                isTable={false}
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
                items={[
                  {
                    index: 0,
                    name: translatedFields.mainAttribute,
                  },
                  {
                    index: 1,
                    name: translatedFields.otherAttribute,
                  },
                ]}
              />
            </caption>
            <tbody>
              {tabIndex === 0 && (
                <PrimaryProperties
                  source={[
                    {
                      field: 'section',
                      translatedField: translatedFields.section,
                      value: `${source.section.name} (ID. ${source.section.id})`,
                      href: `/sections/${source.section.id}`,
                    },
                    {
                      field: 'name',
                      translatedField: translatedFields.name,
                      value: `${source.basic.name} (ID. ${source.basic.id})`,
                      href: `/posts/${source.basic.id}`,
                    },
                    {
                      field: 'overview',
                      translatedField: translatedFields.overview,
                      value: source.basic.overview,
                    },
                    {
                      field: 'cover',
                      translatedField: translatedFields.cover,
                      value: source.basic.cover,
                    },
                    {
                      field: 'customTags',
                      translatedField: translatedFields.customTags,
                      value: source.basic.customTags,
                    },
                    {
                      field: 'badges',
                      translatedField: translatedFields.badges,
                      isCustomValue: true,
                      isEmptyValue: source.basic.badges.length === 0,
                      value: <Badges items={source.basic.badges} />,
                    },
                    {
                      field: 'styles',
                      translatedField: translatedFields.styles,
                      isCustomValue: true,
                      isEmptyValue: source.basic.styles.length === 0,
                      value: (
                        <Styles
                          styles={source.basic.styles}
                          translatedFields={translatedFields}
                        />
                      ),
                    },
                    {
                      field: 'state',
                      translatedField: translatedFields.state,
                      value: translatedFields.STATE[source.details.state],
                    },
                    {
                      field: 'reviewState',
                      translatedField: translatedFields.reviewState,
                      value:
                        translatedFields.REVIEW_STATE[
                          source.details.reviewState
                        ],
                    },
                    {
                      field: 'otherStates',
                      translatedField: translatedFields.otherStates,
                      value: (source.details.otherStates.length === 0
                        ? ['DEFAULT']
                        : source.details.otherStates
                      ).map(
                        (value) => (translatedFields as any).OTHER_STATE[value],
                      ),
                    },
                    {
                      field: 'sortState',
                      translatedField: translatedFields.sortState,
                      value: (translatedFields as any).SORT_STATE[
                        source.details.sortState
                      ],
                    },
                    {
                      field: 'allow',
                      translatedField: translatedFields.allow,
                      value: source.details.allow,
                    },
                    {
                      field: 'block',
                      translatedField: translatedFields.block,
                      value: source.details.block,
                    },
                    {
                      field: 'contentUpdatedOn',
                      translatedField: translatedFields.contentUpdatedOn,
                      value: formatDateTime(source.basic.contentUpdatedOn),
                    },
                  ]}
                />
              )}

              {tabIndex === 1 && (
                <GeneralizedProperties
                  source={source.basic}
                  translatedFields={translatedFields.generalizedProperties}
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
