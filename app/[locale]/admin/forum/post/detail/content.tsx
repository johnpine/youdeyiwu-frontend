import type { IQueryPostDetails } from '@/interfaces';
import ContentHtml from '@/app/[locale]/common/content/html';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPostContent({
  source,
  translatedFields,
}: {
  source: IQueryPostDetails;
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  const content = source.content;

  return (
    <div content="row">
      <div content="col">
        <div className="card">
          <div className="card-header bg-transparent text-secondary">
            <i className="bi bi-card-text me-2"></i>
            {translatedFields.content}
          </div>
          <div className="card-body">
            <ContentHtml content={content} nodata />
          </div>
        </div>
      </div>
    </div>
  );
}
