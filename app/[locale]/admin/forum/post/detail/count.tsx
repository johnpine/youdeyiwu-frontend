import type { IQueryPostDetails } from '@/interfaces';
import { formatCount } from '@/lib/tool';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPostCount({
  source,
  translatedFields,
}: {
  source: IQueryPostDetails;
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-activity me-2"></i>
        {translatedFields.quantity}
      </div>

      <div className="card-body">
        <div className="row">
          <div className="col">
            <div className="d-flex flex-column text-center">
              <div className="my-2">{translatedFields.commentCount}</div>
              <div>{formatCount(source.details.commentCount)}</div>
            </div>
          </div>

          <div className="col">
            <div className="d-flex flex-column text-center">
              <div className="my-2">{translatedFields.replyCount}</div>
              <div>{formatCount(source.details.replyCount)}</div>
            </div>
          </div>

          <div className="col">
            <div className="d-flex flex-column text-center">
              <div className="my-2">{translatedFields.viewCount}</div>
              <div>{formatCount(source.details.viewCount)}</div>
            </div>
          </div>

          <div className="col">
            <div className="d-flex flex-column text-center">
              <div className="my-2">{translatedFields.likeCount}</div>
              <div>{formatCount(source.details.likeCount)}</div>
            </div>
          </div>

          <div className="col">
            <div className="d-flex flex-column text-center">
              <div className="my-2">{translatedFields.followCount}</div>
              <div>{formatCount(source.details.followCount)}</div>
            </div>
          </div>

          <div className="col">
            <div className="d-flex flex-column text-center">
              <div className="my-2">{translatedFields.favoriteCount}</div>
              <div>{formatCount(source.details.favoriteCount)}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
