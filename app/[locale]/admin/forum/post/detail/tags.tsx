import type { IQueryPostDetails } from '@/interfaces';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPostTags({
  source,
  translatedFields,
}: {
  source: IQueryPostDetails;
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  const tags = source.tags;

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-tags me-2"></i>
        {translatedFields.tag}
      </div>
      <div className="card-body">
        {tags.length > 0 && (
          <ul className="list-group list-group-flush">
            {tags.map((item) => {
              return (
                <li key={item.id} className="list-group-item">
                  {item.name}
                </li>
              );
            })}
          </ul>
        )}

        {tags.length === 0 && <Nodata />}
      </div>
    </div>
  );
}
