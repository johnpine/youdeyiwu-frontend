'use client';

import type { IQueryPostDetails } from '@/interfaces';
import { removePost } from '@/services/api';
import { useMutation } from '@tanstack/react-query';
import { type FormEvent } from 'react';
import useToast from '@/hooks/useToast';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import Link from 'next/link';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DeletePostAdminPage({
  source,
  translatedFields,
}: {
  source: IQueryPostDetails;
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  const { show } = useToast();

  const removePostMutation = useMutation(removePost);

  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      show({
        type: 'PRIMARY',
        message: translatedFields.waitForDeletionCompletion,
      });

      const id = source.basic.id;
      await removePostMutation.mutateAsync({
        id,
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.deleteCompleted,
      });

      setTimeout(() => {
        show({
          type: 'SUCCESS',
          message: translatedFields.refreshPending,
        });
      }, 1200);

      setTimeout(() => {
        location.replace('/admin/forum/post');
      }, 1500);
    } catch (e) {
      removePostMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    }
  }

  return (
    <Box>
      <div className="card">
        <div className="card-body">
          <form
            onSubmit={onSubmit}
            className="vstack gap-4 align-items-center py-4"
          >
            <Link
              href={`/posts/${source.basic.id}`}
              className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
            >
              <span>{translatedFields.isDeletionConfirmed}&nbsp;</span>
              <span className="fw-semibold text-danger lead">{`⌈ ${source.basic.name} (ID. ${source.basic.id}) ⌋`}</span>
            </Link>
            <button
              type="submit"
              disabled={removePostMutation.isLoading}
              className="btn btn-danger col col-lg-2 mt-4"
            >
              {removePostMutation.isLoading && <Spinner classs="me-2" />}
              {translatedFields.delete}
            </button>
          </form>
        </div>
      </div>
    </Box>
  );
}
