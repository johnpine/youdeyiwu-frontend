'use client';

import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateNav({
  translatedFields,
}: {
  translatedFields: PrefixedTTranslatedFields<'postAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);
  const searchParams = useSearchParams();
  const id = searchParams.get('id');

  return (
    <ToggleProperties
      tabIndex={tabIndex}
      setTabIndex={setTabIndex}
      mb4={false}
      isTable={false}
      items={[
        {
          index: 0,
          name: translatedFields.state,
          href: `/admin/forum/post/update?id=${id}`,
          segment: null,
        },
        {
          index: 1,
          name: translatedFields.tag,
          href: `/admin/forum/post/update/tag?id=${id}`,
          segment: 'tag',
        },
        {
          index: 2,
          name: translatedFields.tagGroup,
          href: `/admin/forum/post/update/tag-group?id=${id}`,
          segment: 'tag-group',
        },
        {
          index: 3,
          name: translatedFields.badges,
          href: `/admin/forum/post/update/badge?id=${id}`,
          segment: 'badge',
        },
        {
          index: 4,
          name: translatedFields.style,
          href: `/admin/forum/post/update/style?id=${id}`,
          segment: 'style',
        },
      ]}
    />
  );
}
