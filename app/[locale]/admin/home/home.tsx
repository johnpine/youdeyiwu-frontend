'use client';

import Box from '@/app/[locale]/admin/common/box';
import WebsiteAccessCounts from '@/app/[locale]/admin/home/website-access-counts';
import WebsiteQuarterlyAccessCounts from '@/app/[locale]/admin/home/website-quarterly-access-counts';
import WebsiteAccessPercentage from '@/app/[locale]/admin/home/website-access-percentage';
import WebsiteLineDatasets from '@/app/[locale]/admin/home/website-line-datasets';
import type { IPath } from '@/interfaces';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function HomeAdminPage({
  appName,
  source,
  translatedFields,
  isOk,
}: {
  appName?: string;
  source: IPath;
  translatedFields: PrefixedTTranslatedFields<'homeAdminPage'>;
  isOk?: boolean;
}) {
  if (isOk) {
    return (
      <Box>
        <div className="row row-cols-2 g-4">
          <div className="col-12 col-lg-6">
            <WebsiteLineDatasets translatedFields={translatedFields} />
          </div>
          <div className="col-12 col-lg-6">
            <WebsiteAccessCounts translatedFields={translatedFields} />
          </div>
          <div className="col-12 col-lg-6">
            <WebsiteAccessPercentage translatedFields={translatedFields} />
          </div>
          <div className="col-12 col-lg-6">
            <WebsiteQuarterlyAccessCounts translatedFields={translatedFields} />
          </div>
        </div>
      </Box>
    );
  }

  return <DefaultPage appName={appName} />;
}

const DefaultPage = ({ appName = 'Admin' }: { appName?: string }) => {
  return (
    <Box h100 itemClass="p-5">
      <div className="hstack gap-2 position-absolute top-50 start-50 translate-middle">
        <i className="bi bi-emoji-wink fs-1"></i>
        <div className="fs-2">{appName}</div>
      </div>
    </Box>
  );
};
