import type { ChangeEvent } from 'react';
import { useEffect, useState } from 'react';
import classNames from 'classnames';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export interface IDateTimeHeaderOptions {
  clickOptions?: {
    onClickCustom?: (startDateValue: string, endDateValue: string) => void;
    onClickToDay?: () => void;
    onClickYesterday?: () => void;
    onClick3D?: () => void;
    onClick7D?: () => void;
    onClick15D?: () => void;
    onClick30D?: () => void;
    onClickRefresh?: () => void;
    onClickDetails?: () => void;
  };
  activeOptions?: {
    custom?: boolean;
    toDay?: boolean;
    yesterday?: boolean;
    d3?: boolean;
    d7?: boolean;
    d15?: boolean;
    d30?: boolean;
  };
  refreshOptions?: {
    isLoading: boolean;
  };
  configs?: {
    hideLeftContent?: boolean;
    title?: string;
    showDetails?: boolean;
  };
}

export default function DateTimeHeader({
  options,
  translatedFields,
}: {
  options?: IDateTimeHeaderOptions;
  translatedFields: PrefixedTTranslatedFields<'homeAdminPage'>;
}) {
  const _options = options ?? {};
  const {
    onClickCustom = () => {},
    onClickToDay = () => {},
    onClickYesterday = () => {},
    onClick3D = () => {},
    onClick7D = () => {},
    onClick15D = () => {},
    onClick30D = () => {},
    onClickRefresh = () => {},
    onClickDetails = () => {},
  } = _options.clickOptions ?? {};
  const activeOptions = _options.activeOptions ?? {
    custom: false,
    toDay: false,
    yesterday: false,
    d3: false,
    d7: false,
    d15: false,
    d30: false,
  };
  const refreshOptions = _options.refreshOptions ?? { isLoading: false };
  const configs = _options.configs ?? {
    hideLeftContent: false,
    title: '',
    showDetails: false,
  };
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [dateExists, setDateExists] = useState(false);
  const [isOpenCustom, setIsOpenCustom] = useState(false);

  useEffect(() => {
    setDateExists(!!startDate && !!endDate);
  }, [startDate, endDate]);

  function onChangeStartDate(e: ChangeEvent<HTMLInputElement>) {
    const value = e.target.value;
    setStartDate(value);
  }

  function onChangeEndDate(e: ChangeEvent<HTMLInputElement>) {
    const value = e.target.value;
    setEndDate(value);
  }

  function onClickOpenCustom() {
    if (isOpenCustom && dateExists) {
      onClickCustom?.(startDate, endDate);
      return;
    }

    setIsOpenCustom(!isOpenCustom);
  }

  return (
    <div className="d-flex gap-4 justify-content-between align-items-center user-select-none">
      {configs.hideLeftContent ? (
        <div className="text-secondary">
          <i className="bi bi-activity fs-5 me-2"></i>
          {configs.title}
        </div>
      ) : (
        <div className="hstack flex-wrap gap-2">
          {isOpenCustom && (
            <>
              <div>
                <input
                  type="date"
                  className="form-control text-secondary"
                  name="startDate"
                  value={startDate}
                  onChange={onChangeStartDate}
                />
              </div>
              <div>
                <input
                  type="date"
                  className="form-control text-secondary"
                  name="endDate"
                  value={endDate}
                  onChange={onChangeEndDate}
                />
              </div>
            </>
          )}

          <button
            onClick={onClickOpenCustom}
            type="button"
            className={classNames('btn btn-outline-secondary btn-sm', {
              active: activeOptions.custom,
            })}
          >
            {translatedFields.custom}
          </button>

          <button
            onClick={onClickToDay}
            type="button"
            className={classNames('btn btn-outline-secondary btn-sm', {
              active: activeOptions.toDay,
            })}
          >
            {translatedFields.today}
          </button>

          <button
            onClick={onClickYesterday}
            type="button"
            className={classNames('btn btn-outline-secondary btn-sm', {
              active: activeOptions.yesterday,
            })}
          >
            {translatedFields.yesterday}
          </button>

          <button
            onClick={onClick3D}
            type="button"
            className={classNames('btn btn-outline-secondary btn-sm', {
              active: activeOptions.d3,
            })}
          >
            {translatedFields.days3}
          </button>

          <button
            onClick={onClick7D}
            type="button"
            className={classNames('btn btn-outline-secondary btn-sm', {
              active: activeOptions.d7,
            })}
          >
            {translatedFields.days7}
          </button>

          <button
            onClick={onClick15D}
            type="button"
            className={classNames('btn btn-outline-secondary btn-sm', {
              active: activeOptions.d15,
            })}
          >
            {translatedFields.days15}
          </button>

          <button
            onClick={onClick30D}
            type="button"
            className={classNames('btn btn-outline-secondary btn-sm', {
              active: activeOptions.d30,
            })}
          >
            {translatedFields.days30}
          </button>
        </div>
      )}

      <div>
        <div className="hstack flex-wrap gap-2">
          {configs.showDetails && (
            <i
              onClick={onClickDetails}
              className="bi bi-card-text fs-5 text-secondary cursor-pointer"
            ></i>
          )}

          {refreshOptions.isLoading ? (
            <Spinner classs="text-secondary" />
          ) : (
            <i
              onClick={onClickRefresh}
              className="bi bi-arrow-clockwise fs-5 text-secondary cursor-pointer"
            ></i>
          )}
        </div>
      </div>
    </div>
  );
}
