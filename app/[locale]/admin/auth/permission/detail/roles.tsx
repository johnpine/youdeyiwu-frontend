'use client';

import { useQuery } from '@tanstack/react-query';
import type {
  IPagination,
  IPermissionDetails,
  IQueryParams,
  IRole,
} from '@/interfaces';
import { useState } from 'react';
import { queryPermissionRolesById } from '@/services/api/permissions';
import Alert from '@/app/[locale]/alert/alert';
import AlertLoad from '@/app/[locale]/alert/load';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Pagination from '@/app/[locale]/admin/common/pagination';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPermissionRoles({
  source,
  translatedFields,
}: {
  source: IPermissionDetails;
  translatedFields: PrefixedTTranslatedFields<'permissionAdminPage'>;
}) {
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });

  const queryPermissionRolesByIdQuery = useQuery(
    ['/permissions', source.basic.id, '/roles', params],
    async (context) => {
      return (await queryPermissionRolesById({
        id: context.queryKey[1] + '',
        query: context.queryKey[3] as any,
      })) as IPagination<IRole>;
    },
    {
      keepPreviousData: true,
    },
  );

  function onClickPrevious() {
    const data = queryPermissionRolesByIdQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryPermissionRolesByIdQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  if (queryPermissionRolesByIdQuery.data) {
    const data = queryPermissionRolesByIdQuery.data;

    return (
      <div className="card">
        <div className="card-header bg-transparent text-secondary">
          <i className="bi bi-tag me-2"></i>
          角色
        </div>
        <div className="card-body">
          <table className="table align-middle">
            <thead>
              <tr className="text-nowrap">
                <th scope="col" className="w-10 fw-normal">
                  {translatedFields.properties.name} / ID
                </th>
              </tr>
            </thead>
            <tbody>
              {data.content.map((item) => {
                return (
                  <tr key={item.id} className="text-nowrap">
                    <td>
                      {item.name}&nbsp;(ID.&nbsp;{item.id})
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>

          {data.content.length === 0 && <Nodata />}

          <Pagination
            isShow={data.pageable.pages > 0}
            onPrevious={onClickPrevious}
            onNext={onClickNext}
            isPrevious={data.pageable.previous}
            isNext={data.pageable.next}
            translatedFields={translatedFields.pagination}
          />
        </div>
      </div>
    );
  }

  if (queryPermissionRolesByIdQuery.error) {
    return <Alert message={queryPermissionRolesByIdQuery.error} type="error" />;
  }

  return <AlertLoad />;
}
