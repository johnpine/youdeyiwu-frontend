'use client';

import type { IPermissionDetails } from '@/interfaces';
import { useState } from 'react';
import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import PrimaryProperties from '@/app/[locale]/admin/common/primary-properties';
import GeneralizedProperties from '@/app/[locale]/admin/common/generalized-properties';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPermissionInfo({
  source,
  translatedFields,
}: {
  source: IPermissionDetails;
  translatedFields: PrefixedTTranslatedFields<'permissionAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-info-square me-2"></i>
        {source.basic.name}
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle caption-top">
            <caption>
              <ToggleProperties
                isTable={false}
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
                items={[
                  {
                    index: 0,
                    name: translatedFields.operate.mainAttribute,
                  },
                  {
                    index: 1,
                    name: translatedFields.operate.otherAttribute,
                  },
                ]}
              />
            </caption>
            <tbody>
              {tabIndex === 0 && (
                <PrimaryProperties
                  source={[
                    {
                      field: 'name',
                      translatedField: translatedFields.properties.name,
                      value: `${source.basic.name} (ID. ${source.basic.id})`,
                    },
                    {
                      field: 'alias',
                      translatedField: translatedFields.properties.alias,
                      value: source.basic.alias,
                    },
                    {
                      field: 'overview',
                      translatedField: translatedFields.properties.overview,
                      value: source.basic.overview,
                    },
                    {
                      field: 'method',
                      translatedField: translatedFields.properties.method,
                      value: source.basic.method,
                    },
                    {
                      field: 'type',
                      translatedField: translatedFields.properties.type,
                      value: translatedFields.enums.type[source.basic.type],
                    },
                    {
                      field: 'caseInsensitive',
                      translatedField:
                        translatedFields.properties.caseInsensitive,
                      value: source.basic.caseInsensitive,
                    },
                    {
                      field: 'categoryId',
                      translatedField: translatedFields.properties.categoryId,
                      value: source.basic.categoryId,
                    },
                    {
                      field: 'categoryName',
                      translatedField: translatedFields.properties.categoryName,
                      value: source.basic.categoryName,
                    },
                    {
                      field: 'customized',
                      translatedField: translatedFields.properties.customized,
                      value: source.basic.customized,
                    },
                    {
                      field: 'sort',
                      translatedField: translatedFields.properties.sort,
                      value: source.basic.sort,
                    },
                  ]}
                />
              )}

              {tabIndex === 1 && (
                <GeneralizedProperties
                  source={source.basic}
                  translatedFields={translatedFields.generalizedProperties}
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
