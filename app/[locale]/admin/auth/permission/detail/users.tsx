'use client';

import { useQuery } from '@tanstack/react-query';
import type {
  IPagination,
  IPermissionDetails,
  IQueryParams,
  IUser,
} from '@/interfaces';
import { useState } from 'react';
import { queryPermissionUsersById } from '@/services/api';
import Alert from '@/app/[locale]/alert/alert';
import AlertLoad from '@/app/[locale]/alert/load';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Pagination from '@/app/[locale]/admin/common/pagination';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryPermissionUsers({
  source,
  translatedFields,
}: {
  source: IPermissionDetails;
  translatedFields: PrefixedTTranslatedFields<'permissionAdminPage'>;
}) {
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });

  const queryPermissionUsersByIdQuery = useQuery(
    ['/permissions', source.basic.id, '/users', params],
    async (context) => {
      return (await queryPermissionUsersById({
        id: context.queryKey[1] + '',
        query: context.queryKey[3] as any,
      })) as IPagination<IUser>;
    },
    {
      keepPreviousData: true,
    },
  );

  function onClickPrevious() {
    const data = queryPermissionUsersByIdQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryPermissionUsersByIdQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  if (queryPermissionUsersByIdQuery.data) {
    const data = queryPermissionUsersByIdQuery.data;

    return (
      <div className="card">
        <div className="card-header bg-transparent text-secondary">
          <i className="bi bi-people me-2"></i>
          用户
        </div>
        <div className="card-body">
          <div className="table-responsive">
            <table className="table align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="fw-normal">
                    {translatedFields.properties.name} / ID
                  </th>
                </tr>
              </thead>
              <tbody>
                {data.content.map((item) => {
                  return (
                    <tr key={item.id} className="text-nowrap">
                      <td>
                        {item.alias}&nbsp;(ID.&nbsp;{item.id})
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>

          {data.content.length === 0 && <Nodata />}

          <Pagination
            isShow={data.pageable.pages > 0}
            onPrevious={onClickPrevious}
            onNext={onClickNext}
            isPrevious={data.pageable.previous}
            isNext={data.pageable.next}
            translatedFields={translatedFields.pagination}
          />
        </div>
      </div>
    );
  }

  if (queryPermissionUsersByIdQuery.error) {
    return <Alert message={queryPermissionUsersByIdQuery.error} type="error" />;
  }

  return <AlertLoad />;
}
