'use client';

import type { IPermissionDetails } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QueryPermissionInfo from '@/app/[locale]/admin/auth/permission/detail/info';
import QueryPermissionUsers from '@/app/[locale]/admin/auth/permission/detail/users';
import QueryPermissionRoles from '@/app/[locale]/admin/auth/permission/detail/roles';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailPermissionAdminPage({
  source,
  translatedFields,
}: {
  source: IPermissionDetails;
  translatedFields: PrefixedTTranslatedFields<'permissionAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QueryPermissionInfo
            source={source}
            translatedFields={translatedFields}
          />

          <div className="row">
            <div className="col">
              <QueryPermissionUsers
                source={source}
                translatedFields={translatedFields}
              />
            </div>
            <div className="col">
              <QueryPermissionRoles
                source={source}
                translatedFields={translatedFields}
              />
            </div>
          </div>
        </div>
      </div>
    </Box>
  );
}
