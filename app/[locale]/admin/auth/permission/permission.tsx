'use client';

import { useQuery } from '@tanstack/react-query';
import {
  queryAllPermission,
  queryPermissionStatistics,
  searchPermission,
} from '@/services/api';
import type {
  IPagination,
  IPermission,
  IPermissionStatistics,
  IQueryParams,
} from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import Alert from '@/app/[locale]/alert/alert';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import { useRouter } from 'next/navigation';
import { type ChangeEvent, useState } from 'react';
import Pagination from '@/app/[locale]/admin/common/pagination';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function PermissionAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IPagination<IPermission>;
    translatedFields: PrefixedTTranslatedFields<'permissionAdminPage'>;
  },
) {
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });
  const [searchName, setSearchName] = useState('');
  const router = useRouter();

  const queryPermissionStatisticsQuery = useQuery(
    ['/permissions', '/statistics'],
    async () => {
      return (await queryPermissionStatistics()) as IPermissionStatistics;
    },
  );

  const queryAllPermissionQuery = useQuery(
    ['/permissions', params, searchName],
    async (context) => {
      if (searchName) {
        return (await searchPermission({
          query: {
            ...(context.queryKey[1] as any),
            name: context.queryKey[2],
          },
        })) as IPagination<IPermission>;
      } else {
        return (await queryAllPermission({
          query: context.queryKey[1] as any,
        })) as IPagination<IPermission>;
      }
    },
    {
      keepPreviousData: true,
      initialData: source,
    },
  );

  function onClickDetail(item: IPermission) {
    router.push(`permission/detail?id=${item.id}`);
  }

  function onClickUpdate(item: IPermission) {
    router.push(`permission/update?id=${item.id}`);
  }

  function onClickPrevious() {
    const data = queryAllPermissionQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryAllPermissionQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  function onChangeSearch(e: ChangeEvent<HTMLInputElement>) {
    const value = e.target.value;
    setSearchName(value.toLowerCase());
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.count.permission}
              </h6>
              <h5 className="card-title">
                {queryPermissionStatisticsQuery.data &&
                  queryPermissionStatisticsQuery.data.count}

                {queryPermissionStatisticsQuery.isError && (
                  <Alert
                    message={queryPermissionStatisticsQuery.error}
                    type="error"
                  />
                )}

                {queryPermissionStatisticsQuery.isLoading && <Spinner />}
              </h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col">
        <Box>
          <div className="card">
            <div className="card-header bg-transparent">
              <div className="row row-cols-auto justify-content-between g-2">
                <div className="col">
                  <div className="input-group">
                    <span className="input-group-text">
                      <i className="bi bi-search"></i>
                    </span>
                    <input
                      onChange={onChangeSearch}
                      type="search"
                      className="form-control"
                      placeholder={translatedFields.tip.searchPlaceholder}
                    />
                  </div>
                </div>
                <div className="col"></div>
              </div>
            </div>
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        ID
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.alias}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.categoryId}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.categoryName}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.customized}
                      </th>
                      <th scope="col" className=""></th>
                    </tr>
                  </thead>
                  <tbody>
                    {queryAllPermissionQuery.data.content.map((item) => {
                      return (
                        <tr key={item.id} className="text-nowrap">
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.id}
                          </td>
                          <td
                            className="cursor-pointer text-truncate"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.alias}
                          </td>
                          <td
                            className="cursor-pointer text-truncate"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.categoryId}
                          </td>
                          <td
                            className="cursor-pointer text-truncate"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.categoryName}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.customized + ''}
                          </td>
                          <td>
                            <div className="hstack gap-2">
                              <button
                                onClick={onClickUpdate.bind(this, item)}
                                className="btn btn-sm btn-light"
                                type="button"
                              >
                                <i className="bi bi-pencil me-2"></i>
                                {translatedFields.operate.update}
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>

              {queryAllPermissionQuery.data.content.length === 0 && <Nodata />}

              <Pagination
                isShow={queryAllPermissionQuery.data.pageable.pages > 0}
                onPrevious={onClickPrevious}
                onNext={onClickNext}
                isPrevious={queryAllPermissionQuery.data.pageable.previous}
                isNext={queryAllPermissionQuery.data.pageable.next}
                translatedFields={translatedFields.pagination}
              />
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
