'use client';

import { useState } from 'react';
import type {
  IPagination,
  IPermissionDetails,
  IQueryParams,
  IRole,
} from '@/interfaces';
import { useMutation, useQuery } from '@tanstack/react-query';
import {
  assignPermission,
  queryAllRole,
  queryPermissionRolesById,
  unAssignPermission,
} from '@/services/api';
import useToast from '@/hooks/useToast';
import Alert from '@/app/[locale]/alert/alert';
import AlertLoad from '@/app/[locale]/alert/load';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Pagination from '@/app/[locale]/admin/common/pagination';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateRolePermissionAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IPermissionDetails;
    translatedFields: PrefixedTTranslatedFields<'permissionAdminPage'>;
  },
) {
  return (
    <Box>
      <div className="row">
        <div className="col">
          <RoleList source={source} translatedFields={translatedFields} />
        </div>
        <div className="col">
          <RoleAddList source={source} translatedFields={translatedFields} />
        </div>
      </div>
    </Box>
  );
}

const RoleList = ({
  source,
  translatedFields,
}: {
  source: IPermissionDetails;
  translatedFields: PrefixedTTranslatedFields<'permissionAdminPage'>;
}) => {
  const { show } = useToast();
  const [currentItem, setCurrentItem] = useState<IRole>();
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });

  const queryPermissionRolesByIdQuery = useQuery(
    ['/permissions', source.basic.id, '/roles', params],
    async (context) => {
      return (await queryPermissionRolesById({
        id: context.queryKey[1] + '',
        query: context.queryKey[3] as any,
      })) as IPagination<IRole>;
    },
    {
      keepPreviousData: true,
    },
  );

  const unAssignPermissionMutation = useMutation(unAssignPermission);

  async function onClickRemove(item: IRole) {
    try {
      setCurrentItem(item);

      const id = source.basic.id;
      const roleId = item.id + '';
      await unAssignPermissionMutation.mutateAsync({
        id,
        data: {
          roleId,
        },
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.operate.deleteCompleted,
      });
    } catch (e) {
      unAssignPermissionMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setCurrentItem(undefined);
    }
  }

  function onClickPrevious() {
    const data = queryPermissionRolesByIdQuery.data;
    if (!data) {
      show({
        type: 'DANGER',
        message: '数据不存在',
      });
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryPermissionRolesByIdQuery.data;
    if (!data) {
      show({
        type: 'DANGER',
        message: '数据不存在',
      });
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  if (queryPermissionRolesByIdQuery.data) {
    const data = queryPermissionRolesByIdQuery.data;

    return (
      <div className="card">
        <div className="card-header bg-transparent text-secondary">
          <i className="bi bi-list me-2"></i>
          {translatedFields.properties.roleList}
        </div>
        <div className="card-body vstack gap-4">
          <div className="table-responsive">
            <table className="table align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="fw-normal">
                    {translatedFields.properties.name} / ID
                  </th>
                  <th scope="col" className=""></th>
                </tr>
              </thead>
              <tbody>
                {data.content.map((item) => {
                  return (
                    <tr key={item.id} className="text-nowrap">
                      <td>
                        {item.name}&nbsp;(ID.&nbsp;{item.id})
                      </td>
                      <td>
                        <button
                          disabled={
                            item.id === currentItem?.id &&
                            unAssignPermissionMutation.isLoading
                          }
                          onClick={onClickRemove.bind(this, item)}
                          className="btn btn-sm btn-danger"
                          type="button"
                        >
                          {item.id === currentItem?.id &&
                          unAssignPermissionMutation.isLoading ? (
                            <Spinner classs="me-2" />
                          ) : (
                            <i className="bi bi-trash me-2"></i>
                          )}
                          {translatedFields.operate.delete}
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>

          {data.content.length === 0 && <Nodata />}

          <Pagination
            isShow={data.pageable.pages > 0}
            onPrevious={onClickPrevious}
            onNext={onClickNext}
            isPrevious={data.pageable.previous}
            isNext={data.pageable.next}
            translatedFields={translatedFields.pagination}
          />
        </div>
      </div>
    );
  }

  if (queryPermissionRolesByIdQuery.error) {
    return <Alert message={queryPermissionRolesByIdQuery.error} type="error" />;
  }

  return <AlertLoad />;
};

const RoleAddList = ({
  source,
  translatedFields,
}: {
  source: IPermissionDetails;
  translatedFields: PrefixedTTranslatedFields<'permissionAdminPage'>;
}) => {
  const { show } = useToast();
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
    size: 5,
  });
  const [currentItem, setCurrentItem] = useState<IRole>();

  const queryAllRoleQuery = useQuery(
    ['/roles', params],
    async (context) => {
      return (await queryAllRole({
        query: context.queryKey[1] as any,
      })) as IPagination<IRole>;
    },
    {
      keepPreviousData: true,
    },
  );

  const assignPermissionMutation = useMutation(assignPermission);

  async function onClickAdd(item: IRole) {
    try {
      setCurrentItem(item);

      const id = source.basic.id;
      const roleId = item.id + '';
      await assignPermissionMutation.mutateAsync({
        id,
        data: {
          roleId,
        },
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.operate.addCompleted,
      });
    } catch (e) {
      assignPermissionMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setCurrentItem(undefined);
    }
  }

  function onClickPrevious() {
    const data = queryAllRoleQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryAllRoleQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  if (queryAllRoleQuery.data) {
    const data = queryAllRoleQuery.data;

    return (
      <div className="card">
        <div className="card-header bg-transparent text-secondary">
          <i className="bi bi-list me-2"></i>
          {translatedFields.properties.addRole}
        </div>
        <div className="card-body vstack gap-4">
          <div className="table-responsive">
            <table className="table align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="fw-normal">
                    {translatedFields.properties.name} / ID
                  </th>
                  <th scope="col" className=""></th>
                </tr>
              </thead>
              <tbody>
                {data.content.map((item) => {
                  return (
                    <tr key={item.id} className="text-nowrap">
                      <td>
                        {item.name}&nbsp;(ID.&nbsp;{item.id})
                      </td>
                      <td>
                        <button
                          disabled={
                            item.id === currentItem?.id &&
                            assignPermissionMutation.isLoading
                          }
                          onClick={onClickAdd.bind(this, item)}
                          className="btn btn-sm btn-light"
                          type="button"
                        >
                          {item.id === currentItem?.id &&
                          assignPermissionMutation.isLoading ? (
                            <Spinner classs="me-2" />
                          ) : (
                            <i className="bi bi-plus-lg me-2"></i>
                          )}
                          {translatedFields.operate.add}
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>

          {data.content.length === 0 && <Nodata />}

          <Pagination
            isShow={data.pageable.pages > 0}
            onPrevious={onClickPrevious}
            onNext={onClickNext}
            isPrevious={data.pageable.previous}
            isNext={data.pageable.next}
            translatedFields={translatedFields.pagination}
          />
        </div>
      </div>
    );
  }

  if (queryAllRoleQuery.error) {
    return <Alert message={queryAllRoleQuery.error} type="error" />;
  }

  return <AlertLoad />;
};
