import { type ReactNode } from 'react';
import Box from '@/app/[locale]/admin/common/box';
import UpdateNav from '@/app/[locale]/admin/auth/permission/update/update-nav';
import { getTranslatedFields } from '@/lib/dictionaries';
import { authMiddleware } from '@/lib/api';
import { cookies } from 'next/headers';
import { clientQueryAllMenu, queryPath } from '@/services/api';
import type { IMenu, IPath } from '@/interfaces';

async function getData() {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/',
      },
    });
    const req2 = clientQueryAllMenu({
      baseURL: process.env.APP_API_SERVER,
      token,
    });

    const responses = await Promise.all([req1, req2]);
    const resp1 = await ((await responses[0]) as Response).json();
    const resp2 = await ((await responses[1]) as Response).json();

    return {
      isSuccess: true,
      path: resp1.data as IPath,
      menus: resp2.data as IMenu[],
    };
  } catch (e) {
    return {
      isSuccess: false,
    };
  }
}

export default async function DashboardLayout({
  children,
  params: { locale },
}: {
  children: ReactNode;
  params: { locale: string };
}) {
  const data = await getData();

  if (data.isSuccess) {
    const translatedFields = await getTranslatedFields(
      locale,
      'permissionAdminPage',
    );

    return (
      <>
        <Box boxClass="mb-4">
          <UpdateNav translatedFields={translatedFields} />
        </Box>

        {children}
      </>
    );
  }

  return <>{children}</>;
}
