'use client';

import type { IRole } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QueryRoleInfo from '@/app/[locale]/admin/auth/role/detail/info';
import QueryRoleUsers from '@/app/[locale]/admin/auth/role/detail/users';
import QueryRolePermissions from '@/app/[locale]/admin/auth/role/detail/permissions';
import MenusRoleAdminPage from '@/app/[locale]/admin/auth/role/detail/menus';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailRoleAdminPage({
  source,
  translatedFields,
}: {
  source: IRole;
  translatedFields: PrefixedTTranslatedFields<'roleAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QueryRoleInfo source={source} translatedFields={translatedFields} />

          <div className="row">
            <div className="col">
              <QueryRoleUsers
                source={source}
                translatedFields={translatedFields}
              />
            </div>
            <div className="col">
              <QueryRolePermissions
                source={source}
                translatedFields={translatedFields}
              />
            </div>
          </div>

          <MenusRoleAdminPage
            source={source}
            translatedFields={translatedFields}
          />
        </div>
      </div>
    </Box>
  );
}
