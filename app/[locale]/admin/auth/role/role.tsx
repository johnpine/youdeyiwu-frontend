'use client';

import { useQuery } from '@tanstack/react-query';
import { queryAllRole, queryRoleStatistics, searchRole } from '@/services/api';
import type {
  IPagination,
  IQueryParams,
  IRole,
  IRoleStatistics,
} from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import Alert from '@/app/[locale]/alert/alert';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import { useRouter } from 'next/navigation';
import { type ChangeEvent, useState } from 'react';
import Pagination from '@/app/[locale]/admin/common/pagination';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function RoleAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IPagination<IRole>;
    translatedFields: PrefixedTTranslatedFields<'roleAdminPage'>;
  },
) {
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });
  const [searchName, setSearchName] = useState('');
  const router = useRouter();

  const queryRoleStatisticsQuery = useQuery(
    ['/roles', '/statistics'],
    async (context) => {
      return (await queryRoleStatistics()) as IRoleStatistics;
    },
  );

  const queryAllRoleQuery = useQuery(
    ['/roles', params, searchName],
    async (context) => {
      if (searchName) {
        return (await searchRole({
          query: {
            ...(context.queryKey[1] as any),
            name: context.queryKey[2],
          },
        })) as IPagination<IRole>;
      } else {
        return (await queryAllRole({
          query: context.queryKey[1] as any,
        })) as IPagination<IRole>;
      }
    },
    {
      keepPreviousData: true,
      initialData: source,
    },
  );

  function onClickDetail(item: IRole) {
    router.push(`role/detail?id=${item.id}`);
  }

  function onClickUpdate(item: IRole) {
    router.push(`role/update?id=${item.id}`);
  }

  function onClickPrevious() {
    const data = queryAllRoleQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryAllRoleQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  function onChangeSearch(e: ChangeEvent<HTMLInputElement>) {
    const value = e.target.value;
    setSearchName(value.toLowerCase());
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.count.role}
              </h6>
              <h5 className="card-title">
                {queryRoleStatisticsQuery.data &&
                  queryRoleStatisticsQuery.data.count}

                {queryRoleStatisticsQuery.isError && (
                  <Alert
                    message={queryRoleStatisticsQuery.error}
                    type="error"
                  />
                )}

                {queryRoleStatisticsQuery.isLoading && <Spinner />}
              </h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col">
        <Box>
          <div className="card">
            <div className="card-header bg-transparent">
              <div className="row row-cols-auto justify-content-between g-2">
                <div className="col">
                  <div className="input-group">
                    <span className="input-group-text">
                      <i className="bi bi-search"></i>
                    </span>
                    <input
                      onChange={onChangeSearch}
                      type="search"
                      className="form-control"
                      placeholder={translatedFields.tip.searchPlaceholder}
                    />
                  </div>
                </div>
                <div className="col"></div>
              </div>
            </div>
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        ID
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.name}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.sort}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.hide}
                      </th>
                      <th scope="col" className=""></th>
                    </tr>
                  </thead>
                  <tbody>
                    {queryAllRoleQuery.data.content.map((item) => {
                      return (
                        <tr key={item.id} className="text-nowrap">
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.id}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.name}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.sort}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.hide + ''}
                          </td>
                          <td>
                            <div className="hstack gap-2">
                              <button
                                onClick={onClickUpdate.bind(this, item)}
                                className="btn btn-sm btn-light"
                                type="button"
                              >
                                <i className="bi bi-pencil me-2"></i>
                                {translatedFields.operate.update}
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>

              {queryAllRoleQuery.data.content.length === 0 && <Nodata />}

              <Pagination
                isShow={queryAllRoleQuery.data.pageable.pages > 0}
                onPrevious={onClickPrevious}
                onNext={onClickNext}
                isPrevious={queryAllRoleQuery.data.pageable.previous}
                isNext={queryAllRoleQuery.data.pageable.next}
                translatedFields={translatedFields.pagination}
              />
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
