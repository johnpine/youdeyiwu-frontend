'use client';

import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateNav({
  translatedFields,
}: {
  translatedFields: PrefixedTTranslatedFields<'roleAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);
  const searchParams = useSearchParams();
  const id = searchParams.get('id');

  return (
    <ToggleProperties
      tabIndex={tabIndex}
      setTabIndex={setTabIndex}
      mb4={false}
      isTable={false}
      items={[
        {
          index: 0,
          name: translatedFields.properties.info,
          href: `/admin/auth/role/update?id=${id}`,
          segment: null,
        },
        {
          index: 1,
          name: translatedFields.properties.user,
          href: `/admin/auth/role/update/user?id=${id}`,
          segment: 'user',
        },
        {
          index: 2,
          name: translatedFields.properties.menu,
          href: `/admin/auth/role/update/menu?id=${id}`,
          segment: 'menu',
        },
      ]}
    />
  );
}
