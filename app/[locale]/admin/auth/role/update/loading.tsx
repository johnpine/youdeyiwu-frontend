import AlertLoad from '@/app/[locale]/alert/load';

export default function Loading() {
  return <AlertLoad />;
}
