'use client';

import { useState } from 'react';
import type { IPagination, IQueryParams, IRole, IUser } from '@/interfaces';
import { useMutation, useQuery } from '@tanstack/react-query';
import {
  assignRole,
  queryAllUser,
  queryRoleUsersById,
  unAssignRole,
} from '@/services/api';
import useToast from '@/hooks/useToast';
import Alert from '@/app/[locale]/alert/alert';
import AlertLoad from '@/app/[locale]/alert/load';
import Box from '@/app/[locale]/admin/common/box';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Pagination from '@/app/[locale]/admin/common/pagination';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UserRoleAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IRole;
    translatedFields: PrefixedTTranslatedFields<'roleAdminPage'>;
  },
) {
  return (
    <Box>
      <div className="row">
        <div className="col">
          <RoleUserList source={source} translatedFields={translatedFields} />
        </div>
        <div className="col">
          <UserAddList source={source} translatedFields={translatedFields} />
        </div>
      </div>
    </Box>
  );
}

const RoleUserList = ({
  source,
  translatedFields,
}: {
  source: IRole;
  translatedFields: PrefixedTTranslatedFields<'roleAdminPage'>;
}) => {
  const { show } = useToast();
  const [currentItem, setCurrentItem] = useState<IUser>();
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
    size: 5,
  });

  const queryRoleUsersByIdQuery = useQuery(
    ['/roles', source.id, '/users', params],
    async (context) => {
      return (await queryRoleUsersById({
        id: context.queryKey[1] + '',
        query: context.queryKey[3] as any,
      })) as IPagination<IUser>;
    },
    {
      keepPreviousData: true,
    },
  );

  const unAssignRoleMutation = useMutation(unAssignRole);

  async function onClickRemove(item: IUser) {
    try {
      setCurrentItem(item);

      const id = source.id;
      const userId = item.id + '';
      await unAssignRoleMutation.mutateAsync({
        id,
        data: {
          userId,
        },
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.operate.deleteCompleted,
      });
    } catch (e) {
      unAssignRoleMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setCurrentItem(undefined);
    }
  }

  function onClickPrevious() {
    const data = queryRoleUsersByIdQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryRoleUsersByIdQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  if (queryRoleUsersByIdQuery.data) {
    const data = queryRoleUsersByIdQuery.data;

    return (
      <div className="card">
        <div className="card-header bg-transparent text-secondary">
          <i className="bi bi-people me-2"></i>
          {translatedFields.properties.userList}
        </div>
        <div className="card-body vstack gap-4">
          <div className="table-responsive">
            <table className="table align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="fw-normal">
                    {translatedFields.properties.name} / ID
                  </th>
                  <th scope="col" className=""></th>
                </tr>
              </thead>
              <tbody>
                {data.content.map((item) => {
                  return (
                    <tr key={item.id} className="text-nowrap">
                      <td>
                        {item.alias}&nbsp;(ID.&nbsp;{item.id})
                      </td>
                      <td>
                        <button
                          disabled={
                            item.id === currentItem?.id &&
                            unAssignRoleMutation.isLoading
                          }
                          onClick={onClickRemove.bind(this, item)}
                          className="btn btn-sm btn-danger"
                          type="button"
                        >
                          {item.id === currentItem?.id &&
                          unAssignRoleMutation.isLoading ? (
                            <Spinner classs="me-2" />
                          ) : (
                            <i className="bi bi-trash me-2"></i>
                          )}
                          {translatedFields.operate.delete}
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>

          {data.content.length === 0 && <Nodata />}

          <Pagination
            isShow={data.pageable.pages > 0}
            onPrevious={onClickPrevious}
            onNext={onClickNext}
            isPrevious={data.pageable.previous}
            isNext={data.pageable.next}
            translatedFields={translatedFields.pagination}
          />
        </div>
      </div>
    );
  }

  if (queryRoleUsersByIdQuery.error) {
    return <Alert message={queryRoleUsersByIdQuery.error} type="error" />;
  }

  return <AlertLoad />;
};

const UserAddList = ({
  source,
  translatedFields,
}: {
  source: IRole;
  translatedFields: PrefixedTTranslatedFields<'roleAdminPage'>;
}) => {
  const { show } = useToast();
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
    size: 5,
  });
  const [currentItem, setCurrentItem] = useState<IUser>();

  const queryAllUserQuery = useQuery(
    ['/users', params],
    async (context) => {
      return (await queryAllUser({
        query: context.queryKey[1] as any,
      })) as IPagination<IUser>;
    },
    {
      keepPreviousData: true,
    },
  );

  const assignRoleMutation = useMutation(assignRole);

  async function onClickAdd(item: IUser) {
    try {
      setCurrentItem(item);

      const id = source.id;
      const userId = item.id + '';
      await assignRoleMutation.mutateAsync({
        id,
        data: {
          userId,
        },
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.operate.addCompleted,
      });
    } catch (e) {
      assignRoleMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setCurrentItem(undefined);
    }
  }

  function onClickPrevious() {
    const data = queryAllUserQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryAllUserQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  if (queryAllUserQuery.data) {
    const data = queryAllUserQuery.data;

    return (
      <div className="card">
        <div className="card-header bg-transparent text-secondary">
          <i className="bi bi-people me-2"></i>
          {translatedFields.properties.addUser}
        </div>
        <div className="card-body vstack gap-4">
          <div className="table-responsive">
            <table className="table align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="fw-normal">
                    {translatedFields.properties.name} / ID
                  </th>
                  <th scope="col" className=""></th>
                </tr>
              </thead>
              <tbody>
                {data.content.map((item) => {
                  return (
                    <tr key={item.id} className="text-nowrap">
                      <td>
                        {item.alias}&nbsp;(ID.&nbsp;{item.id})
                      </td>
                      <td>
                        <button
                          disabled={
                            item.id === currentItem?.id &&
                            assignRoleMutation.isLoading
                          }
                          onClick={onClickAdd.bind(this, item)}
                          className="btn btn-sm btn-light"
                          type="button"
                        >
                          {item.id === currentItem?.id &&
                          assignRoleMutation.isLoading ? (
                            <Spinner classs="me-2" />
                          ) : (
                            <i className="bi bi-plus-lg me-2"></i>
                          )}
                          {translatedFields.operate.add}
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>

          {data.content.length === 0 && <Nodata />}

          <Pagination
            isShow={data.pageable.pages > 0}
            onPrevious={onClickPrevious}
            onNext={onClickNext}
            isPrevious={data.pageable.previous}
            isNext={data.pageable.next}
            translatedFields={translatedFields.pagination}
          />
        </div>
      </div>
    );
  }

  if (queryAllUserQuery.error) {
    return <Alert message={queryAllUserQuery.error} type="error" />;
  }

  return <AlertLoad />;
};
