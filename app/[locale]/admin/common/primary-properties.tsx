import { Fragment } from 'react';
import Link from 'next/link';

export interface IPrimaryProperty {
  translatedField?: string;
  field: string;
  value: any;
  href?: string;
  isCustomValue?: boolean;
  isEmptyValue?: boolean;
}

export default function PrimaryProperties({
  source,
}: {
  source: IPrimaryProperty[];
}) {
  return (
    <>
      {source.map((item) => {
        let value = item.value;
        if (!item.isCustomValue) {
          if (typeof item.value === 'boolean') {
            value = item.value + '';
          } else if (Array.isArray(item.value)) {
            value = item.value.length > 0 ? item.value.join(', ') : '';
          } else if (typeof item.value === 'object') {
            value =
              Object.keys(item.value).length > 0
                ? JSON.stringify(item.value)
                : '';
          } else if (typeof item.value === 'function') {
            value = '';
          }
        }

        if (
          item.isEmptyValue ||
          value === '' ||
          value === null ||
          value === undefined
        ) {
          return <Fragment key={item.field}></Fragment>;
        }

        return (
          <tr key={item.field} className="text-nowrap align-middle">
            <th>{item.translatedField ? item.translatedField : item.field}</th>
            <td>
              {item.href ? (
                <Link
                  href={item.href}
                  className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
                >
                  {value}
                </Link>
              ) : (
                value
              )}
            </td>
          </tr>
        );
      })}
    </>
  );
}
