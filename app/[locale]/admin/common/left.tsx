import Image from 'next/image';
import Link from 'next/link';
import { getUserAvatar } from '@/lib/tool';
import Menus from '@/app/[locale]/admin/common/menu/menus';
import MenusNodata from '@/app/[locale]/admin/common/menu/nodata';
import type { IMenu, IPath } from '@/interfaces';
import FullScreen from '@/app/[locale]/admin/common/full';

export default async function LeftLayoutAdmin({
  path,
  menus,
  locale,
}: {
  path: IPath;
  menus: IMenu[];
  locale: string;
}) {
  const user = path.user!;

  return (
    <div className="card rounded-4 border-0">
      <div className="card-body d-flex flex-column gap-2">
        <div className="d-flex justify-content-between">
          <div></div>
          <FullScreen />
        </div>

        <div className="text-center my-4">
          <Image
            className="rounded-circle"
            src={
              getUserAvatar(user, process.env.APP_BLUR_DATA_URL as string)
                .mediumAvatarUrl
            }
            alt="avatar"
            width={100}
            height={100}
            placeholder="blur"
            blurDataURL={process.env.APP_BLUR_DATA_URL}
          />
        </div>

        <h5 className="card-title text-center">
          <Link
            href={`/users/${user.id}`}
            className="text-reset link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
          >
            {`${user.alias} (ID. ${user.id})`}
          </Link>
        </h5>

        <div className="card-text text-center">
          {user.roles
            .filter((item) => !item.hide)
            .map((item) => item.name)
            .join(' / ')}
        </div>

        <hr className="text-secondary" />

        {menus.length > 0 ? (
          <Menus items={menus} />
        ) : (
          <MenusNodata locale={locale} />
        )}
      </div>
    </div>
  );
}
