import { getTranslator } from 'next-intl/server';

export default async function MenusNodata({ locale }: { locale: string }) {
  const t = await getTranslator(locale);

  return (
    <div className="text-center text-secondary">
      {t('adminPage.noMenuAvailable')}
    </div>
  );
}
