import classNames from 'classnames';
import { type Dispatch, type MouseEvent, type SetStateAction } from 'react';
import Link from 'next/link';
import { useSelectedLayoutSegment } from 'next/navigation';

export interface IToggleProperty {
  segment?: any;
  index: number;
  name: string;
  icon?: string;
  isLoading?: boolean;
  callback?: (index: number) => Promise<void>;
  href?: string;
}

export default function ToggleProperties(
  this: any,
  {
    tabIndex,
    setTabIndex,
    items,
    navClasss,
    isTable = true,
    mb4 = true,
  }: {
    navClasss?: string;
    tabIndex?: number;
    setTabIndex?: Dispatch<SetStateAction<number>>;
    items: IToggleProperty[];
    isTable?: boolean;
    mb4?: boolean;
  },
) {
  const selectedLayoutSegment = useSelectedLayoutSegment();

  async function onClickTab(
    item: IToggleProperty,
    e: MouseEvent<HTMLAnchorElement>,
  ) {
    if (!item.href) {
      e.stopPropagation();
      e.preventDefault();
    }

    if (typeof item.callback === 'function') {
      await item.callback(item.index);
    } else if (typeof setTabIndex === 'function') {
      setTabIndex(item.index);
    }
  }

  if (isTable) {
    return (
      <tr>
        <td colSpan={2}>
          <ul
            className={classNames('nav nav-pills', navClasss, {
              'mb-4': mb4,
            })}
          >
            {items.map((item) => {
              return (
                <li key={item.index} className="nav-item">
                  <Link
                    href={item.href ? item.href : ''}
                    className={classNames('nav-link cursor-pointer', {
                      active: item.href
                        ? selectedLayoutSegment === item.segment
                        : tabIndex === item.index,
                      disabled: item.isLoading,
                    })}
                    onClick={onClickTab.bind(this, item)}
                    aria-current="page"
                  >
                    {item.isLoading ? (
                      <span
                        className="spinner-border spinner-border-sm"
                        aria-hidden="true"
                      ></span>
                    ) : (
                      <>
                        {item.icon && (
                          <i className={classNames('bi me-2', item.icon)}></i>
                        )}
                      </>
                    )}
                    {item.name}
                  </Link>
                </li>
              );
            })}
          </ul>
        </td>
      </tr>
    );
  }

  return (
    <ul
      className={classNames('nav nav-pills', navClasss, {
        'mb-4': mb4,
      })}
    >
      {items.map((item) => {
        return (
          <li key={item.index} className="nav-item">
            <Link
              href={item.href ? item.href : ''}
              className={classNames('nav-link cursor-pointer', {
                active: item.href
                  ? selectedLayoutSegment === item.segment
                  : tabIndex === item.index,
                disabled: item.isLoading,
              })}
              onClick={onClickTab.bind(this, item)}
              aria-current="page"
            >
              {item.isLoading ? (
                <span
                  className="spinner-border spinner-border-sm"
                  aria-hidden="true"
                ></span>
              ) : (
                <>
                  {item.icon && (
                    <i className={classNames('bi me-2', item.icon)}></i>
                  )}
                </>
              )}
              {item.name}
            </Link>
          </li>
        );
      })}
    </ul>
  );
}
