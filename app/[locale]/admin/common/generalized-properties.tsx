import { formatDateTime } from '@/lib/tool';

export type TGeneralizedPropertiesTranslatedField =
  | 'createdBy'
  | 'updatedBy'
  | 'createdOn'
  | 'updatedOn'
  | 'deleted';

export default function GeneralizedProperties({
  source,
  translatedFields = {
    createdBy: '创建者',
    updatedBy: '更新者',
    createdOn: '创建时间',
    updatedOn: '更新时间',
    deleted: '是否删除',
  },
}: {
  source: any;
  translatedFields?: Record<TGeneralizedPropertiesTranslatedField, any>;
}) {
  return (
    <>
      {source.creatorAlias && source.createdBy && (
        <tr className="text-nowrap align-middle">
          <th>{translatedFields.createdBy}</th>
          <td>
            {source.creatorAlias} (ID. {source.createdBy})
          </td>
        </tr>
      )}

      {source.updaterAlias && source.updatedBy && (
        <tr className="text-nowrap align-middle">
          <th>{translatedFields.updatedBy}</th>
          <td>
            {source.updaterAlias} (ID. {source.updatedBy})
          </td>
        </tr>
      )}

      {source.createdOn && (
        <tr className="text-nowrap align-middle">
          <th>{translatedFields.createdOn}</th>
          <td>{formatDateTime(source.createdOn)}</td>
        </tr>
      )}

      {source.updatedOn && (
        <tr className="text-nowrap align-middle">
          <th>{translatedFields.updatedOn}</th>
          <td>{formatDateTime(source.updatedOn)}</td>
        </tr>
      )}

      {typeof source.deleted === 'boolean' && (
        <tr className="text-nowrap align-middle">
          <th>{translatedFields.deleted}</th>
          <td>{source.deleted + ''}</td>
        </tr>
      )}
    </>
  );
}
