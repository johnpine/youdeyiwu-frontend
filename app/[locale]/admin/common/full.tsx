'use client';

import { useEffect, useState } from 'react';

export default function FullScreen() {
  const [isClick, setIsClick] = useState(false);

  function onClickIcon() {
    setIsClick(!isClick);
  }

  useEffect(() => {
    const selector = document.querySelector('.yw-left-layout-admin');

    if (isClick) {
      selector?.classList.add('w-100');
    } else {
      selector?.classList.remove('w-100');
    }

    return () => {
      if (isClick && selector) {
        selector.classList.remove('w-100');
      }
    };
  }, [isClick]);

  return (
    <div>
      <i
        onClick={onClickIcon}
        className="bi bi-aspect-ratio fs-5 cursor-pointer"
        title="Full Screen"
      ></i>
    </div>
  );
}
