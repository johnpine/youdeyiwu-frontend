import { type ReactNode } from 'react';
import classNames from 'classnames';

export default function Box({
  children,
  boxClass,
  itemClass,
  h100,
}: {
  children: ReactNode;
  boxClass?: string;
  itemClass?: string;
  h100?: boolean;
}) {
  return (
    <div
      className={classNames(
        'card border-0 rounded-4',
        {
          'h-100': h100,
        },
        boxClass,
      )}
    >
      <div className={classNames('card-body', itemClass)}>{children}</div>
    </div>
  );
}
