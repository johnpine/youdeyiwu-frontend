'use client';

import { useState } from 'react';
import { usePathname } from 'next/navigation';
import Link from 'next/link';
import classNames from 'classnames';

export interface ISubmenuNavItem {
  path: string;
  name: string;
  icon?: string;
}

export default function SubmenuNav(
  this: any,
  { items }: { items: ISubmenuNavItem[] },
) {
  const pathname = usePathname();
  const [currentItem, setCurrentItem] = useState<ISubmenuNavItem>();

  function onClickItem(item: ISubmenuNavItem) {
    setCurrentItem(item);
  }

  return (
    <div className="btn-group mb-4" role="group" aria-label="nav">
      {items.map((item) => {
        return (
          <Link
            key={item.path}
            onClick={onClickItem.bind(this, item)}
            href={item.path}
            className={classNames('btn btn-outline-primary', {
              active: item.path === pathname,
            })}
            aria-current={item.path === pathname ? 'page' : 'false'}
          >
            {item.icon && <i className={classNames('bi me-2', item.icon)}></i>}
            {item.name}
          </Link>
        );
      })}
    </div>
  );
}
