'use client';

import { type ReactNode } from 'react';
import { SectionAdminContext } from '@/contexts/admin/forum/section';
import { TagAdminContext } from '@/contexts/admin/forum/tag';
import { PostAdminContext } from '@/contexts/admin/forum/post';
import { ReplyAdminContext } from '@/contexts/admin/forum/reply';
import { TagGroupAdminContext } from '@/contexts/admin/forum/tag-group';
import { SectionGroupAdminContext } from '@/contexts/admin/forum/sectionGroup';
import { PostTemplateAdminContext } from '@/contexts/admin/forum/postTemplate';
import { RoleAdminContext } from '@/contexts/admin/auth/role';
import { PermissionAdminContext } from '@/contexts/admin/auth/permission';
import { MessageAdminContext } from '@/contexts/admin/message';
import { OauthClientAdminContext } from '@/contexts/admin/oauth/client';
import { OauthClientApiAdminContext } from '@/contexts/admin/oauth/clientApi';
import { MenuAdminContext } from '@/contexts/admin/menu';
import { ConfigAdminContext } from '@/contexts/admin/config';
import { LogAdminContext } from '@/contexts/admin/log';

export default function SubmenuContent({
  type,
  children,
}: {
  type:
    | 'section'
    | 'tag'
    | 'post'
    | 'reply'
    | 'tagGroup'
    | 'sectionGroup'
    | 'postTemplate'
    | 'role'
    | 'permission'
    | 'message'
    | 'oauthClient'
    | 'oauthClientApi'
    | 'menu'
    | 'config'
    | 'log';
  children: ReactNode;
}) {
  if (type === 'section') {
    return (
      <SectionAdminContext.Provider value={{}}>
        {children}
      </SectionAdminContext.Provider>
    );
  } else if (type === 'tag') {
    return (
      <TagAdminContext.Provider value={{}}>{children}</TagAdminContext.Provider>
    );
  } else if (type === 'post') {
    return (
      <PostAdminContext.Provider value={{}}>
        {children}
      </PostAdminContext.Provider>
    );
  } else if (type === 'reply') {
    return (
      <ReplyAdminContext.Provider value={{}}>
        {children}
      </ReplyAdminContext.Provider>
    );
  } else if (type === 'tagGroup') {
    return (
      <TagGroupAdminContext.Provider value={{}}>
        {children}
      </TagGroupAdminContext.Provider>
    );
  } else if (type === 'sectionGroup') {
    return (
      <SectionGroupAdminContext.Provider value={{}}>
        {children}
      </SectionGroupAdminContext.Provider>
    );
  } else if (type === 'postTemplate') {
    return (
      <PostTemplateAdminContext.Provider value={{}}>
        {children}
      </PostTemplateAdminContext.Provider>
    );
  } else if (type === 'role') {
    return (
      <RoleAdminContext.Provider value={{}}>
        {children}
      </RoleAdminContext.Provider>
    );
  } else if (type === 'permission') {
    return (
      <PermissionAdminContext.Provider value={{}}>
        {children}
      </PermissionAdminContext.Provider>
    );
  } else if (type === 'message') {
    return (
      <MessageAdminContext.Provider value={{}}>
        {children}
      </MessageAdminContext.Provider>
    );
  } else if (type === 'oauthClient') {
    return (
      <OauthClientAdminContext.Provider value={{}}>
        {children}
      </OauthClientAdminContext.Provider>
    );
  } else if (type === 'oauthClientApi') {
    return (
      <OauthClientApiAdminContext.Provider value={{}}>
        {children}
      </OauthClientApiAdminContext.Provider>
    );
  } else if (type === 'menu') {
    return (
      <MenuAdminContext.Provider value={{}}>
        {children}
      </MenuAdminContext.Provider>
    );
  } else if (type === 'config') {
    return (
      <ConfigAdminContext.Provider value={{}}>
        {children}
      </ConfigAdminContext.Provider>
    );
  } else if (type === 'log') {
    return (
      <LogAdminContext.Provider value={{}}>{children}</LogAdminContext.Provider>
    );
  }

  return <> {children}</>;
}
