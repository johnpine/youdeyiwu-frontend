import type { ReactNode } from 'react';

export default function RightLayoutAdmin({
  children,
}: {
  children: ReactNode;
}) {
  return children;
}
