import { type MouseEvent } from 'react';
import classNames from 'classnames';

export type TPaginationTranslatedField = 'previous' | 'next';

export default function Pagination({
  onPrevious,
  onNext,
  isPrevious,
  isNext,
  translatedFields = {
    previous: '上一页',
    next: '下一页',
  },
  isShow,
}: {
  onPrevious: () => void;
  onNext: () => void;
  isPrevious?: boolean;
  isNext?: boolean;
  translatedFields?: Record<TPaginationTranslatedField, any>;
  isShow?: boolean;
}) {
  function onClickPrevious(e: MouseEvent<HTMLAnchorElement>) {
    e.stopPropagation();
    e.preventDefault();
    onPrevious();
  }

  function onClickNext(e: MouseEvent<HTMLAnchorElement>) {
    e.stopPropagation();
    e.preventDefault();
    onNext();
  }

  if (isShow) {
    return (
      <nav aria-label="Page navigation">
        <ul className="pagination justify-content-end">
          <li
            className={classNames('page-item', {
              disabled: !isPrevious,
            })}
          >
            <a onClick={onClickPrevious} className="page-link" href="">
              {translatedFields.previous}
            </a>
          </li>
          <li
            className={classNames('page-item', {
              disabled: !isNext,
            })}
          >
            <a onClick={onClickNext} className="page-link" href="">
              {translatedFields.next}
            </a>
          </li>
        </ul>
      </nav>
    );
  }

  return <></>;
}
