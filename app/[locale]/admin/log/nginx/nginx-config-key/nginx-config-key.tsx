'use client';

import Box from '@/app/[locale]/admin/common/box';
import type { INginxKey } from '@/interfaces';
import { type PrefixedTTranslatedFields } from '@/lib/dictionaries';
import { formatDate } from '@/lib/tool';
import UpdateNav from '@/app/[locale]/admin/log/nginx/nginx-config-key/update-nav';
import { useRouter } from 'next/navigation';
import useToast from '@/hooks/useToast';
import Nodata from '@/app/[locale]/common/nodata/nodata';

export default function NginxConfigKeyAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: INginxKey[];
    translatedFields: PrefixedTTranslatedFields<'logAdminPage'>;
  },
) {
  const router = useRouter();
  const { show } = useToast();

  function onClickUpdate(item: INginxKey) {
    router.push(`nginx-config-key/update?id=${item.id}`);
  }

  function onClickDelete(item: INginxKey) {
    router.push(`nginx-config-key/delete?id=${item.id}`);
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <UpdateNav translatedFields={translatedFields} />
        </Box>
      </div>
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.total}
              </h6>
              <h5 className="card-title">{source.length}</h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        {translatedFields.secret}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.remark}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.used}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.generalizedProperties.createdOn}
                      </th>
                      <th scope="col" className=""></th>
                    </tr>
                  </thead>
                  <tbody>
                    {source.map((item) => {
                      return (
                        <tr key={item.id} className="text-nowrap">
                          <td className="user-select-all cursor-copy">
                            {item.secret}
                          </td>
                          <td className="">{item.remark}</td>
                          <td className="">{item.used + ''}</td>
                          <td className="">{formatDate(item.createdOn)}</td>
                          <td className="">
                            <div className="hstack gap-2">
                              <button
                                onClick={onClickUpdate.bind(this, item)}
                                className="btn btn-sm btn-light"
                                type="button"
                              >
                                <i className="bi bi-pencil me-2"></i>
                                {translatedFields.update}
                              </button>

                              <button
                                onClick={onClickDelete.bind(this, item)}
                                className="btn btn-sm btn-light text-danger"
                                type="button"
                              >
                                <i className="bi bi-trash me-2"></i>
                                {translatedFields.delete}
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>

              {source.length === 0 && <Nodata />}
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
