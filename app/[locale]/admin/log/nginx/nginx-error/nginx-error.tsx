'use client';

import { useQuery } from '@tanstack/react-query';
import {
  queryNginxErrorLogs,
  queryNginxErrorLogStatistics,
  searchNginxErrorLogs,
} from '@/services/api';
import Box from '@/app/[locale]/admin/common/box';
import Alert from '@/app/[locale]/alert/alert';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type {
  INginxErrorLogEvent,
  INginxErrorLogStatistics,
  IPagination,
  IQueryParams,
} from '@/interfaces';
import { type ChangeEvent, useState } from 'react';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Pagination from '@/app/[locale]/admin/common/pagination';
import { type PrefixedTTranslatedFields } from '@/lib/dictionaries';
import { formatDateTime } from '@/lib/tool';

export default function NginxErrorLogAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IPagination<INginxErrorLogEvent>;
    translatedFields: PrefixedTTranslatedFields<'logAdminPage'>;
  },
) {
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });
  const [searchName, setSearchName] = useState('');

  const queryNginxLogStatisticsQuery = useQuery(
    ['/nginx', '/logs', '/error', '/statistics'],
    async () => {
      return (await queryNginxErrorLogStatistics()) as INginxErrorLogStatistics;
    },
  );
  const queryNginxLogsQuery = useQuery(
    ['/nginx', '/logs', '/error', params, searchName],
    async (context) => {
      if (searchName) {
        return (await searchNginxErrorLogs({
          query: {
            ...(context.queryKey[2] as any),
            name: context.queryKey[3],
          },
        })) as IPagination<INginxErrorLogEvent>;
      } else {
        return (await queryNginxErrorLogs({
          query: context.queryKey[2] as any,
        })) as IPagination<INginxErrorLogEvent>;
      }
    },
    {
      keepPreviousData: true,
      initialData: source,
    },
  );

  function onClickPrevious() {
    const data = queryNginxLogsQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryNginxLogsQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  function onChangeSearch(e: ChangeEvent<HTMLInputElement>) {
    const value = e.target.value.trim();
    setSearchName(value.toLowerCase());
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.errorLogTotal}
              </h6>
              <h5 className="card-title">
                {queryNginxLogStatisticsQuery.data &&
                  queryNginxLogStatisticsQuery.data.count}

                {queryNginxLogStatisticsQuery.isError && (
                  <Alert
                    message={queryNginxLogStatisticsQuery.error}
                    type="error"
                  />
                )}

                {queryNginxLogStatisticsQuery.isLoading && <Spinner />}
              </h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col">
        <Box>
          <div className="card">
            <div className="card-header bg-transparent">
              <div className="row row-cols-auto justify-content-between g-2">
                <div className="col">
                  <div className="input-group">
                    <span className="input-group-text">
                      <i className="bi bi-search"></i>
                    </span>
                    <input
                      onChange={onChangeSearch}
                      type="search"
                      className="form-control"
                      placeholder={translatedFields.searchPlaceholder}
                    />
                  </div>
                </div>
                <div className="col"></div>
              </div>
            </div>
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        {translatedFields.message}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.receivingTime}
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {queryNginxLogsQuery.data.content.map((item) => {
                      return (
                        <tr key={item.id} className="text-nowrap">
                          <td className="">{item.message}</td>
                          <td className="">{formatDateTime(item.createdOn)}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>

              {queryNginxLogsQuery.data.content.length === 0 && <Nodata />}

              <Pagination
                isShow={queryNginxLogsQuery.data.pageable.pages > 0}
                onPrevious={onClickPrevious}
                onNext={onClickNext}
                isPrevious={queryNginxLogsQuery.data.pageable.previous}
                isNext={queryNginxLogsQuery.data.pageable.next}
                translatedFields={translatedFields.pagination}
              />
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
