'use client';

import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import { useState } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateNav({
  translatedFields,
}: {
  translatedFields: PrefixedTTranslatedFields<'logAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <ToggleProperties
      tabIndex={tabIndex}
      setTabIndex={setTabIndex}
      mb4={false}
      isTable={false}
      items={[
        {
          index: 0,
          name: translatedFields.createSchedulerConfig,
          href: `/admin/log/nginx/nginx-config-scheduler/save`,
          segment: null,
        },
      ]}
    />
  );
}
