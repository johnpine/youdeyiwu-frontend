'use client';

import Box from '@/app/[locale]/admin/common/box';
import { useRouter } from 'next/navigation';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function ConfigAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source?: any;
    translatedFields: PrefixedTTranslatedFields<'configAdminPage'>;
  },
) {
  const router = useRouter();
  const configs = [
    'user',
    'site',
    'jwt',
    'section',
    'post',
    'qq',
    'image',
    'phone',
    'email',
    'client',
    'register',
  ];

  function onClickUpdate(item: string) {
    router.push(`config/update/${item}`);
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.count.config}
              </h6>
              <h5 className="card-title">{configs.length}</h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col">
        <Box>
          <div className="card">
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        {translatedFields.properties.name}
                      </th>
                      <th scope="col" className=""></th>
                    </tr>
                  </thead>
                  <tbody>
                    {configs.map((item) => {
                      return (
                        <tr key={item} className="text-nowrap">
                          <td
                            className="cursor-pointer"
                            onClick={onClickUpdate.bind(this, item)}
                          >
                            {(translatedFields as any).properties[item]}
                          </td>
                          <td>
                            <div className="hstack gap-2">
                              <button
                                onClick={onClickUpdate.bind(this, item)}
                                className="btn btn-sm btn-light"
                                type="button"
                              >
                                <i className="bi bi-pencil me-2"></i>
                                {translatedFields.operate.update}
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
