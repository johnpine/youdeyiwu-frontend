'use client';

import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateNav({
  translatedFields,
}: {
  translatedFields: PrefixedTTranslatedFields<'configAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);
  const searchParams = useSearchParams();
  const id = searchParams.get('id');
  const configs = [
    'user',
    'site',
    'jwt',
    'section',
    'post',
    'qq',
    'image',
    'phone',
    'email',
    'client',
    'register',
  ];

  const items = configs.map((item, index) => {
    return {
      index: index,
      name: (translatedFields as any).properties[item],
      href: `/admin/config/update/${item}`,
      segment: item,
    };
  });

  return (
    <ToggleProperties
      tabIndex={tabIndex}
      setTabIndex={setTabIndex}
      mb4={false}
      isTable={false}
      items={items}
    />
  );
}
