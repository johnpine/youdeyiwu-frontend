import { authMiddleware } from '@/lib/api';
import {
  clientQueryAllMenu,
  queryNginxLogsWebsitePageViews,
  queryPath,
} from '@/services/api';
import { cookies } from 'next/headers';
import HomeAdminPage from '@/app/[locale]/admin/home/home';
import ResetPage from '@/app/[locale]/reset/reset';
import { createError, getMetadata } from '@/lib/tool';
import type { Metadata as MetadataNext } from 'next';
import type { IMenu, IPath } from '@/interfaces';
import { getTranslator } from 'next-intl/server';
import { getTranslatedFields } from '@/lib/dictionaries';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('adminPage.managementConsole') });
}

async function tryGetData(path: IPath) {
  const isLogin = !!path.user;
  if (!isLogin) {
    return {
      isSuccess: false,
      isError: true,
    };
  }

  try {
    const token = authMiddleware(cookies());
    await queryNginxLogsWebsitePageViews({
      baseURL: process.env.APP_API_SERVER,
      token,
    });

    return {
      isSuccess: true,
      isError: false,
    };
  } catch (e: any) {
    return {
      isSuccess: false,
      isError: true,
      error: e,
    };
  }
}

async function getData() {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/',
      },
    });
    const req2 = clientQueryAllMenu({
      baseURL: process.env.APP_API_SERVER,
      token,
    });

    const responses = await Promise.all([req1, req2]);
    const resp1 = await ((await responses[0]) as Response).json();
    const resp2 = await ((await responses[1]) as Response).json();

    return {
      isSuccess: true,
      isError: false,
      path: resp1.data as IPath,
      info: resp2.data as IMenu[],
    };
  } catch (e: any) {
    return {
      isSuccess: false,
      isError: true,
      error: e,
    };
  }
}

export default async function Page({
  params: { locale },
}: {
  params: { locale: string };
}) {
  const data = await getData();
  if (data.isError) {
    return <ResetPage error={createError(data.error)} />;
  }

  const source = data.path!;
  const translatedFields = await getTranslatedFields(locale, 'homeAdminPage');
  const state = await tryGetData(source);

  return (
    <HomeAdminPage
      appName={process.env.APP_NAME}
      source={source}
      isOk={state.isSuccess}
      translatedFields={translatedFields}
    />
  );
}
