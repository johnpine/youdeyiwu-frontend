import { type ReactNode } from 'react';
import SubmenuNav, {
  type ISubmenuNavItem,
} from '@/app/[locale]/admin/common/submenu/submenu-nav';
import SubmenuContent from '@/app/[locale]/admin/common/submenu/submenu-content';
import { authMiddleware } from '@/lib/api';
import { cookies } from 'next/headers';
import { clientQueryAllMenu, queryPath } from '@/services/api';
import type { IMenu, IPath } from '@/interfaces';
import { getTranslator } from 'next-intl/server';

async function getData() {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/',
      },
    });
    const req2 = clientQueryAllMenu({
      baseURL: process.env.APP_API_SERVER,
      token,
    });

    const responses = await Promise.all([req1, req2]);
    const resp1 = await ((await responses[0]) as Response).json();
    const resp2 = await ((await responses[1]) as Response).json();

    return {
      isSuccess: true,
      path: resp1.data as IPath,
      menus: resp2.data as IMenu[],
    };
  } catch (e) {
    return {
      isSuccess: false,
    };
  }
}

export default async function DashboardLayout({
  children,
  params: { locale },
}: {
  children: ReactNode;
  params: { locale: string };
}) {
  const data = await getData();

  if (data.isSuccess) {
    const t = await getTranslator(locale);

    const items: ISubmenuNavItem[] = [
      {
        path: '/admin/menu',
        name: t('menuAdminPage.title.all'),
        icon: 'bi-list',
      },
      {
        path: '/admin/menu/create',
        name: t('menuAdminPage.title.add'),
        icon: 'bi-plus-lg',
      },
    ];

    return (
      <div>
        <SubmenuNav items={items} />
        <SubmenuContent type="menu">{children}</SubmenuContent>
      </div>
    );
  }

  return <>{children}</>;
}
