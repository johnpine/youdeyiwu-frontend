'use client';

import type { ISubmenu } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QuerySubmenuInfo from '@/app/[locale]/admin/menu/submenu/detail/info';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailSubmenuAdminPage({
  source,
  translatedFields,
}: {
  source: ISubmenu;
  translatedFields: PrefixedTTranslatedFields<'menuAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QuerySubmenuInfo
            source={source}
            translatedFields={translatedFields}
          />
        </div>
      </div>
    </Box>
  );
}
