import type { Metadata as MetadataNext } from 'next';
import { createError, getMetadata, isNum } from '@/lib/tool';
import { getTranslatedFields } from '@/lib/dictionaries';
import { authMiddleware } from '@/lib/api';
import { cookies } from 'next/headers';
import { queryMenuDetails, queryPath } from '@/services/api';
import type { IMenu, IPath } from '@/interfaces';
import ResetPage from '@/app/[locale]/reset/reset';
import { notFound } from 'next/navigation';
import DetailSubmenuAdminPage from '@/app/[locale]/admin/menu/submenu/detail/detail';
import { getTranslator } from 'next-intl/server';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('menuAdminPage.title.detail') });
}

async function getData(id: string) {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/',
      },
    });
    const req2 = queryMenuDetails({
      baseURL: process.env.APP_API_SERVER,
      token,
      id,
    });

    const responses = await Promise.all([req1, req2]);
    const resp1 = await ((await responses[0]) as Response).json();
    const resp2 = await ((await responses[1]) as Response).json();

    return {
      isSuccess: true,
      isError: false,
      path: resp1.data as IPath,
      info: resp2.data as IMenu,
    };
  } catch (e: any) {
    return {
      isSuccess: false,
      isError: true,
      error: e,
    };
  }
}

export default async function Page({
  params: { locale },
  searchParams: { id, mid },
}: {
  params: { locale: string };
  searchParams: { id?: string; mid?: string };
}) {
  if (!id || !isNum(id) || !mid || !isNum(mid)) {
    notFound();
  }

  const data = await getData(mid);
  if (data.isError) {
    return <ResetPage error={createError(data.error)} />;
  }

  const source = data.info!;
  const find = source.submenus.find((item) => item.id + '' === id);
  if (!find) {
    notFound();
  }

  const translatedFields = await getTranslatedFields(locale, 'menuAdminPage');

  return (
    <DetailSubmenuAdminPage source={find} translatedFields={translatedFields} />
  );
}
