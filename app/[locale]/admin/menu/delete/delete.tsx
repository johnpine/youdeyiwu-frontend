'use client';

import type { IMenu } from '@/interfaces';
import { removeMenu } from '@/services/api';
import { useMutation } from '@tanstack/react-query';
import { type FormEvent } from 'react';
import useToast from '@/hooks/useToast';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DeleteMenuAdminPage({
  source,
  translatedFields,
}: {
  source: IMenu;
  translatedFields: PrefixedTTranslatedFields<'menuAdminPage'>;
}) {
  const { show } = useToast();

  const removeMenuMutation = useMutation(removeMenu);

  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      const id = source.id;
      await removeMenuMutation.mutateAsync({
        id,
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.operate.deleteCompleted,
      });

      setTimeout(() => {
        show({
          type: 'SUCCESS',
          message: translatedFields.operate.refreshPending,
        });
      }, 1200);

      setTimeout(() => {
        location.replace('/admin/menu');
      }, 1500);
    } catch (e) {
      removeMenuMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    }
  }

  return (
    <Box>
      <div className="card">
        <div className="card-body">
          <form
            onSubmit={onSubmit}
            className="vstack gap-4 align-items-center py-4"
          >
            <div>
              <span>{translatedFields.tip.isDeletionConfirmed}&nbsp;</span>
              <span className="fw-semibold text-danger lead">{`⌈ ${source.name} (ID. ${source.id}) ⌋`}</span>
            </div>
            <button
              type="submit"
              disabled={removeMenuMutation.isLoading}
              className="btn btn-danger col col-lg-2 mt-4"
            >
              {removeMenuMutation.isLoading && <Spinner classs="me-2" />}
              {translatedFields.operate.delete}
            </button>
          </form>
        </div>
      </div>
    </Box>
  );
}
