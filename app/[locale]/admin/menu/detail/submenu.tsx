'use client';

import type { IMenu, ISubmenu } from '@/interfaces';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import { useRouter } from 'next/navigation';
import Nodata from '@/app/[locale]/common/nodata/nodata';

export default function QueryMenuSubmenu(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IMenu;
    translatedFields: PrefixedTTranslatedFields<'menuAdminPage'>;
  },
) {
  const router = useRouter();

  function onClickDetail(item: ISubmenu) {
    router.push(`submenu/detail?id=${item.id}&mid=${source.id}`);
  }

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-list-nested me-2"></i>
        {translatedFields.properties.submenus}
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table table-hover align-middle">
            <thead>
              <tr className="text-nowrap">
                <th scope="col" className="fw-normal">
                  ID
                </th>
                <th scope="col" className="fw-normal">
                  {translatedFields.properties.name}
                </th>
                <th scope="col" className="fw-normal">
                  {translatedFields.properties.path}
                </th>
                <th scope="col" className="fw-normal">
                  {translatedFields.properties.sort}
                </th>
                <th scope="col" className="fw-normal">
                  {translatedFields.properties.menuType}
                </th>
              </tr>
            </thead>
            <tbody>
              {source.submenus.map((item) => {
                return (
                  <tr key={item.id} className="text-nowrap">
                    <td
                      className="cursor-pointer"
                      onClick={onClickDetail.bind(this, item)}
                    >
                      {item.id}
                    </td>
                    <td
                      className="cursor-pointer"
                      onClick={onClickDetail.bind(this, item)}
                    >
                      {item.name}
                    </td>
                    <td
                      className="cursor-pointer"
                      onClick={onClickDetail.bind(this, item)}
                    >
                      {item.path}
                    </td>
                    <td
                      className="cursor-pointer"
                      onClick={onClickDetail.bind(this, item)}
                    >
                      {item.sort}
                    </td>
                    <td
                      className="cursor-pointer"
                      onClick={onClickDetail.bind(this, item)}
                    >
                      {translatedFields.enums.menuType[item.menuType]}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>

        {source.submenus.length === 0 && <Nodata />}
      </div>
    </div>
  );
}
