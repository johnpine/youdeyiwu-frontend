'use client';

import type { IMenu } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QueryMenuInfo from '@/app/[locale]/admin/menu/detail/info';
import QueryMenuSubmenu from '@/app/[locale]/admin/menu/detail/submenu';
import RolesMenuAdminPage from '@/app/[locale]/admin/menu/detail/roles';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailMenuAdminPage({
  source,
  translatedFields,
}: {
  source: IMenu;
  translatedFields: PrefixedTTranslatedFields<'menuAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QueryMenuInfo source={source} translatedFields={translatedFields} />

          <QueryMenuSubmenu
            source={source}
            translatedFields={translatedFields}
          />

          <RolesMenuAdminPage
            source={source}
            translatedFields={translatedFields}
          />
        </div>
      </div>
    </Box>
  );
}
