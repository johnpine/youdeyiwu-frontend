import { queryMenuRolesById } from '@/services/api';
import { useQuery } from '@tanstack/react-query';
import type { IMenu, IRole } from '@/interfaces';
import Alert from '@/app/[locale]/alert/alert';
import AlertLoad from '@/app/[locale]/alert/load';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function RolesMenuAdminPage({
  source,
  translatedFields,
}: {
  source: IMenu;
  translatedFields: PrefixedTTranslatedFields<'menuAdminPage'>;
}) {
  const queryMenuRolesByIdQuery = useQuery(
    ['/menus', source.id, '/roles'],
    async (context) => {
      return (await queryMenuRolesById({
        id: context.queryKey[1] + '',
      })) as IRole[];
    },
  );

  if (queryMenuRolesByIdQuery.data) {
    const data = queryMenuRolesByIdQuery.data;
    return (
      <div className="card">
        <div className="card-header bg-transparent text-secondary">
          <i className="bi bi-bookmarks me-2"></i>
          {translatedFields.properties.role}
        </div>

        <div className="card-body">
          <div className="table-responsive">
            <table className="table align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="">
                    {translatedFields.properties.name} / ID
                  </th>
                </tr>
              </thead>
              <tbody>
                {data.map((item) => {
                  return (
                    <tr key={item.id} className="text-nowrap">
                      <td>{`${item.name} (ID. ${item.id})`}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>

          {data.length === 0 && <Nodata />}
        </div>
      </div>
    );
  }

  if (queryMenuRolesByIdQuery.error) {
    return <Alert type="error" message={queryMenuRolesByIdQuery.error} />;
  }

  return <AlertLoad />;
}
