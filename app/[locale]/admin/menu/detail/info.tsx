import type { IMenu } from '@/interfaces';
import { useState } from 'react';
import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import PrimaryProperties from '@/app/[locale]/admin/common/primary-properties';
import GeneralizedProperties from '@/app/[locale]/admin/common/generalized-properties';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryMenuInfo({
  source,
  translatedFields,
}: {
  source: IMenu;
  translatedFields: PrefixedTTranslatedFields<'menuAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-info-square me-2"></i>
        {source.name}
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle caption-top">
            <caption>
              <ToggleProperties
                isTable={false}
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
                items={[
                  {
                    index: 0,
                    name: translatedFields.operate.mainAttribute,
                  },
                  {
                    index: 1,
                    name: translatedFields.operate.otherAttribute,
                  },
                ]}
              />
            </caption>
            <tbody>
              {tabIndex === 0 && (
                <PrimaryProperties
                  source={[
                    {
                      field: 'name',
                      translatedField: translatedFields.properties.name,
                      value: `${source.name} (ID. ${source.id})`,
                    },
                    {
                      field: 'path',
                      translatedField: translatedFields.properties.path,
                      value: source.path,
                    },
                    {
                      field: 'sort',
                      translatedField: translatedFields.properties.sort,
                      value: source.sort,
                    },
                    {
                      field: 'type',
                      translatedField: translatedFields.properties.type,
                      value: translatedFields.enums.type[source.type],
                    },
                    {
                      field: 'menuType',
                      translatedField: translatedFields.properties.menuType,
                      value: translatedFields.enums.menuType[source.menuType],
                    },
                  ]}
                />
              )}

              {tabIndex === 1 && (
                <GeneralizedProperties
                  source={source}
                  translatedFields={translatedFields.generalizedProperties}
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
