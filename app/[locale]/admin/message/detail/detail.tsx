'use client';

import type { IMessage } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QueryMessageInfo from '@/app/[locale]/admin/message/detail/info';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailMessageAdminPage({
  source,
  translatedFields,
}: {
  source: IMessage;
  translatedFields: PrefixedTTranslatedFields<'messageAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QueryMessageInfo
            source={source}
            translatedFields={translatedFields}
          />
        </div>
      </div>
    </Box>
  );
}
