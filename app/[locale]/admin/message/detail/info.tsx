import type { IMessage } from '@/interfaces';
import { useState } from 'react';
import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import PrimaryProperties from '@/app/[locale]/admin/common/primary-properties';
import GeneralizedProperties from '@/app/[locale]/admin/common/generalized-properties';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryMessageInfo({
  source,
  translatedFields,
}: {
  source: IMessage;
  translatedFields: PrefixedTTranslatedFields<'messageAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-info-square me-2"></i>
        {source.name}
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle caption-top">
            <caption>
              <ToggleProperties
                isTable={false}
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
                items={[
                  {
                    index: 0,
                    name: translatedFields.operate.mainAttribute,
                  },
                  {
                    index: 1,
                    name: translatedFields.operate.otherAttribute,
                  },
                ]}
              />
            </caption>
            <tbody>
              {tabIndex === 0 && (
                <PrimaryProperties
                  source={[
                    {
                      field: 'name',
                      translatedField: translatedFields.properties.name,
                      value: `${source.name} (ID. ${source.id})`,
                    },
                    {
                      field: 'overview',
                      translatedField: translatedFields.properties.overview,
                      value: source.overview,
                    },
                    {
                      field: 'messageType',
                      translatedField: translatedFields.properties.messageType,
                      value: (translatedFields as any).enums.messageType[
                        source.messageType
                      ],
                    },
                    {
                      field: 'messageRange',
                      translatedField: translatedFields.properties.messageRange,
                      value: (translatedFields as any).enums.messageRange[
                        source.messageRange
                      ],
                    },
                    {
                      field: 'businessId',
                      translatedField: translatedFields.properties.businessId,
                      value: source.businessId,
                    },
                    {
                      field: 'businessName',
                      translatedField: translatedFields.properties.businessName,
                      value: source.businessName,
                    },
                    {
                      field: 'businessRemark',
                      translatedField:
                        translatedFields.properties.businessRemark,
                      value: source.businessRemark,
                    },
                    {
                      field: 'sender',
                      translatedField: translatedFields.properties.sender,
                      value: `${source.senderAlias} (ID. ${source.sender})`,
                    },
                    {
                      field: 'receiver',
                      translatedField: translatedFields.properties.receiver,
                      value: `${source.receiverAlias} (ID. ${source.receiver})`,
                    },
                  ]}
                />
              )}

              {tabIndex === 1 && (
                <GeneralizedProperties
                  source={source}
                  translatedFields={translatedFields.generalizedProperties}
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
