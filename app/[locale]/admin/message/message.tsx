'use client';

import { useQuery } from '@tanstack/react-query';
import {
  queryMessageStatistics,
  searchMessage,
  serverQueryAllMessage,
} from '@/services/api';
import type {
  IMessage,
  IMessageStatistics,
  IPagination,
  IQueryParams,
} from '@/interfaces';
import Alert from '@/app/[locale]/alert/alert';
import Box from '@/app/[locale]/admin/common/box';
import { type ChangeEvent, useState } from 'react';
import { useRouter } from 'next/navigation';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Pagination from '@/app/[locale]/admin/common/pagination';
import { formatDateTime } from '@/lib/tool';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function MessageAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IPagination<IMessage>;
    translatedFields: PrefixedTTranslatedFields<'messageAdminPage'>;
  },
) {
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });
  const [searchName, setSearchName] = useState('');
  const router = useRouter();

  const queryMessageStatisticsQuery = useQuery(
    ['/messages', '/statistics'],
    async () => {
      return (await queryMessageStatistics()) as IMessageStatistics;
    },
  );

  const queryAllMessageQuery = useQuery(
    ['/messages', '/server', params, searchName],
    async (context) => {
      if (searchName) {
        return (await searchMessage({
          query: {
            ...(context.queryKey[2] as any),
            name: context.queryKey[3],
          },
        })) as IPagination<IMessage>;
      } else {
        return (await serverQueryAllMessage({
          query: context.queryKey[2] as any,
        })) as IPagination<IMessage>;
      }
    },
    {
      keepPreviousData: true,
      initialData: source,
    },
  );

  function onClickDetail(item: IMessage) {
    router.push(`message/detail?id=${item.id}`);
  }

  function onClickPrevious() {
    const data = queryAllMessageQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryAllMessageQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  function onChangeSearch(e: ChangeEvent<HTMLInputElement>) {
    const value = e.target.value;
    setSearchName(value.toLowerCase());
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.count.message}
              </h6>
              <h5 className="card-title">
                {queryMessageStatisticsQuery.data &&
                  queryMessageStatisticsQuery.data.count}

                {queryMessageStatisticsQuery.isError && (
                  <Alert
                    message={queryMessageStatisticsQuery.error}
                    type="error"
                  />
                )}

                {queryMessageStatisticsQuery.isLoading && <Spinner />}
              </h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col">
        <Box>
          <div className="card">
            <div className="card-header bg-transparent">
              <div className="row row-cols-auto justify-content-between g-2">
                <div className="col">
                  <div className="input-group">
                    <span className="input-group-text">
                      <i className="bi bi-search"></i>
                    </span>
                    <input
                      onChange={onChangeSearch}
                      type="search"
                      className="form-control"
                      placeholder={translatedFields.tip.searchPlaceholder}
                    />
                  </div>
                </div>
                <div className="col"></div>
              </div>
            </div>
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        ID
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.name}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.messageType}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.messageRange}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.sender}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.receiver}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.time}
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {queryAllMessageQuery.data.content.map((item) => {
                      return (
                        <tr key={item.id} className="text-nowrap">
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.id}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.name}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {
                              (translatedFields as any).enums.messageType[
                                item.messageType
                              ]
                            }
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {
                              translatedFields.enums.messageRange[
                                item.messageRange
                              ]
                            }
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.sender}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.receiver}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {formatDateTime(item.createdOn)}
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>

              {queryAllMessageQuery.data.content.length === 0 && <Nodata />}

              <Pagination
                isShow={queryAllMessageQuery.data.pageable.pages > 0}
                onPrevious={onClickPrevious}
                onNext={onClickNext}
                isPrevious={queryAllMessageQuery.data.pageable.previous}
                isNext={queryAllMessageQuery.data.pageable.next}
                translatedFields={translatedFields.pagination}
              />
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
