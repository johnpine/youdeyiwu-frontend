'use client';

import { type ChangeEvent, type FormEvent, useEffect, useState } from 'react';
import { useMutation } from '@tanstack/react-query';
import { updateUserBasicInfo } from '@/services/api';
import type { IDifference, IUser } from '@/interfaces';
import diff from 'microdiff';
import useToast from '@/hooks/useToast';
import { getDiffData } from '@/lib/tool';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateUserInfo({
  source,
  translatedFields,
}: {
  source: IUser;
  translatedFields: PrefixedTTranslatedFields<'userAdminPage'>;
}) {
  const [form, setForm] = useState<{
    alias?: string;
    username?: string;
    phone?: string;
    email?: string;
  }>({
    alias: source.alias ?? '',
    username: source.username ?? '',
    phone: source.phone ?? '',
    email: source.email ?? '',
  });
  const [differenceData, setDifferenceData] = useState<IDifference[]>([]);
  const { show } = useToast();
  const [isDisabledSave, setIsDisabledSave] = useState(true);

  const updateUserBasicInfoMutation = useMutation(updateUserBasicInfo);

  useEffect(() => {
    const diffData = diff(
      {
        alias: source.alias ?? '',
        username: source.username ?? '',
        phone: source.phone ?? '',
        email: source.email ?? '',
      },
      form,
    );
    setDifferenceData(diffData);
    setIsDisabledSave(diffData.length === 0);
  }, [form, source]);

  function onChangeForm(e: ChangeEvent<HTMLInputElement>) {
    const name = e.target.name;
    const value = e.target.value;
    setForm({ ...form, [name]: value });
  }

  async function onClickSave(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      const data = getDiffData(differenceData);
      const id = source.id;
      await updateUserBasicInfoMutation.mutateAsync({
        id,
        data,
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.operate.updateCompleted,
      });
    } catch (e) {
      updateUserBasicInfoMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setDifferenceData([]);
    }
  }

  return (
    <Box>
      <form onSubmit={onClickSave} className="vstack gap-4">
        <div>
          <label className="form-label">
            {translatedFields.properties.alias}
          </label>
          <input
            type="text"
            className="form-control"
            name="alias"
            value={form.alias}
            onChange={onChangeForm}
            aria-describedby="alias"
          />
        </div>

        <div>
          <label className="form-label">
            {translatedFields.properties.username}
          </label>
          <input
            type="text"
            className="form-control"
            name="username"
            value={form.username}
            onChange={onChangeForm}
            aria-describedby="username"
          />
        </div>

        <div>
          <label className="form-label">
            {translatedFields.properties.phone}
          </label>
          <input
            type="tel"
            className="form-control"
            name="phone"
            value={form.phone}
            onChange={onChangeForm}
            aria-describedby="phone"
          />
        </div>

        <div>
          <label className="form-label">
            {translatedFields.properties.email}
          </label>
          <input
            type="email"
            className="form-control"
            name="email"
            value={form.email}
            onChange={onChangeForm}
            aria-describedby="email"
          />
        </div>

        <button
          type="submit"
          disabled={updateUserBasicInfoMutation.isLoading || isDisabledSave}
          className="btn btn-success col col-lg-2 my-4"
        >
          {updateUserBasicInfoMutation.isLoading && <Spinner classs="me-2" />}
          {translatedFields.operate.update}
        </button>
      </form>
    </Box>
  );
}
