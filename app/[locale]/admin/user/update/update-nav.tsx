'use client';

import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateNav({
  translatedFields,
}: {
  translatedFields: PrefixedTTranslatedFields<'userAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);
  const searchParams = useSearchParams();
  const id = searchParams.get('id');

  return (
    <ToggleProperties
      tabIndex={tabIndex}
      setTabIndex={setTabIndex}
      mb4={false}
      isTable={false}
      items={[
        {
          index: 0,
          name: translatedFields.properties.info,
          href: `/admin/user/update?id=${id}`,
          segment: null,
        },
        {
          index: 1,
          name: translatedFields.properties.accountState,
          href: `/admin/user/update/account-state?id=${id}`,
          segment: 'account-state',
        },
        {
          index: 2,
          name: translatedFields.properties.personalizedSignature,
          href: `/admin/user/update/personalized-signature?id=${id}`,
          segment: 'personalized-signature',
        },
        {
          index: 3,
          name: translatedFields.properties.changePassword,
          href: `/admin/user/update/change-password?id=${id}`,
          segment: 'change-password',
        },
      ]}
    />
  );
}
