'use client';

import type { IUser } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QueryUserInfo from '@/app/[locale]/admin/user/detail/info';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailUserAdminPage({
  source,
  translatedFields,
}: {
  source: IUser;
  translatedFields: PrefixedTTranslatedFields<'userAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QueryUserInfo source={source} translatedFields={translatedFields} />
        </div>
      </div>
    </Box>
  );
}
