import type { IUser } from '@/interfaces';
import ContentHtml from '@/app/[locale]/common/content/html';
import { useState } from 'react';
import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import PrimaryProperties from '@/app/[locale]/admin/common/primary-properties';
import GeneralizedProperties from '@/app/[locale]/admin/common/generalized-properties';
import Link from 'next/link';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryUserInfo({
  source,
  translatedFields,
}: {
  source: IUser;
  translatedFields: PrefixedTTranslatedFields<'userAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-info-square me-2"></i>
        <Link
          href={`/users/${source.id}`}
          className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
        >
          {source.alias}
        </Link>
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle caption-top">
            <caption>
              <ToggleProperties
                isTable={false}
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
                items={[
                  {
                    index: 0,
                    name: translatedFields.operate.mainAttribute,
                  },
                  {
                    index: 1,
                    name: translatedFields.operate.otherAttribute,
                  },
                ]}
              />
            </caption>
            <tbody>
              {tabIndex === 0 && (
                <PrimaryProperties
                  source={[
                    {
                      field: 'alias',
                      translatedField: translatedFields.properties.alias,
                      value: `${source.alias} (ID. ${source.id})`,
                    },
                    {
                      field: 'username',
                      translatedField: translatedFields.properties.username,
                      value: source.username,
                    },
                    {
                      field: 'phone',
                      translatedField: translatedFields.properties.phone,
                      value: source.phone,
                    },
                    {
                      field: 'email',
                      translatedField: translatedFields.properties.email,
                      value: source.email,
                    },
                    {
                      field: 'qqOpenId',
                      translatedField: translatedFields.properties.qqOpenId,
                      value: source.qqOpenId,
                    },
                    {
                      field: 'qqUnionId',
                      translatedField: translatedFields.properties.qqUnionId,
                      value: source.qqUnionId,
                    },
                    {
                      field: 'wxOpenId',
                      translatedField: translatedFields.properties.wxOpenId,
                      value: source.wxOpenId,
                    },
                    {
                      field: 'wxUnionId',
                      translatedField: translatedFields.properties.wxUnionId,
                      value: source.wxUnionId,
                    },
                    {
                      field: 'accountNonExpired',
                      translatedField:
                        translatedFields.properties.accountNonExpired,
                      value: source.accountNonExpired,
                    },
                    {
                      field: 'credentialsNonExpired',
                      translatedField:
                        translatedFields.properties.credentialsNonExpired,
                      value: source.credentialsNonExpired,
                    },
                    {
                      field: 'accountNonLocked',
                      translatedField:
                        translatedFields.properties.accountNonLocked,
                      value: source.accountNonLocked,
                    },
                    {
                      field: 'enabled',
                      translatedField: translatedFields.properties.enabled,
                      value: source.enabled,
                    },
                    {
                      field: 'smallAvatarUrl',
                      translatedField:
                        translatedFields.properties.smallAvatarUrl,
                      value: source.details!.smallAvatarUrl
                        ? source.details!.smallAvatarUrl
                        : '',
                    },
                    {
                      field: 'mediumAvatarUrl',
                      translatedField:
                        translatedFields.properties.mediumAvatarUrl,
                      value: source.details!.mediumAvatarUrl
                        ? source.details!.mediumAvatarUrl
                        : '',
                    },
                    {
                      field: 'largeAvatarUrl',
                      translatedField:
                        translatedFields.properties.largeAvatarUrl,
                      value: source.details!.largeAvatarUrl
                        ? source.details!.largeAvatarUrl
                        : '',
                    },
                    {
                      field: 'contacts',
                      translatedField: translatedFields.properties.contacts,
                      value: source.details!.contacts
                        ? (source.details!.contacts || [])
                            .map((value) => `${value.key}(${value.val})`)
                            .join(', ')
                        : '',
                    },
                    {
                      field: 'about',
                      translatedField: translatedFields.properties.about,
                      isCustomValue: true,
                      value: source.details!.about ? (
                        <ContentHtml content={source.details!.about} />
                      ) : (
                        ''
                      ),
                    },
                  ]}
                />
              )}

              {tabIndex === 1 && (
                <GeneralizedProperties
                  source={source}
                  translatedFields={translatedFields.generalizedProperties}
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
