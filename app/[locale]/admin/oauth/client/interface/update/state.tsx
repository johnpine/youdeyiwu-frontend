'use client';

import type { TOauthClientApiReviewState } from '@/types';
import type { IDifference, IOauthClientApi } from '@/interfaces';
import useToast from '@/hooks/useToast';
import { type ChangeEvent, type FormEvent, useEffect, useState } from 'react';
import { useMutation } from '@tanstack/react-query';
import { updateOauthClientApiStatus } from '@/services/api';
import diff from 'microdiff';
import { getDiffData } from '@/lib/tool';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateOauthClientInterfaceState({
  source,
  cid,
  translatedFields,
}: {
  source: IOauthClientApi;
  cid: string;
  translatedFields: PrefixedTTranslatedFields<'oauthClientAdminPage'>;
}) {
  const { show } = useToast();
  const [form, setForm] = useState<{
    reviewState: TOauthClientApiReviewState;
    reviewReason?: string;
  }>({
    reviewState: source.reviewState ?? 'PENDING',
    reviewReason: source.reviewReason ?? '',
  });
  const [differenceData, setDifferenceData] = useState<IDifference[]>([]);
  const [isDisabledSave, setIsDisabledSave] = useState(true);

  const updateOauthClientApiStatusMutation = useMutation(
    updateOauthClientApiStatus,
  );

  useEffect(() => {
    const diffData = diff(
      {
        reviewState: source.reviewState ?? 'PENDING',
        reviewReason: source.reviewReason ?? '',
      },
      form,
    );
    setDifferenceData(diffData);
    setIsDisabledSave(diffData.length === 0);
  }, [form, source]);

  async function onClickSave(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      if (!source.reviewState) {
        show({
          type: 'DANGER',
          message: translatedFields.tip.dataNotExists,
        });
        return;
      }

      checkForm();
      const data = getDiffData(differenceData);

      const apiId = source.id + '';
      await updateOauthClientApiStatusMutation.mutateAsync({
        id: cid,
        data: {
          ...data,
          apiId,
        },
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.operate.updateCompleted,
      });
    } catch (e) {
      updateOauthClientApiStatusMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setDifferenceData([]);
    }
  }

  function checkForm() {
    const { reviewState } = form;

    if (!reviewState) {
      throw translatedFields.tip.reviewStateRequired;
    }
  }

  function onChangeForm(
    e: ChangeEvent<HTMLSelectElement | HTMLInputElement | HTMLTextAreaElement>,
  ) {
    const name = e.target.name;
    const value = e.target.value;
    setForm({ ...form, [name]: value });
  }

  return (
    <Box>
      <form onSubmit={onClickSave} className="vstack gap-4">
        <div>
          <label className="form-label">
            {translatedFields.properties.reviewState}
          </label>
          <select
            name="reviewState"
            value={form.reviewState}
            onChange={onChangeForm}
            className="form-select"
            aria-label="reviewState"
          >
            {['PENDING', 'FAILED', 'SUCCESSFUL'].map((key) => {
              return (
                <option key={key} value={key}>
                  {(translatedFields as any).enums.reviewState[key]}
                </option>
              );
            })}
          </select>
        </div>

        <div>
          <label className="form-label">
            {translatedFields.properties.reviewReason}
          </label>
          <textarea
            rows={1}
            name="reviewReason"
            className="form-control"
            value={form.reviewReason}
            onChange={onChangeForm}
            aria-describedby="reviewReason"
          />
        </div>

        <button
          type="submit"
          disabled={
            updateOauthClientApiStatusMutation.isLoading || isDisabledSave
          }
          className="btn btn-success col col-lg-2 my-4"
        >
          {updateOauthClientApiStatusMutation.isLoading && (
            <Spinner classs="me-2" />
          )}
          {translatedFields.operate.update}
        </button>
      </form>
    </Box>
  );
}
