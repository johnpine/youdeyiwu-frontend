'use client';

import type { IOauthClientApi } from '@/interfaces';
import Link from 'next/link';
import { useState } from 'react';
import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import PrimaryProperties from '@/app/[locale]/admin/common/primary-properties';
import GeneralizedProperties from '@/app/[locale]/admin/common/generalized-properties';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryOauthClientInterfaceInfo({
  source,
  translatedFields,
}: {
  source: IOauthClientApi;
  translatedFields: PrefixedTTranslatedFields<'oauthClientAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-info-square me-2"></i>
        {source.name}
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle caption-top">
            <caption>
              <ToggleProperties
                isTable={false}
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
                items={[
                  {
                    index: 0,
                    name: translatedFields.operate.mainAttribute,
                  },
                  {
                    index: 1,
                    name: translatedFields.operate.otherAttribute,
                  },
                ]}
              />
            </caption>
            <tbody>
              {tabIndex === 0 && (
                <PrimaryProperties
                  source={[
                    {
                      field: 'name',
                      translatedField: translatedFields.properties.name,
                      value: source.name,
                    },
                    {
                      field: 'overview',
                      translatedField: translatedFields.properties.overview,
                      value: source.overview,
                    },
                    {
                      field: 'category',
                      translatedField: translatedFields.properties.category,
                      value: source.category,
                    },
                    {
                      field: 'doc',
                      translatedField: translatedFields.properties.doc,
                      isCustomValue: true,
                      value: source.doc ? (
                        <Link
                          href={source.doc}
                          className="link-body-emphasis link-offset-3 link-underline-opacity-0 link-underline-opacity-100-hover"
                        >
                          {source.doc}
                        </Link>
                      ) : (
                        ''
                      ),
                    },
                    {
                      field: 'dailyCallsAndUpperLimit',
                      translatedField:
                        translatedFields.properties.dailyCallsAndUpperLimit,
                      value: `${source['dailyCalls'] ?? 0} / ${
                        source['upperLimit'] === 0
                          ? translatedFields.properties.unlimited
                          : source['upperLimit']
                      }`,
                    },
                    {
                      field: 'type',
                      translatedField: translatedFields.properties.type,
                      value: translatedFields.enums.type[source.type],
                    },
                    {
                      field: 'reviewState',
                      translatedField: translatedFields.properties.reviewState,
                      value: source.reviewState
                        ? (translatedFields as any).enums.reviewState[
                            source.reviewState
                          ]
                        : '',
                    },
                    {
                      field: 'reviewReason',
                      translatedField: translatedFields.properties.reviewReason,
                      value: source.reviewReason,
                    },
                  ]}
                />
              )}

              {tabIndex === 1 && (
                <GeneralizedProperties
                  source={source}
                  translatedFields={translatedFields.generalizedProperties}
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
