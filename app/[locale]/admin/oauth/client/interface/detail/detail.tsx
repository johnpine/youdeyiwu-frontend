'use client';

import type { IOauthClientApi } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QueryOauthClientInterfaceInfo from '@/app/[locale]/admin/oauth/client/interface/detail/info';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailOauthClientInterfaceAdminPage({
  source,
  translatedFields,
}: {
  source: IOauthClientApi;
  translatedFields: PrefixedTTranslatedFields<'oauthClientAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QueryOauthClientInterfaceInfo
            source={source}
            translatedFields={translatedFields}
          />
        </div>
      </div>
    </Box>
  );
}
