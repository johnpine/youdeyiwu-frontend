'use client';

import { type ChangeEvent, type FormEvent, useEffect, useState } from 'react';
import { useMutation } from '@tanstack/react-query';
import { updateOauthClientStatus } from '@/services/api';
import diff from 'microdiff';
import type { IDifference, IOauthClient } from '@/interfaces';
import useToast from '@/hooks/useToast';
import type { TOauthClientReviewState, TOauthClientStatus } from '@/types';
import { getDiffData } from '@/lib/tool';
import Box from '@/app/[locale]/admin/common/box';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function UpdateOauthClientState(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IOauthClient;
    translatedFields: PrefixedTTranslatedFields<'oauthClientAdminPage'>;
  },
) {
  const { show } = useToast();
  const [form, setForm] = useState<{
    state: TOauthClientStatus;
    reviewState: TOauthClientReviewState;
    reviewReason?: string;
  }>({
    state: source.state,
    reviewState: source.reviewState,
    reviewReason: source.reviewReason ?? '',
  });
  const [differenceData, setDifferenceData] = useState<IDifference[]>([]);
  const [isDisabledSave, setIsDisabledSave] = useState(true);

  const updateOauthClientStatusMutation = useMutation(updateOauthClientStatus);

  useEffect(() => {
    const diffData = diff(
      {
        state: source.state,
        reviewState: source.reviewState,
        reviewReason: source.reviewReason ?? '',
      },
      form,
    );
    setDifferenceData(diffData);
    setIsDisabledSave(diffData.length === 0);
  }, [form, source]);

  async function onClickSave(e: FormEvent<HTMLFormElement>) {
    try {
      e.preventDefault();
      e.stopPropagation();

      const data = getDiffData(differenceData);
      const id = source.id;
      await updateOauthClientStatusMutation.mutateAsync({
        id,
        data,
      });

      show({
        type: 'SUCCESS',
        message: translatedFields.operate.updateCompleted,
      });
    } catch (e) {
      updateOauthClientStatusMutation.reset();
      show({
        type: 'DANGER',
        message: e,
      });
    } finally {
      setDifferenceData([]);
    }
  }

  function onChangeForm(
    e: ChangeEvent<HTMLSelectElement | HTMLInputElement | HTMLTextAreaElement>,
  ) {
    const name = e.target.name;
    const value = e.target.value;
    setForm({ ...form, [name]: value });
  }

  return (
    <Box>
      <form onSubmit={onClickSave} className="vstack gap-4">
        <div>
          <label className="form-label">
            {translatedFields.properties.state}
          </label>
          <select
            name="state"
            value={form.state}
            onChange={onChangeForm}
            className="form-select"
            aria-label="state"
          >
            {['ENABLE', 'DISABLED'].map((key) => {
              return (
                <option key={key} value={key}>
                  {(translatedFields as any).enums.state[key]}
                </option>
              );
            })}
          </select>
        </div>

        <div>
          <label className="form-label">
            {translatedFields.properties.reviewState}
          </label>
          <select
            name="reviewState"
            value={form.reviewState}
            onChange={onChangeForm}
            className="form-select"
            aria-label="reviewState"
          >
            {['PENDING', 'FAILED', 'SUCCESSFUL'].map((key) => {
              return (
                <option key={key} value={key}>
                  {(translatedFields as any).enums.reviewState[key]}
                </option>
              );
            })}
          </select>
        </div>

        <div>
          <label className="form-label">
            {translatedFields.properties.reviewReason}
          </label>
          <textarea
            rows={1}
            name="reviewReason"
            className="form-control"
            value={form.reviewReason}
            onChange={onChangeForm}
            aria-describedby="reviewReason"
          />
        </div>

        <button
          type="submit"
          disabled={updateOauthClientStatusMutation.isLoading || isDisabledSave}
          className="btn btn-success col col-lg-2 my-4"
        >
          {updateOauthClientStatusMutation.isLoading && (
            <Spinner classs="me-2" />
          )}
          {translatedFields.operate.update}
        </button>
      </form>
    </Box>
  );
}
