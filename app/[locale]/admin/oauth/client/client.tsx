'use client';

import { useQuery } from '@tanstack/react-query';
import {
  queryAllOauthClient,
  queryOauthClientStatistics,
  searchOauthClient,
} from '@/services/api';
import type {
  IOauthClient,
  IOauthClientStatistics,
  IPagination,
  IQueryParams,
} from '@/interfaces';
import Alert from '@/app/[locale]/alert/alert';
import Box from '@/app/[locale]/admin/common/box';
import { type ChangeEvent, useState } from 'react';
import { useRouter } from 'next/navigation';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import Pagination from '@/app/[locale]/admin/common/pagination';
import Image from 'next/image';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function OauthClientAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IPagination<IOauthClient>;
    translatedFields: PrefixedTTranslatedFields<'oauthClientAdminPage'>;
  },
) {
  const [params, setParams] = useState<IQueryParams>({
    page: 0,
  });
  const [searchName, setSearchName] = useState('');
  const router = useRouter();

  const queryOauthClientStatisticsQuery = useQuery(
    ['/oauth', '/clients', '/statistics'],
    async () => {
      return (await queryOauthClientStatistics()) as IOauthClientStatistics;
    },
  );

  const queryAllOauthClientQuery = useQuery(
    ['/oauth', '/clients', params, searchName],
    async (context) => {
      if (searchName) {
        return (await searchOauthClient({
          query: {
            ...(context.queryKey[2] as any),
            name: context.queryKey[3],
          },
        })) as IPagination<IOauthClient>;
      } else {
        return (await queryAllOauthClient({
          query: context.queryKey[2] as any,
        })) as IPagination<IOauthClient>;
      }
    },
    {
      keepPreviousData: true,
      initialData: source,
    },
  );

  function onClickDetail(item: IOauthClient) {
    router.push(`client/detail?id=${item.id}`);
  }

  function onClickUpdate(item: IOauthClient) {
    router.push(`client/update?id=${item.id}`);
  }

  function onClickPrevious() {
    const data = queryAllOauthClientQuery.data;
    if (!data) {
      return;
    }

    const page = Math.max(0, data.pageable.page - 1);
    setParams({ ...params, page });
  }

  function onClickNext() {
    const data = queryAllOauthClientQuery.data;
    if (!data) {
      return;
    }

    const page = Math.min(data.pageable.pages, data.pageable.page + 1);
    setParams({ ...params, page });
  }

  function onChangeSearch(e: ChangeEvent<HTMLInputElement>) {
    const value = e.target.value;
    setSearchName(value.toLowerCase());
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.count.client}
              </h6>
              <h5 className="card-title">
                {queryOauthClientStatisticsQuery.data &&
                  queryOauthClientStatisticsQuery.data.count}

                {queryOauthClientStatisticsQuery.isError && (
                  <Alert
                    message={queryOauthClientStatisticsQuery.error}
                    type="error"
                  />
                )}

                {queryOauthClientStatisticsQuery.isLoading && <Spinner />}
              </h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col">
        <Box>
          <div className="card">
            <div className="card-header bg-transparent">
              <div className="row row-cols-auto justify-content-between g-2">
                <div className="col">
                  <div className="input-group">
                    <span className="input-group-text">
                      <i className="bi bi-search"></i>
                    </span>
                    <input
                      onChange={onChangeSearch}
                      type="search"
                      className="form-control"
                      placeholder={translatedFields.tip.searchPlaceholder}
                    />
                  </div>
                </div>
                <div className="col"></div>
              </div>
            </div>
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        ID
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.name}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.website}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.callback}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.state}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.reviewState}
                      </th>
                      <th scope="col" className=""></th>
                    </tr>
                  </thead>
                  <tbody>
                    {queryAllOauthClientQuery.data.content.map((item) => {
                      return (
                        <tr key={item.id} className="text-nowrap">
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.clientId}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.logo ? (
                              <Image
                                className="object-fit-cover"
                                src={item.logo}
                                alt={item.name}
                                title={item.name}
                                width={32}
                                height={32}
                              />
                            ) : (
                              <span>{item.name}</span>
                            )}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.website}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.callback}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.state === 'ENABLE' ? (
                              <i className="bi bi-patch-check-fill me-2 text-success fs-5"></i>
                            ) : (
                              <span>
                                {translatedFields.enums.state[item.state]}
                              </span>
                            )}
                          </td>
                          <td
                            className="cursor-pointer"
                            onClick={onClickDetail.bind(this, item)}
                          >
                            {item.reviewState === 'SUCCESSFUL' ? (
                              <i className="bi bi-patch-check-fill me-2 text-success fs-5"></i>
                            ) : (
                              <span>
                                {
                                  translatedFields.enums.reviewState[
                                    item.reviewState
                                  ]
                                }
                              </span>
                            )}
                          </td>
                          <td>
                            <div className="hstack gap-2">
                              <button
                                onClick={onClickUpdate.bind(this, item)}
                                className="btn btn-sm btn-light"
                                type="button"
                              >
                                <i className="bi bi-pencil me-2"></i>
                                {translatedFields.operate.update}
                              </button>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>

              {queryAllOauthClientQuery.data.content.length === 0 && <Nodata />}

              <Pagination
                isShow={queryAllOauthClientQuery.data.pageable.pages > 0}
                onPrevious={onClickPrevious}
                onNext={onClickNext}
                isPrevious={queryAllOauthClientQuery.data.pageable.previous}
                isNext={queryAllOauthClientQuery.data.pageable.next}
                translatedFields={translatedFields.pagination}
              />
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
