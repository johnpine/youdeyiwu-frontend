import type { Metadata as MetadataNext } from 'next';
import { createError, getMetadata } from '@/lib/tool';
import { getTranslatedFields } from '@/lib/dictionaries';
import { authMiddleware } from '@/lib/api';
import { cookies } from 'next/headers';
import { queryAllOauthClient, queryPath } from '@/services/api';
import type { IOauthClient, IPagination, IPath } from '@/interfaces';
import ResetPage from '@/app/[locale]/reset/reset';
import OauthClientAdminPage from '@/app/[locale]/admin/oauth/client/client';
import { getTranslator } from 'next-intl/server';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('oauthClientAdminPage.title.management') });
}

async function getData() {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/',
      },
    });
    const req2 = queryAllOauthClient({
      baseURL: process.env.APP_API_SERVER,
      token,
    });

    const responses = await Promise.all([req1, req2]);
    const resp1 = await ((await responses[0]) as Response).json();
    const resp2 = await ((await responses[1]) as Response).json();

    return {
      isSuccess: true,
      isError: false,
      path: resp1.data as IPath,
      info: resp2.data as IPagination<IOauthClient>,
    };
  } catch (e: any) {
    return {
      isSuccess: false,
      isError: true,
      error: e,
    };
  }
}

export default async function Page({
  params: { locale },
}: {
  params: { locale: string };
}) {
  const data = await getData();
  if (data.isError) {
    return <ResetPage error={createError(data.error)} />;
  }

  const source = data.info!;
  const translatedFields = await getTranslatedFields(
    locale,
    'oauthClientAdminPage',
  );

  return (
    <OauthClientAdminPage source={source} translatedFields={translatedFields} />
  );
}
