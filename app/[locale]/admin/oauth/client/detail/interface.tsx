import { useQuery } from '@tanstack/react-query';
import { queryOauthClientApis } from '@/services/api';
import type { IOauthClient, IOauthClientApi } from '@/interfaces';
import Alert from '@/app/[locale]/alert/alert';
import AlertLoad from '@/app/[locale]/alert/load';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import { useRouter } from 'next/navigation';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function OauthClientInterfaceAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IOauthClient;
    translatedFields: PrefixedTTranslatedFields<'oauthClientAdminPage'>;
  },
) {
  const router = useRouter();

  const queryOauthClientApisByIdQuery = useQuery(
    ['/oauth', '/clients', '/apis', { cid: source.id }],
    async (context) => {
      return (await queryOauthClientApis({
        query: context.queryKey[3] as any,
      })) as IOauthClientApi[];
    },
  );

  function onClickDetail(item: IOauthClientApi) {
    router.push(`interface/detail?id=${item.id}`);
  }

  function onClickUpdate(item: IOauthClientApi) {
    router.push(`interface/update?id=${item.id}&cid=${source.clientId}`);
  }

  if (queryOauthClientApisByIdQuery.data) {
    const data = queryOauthClientApisByIdQuery.data;

    return (
      <div className="card">
        <div className="card-header bg-transparent text-secondary">
          <i className="bi bi-list me-2"></i>
          {translatedFields.properties.oauthClientInterface}
        </div>
        <div className="card-body vstack gap-4">
          <div className="table-responsive">
            <table className="table table-hover align-middle">
              <thead>
                <tr className="text-nowrap">
                  <th scope="col" className="">
                    {translatedFields.properties.name}
                  </th>
                  <th scope="col" className="">
                    {translatedFields.properties.overview}
                  </th>
                  <th scope="col" className="">
                    {translatedFields.properties.name}
                  </th>
                  <th scope="col" className="">
                    {translatedFields.properties.dailyCalls}&nbsp;/&nbsp;
                    {translatedFields.properties.upperLimit}
                  </th>
                  <th scope="col" className="">
                    {translatedFields.properties.type}
                  </th>
                  <th scope="col" className="">
                    {translatedFields.properties.reviewState}
                  </th>
                  <th scope="col" className="">
                    {translatedFields.properties.time}
                  </th>
                  <th scope="col" className=""></th>
                </tr>
              </thead>
              <tbody>
                {data.map((item) => {
                  return (
                    <tr key={item.id} className="text-nowrap">
                      <td
                        className="cursor-pointer"
                        onClick={onClickDetail.bind(this, item)}
                      >
                        {item.name}
                      </td>
                      <td
                        className="cursor-pointer"
                        onClick={onClickDetail.bind(this, item)}
                      >
                        {item.overview}
                      </td>
                      <td
                        className="cursor-pointer"
                        onClick={onClickDetail.bind(this, item)}
                      >
                        {item.category}
                      </td>
                      <td
                        className="cursor-pointer"
                        onClick={onClickDetail.bind(this, item)}
                      >
                        <span>{item.dailyCalls || 0}</span>
                        <span>&nbsp;/&nbsp;</span>
                        <span>
                          {item.upperLimit === 0
                            ? translatedFields.properties.unlimited
                            : item.upperLimit}
                        </span>
                      </td>
                      <td
                        className="cursor-pointer"
                        onClick={onClickDetail.bind(this, item)}
                      >
                        {translatedFields.enums.type[item.type]}
                      </td>
                      <td
                        className="cursor-pointer"
                        onClick={onClickDetail.bind(this, item)}
                      >
                        {item.type === 'DEFAULT' ||
                        item.reviewState === 'SUCCESSFUL' ? (
                          <i className="bi bi-patch-check-fill me-2 text-success fs-5"></i>
                        ) : (
                          <span>
                            {
                              translatedFields.enums.reviewState[
                                item.reviewState!
                              ]
                            }
                          </span>
                        )}
                      </td>
                      <td
                        className="cursor-pointer"
                        onClick={onClickDetail.bind(this, item)}
                      >
                        {item.createdOn}
                      </td>
                      <td>
                        <div className="hstack gap-2">
                          <button
                            onClick={onClickUpdate.bind(this, item)}
                            className="btn btn-sm btn-light"
                            type="button"
                          >
                            <i className="bi bi-pencil me-2"></i>
                            {translatedFields.operate.update}
                          </button>
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>

          {data.length === 0 && <Nodata />}
        </div>
      </div>
    );
  }

  if (queryOauthClientApisByIdQuery.error) {
    return <Alert type="error" message={queryOauthClientApisByIdQuery.error} />;
  }

  return <AlertLoad />;
}
