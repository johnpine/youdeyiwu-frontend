'use client';

import type { IOauthClient } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import QueryOauthClientInfo from '@/app/[locale]/admin/oauth/client/detail/info';
import OauthClientInterfaceAdminPage from '@/app/[locale]/admin/oauth/client/detail/interface';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailOauthClientAdminPage({
  source,
  translatedFields,
}: {
  source: IOauthClient;
  translatedFields: PrefixedTTranslatedFields<'oauthClientAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <QueryOauthClientInfo
            source={source}
            translatedFields={translatedFields}
          />

          <OauthClientInterfaceAdminPage
            source={source}
            translatedFields={translatedFields}
          />
        </div>
      </div>
    </Box>
  );
}
