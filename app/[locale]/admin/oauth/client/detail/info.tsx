'use client';

import type { IOauthClient } from '@/interfaces';
import { useState } from 'react';
import ToggleProperties from '@/app/[locale]/admin/common/toggle-properties';
import PrimaryProperties from '@/app/[locale]/admin/common/primary-properties';
import GeneralizedProperties from '@/app/[locale]/admin/common/generalized-properties';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function QueryOauthClientInfo({
  source,
  translatedFields,
}: {
  source: IOauthClient;
  translatedFields: PrefixedTTranslatedFields<'oauthClientAdminPage'>;
}) {
  const [tabIndex, setTabIndex] = useState(0);

  return (
    <div className="card">
      <div className="card-header bg-transparent text-secondary">
        <i className="bi bi-info-square me-2"></i>
        {source.name}
      </div>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table align-middle caption-top">
            <caption>
              <ToggleProperties
                isTable={false}
                tabIndex={tabIndex}
                setTabIndex={setTabIndex}
                items={[
                  {
                    index: 0,
                    name: translatedFields.operate.mainAttribute,
                  },
                  {
                    index: 1,
                    name: translatedFields.operate.otherAttribute,
                  },
                ]}
              />
            </caption>
            <tbody>
              {tabIndex === 0 && (
                <PrimaryProperties
                  source={[
                    {
                      field: 'name',
                      translatedField: translatedFields.properties.name,
                      value: `${source.name} (ID. ${source.clientId})`,
                    },
                    {
                      field: 'overview',
                      translatedField: translatedFields.properties.overview,
                      value: source.overview,
                    },
                    {
                      field: 'logo',
                      translatedField: translatedFields.properties.logo,
                      value: source.logo,
                    },
                    {
                      field: 'website',
                      translatedField: translatedFields.properties.website,
                      value: source.website,
                    },
                    {
                      field: 'callback',
                      translatedField: translatedFields.properties.callback,
                      value: source.callback,
                    },
                    {
                      field: 'state',
                      translatedField: translatedFields.properties.state,
                      value: translatedFields.enums.state[source.state],
                    },
                    {
                      field: 'reviewState',
                      translatedField: translatedFields.properties.reviewState,
                      value:
                        translatedFields.enums.reviewState[source.reviewState],
                    },
                    {
                      field: 'reviewReason',
                      translatedField: translatedFields.properties.reviewReason,
                      value: source.reviewReason,
                    },
                  ]}
                />
              )}

              {tabIndex === 1 && (
                <GeneralizedProperties
                  source={source}
                  translatedFields={translatedFields.generalizedProperties}
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
