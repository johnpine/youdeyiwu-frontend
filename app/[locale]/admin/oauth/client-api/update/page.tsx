import type { Metadata as MetadataNext } from 'next';
import { createError, getMetadata, isNum } from '@/lib/tool';
import { getTranslatedFields } from '@/lib/dictionaries';
import { authMiddleware } from '@/lib/api';
import { cookies } from 'next/headers';
import { queryOauthClientApis, queryPath } from '@/services/api';
import type { IOauthClientApi, IPath } from '@/interfaces';
import ResetPage from '@/app/[locale]/reset/reset';
import { notFound } from 'next/navigation';
import UpdateOauthClientApiInfo from '@/app/[locale]/admin/oauth/client-api/update/info';
import { getTranslator } from 'next-intl/server';

export async function generateMetadata({
  params: { locale },
}: {
  params: {
    locale: string;
  };
}): Promise<MetadataNext> {
  const t = await getTranslator(locale);
  return getMetadata({ title: t('oauthClientApiAdminPage.title.update') });
}

async function getData() {
  try {
    const token = authMiddleware(cookies());
    const req1 = queryPath({
      baseURL: process.env.APP_API_SERVER,
      token,
      query: {
        name: '/',
      },
    });
    const req2 = queryOauthClientApis({
      baseURL: process.env.APP_API_SERVER,
      token,
    });

    const responses = await Promise.all([req1, req2]);
    const resp1 = await ((await responses[0]) as Response).json();
    const resp2 = await ((await responses[1]) as Response).json();

    return {
      isSuccess: true,
      isError: false,
      path: resp1.data as IPath,
      info: resp2.data as IOauthClientApi[],
    };
  } catch (e: any) {
    return {
      isSuccess: false,
      isError: true,
      error: e,
    };
  }
}

export default async function Page({
  params: { locale },
  searchParams: { id },
}: {
  params: { locale: string };
  searchParams: { id?: string };
}) {
  if (!id || !isNum(id)) {
    notFound();
  }

  const data = await getData();
  if (data.isError) {
    return <ResetPage error={createError(data.error)} />;
  }

  const source = data.info!;
  const translatedFields = await getTranslatedFields(
    locale,
    'oauthClientApiAdminPage',
  );

  const find = source.find((item) => item.id + '' === id);
  if (!find) {
    notFound();
  }

  return (
    <UpdateOauthClientApiInfo
      source={find}
      translatedFields={translatedFields}
    />
  );
}
