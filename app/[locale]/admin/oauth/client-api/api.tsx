'use client';

import { useQuery } from '@tanstack/react-query';
import { queryOauthClientApis } from '@/services/api';
import type { IOauthClientApi } from '@/interfaces';
import Alert from '@/app/[locale]/alert/alert';
import Box from '@/app/[locale]/admin/common/box';
import { useRouter } from 'next/navigation';
import Spinner from '@/app/[locale]/component/spinner/spinner';
import Nodata from '@/app/[locale]/common/nodata/nodata';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function OauthClientApiAdminPage(
  this: any,
  {
    source,
    translatedFields,
  }: {
    source: IOauthClientApi[];
    translatedFields: PrefixedTTranslatedFields<'oauthClientApiAdminPage'>;
  },
) {
  const router = useRouter();

  const queryOauthClientApisQuery = useQuery(
    ['/oauth', '/clients', '/apis'],
    async () => {
      return (await queryOauthClientApis()) as IOauthClientApi[];
    },
    {
      keepPreviousData: true,
      initialData: source,
    },
  );

  function onClickDetail(item: IOauthClientApi) {
    router.push(`client-api/detail?id=${item.id}`);
  }

  function onClickUpdate(item: IOauthClientApi) {
    router.push(`client-api/update?id=${item.id}`);
  }

  return (
    <div className="row g-4">
      <div className="col-12">
        <Box>
          <div className="card">
            <div className="card-body">
              <h6 className="card-subtitle mb-2 text-secondary">
                {translatedFields.count.api}
              </h6>
              <h5 className="card-title">
                {queryOauthClientApisQuery.data &&
                  queryOauthClientApisQuery.data.length}

                {queryOauthClientApisQuery.isError && (
                  <Alert
                    message={queryOauthClientApisQuery.error}
                    type="error"
                  />
                )}

                {queryOauthClientApisQuery.isLoading && <Spinner />}
              </h5>
            </div>
          </div>
        </Box>
      </div>
      <div className="col">
        <Box>
          <div className="card">
            <div className="card-body vstack gap-4">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <thead>
                    <tr className="text-nowrap">
                      <th scope="col" className="">
                        {translatedFields.properties.name}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.overview}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.category}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.type}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.reviewState}
                      </th>
                      <th scope="col" className="">
                        {translatedFields.properties.time}
                      </th>
                      <th scope="col" className=""></th>
                    </tr>
                  </thead>
                  <tbody>
                    {queryOauthClientApisQuery.data &&
                      queryOauthClientApisQuery.data.map((item) => {
                        return (
                          <tr key={item.id} className="text-nowrap">
                            <td
                              className="cursor-pointer"
                              onClick={onClickDetail.bind(this, item)}
                            >
                              {item.name}
                            </td>
                            <td
                              className="cursor-pointer"
                              onClick={onClickDetail.bind(this, item)}
                            >
                              {item.overview}
                            </td>
                            <td
                              className="cursor-pointer"
                              onClick={onClickDetail.bind(this, item)}
                            >
                              {item.category}
                            </td>
                            <td
                              className="cursor-pointer"
                              onClick={onClickDetail.bind(this, item)}
                            >
                              {translatedFields.enums.type[item.type]}
                            </td>
                            <td
                              className="cursor-pointer"
                              onClick={onClickDetail.bind(this, item)}
                            >
                              {item.type === 'DEFAULT'
                                ? translatedFields.properties.successful
                                : translatedFields.enums.reviewState[
                                    item.reviewState!
                                  ]}
                            </td>
                            <td
                              className="cursor-pointer"
                              onClick={onClickDetail.bind(this, item)}
                            >
                              {item.createdOn}
                            </td>
                            <td>
                              <div className="hstack gap-2">
                                <button
                                  onClick={onClickUpdate.bind(this, item)}
                                  className="btn btn-sm btn-light"
                                  type="button"
                                >
                                  <i className="bi bi-pencil me-2"></i>
                                  {translatedFields.operate.update}
                                </button>
                              </div>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </table>
              </div>

              {queryOauthClientApisQuery.data &&
                queryOauthClientApisQuery.data.length === 0 && <Nodata />}
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
}
