'use client';

import type { IOauthClientApi } from '@/interfaces';
import Box from '@/app/[locale]/admin/common/box';
import DetailsOauthClientApiInfo from '@/app/[locale]/admin/oauth/client-api/detail/info';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';

export default function DetailOauthClientApiAdminPage({
  source,
  translatedFields,
}: {
  source: IOauthClientApi;
  translatedFields: PrefixedTTranslatedFields<'oauthClientApiAdminPage'>;
}) {
  return (
    <Box>
      <div className="row">
        <div className="col vstack gap-4">
          <DetailsOauthClientApiInfo
            source={source}
            translatedFields={translatedFields}
          />
        </div>
      </div>
    </Box>
  );
}
