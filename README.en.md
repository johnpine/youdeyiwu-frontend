# <img alt="logo" src="https://youdeyiwu.com/images/logo.svg" width="30rem" /> Youdeyiwu

[![CI Build Status](https://github.com/dafengzhen/youdeyiwu-frontend/actions/workflows/docker-publish.yml/badge.svg)](https://github.com/dafengzhen/youdeyiwu-frontend/actions/workflows/docker-publish.yml)
[![LICENSE](https://img.shields.io/github/license/dafengzhen/youdeyiwu-frontend)](https://github.com/dafengzhen/youdeyiwu-frontend/blob/main/LICENSE)

Youdeyiwu is an open-source forum program that provides rich features. It can be used as a forum or blog for managing or sharing articles, or customized development can be done on this basis.

[简体中文](./README.md)

# Features

The program structure is based on sections, tags, and posts, and has the following features:

- Server-side rendering support, which is beneficial for search engine optimization (SEO)
- Section grouping for easy management and browsing
- Custom tags for organizing post categories
- Rich text editing for posts, making content more diverse
- Post review settings, allowing for post review
- Post content drafts and revision history for easy editing and recovery
- Post templates to speed up posting
- Multi-level comment replies for clearer communication
- Notification of messages for easy access to key information
- OAUTH open access, making it easy for third-party login authorization
- Configuration of the same program OAUTH access, making it easy to access the same instance
- Backend configuration management for global forum configuration
- Backend user management for easy management of user information
- Backend role management for controlling user permissions
- Backend dynamic permission and menu management
- Backend section, tag, and post management
- Backend section and tag group management
- Backend comment reply management
- Backend message management
- Backend OAUTH client and interface management
- Support for dark mode on pages
- Support for page internationalization

# Quick Start

For more installation information, please refer to the [documentation](https://www.youdeyiwu.com/docs) ↗.

# Feedback

If you have suggestions or feedback, or would like to submit a PR, please feel free to communicate with me.

[issues](https://github.com/dafengzhen/youdeyiwu-frontend/issues) ↗

# License

[MIT](https://opensource.org/licenses/MIT)
