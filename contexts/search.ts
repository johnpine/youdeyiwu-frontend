import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath } from '@/interfaces';

export interface ISearchPageContext {
  source: {
    path: IPath;
    queryParams: {
      v?: string;
    };
  };
  translatedFields: PrefixedTTranslatedFields<'searchPage'>;
}

export const SearchPageContext = createContext<ISearchPageContext | null>(null);
