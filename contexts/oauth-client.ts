import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath } from '@/interfaces';
import type { IOauthClient } from '@/interfaces';

export interface IOauthClientPageContext {
  source: {
    path: IPath;
    clients: IOauthClient[];
  };
  translatedFields: PrefixedTTranslatedFields<'oauthPage'>;
}

export const OauthClientPageContext =
  createContext<IOauthClientPageContext | null>(null);
