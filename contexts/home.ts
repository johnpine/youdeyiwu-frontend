import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type {
  IPagination,
  IPath,
  IPost,
  ISectionClient,
  ISectionDetails,
} from '@/interfaces';

export interface IHomePageContext {
  source: {
    path: IPath;
    sections: ISectionClient[];
    randomPosts: IPost[];
    posts: IPagination<IPost>;
    sectionDetails: ISectionDetails | undefined;
    queryParams: {
      sectionId?: string;
      tagId?: string;
      sectionGroupId?: string;
    };
  };
  translatedFields: PrefixedTTranslatedFields<'homePage'>;
}

export const HomePageContext = createContext<IHomePageContext | null>(null);
