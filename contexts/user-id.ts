import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath, IUserClientDetails } from '@/interfaces';

export interface IUserIdPageContext {
  source: {
    path: IPath;
    userDetails: IUserClientDetails;
    queryParams: {
      sectionId?: string;
      tagId?: string;
    };
  };
  translatedFields: PrefixedTTranslatedFields<'userIdPage'>;
}

export const UserIdPageContext = createContext<IUserIdPageContext | null>(null);
