import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IOauthClient, IPath } from '@/interfaces';

export interface IOauthAuthPageContext {
  source: {
    path: IPath;
    client: IOauthClient;
    queryParams: {
      clientId: string;
      responseType: string;
      redirectUri: string;
      scope: string;
      state: string;
    };
  };
  translatedFields: PrefixedTTranslatedFields<'oauthPage'>;
}

export const OauthAuthPageContext = createContext<IOauthAuthPageContext | null>(
  null,
);
