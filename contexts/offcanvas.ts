import { createContext } from 'react';
import type { IOffcanvasProps } from '@/app/[locale]/offcanvas/wrapper';

export const OffcanvasContext = createContext<{
  isLoadingOffcanvasShowing: boolean;
  show: (offcanvasProps?: IOffcanvasProps) => string;
  hide: (id: string) => void;
}>({
  isLoadingOffcanvasShowing: false,
  show: () => '',
  hide: () => undefined,
});
