import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath, ISectionDetails } from '@/interfaces';

export interface ISectionIdPageContext {
  source: {
    path: IPath;
    sectionDetails: ISectionDetails;
    queryParams: {
      tagId?: string;
      tagGroupId?: string;
    };
  };
  translatedFields: PrefixedTTranslatedFields<'sectionIdPage'>;
}

export const SectionIdPageContext = createContext<ISectionIdPageContext | null>(
  null,
);
