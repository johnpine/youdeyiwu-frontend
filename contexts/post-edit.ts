import { createContext, type Dispatch, type SetStateAction } from 'react';
import type {
  IPostEditConfig,
  IPostEditForm,
} from '@/app/[locale]/posts/[id]/edit/wrapper';
import type { IDifference } from '@/interfaces';

export interface IPostEditContext {
  form: IPostEditForm;
  setForm: Dispatch<SetStateAction<IPostEditForm>>;
  config: IPostEditConfig;
  onConfig: (data: Partial<IPostEditConfig>) => void;
  navItemName: string;
  onNavItemName: (value: string) => void;
  onForm: (data: Partial<IPostEditForm>) => void;
  differenceData: IDifference[];
}

export const PostEditContext = createContext<IPostEditContext>({
  form: {
    name: '',
    content: '',
    sectionId: '',
    otherStates: ['DEFAULT'],
    allow: [],
    block: [],
    overview: '',
    statement: '',
    cover: '',
    customTags: [],
    contentType: 'DEFAULT',
    contentLink: '',
    images: [],
    paginationNavLink: {
      prevName: '',
      prevLink: '',
      nextName: '',
      nextLink: '',
    },
  },
  setForm: () => {},
  config: {
    isCenter: false,
  },
  onConfig: () => {},
  navItemName: '编辑',
  onNavItemName: () => {},
  onForm: () => {},
  differenceData: [],
});
