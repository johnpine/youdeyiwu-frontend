import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath, IPostFollowsMessage } from '@/interfaces';

export interface IFollowPageContext {
  source: {
    path: IPath;
    messages: IPostFollowsMessage[];
  };
  translatedFields: PrefixedTTranslatedFields<'followPage'>;
}

export const FollowPageContext = createContext<IFollowPageContext | null>(null);
