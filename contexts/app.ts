import { createContext } from 'react';
import type TBootstrap from 'bootstrap';
import type { TMetadata } from '@/types';

export interface IAppContext {
  bootstrap?: typeof TBootstrap | undefined;
  metadata?: TMetadata;
}

export const AppContext = createContext<IAppContext>({});
