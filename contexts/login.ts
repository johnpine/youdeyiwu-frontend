import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath } from '@/interfaces';

export interface ILoginPageContext {
  source: {
    path: IPath;
    queryParams: {
      type: 'username' | 'email' | 'phone' | 'qq';
    };
  };
  translatedFields: PrefixedTTranslatedFields<'loginPage'>;
}

export const LoginPageContext = createContext<ILoginPageContext | null>(null);
