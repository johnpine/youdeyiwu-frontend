import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath } from '@/interfaces';

export interface IYwPageContext {
  source: {
    path: IPath;
    queryParams: {
      code: string;
      clientId: string;
      state?: string;
    };
  };
  translatedFields: PrefixedTTranslatedFields<'ywPage'>;
}

export const YwPageContext = createContext<IYwPageContext | null>(null);
