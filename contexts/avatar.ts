import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath } from '@/interfaces';

export interface IAvatarPageContext {
  source: {
    path: IPath;
  };
  translatedFields: PrefixedTTranslatedFields<'avatarPage'>;
}

export const AvatarPageContext = createContext<IAvatarPageContext | null>(null);
