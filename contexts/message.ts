import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IClientMessage, IPagination, IPath } from '@/interfaces';

export interface IMessagePageContext {
  source: {
    path: IPath;
    messages: IPagination<IClientMessage>;
  };
  translatedFields: PrefixedTTranslatedFields<'messagePage'>;
}

export const MessagePageContext = createContext<IMessagePageContext | null>(
  null,
);
