import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath, IPostClientDetails } from '@/interfaces';
import type { QueryKey } from '@tanstack/query-core';

export interface IPostIdPageContext {
  source: {
    path: IPath;
    postDetails: IPostClientDetails;
  };
  translatedFields: PrefixedTTranslatedFields<'postIdPage'>;
  queryKey: QueryKey;
}

export const PostIdPageContext = createContext<IPostIdPageContext | null>(null);
