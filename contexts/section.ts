import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath, ISectionClient } from '@/interfaces';

export interface ISectionPageContext {
  source: {
    path: IPath;
    sections: ISectionClient[];
  };
  translatedFields: PrefixedTTranslatedFields<'sectionPage'>;
}

export const SectionPageContext = createContext<ISectionPageContext | null>(
  null,
);
