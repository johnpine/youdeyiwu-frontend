import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath } from '@/interfaces';

export interface IQqPageContext {
  source: {
    path: IPath;
    queryParams: {
      code: string;
      state: string;
    };
  };
  translatedFields: PrefixedTTranslatedFields<'qqPage'>;
}

export const QqPageContext = createContext<IQqPageContext | null>(null);
