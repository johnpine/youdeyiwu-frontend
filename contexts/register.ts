import { createContext } from 'react';
import type { PrefixedTTranslatedFields } from '@/lib/dictionaries';
import type { IPath } from '@/interfaces';
import type { IConfigRegister } from '@/interfaces';

export interface IRegisterPageContext {
  source: {
    path: IPath;
    registerConfig: IConfigRegister;
    queryParams: {
      type: 'username' | 'email' | 'phone' | 'qq';
    };
  };
  translatedFields: PrefixedTTranslatedFields<'registerPage'>;
}

export const RegisterPageContext = createContext<IRegisterPageContext | null>(
  null,
);
