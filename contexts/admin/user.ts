import { createContext, type Dispatch, type SetStateAction } from 'react';
import type { IUser } from '@/interfaces';
import type { TMetadata } from '@/types';
import { type QueryKey } from '@tanstack/query-core';

export const UserAdminContext = createContext<{
  metadata?: TMetadata | null;
  isCreate?: boolean;
  detailItem?: IUser | null;
  updateItem?: IUser | null;
  setIsCreate?: Dispatch<SetStateAction<boolean>> | null;
  setDetailItem?: Dispatch<SetStateAction<IUser | undefined>> | null;
  setUpdateItem?: Dispatch<SetStateAction<IUser | undefined>> | null;
  queryKey?: QueryKey;
}>({});
