import { createContext } from 'react';
import type { IToastProps } from '@/app/[locale]/toast/wrapper';

export const ToastContext = createContext<{
  isLoadingToastShowing: boolean;
  show: (toastProps?: IToastProps) => string;
  hide: (id: string) => void;
}>({
  isLoadingToastShowing: false,
  show: () => '',
  hide: () => undefined,
});
