import type { IBase } from '@/interfaces';
import type { TMessageRange, TMessageState, TMessageTypeEnum } from '@/types';

export interface IMessage extends IBase {
  name: string;
  content: string;
  overview: string;
  messageType: TMessageTypeEnum;
  messageRange: TMessageRange;
  businessId: number;
  businessName: string;
  businessRemark: string;
  sender: number;
  receiver: number;
  senderAlias?: string;
  receiverAlias?: string;
}

export interface IClientMessage extends IBase {
  name: string;
  overview?: string;
  content: string;
  messageType: TMessageTypeEnum;
  messageRange: TMessageRange;
  state?: TMessageState;
}

export interface IMessageStatistics {
  count: number;
}

export interface ISendMessageBody {
  name: string;
  content: string;
  messageRange: TMessageRange;
  businessId?: number;
  businessName?: string;
  businessRemark?: string;
  receiver?: number;
}
