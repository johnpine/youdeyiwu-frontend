export interface IGenerateCaptchaByUsernameBody {
  username: string;
}

export interface IGenerateCaptchaByPhoneBody {
  phone: string;
  contentType: 'LOGIN' | 'REGISTER' | 'VERIFY';
}

export interface IGenerateCaptchaByEmailBody {
  email: string;
  contentType: 'LOGIN' | 'REGISTER' | 'VERIFY';
}

export interface IGenerateCaptchaByUsername {
  id: string;
}

export interface IGenerateImageId {
  id: string;
}
