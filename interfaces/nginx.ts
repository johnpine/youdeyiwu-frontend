import type { IBase } from '@/interfaces/data';

export interface INginxKey extends IBase {
  secret: string;
  remark?: string;
  used: boolean;
}

export interface INginxLogReadRecord extends IBase {
  logType: 'ACCESS' | 'ERROR';
  logFilePath: string;
  fixedDelaySeconds: number;
  autostart: boolean;
  errorMessage?: string;
}

export interface INginxLogEvent extends IBase {
  logType: 'ACCESS' | 'ERROR';
  message: string;
  status?: string;
  httpReferer?: string;
  httpForwardedProto?: string;
  httpCfRay?: string;
  remoteAddr?: string;
  timeLocal?: string;
  request?: string;
  httpCdnLoop?: string;
  httpVersion?: string;
  remoteUser?: string;
  httpCfConnectingIp?: string;
  httpForwardedFor?: string;
  httpPathParam?: string;
  httpCfIpCountry?: string;
  httpUserAgent?: string;
  bodyBytesSent?: string;
  httpTrueClientIp?: string;
  httpMethod?: string;
  httpCfVisitor?: string;
  hostHeader?: string;
}

export interface INginxErrorLogEvent extends IBase {
  logType: 'ACCESS' | 'ERROR';
  message: string;
  timeLocal?: string;
  errorLevel?: string;
  errorMessage?: string;
  client?: string;
  server?: string;
  host?: string;
  upstream?: string;
  referrer?: string;
  request?: string;
  httpVersion?: string;
  httpPathParam?: string;
  httpMethod?: string;
  hostHeader?: string;
}

export interface INginxLogsWebsiteAccessCountByDay {
  ranges: Record<string, number>;
  dates: string[];
}

export interface INginxLogsWebsiteQuarterlyAccessCounts {
  ranges: Record<string, number>;
  dates: number[];
}

export interface INginxLogsWebsiteAccessPercentage {
  terms: Record<string, number>;
}

export interface INginxLogsWebsitePageViews {
  paths: Record<string, number>;
}

export interface INginxLogsWebsiteUniqueVisitors {
  adders: Record<string, number>;
}

export interface INginxLogsWebsiteBounceRate {
  bounceRate: number;
}

export interface INginxLogsWebsiteErrorLogLevel {
  levels: Record<string, number>;
}

export interface INginxLogStatistics {
  count: number;
}

export interface INginxErrorLogStatistics {
  count: number;
}
