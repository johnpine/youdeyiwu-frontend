import type { IConfigRegister, IRole, IYwOauthClientUrl } from '@/interfaces';

export interface IContact {
  id: string;
  key: string;
  val: string;
}

export interface IUserOv {
  id: number;
  alias: string;
  details: {
    personalizedSignature?: string;
    smallAvatarUrl?: string;
    mediumAvatarUrl?: string;
    largeAvatarUrl?: string;
    contacts?: IContact[];
    about?: string;
  };
  roles: IRole[];
  statistic: {
    posts?: number;
    sections?: number;
    tags?: number;
    comments?: number;
    replies?: number;
    views?: number;
  };
}

export interface IPath {
  imageConfig?: {
    enable: boolean;
    total: number;
    interval: string;
  };
  phoneConfig?: {
    enable: boolean;
    total: number;
    interval: string;
  };
  emailConfig?: {
    enable: boolean;
    total: number;
    interval: string;
  };
  qqConfig?: {
    enable: boolean;
  };
  registerConfig?: IConfigRegister;
  clientConfig?: {
    showMenuEntry: boolean;
    doc: string;
    ywClientUrls: IYwOauthClientUrl[];
  };
  postOther?: {
    helpLink: string;
  };
  postConfig: { enableReview: boolean };
  siteConfig: {
    helpLink: string | undefined;
    feedbackLink: string | undefined;
    reportLink: string | undefined;
    githubLink: string | undefined;
    mpImageLink: string | undefined;
    disableRegistration?: boolean;
  };
  user?: IUserOv;
}
