import type { IBase } from '@/interfaces';

export interface IHistory extends IBase {
  name?: string;
  overview?: string;
  content?: string;
}

export interface ICreatePostHistoryBody {
  name?: string;
  overview?: string;
  content?: string;
}

export interface IQueryAllHistoryByPostId {
  [key: string]: IHistory[];
}
