import type { IBase } from '@/interfaces';

export interface IUrl {
  default: string;
}

export interface IFile {
  urls: IUrl;
}

export interface IClientFile extends IFile, IBase {
  originalName: string;
  size: number;
  viewCount: number;
  extension?: string;
  isImage: boolean;
}

export interface IUploadAvatarFileBody {
  file?: string | Blob;
  formData?: FormData;
  signal?: AbortSignal;
  onUploadProgress?: (progressEvent: ProgressEvent) => void;
}

export interface IUploadMessageSendFileBody {
  file?: string | Blob;
  formData?: FormData;
  signal?: AbortSignal;
  onUploadProgress?: (progressEvent: ProgressEvent) => void;
}

export interface IUploadPostTemplateFileBody {
  file?: string | Blob;
  formData?: FormData;
  signal?: AbortSignal;
  onUploadProgress?: (progressEvent: ProgressEvent) => void;
}
