import type { IBase, ISection } from '@/interfaces';

export interface IPostTemplate extends IBase {
  id: number;
  name: string;
  overview?: string;
  content: string;
  sort: number;
  section: ISection;
}

export interface IPostTemplateStatistics {
  count: number;
}
