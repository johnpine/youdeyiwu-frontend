import type { IBase } from '@/interfaces';

export interface IDraft extends IBase {
  type: 'DEFAULT' | 'NEW_POST' | 'EDIT_POST' | 'NEW_SECTION' | 'EDIT_SECTION';
  name?: string;
  overview?: string;
  content?: string;
}

export interface ICreateNewPostDraftBody {
  name?: string;
  overview?: string;
  content?: string;
}

export interface ICreateEditPostDraftBody {
  name?: string;
  overview?: string;
  content?: string;
}
