import type { IBase } from '@/interfaces';

export interface IOauthClient extends IBase {
  name: string;
  logo?: string;
  overview?: string;
  website: string;
  callback: string;
  clientId: string;
  state: 'ENABLE' | 'DISABLED';
  reviewState: 'PENDING' | 'FAILED' | 'SUCCESSFUL';
  reviewReason?: string;
}

export interface IOauthClientApi extends IBase {
  name: string;
  overview?: string;
  category?: string;
  doc?: string;
  type: 'DEFAULT' | 'REQUIRED';
  upperLimit: number;
  dailyCalls?: number;
  reviewState?: 'PENDING' | 'FAILED' | 'SUCCESSFUL';
  reviewReason?: string;
}

export interface ICreateOauthClientBody {
  name: string;
  website: string;
  callback: string;
}

export interface ICheckOauthClientBody {
  responseType: string;
  clientId: string;
  redirectUri: string;
  scope: string;
}

export interface ICheckOauthClient {
  code: string;
  state?: string;
  redirectUri?: string;
}

export interface IOauthClientStatistics {
  count: number;
}

export interface IOauthClientToken {
  accessToken: string;
  tokenType: string;
  expiresIn: number;
}

export interface IOauthClientUserInfo {
  id: number;
  alias: string;
  personalizedSignature?: string;
  smallAvatarUrl?: string;
  mediumAvatarUrl?: string;
  largeAvatarUrl?: string;
}

export interface IUpdateOauthClientStatusBody {
  status?: string;
  reviewStatus?: string;
  reviewReason?: string;
}

export interface IUpdateOauthClientByIdBody {
  name?: string;
  overview?: string;
  logo?: string;
  website?: string;
  callback?: string;
}

export interface IUpdateOauthClientApiByApiIdBody {
  name?: string;
  overview?: string;
  category?: string;
  doc?: string;
  type?: 'DEFAULT' | 'REQUIRED';
  upperLimit?: number;
}

export interface IUpdateOauthClientApiStatusBody {
  apiId: string;
  reviewStatus?: string;
  reviewReason?: string;
}

export interface IApplyOauthClientApiBody {
  apiId: string;
}

export interface IOauthClientAuthCallbackBody {
  code: string;
  state?: string;
  clientId: string;
}
