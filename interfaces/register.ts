export interface IRegisterByUsernameBody {
  alias?: string;
  username: string;
  password: string;
  cid: string;
  code: string;
}

export interface IRegisterByPhoneBody {
  alias?: string;
  phone: string;
  code: string;
}

export interface IRegisterByEmailBody {
  alias?: string;
  email: string;
  code: string;
}
