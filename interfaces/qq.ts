export interface IQueryQqAuthUrlBody {
  alias?: string;
}

export interface IQqAuthCallbackBody {
  code: string;
  state: string;
}
