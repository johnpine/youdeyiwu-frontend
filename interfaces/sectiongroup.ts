import type { IBase, ISection } from '@/interfaces';

export interface ISectionGroup extends IBase {
  name: string;
  sort: number;
  sections?: ISection[];
}

export interface ISectionGroupStatistics {
  count: number;
}

export interface ICreateSectionGroupBody {
  name: string;
}

export interface IUpdateSectionGroupBody {
  name?: string;
  sort?: number;
}

export interface ICreateSectionGroupSectionBody {
  sectionId: string;
}

export interface IRemoveSectionGroupSectionBody {
  sectionId: string;
}
