export interface ILoginByUsernameBody {
  username: string;
  password: string;
  cid: string;
  code: string;
}

export interface ILoginByPhoneBody {
  phone: string;
  code: string;
}

export interface ILoginByEmailBody {
  email: string;
  code: string;
}
