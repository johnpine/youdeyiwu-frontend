import type { IBase } from '@/interfaces';
import type { TMenuMenuType, TMenuType } from '@/types';

export interface IMenu extends IBase {
  name: string;
  path: string;
  sort: number;
  menuType: TMenuMenuType;
  type: TMenuType;
  submenus: ISubmenu[];
}

export interface ISubmenu extends IBase {
  name: string;
  path: string;
  sort: number;
  menuType: TMenuMenuType;
  type: TMenuType;
  menu?: IMenu;
}

export interface ICreateMenuBody {
  name: string;
  path: string;
}

export interface IUpdateMenuBody {
  name?: string;
  path?: string;
  sort?: number;
  menuType?: TMenuMenuType;
}

export interface ICreateSubmenuBody {
  menuId: number;
  name: string;
  path: string;
}

export interface IMenuStatistics {
  count: number;
  submenuCount: number;
}

export interface IAssignMenuBody {
  roleId: string;
}

export interface IUnAssignMenuMenuBody {
  roleId: string;
}
