import type { IBase } from '@/interfaces';
import type { TPermissionMethod, TPermissionType } from '@/types/permission';

export interface IPermission extends IBase {
  name: string;
  alias?: string;
  overview?: string;
  method: TPermissionMethod;
  type: TPermissionType;
  caseInsensitive: boolean;
  sort: number;
  categoryId?: string;
  categoryName?: string;
  customized: boolean;
  matcher?: IPermission;
}

export interface IPermissionDetails {
  basic: IPermission;
  matcher: IPermission;
}

export interface IUpdatePermissionBody {
  name?: string;
  alias?: string;
  overview?: string;
  method?: TPermissionMethod;
  type?: TPermissionType;
  caseInsensitive?: boolean;
  sort?: number;
  categoryId?: string;
  categoryName?: string;
}

export interface IPermissionStatistics {
  count: number;
}

export interface IAssignPermissionBody {
  roleId: string;
}

export interface IUnAssignPermissionBody {
  roleId: string;
}
