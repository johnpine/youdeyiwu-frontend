import type { IUserOv } from '@/interfaces/path';

export interface IBase extends IExtra {
  id: number;
  createdBy?: number;
  updatedBy?: number;
  creatorAlias?: string;
  updaterAlias?: string;
  createdOn: string;
  updatedOn: string;
  deleted: boolean;
  _createdOnText?: string;
  _updatedOnText?: string;
}

export interface IExtra {
  user?: IUserOv;
}

export interface IPagination<item> {
  content: item[];
  pageable: IPageable;
}

export interface IPageable {
  next: boolean;
  page: number;
  pages: number;
  previous: boolean;
  size: number;
  keyset?: IKeySet;
}

export interface IKeySet {
  lowest: [];
  highest: [];
}

export interface IQueryParams {
  page?: number;
  size?: number;
  sort?: string;
  tagId?: number;
  tid?: number;
  sectionId?: number;
  sid?: number;
  postId?: number;
  pid?: number;
  userId?: number;
  uid?: number;
}

export interface IToken {
  id: number;
  alias: string;
  token: string;
  refreshToken: string;
}

export interface IData<T> {
  status: number;
  message: string;
  data: T;
}
