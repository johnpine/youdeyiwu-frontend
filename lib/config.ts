import sanitizeHtml from 'sanitize-html';

/**
 * sanitizeHtml
 */
sanitizeHtml.defaults.allowedTags.push('img');
