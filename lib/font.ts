import { JetBrains_Mono, Raleway } from 'next/font/google';

export const ralewayFont = Raleway({
  style: ['normal', 'italic'],
  subsets: ['latin', 'latin-ext'],
  variable: '--font-raleway',
});

export const jetBrainsMonoFont = JetBrains_Mono({
  style: ['normal', 'italic'],
  subsets: ['latin', 'latin-ext'],
  variable: '--font-jetBrains-mono',
});
