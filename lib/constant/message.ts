export const MESSAGE_TYPE_KEY: Record<string, string> = {
  SYSTEM: '系统',
  OTHER: '其他',
  STATUS: '状态',
  LOGIN: '登录',
  REGISTER: '注册',
  LOGOUT: '登出',
  USER: '用户',
  ROLE: '角色',
  PERMISSION: '权限',
  SECTION: '版块',
  POST: '帖子',
  TAG: '标签',
  COMMENT: '评论',
  REPLY: '回复',
};

export const MESSAGE_RANGE_KEY: Record<string, string> = {
  ALL: '所有用户',
  USER: '用户',
};
