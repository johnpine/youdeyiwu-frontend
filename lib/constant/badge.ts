export const BADGE_COLOR_MODE: Record<string, string> = {
  DEFAULT: '默认',
  CUSTOM: '自定义',
};

export const BADGE_BACKGROUND_COLOR_MODE: Record<string, string> = {
  DEFAULT: '默认',
  CUSTOM: '自定义',
};
