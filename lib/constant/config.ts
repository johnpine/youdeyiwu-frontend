export const CONFIG_TYPE_KEY: Record<string, string> = {
  user: '用户',
  site: '站点',
  jwt: '令牌',
  section: '版块',
  post: '帖子',
  qq: 'QQ ',
  image: '图片',
  phone: '手机',
  email: '邮箱',
  client: '客户端',
};
