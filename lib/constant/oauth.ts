export const OAUTH_CLIENT_STATE_LIST = ['ENABLE', 'DISABLED'];

export const OAUTH_CLIENT_STATE_KEY: Record<string, string> = {
  ENABLE: '启用',
  DISABLED: '禁用',
};

export const OAUTH_CLIENT_REVIEW_STATE_LIST = [
  'PENDING',
  'FAILED',
  'SUCCESSFUL',
];

export const OAUTH_CLIENT_REVIEW_STATE_KEY: Record<string, string> = {
  PENDING: '待审',
  FAILED: '不通过',
  SUCCESSFUL: '通过',
};

export const OAUTH_CLIENT_API_TYPE_LIST = ['DEFAULT', 'REQUIRED'];

export const OAUTH_CLIENT_API_REVIEW_STATE_LIST = [
  'PENDING',
  'FAILED',
  'SUCCESSFUL',
];

export const OAUTH_CLIENT_API_TYPE_KEY: Record<string, string> = {
  DEFAULT: '默认开通',
  REQUIRED: '需要申请',
};

export const OAUTH_CLIENT_API_REVIEW_STATE_KEY: Record<string, string> = {
  PENDING: '待审',
  FAILED: '不通过',
  SUCCESSFUL: '通过',
};
