export const STYLE_TYPE: Record<string, string> = {
  PRESET: '预设',
  NAME: '名称',
};

export const STYLE_COLOR_MODE: Record<string, string> = {
  DEFAULT: '默认',
  CUSTOM: '自定义',
};

export const STYLE_BACKGROUND_COLOR_MODE: Record<string, string> = {
  DEFAULT: '默认',
  CUSTOM: '自定义',
};
