export const TOKEN_NAME =
  process.env.NODE_ENV === 'production' && process.env.APP_URL_HTTPS === 'true'
    ? '__Secure-tk'
    : 'tk';
export const REFRESH_TOKEN_NAME =
  process.env.NODE_ENV === 'production' && process.env.APP_URL_HTTPS === 'true'
    ? '__Secure-rtk'
    : 'rtk';

export const SET_COOKIE = 'Set-Cookie';
export const LOCATION = 'location';
export const X_RATE_LIMIT_REMAINING = 'x-rate-limit-remaining';
export const X_RATE_LIMIT_RETRY_AFTER_MILLISECONDS =
  'x-rate-limit-retry-after-milliseconds';
export const X_POWERED_BY = 'x-powered-by';
export const CONTENT_TYPE = 'content-type';
export const X_POWERED_BY_HEADER = {
  [X_POWERED_BY]: 'www.youdeyiwu.com',
};
export const AUTHORIZATION = 'Authorization';
export const BEARER = 'Bearer ';
