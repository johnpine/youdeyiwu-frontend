export const SECTION_STATE_LIST = ['SHOW', 'HIDE', 'LOCK', 'CLOSE'];

export const SECTION_OTHER_STATE_LIST = [
  'DEFAULT',
  'ALLOW',
  'BLOCK',
  'LOGIN_SEE',
];

export const SECTION_STATE_KEY: Record<string, string> = {
  SHOW: '显示',
  HIDE: '隐藏',
  LOCK: '锁定',
  CLOSE: '关闭',
};

export const SECTION_OTHER_STATE_KEY: Record<string, string> = {
  DEFAULT: '默认',
  ALLOW: '白名单',
  BLOCK: '黑名单',
  LOGIN_SEE: '登录可见',
};

export const SECTION_STATE_OBJ: Record<string, string> = {
  SHOW: 'SHOW',
  HIDE: 'HIDE',
  LOCK: 'LOCK',
  CLOSE: 'CLOSE',
};

export const POST_STATE_LIST = ['SHOW', 'HIDE', 'LOCK', 'CLOSE'];

export const POST_COMPOSITE_STATE_KEY: Record<string, string> = {
  APPROVED: '审核通过',
  DENIED: '审核不通过',
  PENDING: '等待审核',
  CLOSE: '关闭审核',
  SHOW: '显示',
  HIDE: '隐藏',
  LOCK: '锁定',
};

export const POST_STATE_KEY: Record<string, string> = {
  SHOW: '显示',
  HIDE: '隐藏',
  LOCK: '锁定',
  CLOSE: '关闭',
};

export const POST_STATE_OBJ: Record<string, string> = {
  SHOW: 'SHOW',
  HIDE: 'HIDE',
  LOCK: 'LOCK',
  CLOSE: 'CLOSE',
};

export const POST_REVIEW_STATE_KEY: Record<string, string> = {
  APPROVED: '审核通过',
  DENIED: '审核不通过',
  PENDING: '等待审核',
  CLOSE: '关闭审核',
};

export const POST_REVIEW_STATE_LIST = [
  'APPROVED',
  'DENIED',
  'PENDING',
  'CLOSE',
];

export const POST_OTHER_STATE_KEY: Record<string, string> = {
  DEFAULT: '默认',
  ALLOW: '白名单',
  BLOCK: '黑名单',
  CLOSE_COMMENT: '关闭评论',
  CLOSE_REPLY: '关闭回复',
  CLOSE_COMMENT_REPLY: '关闭评论回复',
  LOGIN_SEE: '登录可见',
};

export const POST_OTHER_STATE_LIST = [
  'DEFAULT',
  'ALLOW',
  'BLOCK',
  'CLOSE_COMMENT',
  'CLOSE_REPLY',
  'CLOSE_COMMENT_REPLY',
  'LOGIN_SEE',
];

export const POST_SORT_STATE_KEY: Record<string, string> = {
  GLOBAL: '全局置顶',
  CURRENT: '置顶',
  RECOMMEND: '推荐',
  POPULAR: '热门',
  DEFAULT: '默认',
};

export const POST_SORT_STATE_LIST = [
  'GLOBAL',
  'CURRENT',
  'RECOMMEND',
  'POPULAR',
  'DEFAULT',
];

export const POST_CONTENT_TYPE_OBJ: Record<string, string> = {
  DEFAULT: '默认',
  LINK: '链接',
  NONE: '无内容',
};
