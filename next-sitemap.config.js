/** @type {import('next-sitemap').IConfig} */
const config = {
	siteUrl: process.env.APP_URL || 'http://localhost:3000',
	outDir: 'sitemap',
	generateIndexSitemap: false,
	exclude: [
		'/sitemap-index.xml',
		'/sitemap-section.xml',
		'/sitemap-post.xml',
		'/sitemap-user.xml',
		'/admin',
		'/admin/*',
		'/test',
	],
};

module.exports = config;
