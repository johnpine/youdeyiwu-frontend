export type TMessageTypeEnum =
  | 'SYSTEM'
  | 'OTHER'
  | 'STATUS'
  | 'LOGIN'
  | 'REGISTER'
  | 'LOGOUT'
  | 'USER'
  | 'ROLE'
  | 'PERMISSION'
  | 'SECTION'
  | 'POST'
  | 'TAG'
  | 'COMMENT'
  | 'REPLY'
  | 'OAUTH_CLIENT'
  | 'OAUTH_CLIENT_API';

export type TMessageRange = 'ALL' | 'USER';

export type TMessageState = 'UNREAD' | 'HAVE_READ';
