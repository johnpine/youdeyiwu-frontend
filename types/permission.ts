export type TPermissionMethod =
  | 'GET'
  | 'HEAD'
  | 'POST'
  | 'PUT'
  | 'PATCH'
  | 'DELETE'
  | 'OPTIONS'
  | 'TRACE';

export type TPermissionType = 'ANT' | 'REGEX';

export type TPermissionTypeName = '路径' | '正则';
