import type { TMessageType } from '@/types';

export type TToast = {
  id?: string;
  type?: TMessageType;
  message: any;
  mode?: 'login' | 'default';
  complete?: EventListenerOrEventListenerObject;
};
