export type TSectionState = 'SHOW' | 'HIDE' | 'LOCK' | 'CLOSE';

export type TSectionOtherState = 'DEFAULT' | 'ALLOW' | 'BLOCK' | 'LOGIN_SEE';
