export type TOauthClientStatus = 'ENABLE' | 'DISABLED';

export type TOauthClientReviewState = 'PENDING' | 'FAILED' | 'SUCCESSFUL';

export type TOauthClientApiType = 'DEFAULT' | 'REQUIRED';

export type TOauthClientApiReviewState = 'PENDING' | 'FAILED' | 'SUCCESSFUL';
