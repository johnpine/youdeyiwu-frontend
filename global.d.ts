declare module 'server-only';

declare namespace NodeJS {
  interface ProcessEnv {
    // app
    APP_API_SERVER: string;
    APP_NAME: string;
    APP_NAME_ABBR: string;
    APP_URL: string;
    APP_URL_HOST: string;
    APP_URL_HTTPS: string;
    APP_DESCRIPTION: string;
    APP_OSS_SERVER: string;
    APP_BLUR_DATA_URL: string;
    APP_ICP_NUM: string;
    APP_ICP_LINK: string;
    APP_DEFAULT_LOCALE: string;
    APP_LOCALES: string;

    // token
    TOKEN_SECRET: string;
    REFRESH_TOKEN_SECRET: string;

    // mixpanel
    MIXPANEL: string;
    MIXPANEL_DEBUG: string;
    MIXPANEL_PROJECT_TOKEN: string;

    // plausible
    PLAUSIBLE: string;
    PLAUSIBLE_TRACK_LOCAL_HOST: string;
    PLAUSIBLE_DATA_DOMAIN: string;
    PLAUSIBLE_DATA_API_DOMAIN: string;

    // umami
    UMAMI: string;
    UMAMI_SRC: string;
    UMAMI_WEBSITE_ID: string;
    UMAMI_DATA_CACHE: string;
    UMAMI_DATA_HOST_URL: string;

    // hostname
    HOSTNAME: string;
  }
}
