const withNextIntl = require('next-intl/plugin')('./lib/i18n.ts');

/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    dangerouslyAllowSVG: true,
    contentDispositionType: 'attachment',
    contentSecurityPolicy: "default-src 'self'; script-src 'none'; sandbox;",
    remotePatterns: [
      {
        protocol: 'https',
        hostname: '**',
      },
      {
        protocol: 'http',
        hostname: '**',
      },
    ],
  },
  eslint: {
    dirs: ['app', 'contexts', 'hooks'],
  },
  async rewrites() {
    return [
      {
        source: '/sections/:id(\\D{1,})',
        destination: '/404',
      },
      {
        source: '/posts/new',
        destination: '/posts/new',
      },
      {
        source: '/posts/:id(\\D{1,})',
        destination: '/404',
      },
      {
        source: '/posts/:id(\\D{1,})/edit',
        destination: '/404',
      },
      {
        source: '/users/:id(\\D{1,})',
        destination: '/404',
      },
    ];
  },
  output: 'standalone',
  poweredByHeader: false,
};

if (process.env.APP_BASE_PATH) {
  nextConfig.basePath = process.env.APP_BASE_PATH + '';
}

module.exports = withNextIntl(nextConfig);
