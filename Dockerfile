# syntax=docker/dockerfile:1
FROM node:18 AS deps
LABEL maintainer="dafengzhen" description="构建尤得一物前端应用"
WORKDIR /youdeyiwu-frontend
COPY package.json yarn.lock* package-lock.json* pnpm-lock.yaml* .yarnrc* ./
ENV YARN_CACHE_FOLDER=/root/.yarn
RUN --mount=type=cache,target=/root/.yarn \
  if [ -f yarn.lock ]; then yarn --verbose --frozen-lockfile; \
  elif [ -f package-lock.json ]; then npm ci; \
  elif [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i; \
  else echo "Lockfile not found." && yarn install; \
  fi

FROM node:18 AS builder
WORKDIR /youdeyiwu-frontend
COPY --from=deps /youdeyiwu-frontend/node_modules ./node_modules
COPY . .
ENV NEXT_TELEMETRY_DISABLED 1
RUN if [ -f yarn.lock ]; then yarn build; \
  elif [ -f package-lock.json ]; then npm run build; \
  elif [ -f pnpm-lock.yaml ]; then pnpm build; \
  else yarn build; \
  fi

FROM dafengzhen/distroless-nodejs18-debian11 AS runner
WORKDIR /youdeyiwu-frontend
VOLUME /youdeyiwu-frontend/public/icons
VOLUME /youdeyiwu-frontend/public/images
COPY --from=builder /youdeyiwu-frontend/next.config.js ./next.config.js
COPY --from=builder /youdeyiwu-frontend/public ./public
COPY --from=builder /youdeyiwu-frontend/package.json ./package.json
COPY --from=builder /youdeyiwu-frontend/.next/standalone ./
COPY --from=builder /youdeyiwu-frontend/.next/static ./.next/static
ENV NEXT_TELEMETRY_DISABLED=1 \
    NODE_ENV=production \
    PORT=3000
EXPOSE $PORT
CMD ["server.js"]
