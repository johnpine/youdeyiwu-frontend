import type { TBody } from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';
import type { IToken } from '@/interfaces';

export const loginByUsername = (
  params: TBody<{
    username: string;
    password: string;
    cid: string;
    code: string;
  }>
): Promise<Response | IToken> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/login/username', config).then(
    handleReqMiddleware
  );
};

export const loginByPhone = (
  params: TBody<{
    phone: string;
    code: string;
  }>
): Promise<Response | IToken> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/login/phone', config).then(
    handleReqMiddleware
  );
};

export const loginByEmail = (
  params: TBody<{
    email: string;
    code: string;
  }>
): Promise<Response | IToken> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/login/email', config).then(
    handleReqMiddleware
  );
};
