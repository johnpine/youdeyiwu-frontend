import type { TBody, TParams } from '@/types';
import type {
  IContact,
  IPagination,
  IUser,
  IUserBasicInfo,
  IUserClientDetails,
  IUserStatistic,
} from '@/interfaces';
import { createConfig, getQueryParams, handleReqMiddleware } from '@/lib/api';

export const clientQueryUserDetails = (
  params?: TParams
): Promise<Response | IUserClientDetails> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/users/client/${config.id}/details?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};

export const queryAllUser = (
  params?: TParams
): Promise<Response | IPagination<IUser>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/users?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};

export const queryUserBasicInfo = (
  params?: TParams
): Promise<Response | IUserBasicInfo> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/users/${config.id}/basic`, config).then(
    handleReqMiddleware
  );
};

export const queryUserDetails = (
  params?: TParams
): Promise<Response | IUser> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/users/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const queryUserStatistics = (
  params?: TParams
): Promise<Response | IUserStatistic> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/users/statistics', config).then(
    handleReqMiddleware
  );
};

export const createUserByUsername = (
  params: TBody<{
    alias?: string;
    username: string;
    password: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/users/register/username', config).then(
    handleReqMiddleware
  );
};

export const createUserByPhone = (
  params: TBody<{
    alias?: string;
    phone: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/users/register/phone', config).then(
    handleReqMiddleware
  );
};

export const createUserByEmail = (
  params: TBody<{
    alias?: string;
    email: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/users/register/email', config).then(
    handleReqMiddleware
  );
};

export const updateUserBasicInfo = (
  params: TBody<{
    alias?: string;
    username?: string;
    password?: string;
    email?: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/users/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const updateUserStatusInfo = (
  params: TBody<{
    accountNonExpired?: boolean;
    credentialsNonExpired?: boolean;
    accountNonLocked?: boolean;
    enabled?: boolean;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/users/${config.id}/status-info`, config).then(
    handleReqMiddleware
  );
};

export const updateUserPersonalityInfo = (
  params: TBody<{
    personalizedSignature?: string;
    smallAvatarUrl?: string;
    mediumAvatarUrl?: string;
    largeAvatarUrl?: string;
    contacts?: {
      key: string;
      val: string;
    }[];
    about?: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(
    config.baseURL + `/users/${config.id}/personality-info`,
    config
  ).then(handleReqMiddleware);
};

export const updateUserPassword = (
  params: TBody<{
    password?: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/users/${config.id}/password`, config).then(
    handleReqMiddleware
  );
};

export const clientUpdateUserPassword = (
  params: TBody<{
    password?: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/users/password`, config).then(
    handleReqMiddleware
  );
};

export const clientUpdateUserBasicInfo = (
  params: TBody<{
    personalizedSignature?: string;
    contacts?: IContact[];
    about?: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/users/basicInfo`, config).then(
    handleReqMiddleware
  );
};

export const createContact = (
  params: TBody<{
    key: string;
    val: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + `/users/${config.id}/contacts`, config).then(
    handleReqMiddleware
  );
};

export const removeContact = (
  params: TBody<{
    contactId: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(
    config.baseURL + `/users/${config.id}/contacts/${params.data!.contactId}`,
    config
  ).then(handleReqMiddleware);
};
