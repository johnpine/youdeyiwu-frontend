import type { TBody } from '@/types';
import { createConfig, getQueryParams, handleReqMiddleware } from '@/lib/api';
import type { IToken } from '@/interfaces';

export const queryQqAuthUrl = (
  params: TBody<{
    alias?: string;
  }>
): Promise<Response | string> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/qq/auth/url?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};

export const qqAuthCallback = (
  params: TBody<{
    code: string;
    state: string;
  }>
): Promise<Response | IToken> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/qq/auth/callback?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};
