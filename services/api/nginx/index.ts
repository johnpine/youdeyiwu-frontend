import type { TBody, TParams } from '@/types';
import { createConfig, getQueryParams, handleReqMiddleware } from '@/lib/api';
import type {
  INginxErrorLogEvent,
  INginxErrorLogStatistics,
  INginxKey,
  INginxLogEvent,
  INginxLogReadRecord,
  INginxLogStatistics,
  INginxLogsWebsiteAccessCountByDay,
  INginxLogsWebsiteAccessPercentage,
  INginxLogsWebsiteBounceRate,
  INginxLogsWebsiteErrorLogLevel,
  INginxLogsWebsitePageViews,
  INginxLogsWebsiteQuarterlyAccessCounts,
  INginxLogsWebsiteUniqueVisitors,
  IPagination,
} from '@/interfaces';

export const queryNginxKeys = (
  params?: TParams,
): Promise<Response | INginxKey[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/nginx/keys', config).then(
    handleReqMiddleware,
  );
};

export const queryNginxKey = (
  params?: TParams,
): Promise<Response | INginxKey> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/nginx/keys/${config.id}`, config).then(
    handleReqMiddleware,
  );
};

export const createNginxKey = (
  params: TBody<void>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/nginx/keys', config).then(
    handleReqMiddleware,
  );
};

export const updateNginxKey = (
  params: TBody<{
    newSecret?: boolean;
    remark?: string;
    used?: boolean;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/nginx/keys/${config.id}`, config).then(
    handleReqMiddleware,
  );
};

export const deleteNginxKey = (params: TParams): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(config.baseURL + `/nginx/keys/${config.id}`, config).then(
    handleReqMiddleware,
  );
};

export const queryNginxLogSchedulerConfig = (
  params?: TParams,
): Promise<Response | INginxLogReadRecord[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/nginx/logs/config-scheduler', config).then(
    handleReqMiddleware,
  );
};

export const saveNginxLogSchedulerConfig = (
  params: TBody<{
    logType: 'ACCESS' | 'ERROR';
    logFilePath: string;
    fixedDelaySeconds: number;
    autostart: boolean;
    errorMessage?: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/nginx/logs/config-scheduler', config).then(
    handleReqMiddleware,
  );
};

export const startNginxLogScheduler = (
  params: TBody<{
    type: 'ACCESS' | 'ERROR';
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(
    config.baseURL + `/nginx/logs/start-scheduler?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const stopNginxLogScheduler = (
  params: TBody<{
    type: 'ACCESS' | 'ERROR';
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(
    config.baseURL + `/nginx/logs/stop-scheduler?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryNginxLogIsSchedulerRunning = (
  params: TParams,
): Promise<Response | boolean> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/nginx/logs/is-scheduler-running?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryNginxLogs = (
  params?: TParams,
): Promise<Response | IPagination<INginxLogEvent>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/nginx/logs?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryNginxErrorLogs = (
  params?: TParams,
): Promise<Response | IPagination<INginxErrorLogEvent>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/nginx/logs/error?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryNginxLogStatistics = (
  params?: TParams,
): Promise<Response | INginxLogStatistics> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/nginx/logs/statistics', config).then(
    handleReqMiddleware,
  );
};

export const queryNginxErrorLogStatistics = (
  params?: TParams,
): Promise<Response | INginxErrorLogStatistics> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/nginx/logs/error/statistics', config).then(
    handleReqMiddleware,
  );
};

export const queryNginxLogsWebsiteAccessCountByDay = (
  params?: TParams,
): Promise<Response | INginxLogsWebsiteAccessCountByDay> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/nginx/logs/website/access-count-by-day?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryNginxLogsWebsiteQuarterlyAccessCounts = (
  params?: TParams,
): Promise<Response | INginxLogsWebsiteQuarterlyAccessCounts> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/nginx/logs/website/quarterly-access-counts?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryNginxLogsWebsiteAccessPercentage = (
  params?: TParams,
): Promise<Response | INginxLogsWebsiteAccessPercentage> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/nginx/logs/website/access-percentage?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryNginxLogsWebsitePageViews = (
  params?: TParams,
): Promise<Response | INginxLogsWebsitePageViews> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/nginx/logs/website/page-views?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryNginxLogsWebsiteUniqueVisitors = (
  params?: TParams,
): Promise<Response | INginxLogsWebsiteUniqueVisitors> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/nginx/logs/website/unique-visitors?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryNginxLogsWebsiteBounceRate = (
  params?: TParams,
): Promise<Response | INginxLogsWebsiteBounceRate> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/nginx/logs/website/bounce-rate?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryNginxLogsWebsiteErrorLogLevel = (
  params?: TParams,
): Promise<Response | INginxLogsWebsiteErrorLogLevel> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/nginx/logs/website/error-log-level?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};
