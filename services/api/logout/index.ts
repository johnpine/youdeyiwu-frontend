import type { TParams } from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';

export const logout = (params?: TParams): Promise<Response | void> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/logout', config).then(handleReqMiddleware);
};
