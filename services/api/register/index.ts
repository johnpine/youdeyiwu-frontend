import type { IToken } from '@/interfaces';
import type { TBody } from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';

export const registerByUsername = (
  params: TBody<{
    alias?: string;
    username: string;
    password: string;
    cid: string;
    code: string;
  }>
): Promise<Response | IToken> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/register/username', config).then(
    handleReqMiddleware
  );
};

export const registerByPhone = (
  params: TBody<{
    alias?: string;
    phone: string;
    code: string;
  }>
): Promise<Response | IToken> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/register/phone', config).then(
    handleReqMiddleware
  );
};

export const registerByEmail = (
  params: TBody<{
    alias?: string;
    email: string;
    code: string;
  }>
): Promise<Response | IToken> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/register/email', config).then(
    handleReqMiddleware
  );
};
