import type { IToken } from '@/interfaces';
import type { TBody, TParams } from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';

export const clientClearToken = (
  params?: TParams
): Promise<Response | void> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/token/client/clear', config).then(
    handleReqMiddleware
  );
};

export const clientRefreshToken = (
  params?: TParams
): Promise<Response | void> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/token/client/refresh', config).then(
    handleReqMiddleware
  );
};

export const refreshToken = (
  params: TBody<{
    refreshToken: string;
  }>
): Promise<Response | IToken> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/token/refresh', config).then(
    handleReqMiddleware
  );
};
