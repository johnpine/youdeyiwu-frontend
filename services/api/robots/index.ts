import type { TParams } from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';

export const querySectionSitemap = (
  params?: TParams
): Promise<Response | number[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/robots/sitemap/sections', config).then(
    handleReqMiddleware
  );
};

export const queryPostSitemap = (
  params?: TParams
): Promise<Response | number[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/robots/sitemap/posts', config).then(
    handleReqMiddleware
  );
};

export const queryUserSitemap = (
  params?: TParams
): Promise<Response | number[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/robots/sitemap/users', config).then(
    handleReqMiddleware
  );
};
