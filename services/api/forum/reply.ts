import type {
  IChildReplyStatistics,
  IPagination,
  IParentReply,
  IParentReplyStatistics,
  IPostComment,
  IPostCommentParentReply,
  IPostCommentReply,
  IReply,
  IReplyStatistics,
} from '@/interfaces';
import type { TBody, TParams } from '@/types';
import {
  TChildReplyReviewState,
  TParentReplyReviewState,
  TReplyReviewState,
} from '@/types';
import { createConfig, getQueryParams, handleReqMiddleware } from '@/lib/api';

export const queryAllReplyByCommentId = (
  params?: TParams
): Promise<Response | IPostComment> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/forum/replies/comments/${config.id}?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};

export const queryAllParentReplyByReplyId = (
  params?: TParams
): Promise<Response | IPostCommentReply> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/forum/replies/${config.id}/parent?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};

export const queryAllChildReplyByParentReplyId = (
  params?: TParams
): Promise<Response | IPostCommentParentReply> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/forum/replies/${config.id}/child?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};

export const queryReplyStatistics = (
  params?: TParams
): Promise<Response | IReplyStatistics> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/forum/replies/statistics', config).then(
    handleReqMiddleware
  );
};

export const queryParentReplyStatistics = (
  params?: TParams
): Promise<Response | IParentReplyStatistics> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + '/forum/replies/statistics/parent',
    config
  ).then(handleReqMiddleware);
};

export const queryChildReplyStatistics = (
  params?: TParams
): Promise<Response | IChildReplyStatistics> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/forum/replies/statistics/child', config).then(
    handleReqMiddleware
  );
};

export const serverQueryAllReplyByCommentId = (
  params?: TParams
): Promise<Response | IPagination<IReply>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/forum/replies/server/comments/${config.id}?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};

export const serverQueryAllParentReplyByReplyId = (
  params?: TParams
): Promise<Response | IPagination<IParentReply>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/forum/replies/server/${config.id}/parent?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};

export const serverQueryAllChildReplyByReplyId = (
  params?: TParams
): Promise<Response | IPagination<IParentReply>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/forum/replies/server/${config.id}/child?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};

export const createReply = (
  params: TBody<{
    commentId: number;
    content: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/forum/replies', config).then(
    handleReqMiddleware
  );
};

export const createParentReply = (
  params: TBody<{
    replyId: number;
    content: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/forum/replies/parent', config).then(
    handleReqMiddleware
  );
};

export const createChildReply = (
  params: TBody<{
    parentReplyId: number;
    content: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/forum/replies/child', config).then(
    handleReqMiddleware
  );
};

export const updateReplyReviewStatus = (
  params: TBody<{
    reviewReason?: string;
    reviewStatus: TReplyReviewState;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PATCH' });
  return fetch(
    config.baseURL + `/forum/replies/${config.id}/reviewStatus`,
    config
  ).then(handleReqMiddleware);
};

export const updateParentReplyReviewStatus = (
  params: TBody<{
    reviewReason?: string;
    reviewStatus: TParentReplyReviewState;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PATCH' });
  return fetch(
    config.baseURL + `/forum/replies/${config.id}/parent/reviewStatus`,
    config
  ).then(handleReqMiddleware);
};

export const updateChildReplyReviewStatus = (
  params: TBody<{
    reviewReason?: string;
    reviewStatus: TChildReplyReviewState;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PATCH' });
  return fetch(
    config.baseURL + `/forum/replies/${config.id}/child/reviewStatus`,
    config
  ).then(handleReqMiddleware);
};
