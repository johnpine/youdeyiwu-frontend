import type {
  IPagination,
  ISection,
  ISectionClient,
  ISectionDetails,
  ISectionStatistics,
  IUploadSectionContentFile,
  IUploadSectionCover,
} from '@/interfaces';
import type { TBody, TParams } from '@/types';
import { createConfig, getQueryParams, handleReqMiddleware } from '@/lib/api';

export const querySectionDetailsById = (
  params?: TParams,
): Promise<Response | ISectionDetails> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/forum/sections/${config.id}/details`,
    config,
  ).then(handleReqMiddleware);
};

export const clientQuerySectionDetailsById = (
  params?: TParams,
): Promise<Response | ISectionDetails> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/forum/sections/client/${config.id}/details?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const querySectionContentById = (
  params?: TParams,
): Promise<Response | string> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/forum/sections/${config.id}/content`,
    config,
  ).then(handleReqMiddleware);
};

export const queryAllSection = (
  params?: TParams,
): Promise<Response | IPagination<ISection>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/forum/sections?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const clientQueryAllSection = (
  params?: TParams,
): Promise<Response | ISectionClient[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/forum/sections/client', config).then(
    handleReqMiddleware,
  );
};

export const querySectionStatistics = (
  params?: TParams,
): Promise<Response | ISectionStatistics> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/forum/sections/statistics', config).then(
    handleReqMiddleware,
  );
};

export const createSection = (
  params: TBody<{
    name: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/forum/sections', config).then(
    handleReqMiddleware,
  );
};

export const updateSection = (
  params: TBody<{
    name?: string;
    cover?: string;
    overview?: string;
    sort?: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/forum/sections/${config.id}`, config).then(
    handleReqMiddleware,
  );
};

export const uploadSectionCover = (
  params: TBody<{
    file?: string | Blob;
    formData?: FormData;
  }>,
): Promise<Response | IUploadSectionCover> => {
  const { file } = params.data as any;
  const formData = new FormData();
  formData.append('file', file);

  const config = createConfig(params, {
    method: 'POST',
    body: formData as any,
  });
  return fetch(
    config.baseURL + `/file/sections/${config.id}/cover`,
    config,
  ).then(handleReqMiddleware);
};

export const uploadSectionContent = (
  params: TBody<{
    file?: string | Blob;
    signal?: AbortSignal;
    onUploadProgress?: (progressEvent: ProgressEvent) => void;
    formData?: FormData;
  }>,
): Promise<Response | IUploadSectionContentFile> => {
  const { file } = params.data as any;
  const formData = new FormData();
  formData.append('file', file);

  const config = createConfig(params, {
    method: 'POST',
    body: formData as any,
  });
  return fetch(
    config.baseURL + `/file/sections/${config.id}/content`,
    config,
  ).then(handleReqMiddleware);
};

export const updateSectionStatus = (
  params: TBody<{
    status: string;
    secret?: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PATCH' });
  return fetch(
    config.baseURL + `/forum/sections/${config.id}/status`,
    config,
  ).then(handleReqMiddleware);
};

export const updateSectionContent = (
  params: TBody<{
    content: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PATCH' });
  return fetch(
    config.baseURL + `/forum/sections/${config.id}/content`,
    config,
  ).then(handleReqMiddleware);
};

export const updateSectionTagByName = (
  params: TBody<{
    name: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PATCH' });
  return fetch(
    config.baseURL + `/forum/sections/${config.id}/tags/${params.data!.name}`,
    config,
  ).then(handleReqMiddleware);
};

export const removeSectionTagByName = (
  params: TBody<{
    name: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(
    config.baseURL + `/forum/sections/${config.id}/tags/${params.data!.name}`,
    config,
  ).then(handleReqMiddleware);
};

export const updateSectionAdminByUserId = (
  params: TBody<{
    userId: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PATCH' });
  return fetch(
    config.baseURL +
      `/forum/sections/${config.id}/admins/${params.data!.userId}`,
    config,
  ).then(handleReqMiddleware);
};

export const removeSectionAdminByUserId = (
  params: TBody<{
    userId: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(
    config.baseURL +
      `/forum/sections/${config.id}/admins/${params.data!.userId}`,
    config,
  ).then(handleReqMiddleware);
};

export const removeSection = (
  params: TBody<void>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(config.baseURL + `/forum/sections/${config.id}`, config).then(
    handleReqMiddleware,
  );
};
