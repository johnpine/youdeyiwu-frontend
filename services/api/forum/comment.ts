import type { TBody, TCommentReviewState, TParams } from '@/types';
import { createConfig, getQueryParams, handleReqMiddleware } from '@/lib/api';
import type { IComment, ICommentStatistics, IPagination } from '@/interfaces';

export const createComment = (
  params: TBody<{
    postId: number;
    content: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/forum/comments', config).then(
    handleReqMiddleware
  );
};

export const updateCommentReviewStatus = (
  params: TBody<{
    reviewReason?: string;
    reviewState: TCommentReviewState;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PATCH' });
  return fetch(
    config.baseURL + `/forum/comments/${config.id}/reviewStatus`,
    config
  ).then(handleReqMiddleware);
};

export const queryCommentStatistics = (
  params?: TParams
): Promise<Response | ICommentStatistics> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/forum/comments/statistics', config).then(
    handleReqMiddleware
  );
};

export const queryCommentByPostId = (
  params?: TParams
): Promise<Response | IPagination<IComment>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/forum/comments/posts/${config.id}?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};
