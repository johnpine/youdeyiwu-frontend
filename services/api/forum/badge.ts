import type {
  TBadgeBackgroundColorMode,
  TBadgeColorMode,
  TBody,
  TParams,
} from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';
import type { IPostBadge } from '@/interfaces';

export const createPostBadge = (
  params: TBody<{
    postId: number;
    name: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/forum/badges', config).then(
    handleReqMiddleware
  );
};

export const updatePostBadge = (
  params: TBody<{
    name?: string;
    sort?: number;
    reason?: string;
    colorMode?: TBadgeColorMode;
    color?: string;
    backgroundColorMode?: TBadgeBackgroundColorMode;
    backgroundColor?: string;
    roundedPill?: boolean;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/forum/badges/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const removePostBadge = (
  params: TBody<void>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(config.baseURL + `/forum/badges/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const queryPostBadge = (
  params?: TParams
): Promise<Response | IPostBadge> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/forum/badges/${config.id}`, config).then(
    handleReqMiddleware
  );
};
