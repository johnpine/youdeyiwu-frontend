import type { IDraft } from '@/interfaces';
import type { TBody, TParams } from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';

export const createNewPostDraft = (
  params: TBody<{
    name?: string;
    overview?: string;
    content?: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/forum/drafts/posts/new', config).then(
    handleReqMiddleware
  );
};

export const createEditPostDraft = (
  params: TBody<{
    name?: string;
    overview?: string;
    content?: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(
    config.baseURL + `/forum/drafts/posts/${config.id}/edit`,
    config
  ).then(handleReqMiddleware);
};

export const removeDraft = (params: TBody<void>): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(config.baseURL + `/forum/drafts/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const queryPostDrafts = (
  params?: TParams
): Promise<Response | IDraft[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/forum/drafts/posts/new', config).then(
    handleReqMiddleware
  );
};

export const queryEditPostDrafts = (
  params?: TParams
): Promise<Response | IDraft[]> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/forum/drafts/posts/${config.id}/edit`,
    config
  ).then(handleReqMiddleware);
};

export const queryDraft = (params?: TParams): Promise<Response | IDraft> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/forum/drafts/${config.id}`, config).then(
    handleReqMiddleware
  );
};
