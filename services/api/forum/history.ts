import type { TBody, TParams } from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';
import type { IHistory, IQueryAllHistoryByPostId } from '@/interfaces';

export const createPostHistory = (
  params: TBody<{
    name?: string;
    overview?: string;
    content?: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(
    config.baseURL + `/forum/histories/posts/${config.id}`,
    config
  ).then(handleReqMiddleware);
};

export const queryAllHistoryByPostId = (
  params?: TParams
): Promise<Response | IQueryAllHistoryByPostId> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/forum/histories/posts/${config.id}`,
    config
  ).then(handleReqMiddleware);
};

export const queryHistory = (
  params?: TParams
): Promise<Response | IHistory> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/forum/histories/${config.id}`, config).then(
    handleReqMiddleware
  );
};
