import type { TBody, TParams } from '@/types';
import { createConfig, getQueryParams, handleReqMiddleware } from '@/lib/api';
import type {
  IPagination,
  IPost,
  ISection,
  ITagGroup,
  ITagGroupStatistics,
} from '@/interfaces';

export const queryTagGroupStatistics = (
  params?: TParams
): Promise<Response | ITagGroupStatistics> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/forum/tagGroups/statistics', config).then(
    handleReqMiddleware
  );
};

export const queryAllTagGroup = (
  params?: TParams
): Promise<Response | IPagination<ITagGroup>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/forum/tagGroups?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};

export const queryTagGroup = (
  params?: TParams
): Promise<Response | ITagGroup> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/forum/tagGroups/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const querySectionsByTagGroup = (
  params?: TParams
): Promise<Response | ISection[]> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/forum/tagGroups/${config.id}/sections`,
    config
  ).then(handleReqMiddleware);
};

export const queryPostsByTagGroup = (
  params?: TParams
): Promise<Response | IPagination<IPost>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/forum/tagGroups/${config.id}/posts?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};

export const createTagGroup = (
  params: TBody<{
    name: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/forum/tagGroups', config).then(
    handleReqMiddleware
  );
};

export const updateTagGroup = (
  params: TBody<{
    name: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/forum/tagGroups/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const createSectionTagGroup = (
  params: TBody<{
    sectionId: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(
    config.baseURL +
      `/forum/tagGroups/${config.id}/sections/${params.data!.sectionId}`,
    config
  ).then(handleReqMiddleware);
};

export const createTagTagGroup = (
  params: TBody<{
    tagId: string;
    sort: number;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(
    config.baseURL + `/forum/tagGroups/${config.id}/tags/${params.data!.tagId}`,
    config
  ).then(handleReqMiddleware);
};

export const createPostTagGroup = (
  params: TBody<{
    tagId: string;
    postId: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(
    config.baseURL +
      `/forum/tagGroups/${config.id}/tags/${params.data!.tagId}/posts/${
        params.data!.postId
      }`,
    config
  ).then(handleReqMiddleware);
};

export const removeSectionTagGroup = (
  params: TBody<{
    sectionId: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(
    config.baseURL +
      `/forum/tagGroups/${config.id}/sections/${params.data!.sectionId}`,
    config
  ).then(handleReqMiddleware);
};

export const removeTagTagGroup = (
  params: TBody<{
    tagId: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(
    config.baseURL + `/forum/tagGroups/${config.id}/tags/${params.data!.tagId}`,
    config
  ).then(handleReqMiddleware);
};

export const removePostTagGroup = (
  params: TBody<{
    tagId: string;
    postId: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(
    config.baseURL +
      `/forum/tagGroups/${config.id}/tags/${params.data!.tagId}/posts/${
        params.data!.postId
      }`,
    config
  ).then(handleReqMiddleware);
};
