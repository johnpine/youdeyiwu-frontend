import type { TBody, TParams } from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';
import type { IPostStyle } from '@/interfaces';

export const createPostStyle = (
  params: TBody<{
    postId: number;
    name: string;
    type: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/forum/styles', config).then(
    handleReqMiddleware
  );
};

export const updatePostStyle = (
  params: TBody<{
    name?: string;
    type?: string;
    colorMode?: string;
    color?: string;
    backgroundColorMode?: string;
    backgroundColor?: string;
    icons?: string[];
    styles?: Record<string, string>;
    classes?: string[];
    useStyle?: boolean;
    useClass?: boolean;
    postId?: number;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/forum/styles/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const removePostStyle = (
  params: TBody<void>
): Promise<Response | boolean> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(config.baseURL + `/forum/styles/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const queryPostStyle = (
  params?: TParams
): Promise<Response | IPostStyle> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/forum/styles/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const queryAllPostStylePreset = (
  params?: TParams
): Promise<Response | IPostStyle[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/forum/styles/preset`, config).then(
    handleReqMiddleware
  );
};
