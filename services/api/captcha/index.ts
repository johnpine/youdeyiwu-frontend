import type { TBody, TParams } from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';
import type {
  IGenerateCaptchaByUsername,
  IGenerateImageId,
} from '@/interfaces';

export const generateImageById = (
  params?: TParams
): Promise<Response | ArrayBuffer> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/captcha/image/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const generateImageId = (
  params?: TParams
): Promise<Response | IGenerateImageId> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/captcha/image', config).then(
    handleReqMiddleware
  );
};

export const generateCaptchaByUsername = (
  params: TBody<{
    username: string;
  }>
): Promise<Response | IGenerateCaptchaByUsername> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/captcha/username', config).then(
    handleReqMiddleware
  );
};

export const generateCaptchaByPhone = (
  params: TBody<{
    phone: string;
    contentType: 'LOGIN' | 'REGISTER' | 'VERIFY';
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/captcha/phone', config).then(
    handleReqMiddleware
  );
};

export const generateCaptchaByEmail = (
  params: TBody<{
    email: string;
    contentType: 'LOGIN' | 'REGISTER' | 'VERIFY';
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/captcha/email', config).then(
    handleReqMiddleware
  );
};
