import type { TParams } from '@/types';
import { createConfig, getQueryParams, handleReqMiddleware } from '@/lib/api';
import type {
  IChildReply,
  IComment,
  IMessage,
  INginxLogEvent,
  IOauthClient,
  IPagination,
  IParentReply,
  IPermission,
  IPost,
  IPostTemplate,
  IReply,
  IRole,
  ISection,
  ITag,
  ITagGroup,
  IUser,
} from '@/interfaces';
import type { INginxErrorLogEvent } from '@/interfaces';

export const search = (
  params?: TParams,
): Promise<Response | IPagination<IPost>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchUser = (
  params?: TParams,
): Promise<Response | IPagination<IUser>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/user?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchSection = (
  params?: TParams,
): Promise<Response | IPagination<ISection>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/section?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchPost = (
  params?: TParams,
): Promise<Response | IPagination<IPost>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/post?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchTag = (
  params?: TParams,
): Promise<Response | IPagination<ITag>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/tag?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchRole = (
  params?: TParams,
): Promise<Response | IPagination<IRole>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/role?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchPermission = (
  params?: TParams,
): Promise<Response | IPagination<IPermission>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/permission?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchMessage = (
  params?: TParams,
): Promise<Response | IPagination<IMessage>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/message?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchComment = (
  params?: TParams,
): Promise<Response | IPagination<IComment>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/comment?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchReply = (
  params?: TParams,
): Promise<Response | IPagination<IReply>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  // IData<IPagination<IReply>>
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/reply?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchParentReply = (
  params?: TParams,
): Promise<Response | IPagination<IParentReply>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/reply/parent?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchChildReply = (
  params?: TParams,
): Promise<Response | IPagination<IChildReply>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/reply/child?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchTagGroup = (
  params?: TParams,
): Promise<Response | IPagination<ITagGroup>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/tagGroup?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchOauthClient = (
  params?: TParams,
): Promise<Response | IPagination<IOauthClient>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/oauth/client?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchPostTemplate = (
  params?: TParams,
): Promise<Response | IPagination<IPostTemplate>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/search/post/template?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchNginxLogs = (
  params?: TParams,
): Promise<Response | IPagination<INginxLogEvent>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL + `/search/nginx/logs?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};

export const searchNginxErrorLogs = (
  params?: TParams,
): Promise<Response | IPagination<INginxErrorLogEvent>> => {
  let name = params?.query!.name;
  if (!name.includes('*')) {
    name = `*${name}*`;
  }

  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/search/nginx/logs/error?${getQueryParams(config, { name })}`,
    config,
  ).then(handleReqMiddleware);
};
