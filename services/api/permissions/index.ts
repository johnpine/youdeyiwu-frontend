import type {
  TBody,
  TParams,
  TPermissionMethod,
  TPermissionType,
} from '@/types';
import type {
  IPagination,
  IPermission,
  IPermissionDetails,
  IPermissionStatistics,
  IRole,
  IUser,
} from '@/interfaces';
import { createConfig, getQueryParams, handleReqMiddleware } from '@/lib/api';

export const queryAllPermission = (
  params?: TParams,
): Promise<Response | IPagination<IPermission>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/permissions?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryPermissionDetails = (
  params?: TParams,
): Promise<Response | IPermissionDetails> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/permissions/${config.id}`, config).then(
    handleReqMiddleware,
  );
};

export const queryPermissionUsersById = (
  params?: TParams,
): Promise<Response | IPagination<IUser>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/permissions/${config.id}/users?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryPermissionRolesById = (
  params?: TParams,
): Promise<Response | IPagination<IRole>> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL +
      `/permissions/${config.id}/roles?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};

export const queryPermissionStatistics = (
  params?: TParams,
): Promise<Response | IPermissionStatistics> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/permissions/statistics', config).then(
    handleReqMiddleware,
  );
};

export const createPermission = (
  params: TBody<{
    name: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/permissions', config).then(
    handleReqMiddleware,
  );
};

export const updatePermission = (
  params: TBody<{
    name?: string;
    alias?: string;
    overview?: string;
    method?: TPermissionMethod;
    type?: TPermissionType;
    caseInsensitive?: boolean;
    sort?: number;
    categoryId?: string;
    categoryName?: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/permissions/${config.id}`, config).then(
    handleReqMiddleware,
  );
};

export const assignPermission = (
  params: TBody<{
    roleId: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(
    config.baseURL + `/permissions/${config.id}/assign`,
    config,
  ).then(handleReqMiddleware);
};

export const unAssignPermission = (
  params: TBody<{
    roleId: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(
    config.baseURL +
      `/permissions/${config.id}/assign?${getQueryParams(config)}`,
    config,
  ).then(handleReqMiddleware);
};
