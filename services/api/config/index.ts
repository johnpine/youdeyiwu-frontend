import type {
  IConfigEmailContentType,
  IConfigImageContentType,
  IConfigPhoneContentType,
  TBody,
  TParams,
} from '@/types';
import type {
  IConfigEmail,
  IConfigEmailItemBody,
  IConfigImage,
  IConfigImageItemBody,
  IConfigJwt,
  IConfigOauthClient,
  IConfigPhone,
  IConfigPhoneItemBody,
  IConfigPost,
  IConfigQq,
  IConfigRegister,
  IConfigSection,
  IConfigSite,
  IConfigUser,
  IYwOauthClient,
} from '@/interfaces';
import { createConfig, handleReqMiddleware } from '@/lib/api';

export const queryUserConfig = (
  params?: TParams,
): Promise<Response | IConfigUser> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/configs/user', config).then(
    handleReqMiddleware,
  );
};

export const querySectionConfig = (
  params?: TParams,
): Promise<Response | IConfigSection> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/configs/section', config).then(
    handleReqMiddleware,
  );
};

export const queryPostConfig = (
  params?: TParams,
): Promise<Response | IConfigPost> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/configs/post', config).then(
    handleReqMiddleware,
  );
};

export const querySiteConfig = (
  params?: TParams,
): Promise<Response | IConfigSite> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/configs/site', config).then(
    handleReqMiddleware,
  );
};

export const queryJwtConfig = (
  params?: TParams,
): Promise<Response | IConfigJwt> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/configs/jwt', config).then(
    handleReqMiddleware,
  );
};

export const queryQqConfig = (
  params?: TParams,
): Promise<Response | IConfigQq> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/configs/qq', config).then(
    handleReqMiddleware,
  );
};

export const queryEmailConfig = (
  params?: TParams,
): Promise<Response | IConfigEmail> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/configs/email', config).then(
    handleReqMiddleware,
  );
};

export const queryPhoneConfig = (
  params?: TParams,
): Promise<Response | IConfigPhone> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/configs/phone', config).then(
    handleReqMiddleware,
  );
};

export const queryImageConfig = (
  params?: TParams,
): Promise<Response | IConfigImage> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/configs/image', config).then(
    handleReqMiddleware,
  );
};

export const queryOauthClientConfig = (
  params?: TParams,
): Promise<Response | IConfigOauthClient> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/configs/oauth/client', config).then(
    handleReqMiddleware,
  );
};

export const queryRegisterConfig = (
  params?: TParams,
): Promise<Response | IConfigRegister> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/configs/register', config).then(
    handleReqMiddleware,
  );
};

export const updateUserConfig = (
  params: TBody<{
    rootId?: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + '/configs/user', config).then(
    handleReqMiddleware,
  );
};

export const updateSectionConfig = (
  params: TBody<{
    enableCreateNotice?: boolean;
    enableUpdateNotice?: boolean;
    enableUpdateStatusNotice?: boolean;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + '/configs/section', config).then(
    handleReqMiddleware,
  );
};

export const updatePostConfig = (
  params: TBody<{
    enableReview?: boolean;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + '/configs/post', config).then(
    handleReqMiddleware,
  );
};

export const updateSiteConfig = (
  params: TBody<{
    helpLink?: string;
    feedbackLink?: string;
    reportLink?: string;
    githubLink?: string;
    mpImageLink?: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + '/configs/site', config).then(
    handleReqMiddleware,
  );
};

export const updateJwtConfig = (
  params: TBody<{
    tokenExp?: string;
    refreshTokenExp?: string;
    generateNewKey?: boolean;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + '/configs/jwt', config).then(
    handleReqMiddleware,
  );
};

export const updateQqConfig = (
  params: TBody<{
    enable?: boolean;
    clientId?: string;
    clientSecret?: string;
    redirectUri?: string;
    state?: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + '/configs/qq', config).then(
    handleReqMiddleware,
  );
};

export const updateEmailConfig = (
  params: TBody<{
    list: { [key in IConfigEmailContentType]?: IConfigEmailItemBody };
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + '/configs/email', config).then(
    handleReqMiddleware,
  );
};

export const updatePhoneConfig = (
  params: TBody<{
    list: { [key in IConfigPhoneContentType]?: IConfigPhoneItemBody };
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + '/configs/phone', config).then(
    handleReqMiddleware,
  );
};

export const updateImageConfig = (
  params: TBody<{
    list: { [key in IConfigImageContentType]?: IConfigImageItemBody };
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + '/configs/image', config).then(
    handleReqMiddleware,
  );
};

export const updateOauthClientConfig = (
  params: TBody<{
    showMenuEntry?: boolean;
    doc?: string;
    ywClients?: IYwOauthClient[];
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + '/configs/oauth/client', config).then(
    handleReqMiddleware,
  );
};

export const updateRegisterConfig = (
  params: TBody<{
    enableRegistrationConsent?: boolean;
    terms?: string;
    privacyPolicy?: string;
  }>,
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + '/configs/register', config).then(
    handleReqMiddleware,
  );
};
