import type { TParams } from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';

export const getWebviewToken = (
  params?: TParams,
): Promise<Response | string> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/weixin/mp/webview/tokens/${config.id}`,
    config,
  ).then(handleReqMiddleware);
};
