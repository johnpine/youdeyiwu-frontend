import type { TParams } from '@/types';
import { createConfig, handleReqMiddleware } from '@/lib/api';

export const queryAccountPublicKey = (
  params?: TParams
): Promise<Response | string> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/key/public/account', config).then(
    handleReqMiddleware
  );
};

export const queryPasswordPublicKey = (
  params?: TParams
): Promise<Response | string> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/key/public/password', config).then(
    handleReqMiddleware
  );
};
