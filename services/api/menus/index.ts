import type { IMenu, IMenuStatistics, IRole, ISubmenu } from '@/interfaces';
import type { TBody, TMenuMenuType, TParams } from '@/types';
import { createConfig, getQueryParams, handleReqMiddleware } from '@/lib/api';

export const clientQueryAllMenu = (
  params?: TParams
): Promise<Response | IMenu[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/menus/client', config).then(
    handleReqMiddleware
  );
};

export const queryMenus = (params?: TParams): Promise<Response | IMenu[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/menus', config).then(handleReqMiddleware);
};

export const queryAllMenuAndSubmenu = (
  params?: TParams
): Promise<Response | IMenu[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/menus/submenus', config).then(
    handleReqMiddleware
  );
};

export const queryAllSubmenuById = (
  params?: TParams
): Promise<Response | ISubmenu[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/menus/${config.id}/submenus`, config).then(
    handleReqMiddleware
  );
};

export const queryMenuDetails = (
  params?: TParams
): Promise<Response | IMenu> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/menus/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const querySubmenuDetails = (
  params?: TParams
): Promise<Response | ISubmenu> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/submenus/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const queryMenuStatistics = (
  params?: TParams
): Promise<Response | IMenuStatistics> => {
  const config = createConfig(params);
  return fetch(config.baseURL + '/menus/statistics', config).then(
    handleReqMiddleware
  );
};

export const queryMenuRolesById = (
  params?: TParams
): Promise<Response | IRole[]> => {
  const config = createConfig(params);
  return fetch(config.baseURL + `/menus/${config.id}/roles`, config).then(
    handleReqMiddleware
  );
};

export const createMenu = (
  params: TBody<{
    name: string;
    path: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/menus', config).then(handleReqMiddleware);
};

export const createSubmenu = (
  params: TBody<{
    menuId: number;
    name: string;
    path: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'POST' });
  return fetch(config.baseURL + '/submenus', config).then(handleReqMiddleware);
};

export const updateMenu = (
  params: TBody<{
    name?: string;
    path?: string;
    sort?: number;
    menuType?: TMenuMenuType;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/menus/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const updateSubmenu = (
  params: TBody<{
    name?: string;
    path?: string;
    sort?: number;
    menuType?: TMenuMenuType;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/submenus/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const removeMenu = (params: TBody<void>): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(config.baseURL + `/menus/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const removeSubmenu = (
  params: TBody<void>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(config.baseURL + `/submenus/${config.id}`, config).then(
    handleReqMiddleware
  );
};

export const assignMenu = (
  params: TBody<{
    roleId: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'PUT' });
  return fetch(config.baseURL + `/menus/${config.id}/assign`, config).then(
    handleReqMiddleware
  );
};

export const unAssignMenu = (
  params: TBody<{
    roleId: string;
  }>
): Promise<Response | void> => {
  const config = createConfig(params, { method: 'DELETE' });
  return fetch(
    config.baseURL + `/menus/${config.id}/assign?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};
