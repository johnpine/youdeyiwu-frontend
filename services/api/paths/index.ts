import type { TParams } from '@/types';
import { createConfig, getQueryParams, handleReqMiddleware } from '@/lib/api';
import type { IPath } from '@/interfaces';

export const queryPath = (params?: TParams): Promise<Response | IPath> => {
  const config = createConfig(params);
  return fetch(
    config.baseURL + `/paths?${getQueryParams(config)}`,
    config
  ).then(handleReqMiddleware);
};
