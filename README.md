# <img alt="logo" src="https://youdeyiwu.com/images/logo.svg" width="30rem" /> 尤得一物

[![CI Build Status](https://github.com/dafengzhen/youdeyiwu-frontend/actions/workflows/docker-publish.yml/badge.svg)](https://github.com/dafengzhen/youdeyiwu-frontend/actions/workflows/docker-publish.yml)
[![LICENSE](https://img.shields.io/github/license/dafengzhen/youdeyiwu-frontend)](https://github.com/dafengzhen/youdeyiwu-frontend/blob/main/LICENSE)

尤得一物是一个开源论坛程序，提供丰富的功能，可以作为管理或分享文章的论坛博客，也可以在此基础上进行自定义开发.

[English](./README.en.md)

# 功能特点

程序结构以版块、标签和帖子为基础进行扩展，具备以下功能特点：

- 服务端预渲染支持，有利于搜索引擎优化（SEO）
- 版块分组，方便管理浏览
- 自定义标签，用于帖子分类组织
- 帖子富文本编辑，使内容更丰富多样
- 帖子审核设置，可以对帖子进行审核
- 帖子内容草稿和历史记录，方便编辑恢复
- 帖子模板，可以预设模板来加速发帖
- 多层级评论回复，使交流更清晰
- 消息通知，方便获取关键动态
- OAUTH 开放接入，便于第三方登录授权
- 配置相同程序 OAUTH 接入，方便接入同个实例
- 后台配置管理，可对论坛进行全局配置
- 后台用户管理，方便管理用户信息
- 后台角色管理，用于控制用户权限
- 后台动态权限、菜单管理
- 后台版块、标签、帖子管理
- 后台版块，标签组管理
- 后台评论回复管理
- 后台消息管理
- 后台 OAUTH 客户端、接口管理
- 页面深色模式支持
- 页面国际化支持

# 快速开始

更多安装信息请查阅[文档](https://www.youdeyiwu.com/docs) ↗.

# 相关反馈

如果你在使用中有相关建议或问题反馈、PR 等，欢迎与我交流改进

[issues](https://github.com/dafengzhen/youdeyiwu-frontend/issues) ↗

# License

[MIT](https://opensource.org/licenses/MIT)
