// @ts-ignore
import PhotoSwipeLightbox from 'photoswipe/lightbox';
import 'photoswipe/style.css';
import type { DataSourceArray } from 'photoswipe';
import { useEffect, useRef } from 'react';

export default function useGallery({
  dataSource = [],
}: {
  dataSource?: DataSourceArray;
}) {
  const lightboxRef = useRef<typeof PhotoSwipeLightbox | null>(null);

  useEffect(() => {
    const lightbox: typeof PhotoSwipeLightbox = (lightboxRef.current =
      new PhotoSwipeLightbox({
        pswpModule: () => import('photoswipe'),
        wheelToZoom: true,
        closeTitle: '关闭',
        zoomTitle: '缩放',
        arrowPrevTitle: '上一张',
        arrowNextTitle: '下一张',
        errorMsg: '图片无法加载',
        dataSource,
      }));

    if (lightbox) {
      lightbox.init();
    }

    return () => {
      if (lightbox) {
        lightbox.destroy();
      }
    };
  }, [dataSource]);

  return lightboxRef;
}
