import { useContext } from 'react';
import { OffcanvasContext } from '@/contexts/offcanvas';

export default function useOffcanvas() {
  const { show, hide, isLoadingOffcanvasShowing } =
    useContext(OffcanvasContext);

  return {
    show,
    hide,
    showOffcanvas: show,
    hideOffcanvas: hide,
    isLoadingOffcanvasShowing,
  };
}
