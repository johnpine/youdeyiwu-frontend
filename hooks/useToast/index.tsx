import { useContext } from 'react';
import Link from 'next/link';
import { ToastContext } from '@/contexts/toast';

export default function useToast() {
  const { show, hide, isLoadingToastShowing } = useContext(ToastContext);

  return {
    show,
    hide,
    showToast: show,
    hideToast: hide,
    isLoadingToastShowing,
  };
}

export const LoginReminderContent = () => {
  return (
    <div>
      <Link
        href="/login"
        role="button"
        className="btn btn-outline-primary btn-sm"
      >
        我有账号
      </Link>
      <Link
        href="/register"
        role="button"
        className="btn btn-outline-primary btn-sm ms-2"
      >
        注册新账号
      </Link>
    </div>
  );
};
